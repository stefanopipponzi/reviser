VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MadmdF_EditDli 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " "
   ClientHeight    =   7740
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11805
   ClipControls    =   0   'False
   FillColor       =   &H80000004&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7740
   ScaleWidth      =   11805
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame8 
      Caption         =   "Notes : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   825
      Left            =   30
      TabIndex        =   159
      Top             =   6870
      Width           =   11745
      Begin VB.CommandButton ClearSign 
         Caption         =   "Clear"
         Height          =   465
         Left            =   11160
         TabIndex        =   160
         ToolTipText     =   "Clear Log"
         Top             =   180
         Width           =   525
      End
      Begin RichTextLib.RichTextBox RichTextBox1 
         Height          =   525
         Left            =   60
         TabIndex        =   161
         Top             =   180
         Width           =   11025
         _ExtentX        =   19447
         _ExtentY        =   926
         _Version        =   393217
         BackColor       =   16777152
         BorderStyle     =   0
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"MadmdF_EditDli.frx":0000
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar2 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   158
      Top             =   0
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVE"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "REST"
            Object.ToolTipText     =   "Restore Object"
            ImageIndex      =   6
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   4
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "RESTDBD"
                  Text            =   "Restore Initial DBD"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "RESTSEG"
                  Text            =   "Restore Initial Selected Segment"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "RESTAREA"
                  Text            =   "Restore Reference Area for Segment "
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CHECK"
            Object.ToolTipText     =   "Check object"
            ImageKey        =   "Avvio"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   7
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "CONTRFILLER"
                  Text            =   "Filler Area"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "SELFIELD"
                  Text            =   "Selected Field"
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "SELAREA"
                  Text            =   "Area for selected Segment"
               EndProperty
               BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "SELSEGMENT"
                  Text            =   "Selected Segment"
               EndProperty
               BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "GLOBAL"
                  Text            =   "Global Check for all Segments"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   "CREATE"
            Object.ToolTipText     =   "Create"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6075
      Left            =   2700
      TabIndex        =   0
      Top             =   780
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   10716
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Segments"
      TabPicture(0)   =   "MadmdF_EditDli.frx":0077
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "FraTab1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame7"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame6"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "comDialog"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Field details"
      TabPicture(1)   =   "MadmdF_EditDli.frx":0093
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FraTab2"
      Tab(1).Control(1)=   "Frame9"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Structure Details"
      TabPicture(2)   =   "MadmdF_EditDli.frx":00AF
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FraTab3"
      Tab(2).Control(1)=   "Frame5"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Relationships"
      TabPicture(3)   =   "MadmdF_EditDli.frx":00CB
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FraTab4"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Source DBD"
      TabPicture(4)   =   "MadmdF_EditDli.frx":00E7
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FraTab5"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "DL1 Access 2nd Index"
      TabPicture(5)   =   "MadmdF_EditDli.frx":0103
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraIdxDliPgm"
      Tab(5).Control(1)=   "chkDbdSelected"
      Tab(5).Control(2)=   "cmdLoadIdx2PgmDli"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "()"
      TabPicture(6)   =   "MadmdF_EditDli.frx":011F
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Frame16"
      Tab(6).ControlCount=   1
      Begin MSComDlg.CommonDialog comDialog 
         Left            =   8400
         Top             =   5880
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Frame FraTab5 
         Height          =   5535
         Left            =   -74880
         TabIndex        =   523
         Top             =   60
         Width           =   8865
         Begin RichTextLib.RichTextBox RTBDBD 
            Height          =   4875
            Left            =   120
            TabIndex        =   524
            Top             =   540
            Width           =   8625
            _ExtentX        =   15214
            _ExtentY        =   8599
            _Version        =   393217
            BackColor       =   -2147483624
            ReadOnly        =   -1  'True
            ScrollBars      =   2
            RightMargin     =   10000
            TextRTF         =   $"MadmdF_EditDli.frx":013B
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame16 
         Caption         =   "Structure Snapshot"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   5595
         Left            =   -74940
         TabIndex        =   509
         Top             =   30
         Width           =   8895
         Begin MSComctlLib.TreeView TreeViewMerge 
            Height          =   5205
            Left            =   150
            TabIndex        =   510
            Top             =   270
            Width           =   8625
            _ExtentX        =   15214
            _ExtentY        =   9181
            _Version        =   393217
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            OLEDropMode     =   1
         End
      End
      Begin VB.CommandButton cmdLoadIdx2PgmDli 
         Caption         =   "load"
         Height          =   375
         Left            =   -67440
         TabIndex        =   508
         Top             =   5070
         Width           =   1035
      End
      Begin VB.CheckBox chkDbdSelected 
         Alignment       =   1  'Right Justify
         Caption         =   "Load Programs with DL1 access by secondary index only for a current dbd"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   -74460
         TabIndex        =   507
         Top             =   5160
         Width           =   6855
      End
      Begin VB.Frame Frame5 
         Caption         =   "Field's Details"
         ForeColor       =   &H8000000D&
         Height          =   1335
         Left            =   -74910
         TabIndex        =   491
         Top             =   4260
         Width           =   8865
         Begin VB.CheckBox chkOccursFlat 
            Alignment       =   1  'Right Justify
            Caption         =   "Occurs Flattened"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   5100
            TabIndex        =   534
            Top             =   240
            Width           =   1515
         End
         Begin VB.CheckBox chkDaMigrare 
            Alignment       =   1  'Right Justify
            Caption         =   "To Migrate"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   5520
            TabIndex        =   533
            Top             =   540
            Width           =   1095
         End
         Begin VB.CommandButton CmdFldOk 
            BackColor       =   &H80000016&
            Height          =   405
            Left            =   8220
            Picture         =   "MadmdF_EditDli.frx":01BB
            Style           =   1  'Graphical
            TabIndex        =   531
            ToolTipText     =   "Update"
            Top             =   180
            Width           =   495
         End
         Begin VB.TextBox TxtOccurs 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   4620
            TabIndex        =   530
            Top             =   330
            Width           =   375
         End
         Begin VB.TextBox TxtLiv 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   630
            TabIndex        =   499
            Top             =   330
            Width           =   345
         End
         Begin VB.TextBox TxtNome 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   630
            TabIndex        =   498
            Top             =   840
            Width           =   2205
         End
         Begin VB.TextBox TxtNomeRed 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   4620
            TabIndex        =   497
            Top             =   870
            Width           =   1905
         End
         Begin VB.ComboBox CmbType 
            BackColor       =   &H00FFFFC0&
            Height          =   315
            ItemData        =   "MadmdF_EditDli.frx":0305
            Left            =   1620
            List            =   "MadmdF_EditDli.frx":0321
            TabIndex        =   496
            Top             =   330
            Width           =   1215
         End
         Begin VB.TextBox TxtLen 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   3330
            TabIndex        =   495
            Top             =   330
            Width           =   405
         End
         Begin VB.TextBox TxTDec 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   3330
            TabIndex        =   494
            Top             =   870
            Width           =   405
         End
         Begin VB.Frame FrmSelValue 
            Caption         =   "Selector Value"
            ForeColor       =   &H8000000D&
            Height          =   675
            Left            =   6780
            TabIndex        =   492
            Top             =   540
            Width           =   1335
            Begin VB.TextBox TxtSelValue 
               BackColor       =   &H00FFFFC0&
               Height          =   345
               Left            =   90
               TabIndex        =   493
               Top             =   240
               Width           =   1155
            End
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Level"
            ForeColor       =   &H8000000D&
            Height          =   165
            Index           =   4
            Left            =   90
            TabIndex        =   506
            Top             =   390
            Width           =   465
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            Caption         =   "Name"
            ForeColor       =   &H8000000D&
            Height          =   165
            Left            =   90
            TabIndex        =   505
            Top             =   960
            Width           =   465
         End
         Begin VB.Label Label7 
            Caption         =   "Type"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   1170
            TabIndex        =   504
            Top             =   390
            Width           =   405
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "Occurs"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   3840
            TabIndex        =   503
            Top             =   390
            Width           =   675
         End
         Begin VB.Label Label19 
            Alignment       =   1  'Right Justify
            Caption         =   "Name Red"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   3780
            TabIndex        =   502
            Top             =   900
            Width           =   795
         End
         Begin VB.Label Label21 
            Alignment       =   1  'Right Justify
            Caption         =   "Len"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2880
            TabIndex        =   501
            Top             =   360
            Width           =   345
         End
         Begin VB.Label Label23 
            Alignment       =   1  'Right Justify
            Caption         =   "Dec"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2880
            TabIndex        =   500
            Top             =   900
            Width           =   345
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Used Cobol Areas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   1605
         Left            =   -74940
         TabIndex        =   487
         Top             =   3990
         Width           =   8895
         Begin MSComctlLib.ListView ListStr2 
            Height          =   1125
            Left            =   90
            TabIndex        =   488
            Top             =   390
            Width           =   4455
            _ExtentX        =   7858
            _ExtentY        =   1984
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Id"
               Text            =   "Id"
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "Struttura"
               Text            =   "Struttura"
               Object.Width           =   2787
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Campo"
               Object.Width           =   1940
            EndProperty
         End
         Begin MSComctlLib.ListView ListIncidenza 
            Height          =   1125
            Left            =   4620
            TabIndex        =   489
            Top             =   390
            Width           =   4185
            _ExtentX        =   7382
            _ExtentY        =   1984
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            HoverSelection  =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Livello"
               Text            =   "Liv."
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "NCampo"
               Text            =   "Nome campo"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "Bytes"
               Text            =   "Bytes"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "Lunghezza"
               Text            =   "Len"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "Posizione"
               Text            =   "Start"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Key             =   "Tipo"
               Text            =   "T"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Key             =   "Occurs"
               Text            =   "Occ."
               Object.Width           =   1058
            EndProperty
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackColor       =   &H8000000D&
            ForeColor       =   &H8000000E&
            Height          =   225
            Left            =   4620
            TabIndex        =   490
            Top             =   150
            Width           =   4185
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Critical Overlapping Cobol Areas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   1935
         Left            =   3570
         TabIndex        =   204
         Top             =   3750
         Width           =   5415
         Begin VB.HScrollBar HScroll1 
            Height          =   255
            Left            =   60
            TabIndex        =   206
            Top             =   1530
            Width           =   3765
         End
         Begin VB.CheckBox ChkFiller 
            Caption         =   "Check Filler"
            Height          =   195
            Left            =   3870
            TabIndex        =   205
            Top             =   1560
            Width           =   1425
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   1
            Left            =   210
            TabIndex        =   486
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   0
            Left            =   60
            TabIndex        =   485
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   2
            Left            =   360
            TabIndex        =   484
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   3
            Left            =   510
            TabIndex        =   483
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   4
            Left            =   660
            TabIndex        =   482
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   5
            Left            =   810
            TabIndex        =   481
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   6
            Left            =   960
            TabIndex        =   480
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   7
            Left            =   1110
            TabIndex        =   479
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   8
            Left            =   1260
            TabIndex        =   478
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   9
            Left            =   1410
            TabIndex        =   477
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   10
            Left            =   1560
            TabIndex        =   476
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   11
            Left            =   1710
            TabIndex        =   475
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   12
            Left            =   1860
            TabIndex        =   474
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   13
            Left            =   2010
            TabIndex        =   473
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   14
            Left            =   2160
            TabIndex        =   472
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   15
            Left            =   2310
            TabIndex        =   471
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   16
            Left            =   2460
            TabIndex        =   470
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   17
            Left            =   2610
            TabIndex        =   469
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   18
            Left            =   2760
            TabIndex        =   468
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   19
            Left            =   2910
            TabIndex        =   467
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   20
            Left            =   3060
            TabIndex        =   466
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   21
            Left            =   3210
            TabIndex        =   465
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   22
            Left            =   3360
            TabIndex        =   464
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   23
            Left            =   3510
            TabIndex        =   463
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   24
            Left            =   3660
            TabIndex        =   462
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   25
            Left            =   3810
            TabIndex        =   461
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   26
            Left            =   3960
            TabIndex        =   460
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   27
            Left            =   4110
            TabIndex        =   459
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   28
            Left            =   4260
            TabIndex        =   458
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   29
            Left            =   4410
            TabIndex        =   457
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   30
            Left            =   4560
            TabIndex        =   456
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   31
            Left            =   4710
            TabIndex        =   455
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   32
            Left            =   4860
            TabIndex        =   454
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   33
            Left            =   5010
            TabIndex        =   453
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   34
            Left            =   5160
            TabIndex        =   452
            Tag             =   "fixed"
            Top             =   210
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   35
            Left            =   60
            TabIndex        =   451
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   36
            Left            =   210
            TabIndex        =   450
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   37
            Left            =   360
            TabIndex        =   449
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   38
            Left            =   510
            TabIndex        =   448
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   39
            Left            =   660
            TabIndex        =   447
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   40
            Left            =   810
            TabIndex        =   446
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   41
            Left            =   960
            TabIndex        =   445
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   42
            Left            =   1110
            TabIndex        =   444
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   43
            Left            =   1260
            TabIndex        =   443
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   44
            Left            =   1410
            TabIndex        =   442
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   45
            Left            =   1560
            TabIndex        =   441
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   46
            Left            =   1710
            TabIndex        =   440
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   47
            Left            =   1860
            TabIndex        =   439
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   48
            Left            =   2010
            TabIndex        =   438
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   49
            Left            =   2160
            TabIndex        =   437
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   50
            Left            =   2310
            TabIndex        =   436
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   51
            Left            =   2460
            TabIndex        =   435
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   52
            Left            =   2610
            TabIndex        =   434
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   53
            Left            =   2760
            TabIndex        =   433
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   54
            Left            =   2910
            TabIndex        =   432
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   55
            Left            =   3060
            TabIndex        =   431
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   56
            Left            =   3210
            TabIndex        =   430
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   57
            Left            =   3360
            TabIndex        =   429
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   58
            Left            =   3510
            TabIndex        =   428
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   59
            Left            =   3660
            TabIndex        =   427
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   60
            Left            =   3810
            TabIndex        =   426
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   61
            Left            =   3960
            TabIndex        =   425
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   62
            Left            =   4110
            TabIndex        =   424
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   63
            Left            =   4260
            TabIndex        =   423
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   64
            Left            =   4410
            TabIndex        =   422
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   65
            Left            =   4560
            TabIndex        =   421
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   66
            Left            =   4710
            TabIndex        =   420
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   67
            Left            =   4860
            TabIndex        =   419
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   68
            Left            =   5010
            TabIndex        =   418
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   69
            Left            =   5160
            TabIndex        =   417
            Tag             =   "fixed"
            Top             =   360
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   70
            Left            =   60
            TabIndex        =   416
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   71
            Left            =   210
            TabIndex        =   415
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   72
            Left            =   360
            TabIndex        =   414
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   73
            Left            =   510
            TabIndex        =   413
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   74
            Left            =   660
            TabIndex        =   412
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   75
            Left            =   810
            TabIndex        =   411
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   76
            Left            =   960
            TabIndex        =   410
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   77
            Left            =   1110
            TabIndex        =   409
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   78
            Left            =   1260
            TabIndex        =   408
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   79
            Left            =   1410
            TabIndex        =   407
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   80
            Left            =   1560
            TabIndex        =   406
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   81
            Left            =   1710
            TabIndex        =   405
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   82
            Left            =   1860
            TabIndex        =   404
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   83
            Left            =   2010
            TabIndex        =   403
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   84
            Left            =   2160
            TabIndex        =   402
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   85
            Left            =   2310
            TabIndex        =   401
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   86
            Left            =   2460
            TabIndex        =   400
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   87
            Left            =   2610
            TabIndex        =   399
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   88
            Left            =   2760
            TabIndex        =   398
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   89
            Left            =   2910
            TabIndex        =   397
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   90
            Left            =   3060
            TabIndex        =   396
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   91
            Left            =   3210
            TabIndex        =   395
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   92
            Left            =   3360
            TabIndex        =   394
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   93
            Left            =   3510
            TabIndex        =   393
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   94
            Left            =   3660
            TabIndex        =   392
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   95
            Left            =   3810
            TabIndex        =   391
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   96
            Left            =   3960
            TabIndex        =   390
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   97
            Left            =   4110
            TabIndex        =   389
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   98
            Left            =   4260
            TabIndex        =   388
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   99
            Left            =   4410
            TabIndex        =   387
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   100
            Left            =   4560
            TabIndex        =   386
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   101
            Left            =   4710
            TabIndex        =   385
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   102
            Left            =   4860
            TabIndex        =   384
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   103
            Left            =   5010
            TabIndex        =   383
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   104
            Left            =   5160
            TabIndex        =   382
            Tag             =   "fixed"
            Top             =   510
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   105
            Left            =   60
            TabIndex        =   381
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   106
            Left            =   210
            TabIndex        =   380
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   107
            Left            =   360
            TabIndex        =   379
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   108
            Left            =   510
            TabIndex        =   378
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   109
            Left            =   660
            TabIndex        =   377
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   110
            Left            =   810
            TabIndex        =   376
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   111
            Left            =   960
            TabIndex        =   375
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   112
            Left            =   1110
            TabIndex        =   374
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   113
            Left            =   1260
            TabIndex        =   373
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   114
            Left            =   1410
            TabIndex        =   372
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   115
            Left            =   1560
            TabIndex        =   371
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   116
            Left            =   1710
            TabIndex        =   370
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   117
            Left            =   1860
            TabIndex        =   369
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   118
            Left            =   2010
            TabIndex        =   368
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   119
            Left            =   2160
            TabIndex        =   367
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   120
            Left            =   2310
            TabIndex        =   366
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   121
            Left            =   2460
            TabIndex        =   365
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   122
            Left            =   2610
            TabIndex        =   364
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   123
            Left            =   2760
            TabIndex        =   363
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   124
            Left            =   2910
            TabIndex        =   362
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   125
            Left            =   3060
            TabIndex        =   361
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   126
            Left            =   3210
            TabIndex        =   360
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   127
            Left            =   3360
            TabIndex        =   359
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   128
            Left            =   3510
            TabIndex        =   358
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   129
            Left            =   3660
            TabIndex        =   357
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   130
            Left            =   3810
            TabIndex        =   356
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   131
            Left            =   3960
            TabIndex        =   355
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   132
            Left            =   4110
            TabIndex        =   354
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   133
            Left            =   4260
            TabIndex        =   353
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   134
            Left            =   4410
            TabIndex        =   352
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   135
            Left            =   4560
            TabIndex        =   351
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   136
            Left            =   4710
            TabIndex        =   350
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   137
            Left            =   4860
            TabIndex        =   349
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   138
            Left            =   5010
            TabIndex        =   348
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   139
            Left            =   5160
            TabIndex        =   347
            Tag             =   "fixed"
            Top             =   660
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   140
            Left            =   60
            TabIndex        =   346
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   141
            Left            =   210
            TabIndex        =   345
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   142
            Left            =   360
            TabIndex        =   344
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   143
            Left            =   510
            TabIndex        =   343
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   144
            Left            =   660
            TabIndex        =   342
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   145
            Left            =   810
            TabIndex        =   341
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   146
            Left            =   960
            TabIndex        =   340
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   147
            Left            =   1110
            TabIndex        =   339
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   148
            Left            =   1260
            TabIndex        =   338
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   149
            Left            =   1410
            TabIndex        =   337
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   150
            Left            =   1560
            TabIndex        =   336
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   151
            Left            =   1710
            TabIndex        =   335
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   152
            Left            =   1860
            TabIndex        =   334
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   153
            Left            =   2010
            TabIndex        =   333
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   154
            Left            =   2160
            TabIndex        =   332
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   155
            Left            =   2310
            TabIndex        =   331
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   156
            Left            =   2460
            TabIndex        =   330
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   157
            Left            =   2610
            TabIndex        =   329
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   158
            Left            =   2760
            TabIndex        =   328
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   159
            Left            =   2910
            TabIndex        =   327
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   160
            Left            =   3060
            TabIndex        =   326
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   161
            Left            =   3210
            TabIndex        =   325
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   162
            Left            =   3360
            TabIndex        =   324
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   163
            Left            =   3510
            TabIndex        =   323
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   164
            Left            =   3660
            TabIndex        =   322
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   165
            Left            =   3810
            TabIndex        =   321
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   166
            Left            =   3960
            TabIndex        =   320
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   167
            Left            =   4110
            TabIndex        =   319
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   168
            Left            =   4260
            TabIndex        =   318
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   169
            Left            =   4410
            TabIndex        =   317
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   170
            Left            =   4560
            TabIndex        =   316
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   171
            Left            =   4710
            TabIndex        =   315
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   172
            Left            =   4860
            TabIndex        =   314
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   173
            Left            =   5010
            TabIndex        =   313
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   174
            Left            =   5160
            TabIndex        =   312
            Tag             =   "fixed"
            Top             =   810
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   175
            Left            =   60
            TabIndex        =   311
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   176
            Left            =   210
            TabIndex        =   310
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   177
            Left            =   360
            TabIndex        =   309
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   178
            Left            =   510
            TabIndex        =   308
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   179
            Left            =   660
            TabIndex        =   307
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   180
            Left            =   810
            TabIndex        =   306
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   181
            Left            =   960
            TabIndex        =   305
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   182
            Left            =   1110
            TabIndex        =   304
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   183
            Left            =   1260
            TabIndex        =   303
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   184
            Left            =   1410
            TabIndex        =   302
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   185
            Left            =   1560
            TabIndex        =   301
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   186
            Left            =   1710
            TabIndex        =   300
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   187
            Left            =   1860
            TabIndex        =   299
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   188
            Left            =   2010
            TabIndex        =   298
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   189
            Left            =   2160
            TabIndex        =   297
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   190
            Left            =   2310
            TabIndex        =   296
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   191
            Left            =   2460
            TabIndex        =   295
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   192
            Left            =   2610
            TabIndex        =   294
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   193
            Left            =   2760
            TabIndex        =   293
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   194
            Left            =   2910
            TabIndex        =   292
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   195
            Left            =   3060
            TabIndex        =   291
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   196
            Left            =   3210
            TabIndex        =   290
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   197
            Left            =   3360
            TabIndex        =   289
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   198
            Left            =   3510
            TabIndex        =   288
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   199
            Left            =   3660
            TabIndex        =   287
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   200
            Left            =   3810
            TabIndex        =   286
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   201
            Left            =   3960
            TabIndex        =   285
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   202
            Left            =   4110
            TabIndex        =   284
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   203
            Left            =   4260
            TabIndex        =   283
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   204
            Left            =   4410
            TabIndex        =   282
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   205
            Left            =   4560
            TabIndex        =   281
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   206
            Left            =   4710
            TabIndex        =   280
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   207
            Left            =   4860
            TabIndex        =   279
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   208
            Left            =   5010
            TabIndex        =   278
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   209
            Left            =   5160
            TabIndex        =   277
            Tag             =   "fixed"
            Top             =   960
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   210
            Left            =   60
            TabIndex        =   276
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   211
            Left            =   210
            TabIndex        =   275
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   212
            Left            =   360
            TabIndex        =   274
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   213
            Left            =   510
            TabIndex        =   273
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   214
            Left            =   660
            TabIndex        =   272
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   215
            Left            =   810
            TabIndex        =   271
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   216
            Left            =   960
            TabIndex        =   270
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   217
            Left            =   1110
            TabIndex        =   269
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   218
            Left            =   1260
            TabIndex        =   268
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   219
            Left            =   1410
            TabIndex        =   267
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   220
            Left            =   1560
            TabIndex        =   266
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   221
            Left            =   1710
            TabIndex        =   265
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   222
            Left            =   1860
            TabIndex        =   264
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   223
            Left            =   2010
            TabIndex        =   263
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   224
            Left            =   2160
            TabIndex        =   262
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   225
            Left            =   2310
            TabIndex        =   261
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   226
            Left            =   2460
            TabIndex        =   260
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   227
            Left            =   2610
            TabIndex        =   259
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   228
            Left            =   2760
            TabIndex        =   258
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   229
            Left            =   2910
            TabIndex        =   257
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   230
            Left            =   3060
            TabIndex        =   256
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   231
            Left            =   3210
            TabIndex        =   255
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   232
            Left            =   3360
            TabIndex        =   254
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   233
            Left            =   3510
            TabIndex        =   253
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   234
            Left            =   3660
            TabIndex        =   252
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   235
            Left            =   3810
            TabIndex        =   251
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   236
            Left            =   3960
            TabIndex        =   250
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   237
            Left            =   4110
            TabIndex        =   249
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   238
            Left            =   4260
            TabIndex        =   248
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   239
            Left            =   4410
            TabIndex        =   247
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   240
            Left            =   4560
            TabIndex        =   246
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   241
            Left            =   4710
            TabIndex        =   245
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   242
            Left            =   4860
            TabIndex        =   244
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   243
            Left            =   5010
            TabIndex        =   243
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   244
            Left            =   5160
            TabIndex        =   242
            Tag             =   "fixed"
            Top             =   1110
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   245
            Left            =   60
            TabIndex        =   241
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   246
            Left            =   210
            TabIndex        =   240
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   247
            Left            =   360
            TabIndex        =   239
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   248
            Left            =   510
            TabIndex        =   238
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   249
            Left            =   660
            TabIndex        =   237
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   250
            Left            =   810
            TabIndex        =   236
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   251
            Left            =   960
            TabIndex        =   235
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   252
            Left            =   1110
            TabIndex        =   234
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   253
            Left            =   1260
            TabIndex        =   233
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   254
            Left            =   1410
            TabIndex        =   232
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   255
            Left            =   1560
            TabIndex        =   231
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   256
            Left            =   1710
            TabIndex        =   230
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   257
            Left            =   1860
            TabIndex        =   229
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   258
            Left            =   2010
            TabIndex        =   228
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   259
            Left            =   2160
            TabIndex        =   227
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   260
            Left            =   2310
            TabIndex        =   226
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   261
            Left            =   2460
            TabIndex        =   225
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   262
            Left            =   2610
            TabIndex        =   224
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   263
            Left            =   2760
            TabIndex        =   223
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   264
            Left            =   2910
            TabIndex        =   222
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   265
            Left            =   3060
            TabIndex        =   221
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   266
            Left            =   3210
            TabIndex        =   220
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   267
            Left            =   3360
            TabIndex        =   219
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   268
            Left            =   3510
            TabIndex        =   218
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   269
            Left            =   3660
            TabIndex        =   217
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   270
            Left            =   3810
            TabIndex        =   216
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   271
            Left            =   3960
            TabIndex        =   215
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   272
            Left            =   4110
            TabIndex        =   214
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   273
            Left            =   4260
            TabIndex        =   213
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   274
            Left            =   4410
            TabIndex        =   212
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   275
            Left            =   4560
            TabIndex        =   211
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   276
            Left            =   4710
            TabIndex        =   210
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   277
            Left            =   4860
            TabIndex        =   209
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   278
            Left            =   5010
            TabIndex        =   208
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
         Begin VB.Label LblConfronto 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   279
            Left            =   5160
            TabIndex        =   207
            Tag             =   "fixed"
            Top             =   1260
            Width           =   165
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Field Details"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2715
         Left            =   60
         TabIndex        =   200
         Top             =   2970
         Width           =   3435
         Begin VB.CommandButton CancRipField 
            Height          =   330
            Left            =   3030
            Picture         =   "MadmdF_EditDli.frx":0370
            Style           =   1  'Graphical
            TabIndex        =   202
            ToolTipText     =   "Delete Field Reference"
            Top             =   210
            Width           =   330
         End
         Begin VB.CommandButton CmdImpostaChiave 
            Height          =   330
            Left            =   3030
            Picture         =   "MadmdF_EditDli.frx":04BA
            Style           =   1  'Graphical
            TabIndex        =   201
            ToolTipText     =   "Setting Key"
            Top             =   600
            Width           =   330
         End
         Begin MSComctlLib.ListView ListField 
            Height          =   2430
            Left            =   60
            TabIndex        =   203
            Top             =   210
            Width           =   2925
            _ExtentX        =   5159
            _ExtentY        =   4286
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            HoverSelection  =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   -2147483624
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "NCampo"
               Text            =   "Nome campo"
               Object.Width           =   2294
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "Sequenziale"
               Text            =   "S"
               Object.Width           =   564
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "Unique"
               Text            =   "U"
               Object.Width           =   564
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "Lunghezza"
               Text            =   "Lunghezza"
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "Start"
               Text            =   "Start"
               Object.Width           =   970
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Key             =   "Tipo"
               Text            =   "Tipo"
               Object.Width           =   1058
            EndProperty
         End
      End
      Begin VB.Frame fraIdxDliPgm 
         BorderStyle     =   0  'None
         Height          =   4815
         Left            =   -74970
         TabIndex        =   198
         Top             =   30
         Width           =   8865
         Begin MSComctlLib.ListView lsw2ndIdxAccess 
            Height          =   4665
            Left            =   150
            TabIndex        =   199
            Top             =   120
            Width           =   8745
            _ExtentX        =   15425
            _ExtentY        =   8229
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "IdOggetto"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Nome"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraTab4 
         BorderStyle     =   0  'None
         Height          =   5265
         Left            =   -74940
         TabIndex        =   157
         Top             =   30
         Width           =   8955
         Begin VB.Frame FrmRelazioni 
            Height          =   4755
            Left            =   30
            TabIndex        =   525
            Top             =   60
            Width           =   8895
            Begin MSComctlLib.TreeView TreeRelazioni 
               Height          =   4245
               Left            =   120
               TabIndex        =   526
               Top             =   420
               Width           =   8625
               _ExtentX        =   15214
               _ExtentY        =   7488
               _Version        =   393217
               LabelEdit       =   1
               LineStyle       =   1
               Style           =   6
               Checkboxes      =   -1  'True
               Appearance      =   1
            End
            Begin VB.Label Label25 
               Alignment       =   2  'Center
               BackColor       =   &H8000000D&
               Caption         =   "Father - Child Relationships"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H8000000E&
               Height          =   255
               Left            =   120
               TabIndex        =   527
               Top             =   150
               Width           =   8595
            End
         End
      End
      Begin VB.Frame FraTab3 
         BorderStyle     =   0  'None
         Height          =   4845
         Left            =   -74940
         TabIndex        =   156
         Top             =   30
         Width           =   8985
         Begin MSComctlLib.ListView LstFldEdit 
            Height          =   3645
            Left            =   60
            TabIndex        =   194
            Top             =   570
            Width           =   8865
            _ExtentX        =   15637
            _ExtentY        =   6429
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   14
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Livello"
               Text            =   "Lv."
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "Ordinale"
               Text            =   "Ord."
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "NCampo"
               Text            =   "Nome campo"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "Bytes"
               Text            =   "Bytes"
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "Lunghezza"
               Text            =   "Len."
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Text            =   "Dec."
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "Posizione"
               Text            =   "Start"
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Key             =   "Tipo"
               Text            =   "Type"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   8
               Key             =   "Occurs"
               Text            =   "Occurs"
               Object.Width           =   1235
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Text            =   "Flattened"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Text            =   "Redefines"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Text            =   "Sel Value"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   12
               Text            =   "Id_Cmp"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   13
               Text            =   "To_Migrate"
               Object.Width           =   1411
            EndProperty
         End
         Begin MSComctlLib.ImageList imlToolbarIcons 
            Left            =   4110
            Top             =   0
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   11
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0604
                  Key             =   "New"
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0716
                  Key             =   "Copy"
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0828
                  Key             =   "Paste"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":093A
                  Key             =   "Cut"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0A4C
                  Key             =   "Open"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0B5E
                  Key             =   "Save"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0C70
                  Key             =   "Help"
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0D82
                  Key             =   "Close"
               EndProperty
               BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":0EDE
                  Key             =   "Check"
               EndProperty
               BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":133E
                  Key             =   "Up"
               EndProperty
               BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "MadmdF_EditDli.frx":165A
                  Key             =   "Down"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   390
            Left            =   0
            TabIndex        =   511
            Tag             =   "fixed"
            Top             =   0
            Width           =   3495
            _ExtentX        =   6165
            _ExtentY        =   688
            ButtonWidth     =   609
            ButtonHeight    =   582
            Appearance      =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   15
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "New"
                  Object.ToolTipText     =   "New"
                  ImageKey        =   "New"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Copy"
                  Object.ToolTipText     =   "Copy"
                  ImageKey        =   "Copy"
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Paste"
                  Object.ToolTipText     =   "Paste"
                  ImageKey        =   "Paste"
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Cut"
                  Object.ToolTipText     =   "Cut"
                  ImageKey        =   "Cut"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Up"
                  Object.ToolTipText     =   "Up"
                  ImageKey        =   "Up"
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Down"
                  Object.ToolTipText     =   "Down"
                  ImageKey        =   "Down"
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Open"
                  Object.ToolTipText     =   "Open"
                  ImageKey        =   "Open"
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "Save"
                  Object.ToolTipText     =   "Save"
                  ImageKey        =   "Save"
               EndProperty
               BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Style           =   3
               EndProperty
               BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "Check"
                  Object.ToolTipText     =   "Check Area"
                  ImageKey        =   "Check"
               EndProperty
               BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "Help"
                  Object.ToolTipText     =   "Help"
                  ImageKey        =   "Help"
               EndProperty
               BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
            EndProperty
         End
         Begin VB.Label LblModif 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   375
            Left            =   6150
            TabIndex        =   195
            Top             =   120
            Width           =   2775
         End
      End
      Begin VB.Frame FraTab2 
         BorderStyle     =   0  'None
         Height          =   4845
         Left            =   -74940
         TabIndex        =   2
         Top             =   30
         Width           =   8985
         Begin VB.Frame Areaincidenza 
            Caption         =   "Matching Areas info"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   1785
            Left            =   0
            TabIndex        =   7
            Top             =   2085
            Width           =   4665
            Begin VB.Frame Frame19 
               Caption         =   "Keys"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000080&
               Height          =   1425
               Left            =   2610
               TabIndex        =   9
               Top             =   180
               Width           =   1965
               Begin VB.Label Label3 
                  BackColor       =   &H0000FFFF&
                  Height          =   135
                  Left            =   150
                  TabIndex        =   15
                  Top             =   330
                  Width           =   135
               End
               Begin VB.Label Label5 
                  BackColor       =   &H000000FF&
                  Height          =   135
                  Left            =   150
                  TabIndex        =   14
                  Top             =   810
                  Width           =   135
               End
               Begin VB.Label Label17 
                  BackColor       =   &H00FF0000&
                  Height          =   135
                  Left            =   150
                  TabIndex        =   13
                  Top             =   570
                  Width           =   135
               End
               Begin VB.Label Label18 
                  Caption         =   "Numeric"
                  ForeColor       =   &H00C00000&
                  Height          =   285
                  Left            =   450
                  TabIndex        =   12
                  Top             =   300
                  Width           =   975
               End
               Begin VB.Label Label20 
                  Caption         =   "Area doesn't match with declared fields"
                  ForeColor       =   &H00C00000&
                  Height          =   495
                  Left            =   450
                  TabIndex        =   11
                  Top             =   780
                  Width           =   1395
               End
               Begin VB.Label Label22 
                  Caption         =   "Alphanumeric"
                  ForeColor       =   &H00C00000&
                  Height          =   255
                  Left            =   450
                  TabIndex        =   10
                  Top             =   540
                  Width           =   1095
               End
            End
            Begin VB.VScrollBar VScroll1 
               Height          =   1455
               Left            =   2280
               TabIndex        =   8
               Top             =   210
               Width           =   255
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   0
               Left            =   60
               TabIndex        =   155
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   1
               Left            =   180
               TabIndex        =   154
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   2
               Left            =   60
               TabIndex        =   153
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   3
               Left            =   180
               TabIndex        =   152
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   4
               Left            =   360
               TabIndex        =   151
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   5
               Left            =   480
               TabIndex        =   150
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   6
               Left            =   360
               TabIndex        =   149
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   7
               Left            =   480
               TabIndex        =   148
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   8
               Left            =   660
               TabIndex        =   147
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   9
               Left            =   780
               TabIndex        =   146
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   10
               Left            =   660
               TabIndex        =   145
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   11
               Left            =   780
               TabIndex        =   144
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   12
               Left            =   960
               TabIndex        =   143
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   13
               Left            =   1080
               TabIndex        =   142
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   14
               Left            =   960
               TabIndex        =   141
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   15
               Left            =   1080
               TabIndex        =   140
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   16
               Left            =   1260
               TabIndex        =   139
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   17
               Left            =   1380
               TabIndex        =   138
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   18
               Left            =   1260
               TabIndex        =   137
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   19
               Left            =   1380
               TabIndex        =   136
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   20
               Left            =   1560
               TabIndex        =   135
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   21
               Left            =   1680
               TabIndex        =   134
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   22
               Left            =   1560
               TabIndex        =   133
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   23
               Left            =   1680
               TabIndex        =   132
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   24
               Left            =   1860
               TabIndex        =   131
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   25
               Left            =   1980
               TabIndex        =   130
               Tag             =   "fixed"
               Top             =   210
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   26
               Left            =   1860
               TabIndex        =   129
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   27
               Left            =   1980
               TabIndex        =   128
               Tag             =   "fixed"
               Top             =   330
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   28
               Left            =   60
               TabIndex        =   127
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   29
               Left            =   180
               TabIndex        =   126
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   30
               Left            =   60
               TabIndex        =   125
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   31
               Left            =   180
               TabIndex        =   124
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   32
               Left            =   360
               TabIndex        =   123
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   33
               Left            =   480
               TabIndex        =   122
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   34
               Left            =   360
               TabIndex        =   121
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   35
               Left            =   480
               TabIndex        =   120
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   36
               Left            =   660
               TabIndex        =   119
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   37
               Left            =   780
               TabIndex        =   118
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   38
               Left            =   660
               TabIndex        =   117
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   39
               Left            =   780
               TabIndex        =   116
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   40
               Left            =   960
               TabIndex        =   115
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   41
               Left            =   1080
               TabIndex        =   114
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   42
               Left            =   960
               TabIndex        =   113
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   43
               Left            =   1080
               TabIndex        =   112
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   44
               Left            =   1260
               TabIndex        =   111
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   45
               Left            =   1380
               TabIndex        =   110
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   46
               Left            =   1260
               TabIndex        =   109
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   47
               Left            =   1380
               TabIndex        =   108
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   48
               Left            =   1560
               TabIndex        =   107
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   49
               Left            =   1680
               TabIndex        =   106
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   50
               Left            =   1560
               TabIndex        =   105
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   51
               Left            =   1680
               TabIndex        =   104
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   52
               Left            =   1860
               TabIndex        =   103
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   53
               Left            =   1980
               TabIndex        =   102
               Tag             =   "fixed"
               Top             =   510
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   54
               Left            =   1860
               TabIndex        =   101
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   55
               Left            =   1980
               TabIndex        =   100
               Tag             =   "fixed"
               Top             =   630
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   56
               Left            =   60
               TabIndex        =   99
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   57
               Left            =   180
               TabIndex        =   98
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   58
               Left            =   60
               TabIndex        =   97
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   59
               Left            =   180
               TabIndex        =   96
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   60
               Left            =   360
               TabIndex        =   95
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   61
               Left            =   480
               TabIndex        =   94
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   62
               Left            =   360
               TabIndex        =   93
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   63
               Left            =   480
               TabIndex        =   92
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   64
               Left            =   660
               TabIndex        =   91
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   65
               Left            =   780
               TabIndex        =   90
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   66
               Left            =   660
               TabIndex        =   89
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   67
               Left            =   780
               TabIndex        =   88
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   68
               Left            =   960
               TabIndex        =   87
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   69
               Left            =   1080
               TabIndex        =   86
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   70
               Left            =   960
               TabIndex        =   85
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   71
               Left            =   1080
               TabIndex        =   84
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   72
               Left            =   1260
               TabIndex        =   83
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   73
               Left            =   1380
               TabIndex        =   82
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   74
               Left            =   1260
               TabIndex        =   81
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   75
               Left            =   1380
               TabIndex        =   80
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   76
               Left            =   1560
               TabIndex        =   79
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   77
               Left            =   1680
               TabIndex        =   78
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   78
               Left            =   1560
               TabIndex        =   77
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   79
               Left            =   1680
               TabIndex        =   76
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   80
               Left            =   1860
               TabIndex        =   75
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   81
               Left            =   1980
               TabIndex        =   74
               Tag             =   "fixed"
               Top             =   810
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   82
               Left            =   1860
               TabIndex        =   73
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   83
               Left            =   1980
               TabIndex        =   72
               Tag             =   "fixed"
               Top             =   930
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   84
               Left            =   60
               TabIndex        =   71
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   85
               Left            =   180
               TabIndex        =   70
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   86
               Left            =   60
               TabIndex        =   69
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   87
               Left            =   180
               TabIndex        =   68
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   88
               Left            =   360
               TabIndex        =   67
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   89
               Left            =   480
               TabIndex        =   66
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   90
               Left            =   360
               TabIndex        =   65
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   91
               Left            =   480
               TabIndex        =   64
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   92
               Left            =   660
               TabIndex        =   63
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   93
               Left            =   780
               TabIndex        =   62
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   94
               Left            =   660
               TabIndex        =   61
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   95
               Left            =   780
               TabIndex        =   60
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   96
               Left            =   960
               TabIndex        =   59
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   97
               Left            =   1080
               TabIndex        =   58
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   98
               Left            =   960
               TabIndex        =   57
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   99
               Left            =   1080
               TabIndex        =   56
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   100
               Left            =   1260
               TabIndex        =   55
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   101
               Left            =   1380
               TabIndex        =   54
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   102
               Left            =   1260
               TabIndex        =   53
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   103
               Left            =   1380
               TabIndex        =   52
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   104
               Left            =   1560
               TabIndex        =   51
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   105
               Left            =   1680
               TabIndex        =   50
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   106
               Left            =   1560
               TabIndex        =   49
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   107
               Left            =   1680
               TabIndex        =   48
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   108
               Left            =   1860
               TabIndex        =   47
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   109
               Left            =   1980
               TabIndex        =   46
               Tag             =   "fixed"
               Top             =   1110
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   110
               Left            =   1860
               TabIndex        =   45
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   111
               Left            =   1980
               TabIndex        =   44
               Tag             =   "fixed"
               Top             =   1230
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   112
               Left            =   60
               TabIndex        =   43
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   113
               Left            =   180
               TabIndex        =   42
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   114
               Left            =   60
               TabIndex        =   41
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   115
               Left            =   180
               TabIndex        =   40
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   116
               Left            =   360
               TabIndex        =   39
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   117
               Left            =   480
               TabIndex        =   38
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   118
               Left            =   360
               TabIndex        =   37
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   119
               Left            =   480
               TabIndex        =   36
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   120
               Left            =   660
               TabIndex        =   35
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   121
               Left            =   780
               TabIndex        =   34
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   122
               Left            =   660
               TabIndex        =   33
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   123
               Left            =   780
               TabIndex        =   32
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   124
               Left            =   960
               TabIndex        =   31
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   125
               Left            =   1080
               TabIndex        =   30
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   126
               Left            =   960
               TabIndex        =   29
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   127
               Left            =   1080
               TabIndex        =   28
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   128
               Left            =   1260
               TabIndex        =   27
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   129
               Left            =   1380
               TabIndex        =   26
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   130
               Left            =   1260
               TabIndex        =   25
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   131
               Left            =   1380
               TabIndex        =   24
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   132
               Left            =   1560
               TabIndex        =   23
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   133
               Left            =   1680
               TabIndex        =   22
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   134
               Left            =   1560
               TabIndex        =   21
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   135
               Left            =   1680
               TabIndex        =   20
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   136
               Left            =   1860
               TabIndex        =   19
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   137
               Left            =   1980
               TabIndex        =   18
               Tag             =   "fixed"
               Top             =   1410
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   138
               Left            =   1860
               TabIndex        =   17
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
            Begin VB.Label LblIncidenza 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   135
               Index           =   139
               Left            =   1980
               TabIndex        =   16
               Tag             =   "fixed"
               Top             =   1530
               Width           =   135
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Field Details"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   1965
            Left            =   0
            TabIndex        =   5
            Top             =   -30
            Width           =   4635
            Begin MSComctlLib.ListView ListField2 
               Height          =   1620
               Left            =   60
               TabIndex        =   6
               Top             =   240
               Width           =   4485
               _ExtentX        =   7911
               _ExtentY        =   2858
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               HoverSelection  =   -1  'True
               _Version        =   393217
               ForeColor       =   12582912
               BackColor       =   16777152
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   6
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Key             =   "NCampo"
                  Text            =   "Nome campo"
                  Object.Width           =   2293
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Key             =   "Sequenziale"
                  Text            =   "S"
                  Object.Width           =   794
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Key             =   "Unique"
                  Text            =   "U"
                  Object.Width           =   794
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Key             =   "Lunghezza"
                  Text            =   "Lunghezza"
                  Object.Width           =   1323
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   4
                  Key             =   "Start"
                  Text            =   "Start"
                  Object.Width           =   1058
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   5
                  Key             =   "Tipo"
                  Text            =   "Tipo"
                  Object.Width           =   1058
               EndProperty
            End
         End
         Begin VB.Frame Frame10 
            ForeColor       =   &H8000000D&
            Height          =   3915
            Left            =   4830
            TabIndex        =   3
            Top             =   -30
            Width           =   3945
            Begin VB.TextBox Text11 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   188
               Top             =   2805
               Width           =   2505
            End
            Begin VB.TextBox Text12 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   187
               Top             =   2115
               Width           =   2505
            End
            Begin VB.TextBox Text13 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   186
               Top             =   1770
               Width           =   2505
            End
            Begin VB.TextBox Text14 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   185
               Top             =   2460
               Width           =   2505
            End
            Begin VB.TextBox Text15 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   184
               Top             =   3150
               Width           =   2505
            End
            Begin VB.TextBox Text10 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   180
               Top             =   1440
               Width           =   2505
            End
            Begin VB.TextBox Text9 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   179
               Top             =   1110
               Width           =   2505
            End
            Begin VB.TextBox Text8 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   1350
               Locked          =   -1  'True
               TabIndex        =   178
               Top             =   780
               Width           =   2505
            End
            Begin VB.TextBox TxtNCampo 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Left            =   150
               Locked          =   -1  'True
               TabIndex        =   4
               Top             =   300
               Width           =   3705
            End
            Begin VB.Image Image2 
               Height          =   1620
               Left            =   90
               Picture         =   "MadmdF_EditDli.frx":1976
               Top             =   1860
               Width           =   330
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Subseq :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   585
               TabIndex        =   193
               Top             =   3195
               Width           =   675
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Nullval :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   600
               TabIndex        =   192
               Top             =   2490
               Width           =   660
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               Caption         =   "Name :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   705
               TabIndex        =   191
               Top             =   1800
               Width           =   555
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               Caption         =   "Segment :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   480
               TabIndex        =   190
               Top             =   2145
               Width           =   780
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               Caption         =   "Shrc :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   765
               TabIndex        =   189
               Top             =   2835
               Width           =   495
            End
            Begin VB.Image Image1 
               Height          =   825
               Left            =   90
               Picture         =   "MadmdF_EditDli.frx":3668
               Top             =   900
               Width           =   315
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Pointer :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   615
               TabIndex        =   183
               Top             =   1470
               Width           =   645
            End
            Begin VB.Label Label10 
               AutoSize        =   -1  'True
               Caption         =   "DBD name :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   390
               TabIndex        =   182
               Top             =   1155
               Width           =   870
            End
            Begin VB.Label Label9 
               AutoSize        =   -1  'True
               Caption         =   "Punt. seg. :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Left            =   450
               TabIndex        =   181
               Top             =   855
               Width           =   810
            End
         End
      End
      Begin VB.Frame FraTab1 
         BorderStyle     =   0  'None
         Height          =   4845
         Left            =   60
         TabIndex        =   1
         Top             =   90
         Width           =   8985
         Begin VB.CommandButton cmdAutomAssArea 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   8580
            Picture         =   "MadmdF_EditDli.frx":446A
            Style           =   1  'Graphical
            TabIndex        =   535
            ToolTipText     =   "Automatic Association Area From File"
            Top             =   1275
            Width           =   300
         End
         Begin VB.CheckBox ChkRedefines 
            Caption         =   "Create Dispatcher"
            Height          =   195
            Left            =   6840
            TabIndex        =   532
            Top             =   0
            Value           =   1  'Checked
            Width           =   1635
         End
         Begin VB.CommandButton Cmd 
            Height          =   300
            Left            =   8580
            Picture         =   "MadmdF_EditDli.frx":49F4
            Style           =   1  'Graphical
            TabIndex        =   519
            ToolTipText     =   "Associate/Unassociate Area to segment"
            Top             =   0
            Visible         =   0   'False
            Width           =   300
         End
         Begin VB.CommandButton CmdAreaPrim 
            Height          =   300
            Left            =   8580
            Picture         =   "MadmdF_EditDli.frx":4B3E
            Style           =   1  'Graphical
            TabIndex        =   518
            ToolTipText     =   "Select/Unselect Primary Area"
            Top             =   360
            Width           =   300
         End
         Begin VB.CommandButton cmdAddArea 
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   8580
            TabIndex        =   517
            ToolTipText     =   "Add association"
            Top             =   660
            Width           =   300
         End
         Begin VB.CommandButton cmdRemoveArea 
            Caption         =   "R"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   8580
            TabIndex        =   516
            ToolTipText     =   "Remove association"
            Top             =   945
            Width           =   300
         End
         Begin VB.Frame Frame1 
            Caption         =   "Segment Details"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   2325
            Left            =   0
            TabIndex        =   165
            Top             =   420
            Width           =   3405
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   6
               Left            =   930
               TabIndex        =   512
               Tag             =   "fixed"
               Top             =   1890
               Width           =   1965
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   5
               Left            =   930
               TabIndex        =   196
               Tag             =   "fixed"
               Top             =   1560
               Width           =   1965
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   0
               Left            =   930
               Locked          =   -1  'True
               TabIndex        =   172
               Tag             =   "fixed"
               Top             =   210
               Width           =   675
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   1
               Left            =   2160
               Locked          =   -1  'True
               TabIndex        =   171
               Tag             =   "fixed"
               Top             =   210
               Width           =   735
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   2
               Left            =   930
               Locked          =   -1  'True
               TabIndex        =   170
               Tag             =   "fixed"
               Top             =   555
               Width           =   1965
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   3
               Left            =   930
               Locked          =   -1  'True
               TabIndex        =   169
               Tag             =   "fixed"
               Top             =   885
               Width           =   1965
            End
            Begin VB.TextBox Text1 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   330
               Index           =   4
               Left            =   930
               Locked          =   -1  'True
               TabIndex        =   168
               Tag             =   "fixed"
               Top             =   1230
               Width           =   1965
            End
            Begin VB.CommandButton CmdDelVirt 
               Height          =   330
               Left            =   3000
               Picture         =   "MadmdF_EditDli.frx":4C88
               Style           =   1  'Graphical
               TabIndex        =   167
               Tag             =   "fixed"
               ToolTipText     =   "Delete Virtual segment"
               Top             =   210
               Width           =   330
            End
            Begin VB.CommandButton CmdInsVirt 
               Height          =   330
               Left            =   3000
               Picture         =   "MadmdF_EditDli.frx":4DD2
               Style           =   1  'Graphical
               TabIndex        =   166
               Tag             =   "fixed"
               ToolTipText     =   "New Virtual segment"
               Top             =   600
               Width           =   330
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "Condition :"
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   7
               Left            =   120
               TabIndex        =   513
               Tag             =   "fixed"
               ToolTipText     =   "Template name for ""Routines condition"""
               Top             =   1950
               Width           =   750
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "TB name :"
               ForeColor       =   &H00C00000&
               Height          =   255
               Index           =   6
               Left            =   120
               TabIndex        =   197
               Tag             =   "fixed"
               ToolTipText     =   "RDBMS Table Name "
               Top             =   1620
               Width           =   735
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "Len max. :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   0
               Left            =   90
               TabIndex        =   177
               Tag             =   "fixed"
               Top             =   240
               Width           =   765
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   " min. :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   1
               Left            =   1680
               TabIndex        =   176
               Tag             =   "fixed"
               Top             =   240
               Width           =   420
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "Comprtn :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   2
               Left            =   135
               TabIndex        =   175
               Tag             =   "fixed"
               Top             =   570
               Width           =   720
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "Rules :"
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   3
               Left            =   330
               TabIndex        =   174
               Tag             =   "fixed"
               Top             =   915
               Width           =   495
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackColor       =   &H8000000E&
               BackStyle       =   0  'Transparent
               Caption         =   "Pointer :"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   5
               Left            =   210
               TabIndex        =   173
               Tag             =   "fixed"
               Top             =   1260
               Width           =   645
            End
         End
         Begin VB.CheckBox Check1 
            BackColor       =   &H8000000C&
            Caption         =   "Excluded Segment"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000E&
            Height          =   240
            Left            =   60
            TabIndex        =   164
            Top             =   60
            Width           =   2085
         End
         Begin MSComctlLib.ListView ListCampi 
            Height          =   1845
            Left            =   3480
            TabIndex        =   520
            Top             =   1740
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   3254
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            HoverSelection  =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Livello"
               Text            =   "Liv."
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "NCampo"
               Text            =   "Nome campo"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "Bytes"
               Text            =   "Bytes"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "Lunghezza"
               Text            =   "Len"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "Posizione"
               Text            =   "Start"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Key             =   "Tipo"
               Text            =   "T"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Key             =   "Occurs"
               Text            =   "Occ."
               Object.Width           =   1058
            EndProperty
         End
         Begin MSComctlLib.ListView ListStr 
            Height          =   1155
            Left            =   3495
            TabIndex        =   521
            Top             =   240
            Width           =   4995
            _ExtentX        =   8811
            _ExtentY        =   2037
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            HoverSelection  =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Id"
               Text            =   "Id"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "Struttura"
               Text            =   "Struttura"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Campo"
               Object.Width           =   2646
            EndProperty
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Area Details"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   225
            Left            =   3540
            TabIndex        =   528
            Top             =   1500
            Width           =   5295
         End
         Begin VB.Label Label26 
            BackStyle       =   0  'Transparent
            Caption         =   "Used Cobol Areas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   165
            Left            =   3510
            TabIndex        =   522
            Top             =   0
            Width           =   1755
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   690
      Top             =   7350
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":4F1C
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":5076
            Key             =   "Exit"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":51D0
            Key             =   "Avvio"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":54EA
            Key             =   "Incrementa"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":5644
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":579E
            Key             =   "Restore"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":5BF0
            Key             =   "Cpy"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   90
      Top             =   7350
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":5D4A
            Key             =   "SegVero"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":61A2
            Key             =   "SegVirt"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":65FA
            Key             =   "ElemOk"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":6A4E
            Key             =   "ElemKO"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":6EA2
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":72F6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":7756
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":7A7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":7ED6
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":8032
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":818E
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_EditDli.frx":85E0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RtbFile 
      Height          =   645
      Left            =   2430
      TabIndex        =   162
      Top             =   6870
      Visible         =   0   'False
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   1138
      _Version        =   393217
      Enabled         =   -1  'True
      RightMargin     =   80000
      TextRTF         =   $"MadmdF_EditDli.frx":873A
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   6120
      Left            =   60
      TabIndex        =   514
      Top             =   720
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   10795
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label27 
      Alignment       =   1  'Right Justify
      Caption         =   "Occurs"
      ForeColor       =   &H8000000D&
      Height          =   225
      Left            =   0
      TabIndex        =   529
      Top             =   0
      Width           =   675
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "DB Structure"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   60
      TabIndex        =   515
      Top             =   420
      Width           =   2595
   End
   Begin VB.Label TitSeg 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Segment :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   2670
      TabIndex        =   163
      Top             =   420
      Width           =   9135
   End
   Begin VB.Menu Mnu_Key 
      Caption         =   "Key"
      Visible         =   0   'False
      Begin VB.Menu Mnu_PrimaryKey 
         Caption         =   "PrimaryKey"
      End
      Begin VB.Menu Mnu_SelectedKey 
         Caption         =   "SelectedKey"
      End
   End
End
Attribute VB_Name = "MadmdF_EditDli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'ALE
Public indsegSelected As Long
'SQ:
Const PRIMARY_ICON = 12
Const NORMAL_ICON = 3
Const CORRECT_ICON = 4

Dim MAX As Integer
Dim charris() As String
'Dim gFiller() As Filler
Dim NodoClick As String
Dim NodoIndex As Integer
Dim ContrFiller As Boolean
Dim DMswitch As Boolean, Esame As Boolean, NuovaPag As Boolean
Dim BottonePremuto As Integer, risp As Integer
Dim nodX As Object
Dim Index As Integer
Dim NomeSeg As String
Dim valore As Integer
Dim FrmCaption As String

'  presi da editAreaDli
Dim SwMod As Boolean
Dim IndSel As Integer, IndSelC As Integer

'Variabile per definire lo stato di caricamento dei dati
Public StatusOK As Boolean

Private Type tDL1_PgmIndex_Type2
  IdOggetto As Long
  Nome As String
  IdField As Long
  NomeField As String
  IdSegmento As Long
  nomeSegmento As String
  nomeIndice As String
  KeyIndice As String
End Type

Public lFlag As Boolean
Dim Parola As String
Dim Idxpos As Long


Private Sub Check1_Click()
  ' **************************************
  ' * ROUTINE PER IGNORARARE UN SEGMENTO *
  ' **************************************
  Dim indseg As Long
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo And Check1.Value = 1 Then
      gSegm(indseg).Correzione = "O"
      CheckSegNT False
      Exit Sub
    ElseIf gSegm(indseg).Name = Nomenodo And Check1.Value = 0 Then
      gSegm(indseg).Correzione = ""
      CheckSegNT True
      TreeView1_NodeClick TreeView1.Nodes(TreeView1.SelectedItem.Index)
      Exit Sub
    End If
  Next indseg
  CaricaSeg indseg
End Sub

Private Sub ChkFiller_Click()
  If ChkFiller = vbChecked Then
    ContrFiller = True
  Else
    ContrFiller = False
  End If
  ControllaFiller
End Sub

Private Sub ChkRedefines_Click()
  Dim indseg As Long
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo And ChkRedefines.Value = 1 Then
      gSegm(indseg).Redefines = True
      Exit Sub
    ElseIf gSegm(indseg).Name = Nomenodo And ChkRedefines.Value = 0 Then
      gSegm(indseg).Redefines = False
      Exit Sub
    End If
  Next indseg
  CaricaSeg indseg
End Sub

Private Sub ClearSign_Click()
  RichTextBox1.text = ""
End Sub

Private Sub Cmd_Click()
  Dim indseg As Integer
  Dim indStr As Integer
  Dim App As String
  Dim i As Integer
  Dim esci As Boolean
  
  ' ***************************************************
  ' * ROUTINE PER ASSOCIARE\DISASSOCIARE LE STRUTTURE *
  ' ***************************************************
  Modificato = True
  If ListCampi.ListItems.count <> 0 Then
    If ListStr.ListItems.count <> 0 Then
      If ListStr.SelectedItem.SmallIcon <> 12 Then
        If ListStr.SelectedItem.SmallIcon = 3 Or ListStr.SelectedItem.SmallIcon = 12 Then
          ListStr.SelectedItem.SmallIcon = 4
          esci = False
          For i = 1 To ListStr.ListItems.count
            If ListStr.ListItems(i).SmallIcon <> 4 Then
              esci = False
              Exit For
            Else
              esci = True
            End If
          Next i
          If esci Then
            MsgBox "You can't delete all the areas!!!", vbInformation + vbOKOnly, DataMan.DmNomeProdotto
            ListStr.SelectedItem.SmallIcon = 3
            Exit Sub
          End If
        Else
          ListStr.SelectedItem.SmallIcon = 3
        End If
        
        For indseg = 1 To UBound(gSegm)
          If gSegm(indseg).Name = Nomenodo Then Exit For
        Next indseg
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          App = ListStr.SelectedItem.text
          wIdOggetto = Mid$(App, 1, InStr(App, "/") - 1)
          wIdArea = Mid$(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))
          If gSegm(indseg).Strutture(indStr).StrIdOggetto = wIdOggetto And gSegm(indseg).Strutture(indStr).StrIdArea = wIdArea And gSegm(indseg).Strutture(indStr).nomeStruttura = ListStr.SelectedItem.SubItems(1) Then
            If ListStr.SelectedItem.SmallIcon = 3 Then
              gSegm(indseg).Strutture(indStr).Correzione = ""
              Exit For
            Else
              gSegm(indseg).Strutture(indStr).Correzione = "C"
              Exit For
            End If
          End If
        Next indStr
        ConfrontaAreeCobol
      End If
    End If
  Else
    MsgBox "Select an area!!!", vbInformation + vbOKOnly, DataMan.DmNomeProdotto
  End If
End Sub

Private Sub cmdAddArea_Click()
  Dim nodoSel As MSComctlLib.Node
  
  Set nodoSel = TreeView1.SelectedItem
  MadmdF_AddArea.Show vbModal

  nodoSel.Selected = True
  nodoSel.Expanded = True

  TreeView1.SetFocus
  TreeView1_NodeClick nodoSel
  
  ListStr.Refresh

  'SetParent MadmdF_AddArea.hwnd, formParentResize.hwnd
  'MadmdF_AddArea.Show
  'MadmdF_AddArea.Move 0, 0, formParentResize.Width, formParentResize.Height
End Sub

Public Sub areaAssociata(boolPrimaria As Boolean, boolPrimariaSelected As Boolean)
  Dim ids() As String
  Dim NomeCampo As String, IdOggetto As String, idArea As String, nomeArea As String
  Dim i As Long
  Dim rsPs As New Recordset
  Dim rsC As New Recordset
  
  'indexseg 'indice del seg selezionato
  ids() = Split(ListStr.SelectedItem, "/")
  IdOggetto = ids(0)
  idArea = ids(1)
  NomeCampo = ListStr.SelectedItem.SubItems(2)
  nomeArea = ListStr.SelectedItem.SubItems(1)
  
  If boolPrimaria And boolPrimariaSelected Then
    'metto il baffo e tolgo i dati dalle tabelle MgRel_SegAree, Mgdata_Area, MgData_Cmp
    ListStr.SelectedItem.SmallIcon = 3
    DataMan.DmConnection.Execute ("delete * from MgRel_SegAree where idSegmento = " & indexseg & " AND StrIdOggetto =  " & IdOggetto & " AND StrIdArea = " & idArea)
    DataMan.DmConnection.Execute ("delete * from MgData_Area where idSegmento = " & indexseg)
    DataMan.DmConnection.Execute ("delete * from MgData_Cmp where idSegmento = " & indexseg)
  ElseIf boolPrimaria And Not boolPrimariaSelected Then
    MsgBox "An existing structure is already associated as primary Area", vbInformation, "i-4.Migration"
  ElseIf Not boolPrimaria Then
    'metto il fulminello e metto i dati nelle tabelle MgRel_SegAree, Mgdata_Area, MgData_Cmp
    ListStr.SelectedItem.SmallIcon = 12
    Set rsPs = m_fun.Open_Recordset("select * from MgRel_SegAree where idSegmento = " & indexseg & " AND StrIdOggetto =  " & IdOggetto & " AND StrIdArea = " & idArea)
    If rsPs.RecordCount > 0 Then DataMan.DmConnection.Execute ("delete * from MgRel_SegAree where idSegmento = " & indexseg & " AND StrIdOggetto =  " & IdOggetto & " AND StrIdArea = " & idArea)
    rsPs.AddNew
    rsPs!IdSegmento = indexseg
    rsPs!StrIdOggetto = IdOggetto
    rsPs!StrIdArea = idArea
    rsPs!NomeCmp = nomeArea
    rsPs!Correzione = "P"
    rsPs.Update
    rsPs.Close
    
    Set rsPs = m_fun.Open_Recordset("select * from PsData_Area where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea)
    Set rsC = m_fun.Open_Recordset("select * from MgData_Area where idSegmento = " & indexseg)
    If rsC.RecordCount > 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Area where idSegmento = " & indexseg)
    Do Until rsPs.EOF
      rsC.AddNew
      rsC!IdSegmento = indexseg
      rsC!Nome = rsPs!Nome
      rsC!livello = rsPs!livello
      rsC!NomeRed = rsPs!NomeRed
      rsC!Correzione = " "
      rsC.Update
      rsPs.MoveNext
    Loop
    rsC.Close
    rsPs.Close
    i = 0
    Set rsPs = m_fun.Open_Recordset("select * from PsData_Cmp where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea & " and livello <> 88")
    Set rsC = m_fun.Open_Recordset("select * from MgData_Cmp where idSegmento = " & indexseg)
    If rsC.RecordCount > 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Cmp where idSegmento = " & indexseg)
    Do Until rsPs.EOF
      i = i + 1
      rsC.AddNew
      rsC!IdSegmento = indexseg
      rsC!IdCmp = i
      rsC!Nome = rsPs!Nome
      rsC!Ordinale = rsPs!Ordinale
      rsC!livello = rsPs!livello
      rsC!Tipo = rsPs!Tipo
      rsC!Posizione = rsPs!Posizione
      rsC!Byte = rsPs!Byte
      rsC!lunghezza = rsPs!lunghezza
      rsC!Decimali = rsPs!Decimali
      rsC!segno = rsPs!segno
      rsC!Usage = rsPs!Usage
      rsC!Picture = rsPs!Picture
      rsC!Occurs = rsPs!Occurs
      rsC!NomeRed = rsPs!NomeRed
      rsC!Selettore = False
      rsC!Selettore_Valore = ""
      rsC!Correzione = " "
      ' Mauro 15/01/2008
      rsC!OccursFlat = False
      rsC!DaMigrare = True
      rsC.Update
      rsPs.MoveNext
    Loop
    rsC.Close
    rsPs.Close
  End If
  
  MadmdM_DLI_Dati.CaricaStruttura
End Sub
'''''''''''''''''''''''''
'SQ-4: rifatta
'''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''
' SQ: RIFATTA...
'''''''''''''''''''''''''''''''''''''''
Private Sub CmdAreaPrim_Click()
  
  Dim i As Long
  Dim boolPrimaria As Boolean
  Dim boolPrimariaSelected As Boolean
  
  If ListStr.ListItems.count > 0 Then
    For i = 1 To ListStr.ListItems.count
      If ListStr.ListItems.Item(i).SmallIcon = 12 Then
        boolPrimaria = True
        If ListStr.SelectedItem.Index = i Then
          boolPrimariaSelected = True
        Else
          boolPrimariaSelected = False
        End If
        Exit For
      Else
        boolPrimaria = False
      End If
    Next
    
    areaAssociata boolPrimaria, boolPrimariaSelected
  
  End If
  
'  Dim indseg As Double
'  Dim indstr As Double
'  Dim i As Double
'  Dim App As String
'  Dim AppSelect As String
'  Dim Appindseg As Double
'  Dim Appindstr As Double
'
'
'  Modificato = True  'SQ: CHI L'HA DETTO?
'  'CERCO L'INDICE DELLA STRUTTURA: (non ce l'ho gia?)
'  For indseg = 1 To UBound(gSegm)
'    If gSegm(indseg).Name = Nomenodo Then
'      Appindseg = indseg
'      Exit For
'    End If
'  Next
'
''  Dim tbverifica As Recordset
''  Set tbverifica = m_fun.Open_Recordset("select * from MgRel_SegAree where idsegmento = " & gSegm(indseg).IdSegm & " and correzione = 'P' ")
''  If tbverifica.RecordCount > 0 Then
''    MsgBox "A Structure is already associated"
''    Exit Sub
''  End If
'
'  'SQ: CERCO INDICE DEL SEGMENTO ATTIVO (COME FACCIO A NON AVERLO GIA'?)
'  '    NON HO INDEXSEG?!
'  'CERCO L'EVENTUALE STRUTTURA ATTIVA
'  For indstr = 2 To UBound(gSegm(indseg).Strutture)
'    If gSegm(indseg).Strutture(indstr).Primario Then
'      If Trim(gSegm(indseg).Strutture(indstr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indstr).StrIdArea) = Trim(ListStr.SelectedItem.text) Then
'         Appindstr = indstr
'         'I RECORD SELEZIONATO SIA GIA IL PRIMARIO
'         'SQ: NON E' "PRIMARY"?
'         If MsgBox("Delete Reference Area?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
'            gSegm(Appindseg).Strutture(Appindstr).Primario = False
'            gSegm(Appindseg).Strutture(Appindstr).Correzione = ""
'            ListStr.SelectedItem.SmallIcon = 3
'         Else
'            MsgBox "Reference Area manteined", vbOKOnly, DataMan.DmNomeProdotto
'         End If
'         Exit For
'      Else
'        Appindstr = indstr
'        'ESISTE GIA UN PRIMARIO E LO SI VUOLE SOVRASCRIVERE
'        'SQ: e' un po' piu' chiaro? - Controllare pero' l'inglese!
'        'If MsgBox("A ""Primary Area"" already exits. Do you want to change it?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
'            'RIPRISTINO IL VECCHIO PRIMARIO
'            '''''''''''gSegm(Appindseg).Strutture(Appindstr).Primario = False
'            '''''''''''gSegm(Appindseg).Strutture(Appindstr).Correzione = ""
'            'MODIFICO L'ICONA AL NUOVO (MA LA MODIFICA ANCHE SOTTO, DIVERSAMENTE)?
'            ListStr.SelectedItem.SmallIcon = 3
'            'CERCO LA STRUTTURA DEL NUOVO: non posso tenere la correlazione fra
'            'indice della listview da quello della struttura gSegm?????
'            For i = 2 To UBound(gSegm(Appindseg).Strutture)
'               App = ListStr.SelectedItem.text
'               wIdOggetto = Mid$(App, 1, InStr(App, "/") - 1)
'               wIdArea = Mid$(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))
'               If gSegm(Appindseg).Strutture(i).StrIdOggetto = wIdOggetto And gSegm(Appindseg).Strutture(i).StrIdArea = wIdArea And gSegm(Appindseg).Strutture(i).nomeStruttura = ListStr.SelectedItem.SubItems(1) Then
'                  If gSegm(Appindseg).Strutture(i).Correzione <> "C" Then
'                     If gSegm(Appindseg).Strutture(i).Primario = False Then  'SE TUTTO CORRETTO QUESTA E' SEMPRE VERIFICATA
'                        gSegm(Appindseg).Strutture(i).Primario = True
'                        'RIPRISTINO L'ICONA!
'                        ListStr.SelectedItem.SmallIcon = NORMAL_ICON 'SQ
'         '                Stop
'                        Area_Primaria indseg, i
'                        ' routine che inserisce nella struttura 1 i CAMPI
'                        Exit For
'                     End If
'                  End If
'               End If
'            Next i
'        'End If
'        Exit For
'      End If
'    Else
'      'NON CI SONO PRIMARI ESISTENTI
'      'SQ: NON SI CHIAMA "PRIMARY"? - QUALI SONO I PRECEDENTI SETTINGS?
'      If MsgBox("If you set this area as Reference area, you lost all previous settings, Continue?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
'        'scrivere che � l'area primaria
'        If ListCampi.ListItems.count <> 0 Then
'          If ListStr.SelectedItem.text <> "" Then
'            'SQ: CERCO ANCORA?
'            For indseg = 1 To UBound(gSegm)
'              If gSegm(indseg).Name = Nomenodo Then Exit For
'            Next indseg
'            For i = 2 To UBound(gSegm(indseg).Strutture)
'              App = ListStr.SelectedItem.text
'              wIdOggetto = Mid$(App, 1, InStr(App, "/") - 1)
'              wIdArea = Mid$(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))
'              If gSegm(indseg).Strutture(i).StrIdOggetto = wIdOggetto And gSegm(indseg).Strutture(i).StrIdArea = wIdArea And gSegm(indseg).Strutture(i).nomeStruttura = ListStr.SelectedItem.SubItems(1) Then
'                'CORREZIONE - PERCHE' NON MODIFICO IN QUESTO CASO?
'                If gSegm(indseg).Strutture(i).Correzione <> "C" Then
'                  gSegm(indseg).Strutture(i).Primario = True
'                  ListStr.SelectedItem.SmallIcon = 12
'                  'SQ: ATTENZIONE!!!!!!!!!!!!!!!
'                  '    ERA SOTTO...
'                  'COPIA nella struttura 1 i CAMPI... (A COSA SERVE?)
'                  Area_Primaria indseg, i
'                  Exit For
'                End If
'              End If
'            Next i
'            'SQ: ATTENZIONE!!!!!!!!!!!!!!!
'            '    PORTATA SOPRA!!!!!!!!!!!!
'            ' routine che inserisce nella struttura 1 i CAMPI
'            'Area_Primaria indseg, indstr
'          End If
'        End If
'      End If
'
'      Exit For
'    End If
'  Next indstr
'
'  AggiornaSpecField
'
End Sub
'Private Sub CmdAreaPrim_Click()
'  Dim indseg As Double
'  Dim indstr As Double
'  Dim App As String
'  Dim AppSelect As String
'  Dim wResp As Integer
'  Dim bool As Integer
'  Dim Appindseg As Double
'  Dim Appindstr As Double
'
'  Modificato = True
'  bool = 1
''...
'
'  Select Case bool
'  Case 3 'CASO IN CUI I RECORD SELEZIONATO SIA GIA IL PRIMARIO
'    wResp = MsgBox("Delete Reference Area?", vbYesNo, DataMan.DmNomeProdotto)
' '...
'End Sub

Private Sub cmdAutomAssArea_Click()

   importAreaSegments

End Sub

Private Sub CmdDelVirt_Click()
  Dim tbSegVirt As Recordset
  Dim indseg As Long
  Dim k As Integer
  Dim SQL As String
  
  ' ROUTINE PER CANCELLARE UN SEGMENTO VIRTUALE
  EliminaRelazioni SegAssApp() ' Cancella le relazioni create per quel segmento
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      gSegm(indseg).Correzione = "C"
      For k = 1 To TreeView1.Nodes.count
        If Nomenodo = Trim(UCase(TreeView1.Nodes(k).Key)) Then
          TreeView1.Nodes.Remove k
          k = k - 1
          Nomenodo = Trim(UCase(TreeView1.Nodes(k).Key))
          TitSeg.Caption = UCase("Segment : ") & Nomenodo
          CaricaSeg indseg
          CaricaField indseg
          CaricaStr ListStr, ImageList1, Nomenodo
          CaricaCampi indseg
          TreeView1_NodeClick TreeView1.Nodes(1)
          Exit Sub
        End If
      Next k
    End If
  Next indseg
End Sub

Public Sub EsaminaField_Check()
  Dim indseg As Integer
  Dim IndFld As Integer
  Dim indCmp As Integer
  Dim indStr As Integer
  Dim i As Integer
  Dim App As String
  
  ' ******************************************************************************* '
  ' *      ROUTINE PER IL CONTROLLO DEGLI ERRORI SUL CAMPO SELEZIONATO TAB2       * '
  ' ******************************************************************************* '
  
  'RichTextBox2.text = ""
  'Stop
  On Error GoTo ERR:
  If ListField2.ListItems.count = 0 Then Exit Sub
  For indseg = 1 To UBound(gSegm)
    With gSegm(indseg)
      If .Name = Nomenodo Then
        For IndFld = 1 To UBound(.gField)
          If .gField(IndFld).Name = ListField2.SelectedItem.text Then
            For i = .gField(IndFld).Start To (.gField(IndFld).MaxLen + .gField(IndFld).Start) - 1
              If IsEmpty(charris) = True Then
                If charris(i) = "-" Then
                  AggiungiTB "DMW0001", "The field is declared in different types (X-9)", RichTextBox1
                  Exit For
                End If
              End If
            Next i
            Exit For
          End If
        Next IndFld
      End If
      Exit For
    End With
  Next indseg
  If ListStr2.ListItems.count = 0 Then Exit Sub
  For indseg = 1 To UBound(gSegm)
    With gSegm(indseg)
      ' Mauro 27/08/2007 : Controllo se il segmento � obsoleto
      'If .Name = Nomenodo Then
      If .Name = Nomenodo And .Correzione <> "O" Then
        For IndFld = 1 To UBound(.gField)
          If .gField(IndFld).Name = ListField2.SelectedItem.text Then
            For indStr = 2 To UBound(.Strutture)
              'If .Strutture(indstr).nomeStruttura = ListStr2.SelectedItem.SubItems(1) And gSegm(indseg).Strutture(indstr).IdStruttura = val(ListStr2.SelectedItem.text) Then
              App = ListStr2.SelectedItem.text
              wIdOggetto = Mid$(App, 1, InStr(App, "/") - 1)
              wIdArea = Mid$(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))

              If .Strutture(indStr).nomeStruttura = ListStr2.SelectedItem.SubItems(1) And gSegm(indseg).Strutture(indStr).StrIdArea = val(wIdArea) And gSegm(indseg).Strutture(indStr).StrIdOggetto = val(wIdOggetto) Then
                For indCmp = 1 To UBound(.Strutture(indStr).Campi)
                  If .Strutture(indStr).Campi(indCmp).Correzione <> "C" Then
                    If val(.Strutture(indStr).Campi(indCmp).Posizione) < val(.gField(IndFld).Start) And _
                       val(.Strutture(indStr).Campi(indCmp).bytes) + val(.Strutture(indStr).Campi(indCmp).Posizione) > val(.gField(IndFld).Start) And _
                       val(.Strutture(indStr).Campi(indCmp).lunghezza) <> 0 Then
                      AggiungiTB "DMW0002", "The selected structure exceed the length (left)", RichTextBox1
                      Exit Sub
                    End If
                    If val(.Strutture(indStr).Campi(indCmp).Posizione) < val(.gField(IndFld).Start) + val(.gField(IndFld).MaxLen) And _
                       (val(.Strutture(indStr).Campi(indCmp).bytes) + val(.Strutture(indStr).Campi(indCmp).Posizione)) > val(.gField(IndFld).Start) + val(.gField(IndFld).MaxLen) And _
                       val(.Strutture(indStr).Campi(indCmp).lunghezza) <> 0 Then
                      AggiungiTB "DMW0002", "The selected structure exceed the length (right)", RichTextBox1
                      Exit Sub
                    End If
                  End If
                Next indCmp
              End If
            Next indStr
          End If
        Next IndFld
      End If
    End With
  Next indseg
  
  Exit Sub
ERR:
  If ERR.Number = 9 Then
    AggiungiTB "DMW0002", "There is no structure associated", RichTextBox1
  End If
End Sub

Public Sub EsaminaStruttura_Check()
  ' ********************************************************************
  ' *     ROUTINE PER IL CONTROLLO DEGLI ERRORI SULLLA STRUTTURA       *
  ' ********************************************************************
  RichTextBox1.text = ""
  CheckConfini ' Controllo lunghezza struttura
  ControllaChiaveField ' Controllo chiavi della struttura
End Sub

Public Sub EsaminaSeg_Check()
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim controllo As Integer
  Dim Lungh As Integer
  Dim i As Integer

  ' *****************************************************************
  ' * ROUTINE PER IL CONTROLLO DEGLI ERRORI SUL SEGMENTO NELLA TAB1 *
  ' *****************************************************************
  On Error Resume Next
  'RichTextBox1.text = ""
  controllo = 0
  For indseg = 1 To UBound(gSegm)
    With gSegm(indseg)
    If Trim(UCase(.Name)) = Trim(UCase(Nomenodo)) Then
      For indStr = 2 To UBound(.Strutture)
        If .Strutture(indStr).Correzione = "" Then
          Lungh = .Strutture(indStr).Campi(1).bytes
          For i = (indStr + 1) To UBound(.Strutture)
            If .Strutture(i).Correzione = "" And .Strutture(i).Campi(1).bytes <> Lungh Then
              AggiungiTB "DMW0001", "There are different structures associated to the segment: " & Nomenodo, RichTextBox1
'              CheckSeg(1).Value = 1
              Exit Sub
            End If
          Next i
        End If
      Next indStr
    End If
    End With
  Next indseg
  On Error Resume Next
  
  If IsEmpty(charris) = True Then
    For i = 1 To UBound(charris)
      If valore <> 0 Then
        If charris(i) = "-" And RichTextBox1.text = "" Then
          AggiungiTB "DMW0001", "There are different structures associated to the segment: " & Nomenodo, RichTextBox1
'          CheckSeg(1).Value = 1
          Exit Sub
        End If
      End If
    Next i
  End If
      
'  CheckSeg(1).Value = 0
  For indseg = 1 To UBound(gSegm)
    With gSegm(indseg)
    If .Name = Nomenodo Then
      For indStr = 2 To UBound(.Strutture)
        If .Strutture(indStr).Correzione = "" Then
          controllo = 1
        End If
      Next indStr
    End If
    End With
  Next indseg
  ControllaSovrapposizioneCampi
End Sub


Private Sub CmdFldOk_Click()
  Dim wLen As Long
  If IndSel = 0 Or IndSel > LstFldEdit.ListItems.count Then Exit Sub
  
  LstFldEdit.ListItems(IndSel).text = TxtLiv
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_NOME) = UCase(TxtNome)
  'Byte
  If txtLen > 0 Then
    wLen = val(txtLen) + val(TxTDec)
    If InStr(CmbType.text, "PCK") > 0 Then
      wLen = Int((wLen) / 2) + 1
    End If
    If InStr(CmbType.text, "BNR") > 0 Then
      wLen = 2
      If txtLen > 4 Then wLen = 4
      If txtLen > 8 Then wLen = 8
    End If
    LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_BYTE) = wLen
  End If
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_LENGTH) = txtLen
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_DEC) = TxTDec
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_TYPE) = UCase(CmbType.text)
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_OCCURS) = TxtOccurs
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_NOMERED) = UCase(TxtNomeRed)
  ' Mauro 15/01/2008
  If TxtOccurs = "" Or TxtOccurs = "0" Then
    LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_OCCURSFLAT) = ""
  Else
    LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_OCCURSFLAT) = IIf(chkOccursFlat, "YES", "NO")
  End If
  LstFldEdit.ListItems(IndSel).SubItems(LstFldEdit_DAMIGR) = IIf(chkDaMigrare, "YES", "NO")
  
  TxtLiv = ""
  TxtNome = ""
  txtLen = ""
  TxTDec = ""
  CmbType = ""
  TxtOccurs = ""
  TxtNomeRed = ""
  ' Mauro 15/01/2008
  chkOccursFlat = 0
  chkDaMigrare = 0

  SwMod = True
  LblModif = "Area modified"

'  Case "Check"
  SwMod = True
  LblModif = "Area modified"
  VerificaArea
  AggiornaDmAreaSeg
  SwMod = False
  LblModif = "Area saved"
   
End Sub

Private Sub CmdImpostaChiave_Click()
  Modificato = True
  ImpostaChiave ListField, ImageList1, Nomenodo ' Aggiorna la listview e il campo index della struttura gsegm...
  AggiornaSpecField ' Aggiorna i valori del Field
End Sub

Private Sub cmdLoadIdx2PgmDli_Click()
  If chkDbdSelected.Value = 1 Then
    Load_PgmWithDLIAccess_IdxSecondary CLng(pIndexDBSelected)
  Else
    Load_PgmWithDLIAccess_IdxSecondary
  End If
End Sub

Private Sub cmdRemoveArea_Click()
  Dim indseg As Integer
  
  If ListStr.ListItems.count Then
    If MsgBox("Do you want remove area '" & Trim(ListStr.SelectedItem.ListSubItems(2)) & "'?", vbYesNo + vbQuestion, "Remove Area") = vbYes Then
      Dim IdOggetto As String, idArea As String
      IdOggetto = Left(ListStr.SelectedItem, InStr(ListStr.SelectedItem, "/") - 1)
      idArea = Mid(ListStr.SelectedItem, InStr(ListStr.SelectedItem, "/") + 1, Len(ListStr.SelectedItem) - InStr(ListStr.SelectedItem, "/"))

      removeArea IdOggetto, idArea
      MadmdM_DLI_Dati.CaricaStruttura
    End If
  End If
End Sub

Public Sub removeGsegArea(IdOggetto As String, idArea As String)

Dim seg As Long
seg = indsegSelected

Dim indStr As Integer
Dim Newindstr As Integer
Dim AppgSegm() As wSegm
Dim i As Long

  ListCampi.ListItems.Clear
  ListStr.ListItems.Remove (ListStr.SelectedItem.Index)
  
  AppgSegm = gSegm
  ReDim AppgSegm(seg).Strutture(1)
  ReDim AppgSegm(seg).Strutture(0).Campi(0)
  ReDim AppgSegm(seg).Strutture(1).Campi(0)
  i = 1
  For indStr = 2 To UBound(gSegm(seg).Strutture) '2
    If gSegm(seg).Strutture(indStr).StrIdArea = idArea And gSegm(seg).Strutture(indStr).StrIdOggetto = IdOggetto Then
      'salta l'area cancellata
      Stop
    Else
      i = i + 1
      ReDim Preserve AppgSegm(seg).Strutture(i)
      AppgSegm(seg).Strutture(i) = gSegm(seg).Strutture(indStr)
    End If
  Next indStr
  gSegm = AppgSegm

End Sub



Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   Dim wScelta As Long
   
   If m_fun.FnProcessRunning Then
      wScelta = m_fun.Show_MsgBoxError("FB01I")
      
      If wScelta = vbNo Then
         Cancel = 0 'No unload
         m_fun.FnStopProcess = False
      Else
         Cancel = 1 'Unload
         m_fun.FnStopProcess = True
      End If
   End If
End Sub

Private Sub Form_Resize()
  Elimina_Flickering_Object Me.hwnd
  ResizeForm Me
  If Me.TreeView1.Nodes.count Then
    Me.TreeView1.Nodes(1).Expanded = True
  End If
  Terminate_Flickering_Object
End Sub

Private Sub LstFldEdit_DblClick()
  IndSel = LstFldEdit.SelectedItem.Index
  TxtLiv = LstFldEdit.SelectedItem.text
  TxtNome = LstFldEdit.SelectedItem.SubItems(LstFldEdit_NOME)
  txtLen = LstFldEdit.SelectedItem.SubItems(LstFldEdit_LENGTH)
  TxTDec = LstFldEdit.SelectedItem.SubItems(LstFldEdit_DEC)
  TxtOccurs = LstFldEdit.SelectedItem.SubItems(LstFldEdit_OCCURS)
  TxtNomeRed = LstFldEdit.SelectedItem.SubItems(LstFldEdit_NOMERED)
  CmbType.text = LstFldEdit.SelectedItem.SubItems(LstFldEdit_TYPE)
'  If TxtOccurs = 0 Then
'    chkOccursFlat = 0
'    chkOccursFlat.Enabled = False
'  Else
    chkOccursFlat = IIf(LstFldEdit.SelectedItem.SubItems(LstFldEdit_OCCURSFLAT) = "YES", 1, 0)
'  End If
  chkDaMigrare = IIf(LstFldEdit.SelectedItem.SubItems(LstFldEdit_DAMIGR) = "YES", 1, 0)
  FrmSelValue.Enabled = False
  If LstFldEdit.SelectedItem.ListSubItems(LstFldEdit_NOME).ForeColor = vbRed Then
    FrmSelValue.Enabled = True
  End If
End Sub

Private Sub LstFldEdit_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = 2 Then
    PopupMenu Mnu_Key
  End If
End Sub


Private Sub lsw2ndIdxAccess_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
       'ordina la lista
   Dim Key As Long
   Static Order As String
  
  Order = lsw2ndIdxAccess.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lsw2ndIdxAccess.SortOrder = Order
  lsw2ndIdxAccess.SortKey = Key - 1
  lsw2ndIdxAccess.Sorted = True
End Sub

Private Sub Mnu_PrimaryKey_Click()
  Dim k As Integer

  For k = 1 To LstFldEdit.ListItems.count
    LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).Bold = False
    If LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor <> vbRed Then
      LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor = vbBlack
    End If
    If LstFldEdit.ListItems(k).Selected = True Then
      LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).Bold = True
      If LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor <> vbRed Then
        LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor = vbBlue
      End If
    End If
  Next k
End Sub

Private Sub Mnu_SelectedKey_Click()
  Dim k As Integer
  
  For k = 1 To LstFldEdit.ListItems.count
    If LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).Bold = True Then
       LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor = vbBlue
    Else
       LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor = vbBlack
    End If
    If LstFldEdit.ListItems(k).Selected = True Then
      LstFldEdit.ListItems(k).ListSubItems(LstFldEdit_NOME).ForeColor = vbRed
    End If
  Next k
  LstFldEdit.Refresh

End Sub


Private Sub Text1_Change(Index As Integer)
Dim indseg As Integer
If Index <> 6 Then Exit Sub
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
       gSegm(indseg).TbName = Text1(5)
       gSegm(indseg).Condizione = Text1(6)
       Exit For
    End If
  Next indseg
  
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim wResp As Variant
  Dim k As Integer
  Dim IndSel As Integer

  On Error Resume Next
  Select Case Button.Key
    Case "New"
      SwMod = True
      InserisciCampoEdit
      LblModif = "Area modified"
    Case "Up"
      SpostaCampo Button.Key
    Case "Down"
      SpostaCampo Button.Key
    Case "Copy"
      IndSelC = LstFldEdit.SelectedItem.Index
    Case "Paste"
      IndSel = LstFldEdit.SelectedItem.Index
      With LstFldEdit.ListItems.Add(, , LstFldEdit.ListItems(IndSelC).text)
        For k = 1 To 13
          If k <> 12 Then .SubItems(k) = LstFldEdit.ListItems(IndSelC).SubItems(k)
        Next k
      End With
      For k = LstFldEdit.ListItems.count To IndSel Step -1
        LstFldEdit.ListItems(k).Selected = True
        SpostaCampo "Up"
      Next k
      SwMod = True
      LblModif = "Area modified"
    Case "Cut"
      SwMod = True
      LstFldEdit.ListItems.Remove LstFldEdit.SelectedItem.Index
      LstFldEdit.Refresh
      LblModif = "Area modified"
    Case "Open"
      SwMod = True
      CaricaDMAreaSegZero
      LblModif = "Area modified"
    Case "Save"
      If MsgBox("Warning: saving this structure you'll have to migrate again this DBD. " & vbCrLf & _
         "Are you sure?", vbQuestion + vbCritical + vbYesNo, "i-4.Migration - DataManager") = vbYes Then
        AggiornaDmAreaSeg
        SwMod = False
        LblModif = "Area saved"
      End If
    Case "Help"
    Case "Check"
      SwMod = True
      LblModif = "Area modified"
      VerificaArea
    Case "Close"
      If SwMod Then
        wResp = MsgBox("Save the new area?", vbYesNo)
      End If
      If wResp = vbYes Then AggiornaDmAreaSeg
  End Select
End Sub
Sub VerificaArea()
   
  Dim K1 As Integer
  Dim Y1 As Integer
  Dim wTot As Integer
  Dim wLen As Integer
  Dim LivLen(50, 2)
  For K1 = 1 To LstFldEdit.ListItems.count
    If K1 < LstFldEdit.ListItems.count Then
      If val(LstFldEdit.ListItems(K1).text) < val(LstFldEdit.ListItems(K1 + 1).text) Then
        'If LstFldEdit.ListItems(K1 + 1) <> "88" Then
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_LENGTH) = 0
        'End If
        If LstFldEdit.ListItems(K1).SubItems(LstFldEdit_LENGTH) > 0 Then
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_DEC) = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_DEC))
          wLen = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_LENGTH)) + val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_DEC))
          If InStr(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_TYPE), "PCK") > 0 Then
            wLen = Int((wLen) / 2) + 1
          End If
          If InStr(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_TYPE), "BIN") > 0 Then
            wLen = 2
            If LstFldEdit.ListItems(K1).SubItems(LstFldEdit_LENGTH) > 4 Then wLen = 4
            If LstFldEdit.ListItems(K1).SubItems(LstFldEdit_LENGTH) > 8 Then wLen = 8
          End If
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE) = wLen
        Else
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_DEC) = 0
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE) = 0
        End If
      End If
    End If
  Next K1
  Y1 = 1
  K1 = LstFldEdit.ListItems.count
   
  LivLen(Y1, 1) = 1
  LivLen(Y1, 2) = 0
  Y1 = 2
  LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
  If val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)) > 0 Then
    LivLen(Y1, 2) = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE)) * LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)
  Else
    LivLen(Y1, 2) = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE))
  End If
  
  For K1 = LstFldEdit.ListItems.count - 1 To 1 Step -1
    If val(LstFldEdit.ListItems(K1)) > val(LstFldEdit.ListItems(K1 + 1)) Then
      wTot = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE))
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) > val(LstFldEdit.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          LivLen(Y1, 2) = 0
        End If
        If val(LivLen(Y1, 1)) = val(LstFldEdit.ListItems(K1).text) Then
          If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
            LivLen(Y1, 2) = val(LivLen(Y1, 2)) + wTot
          End If
        End If
        If Trim(LivLen(Y1, 1)) = "" Then
          LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
          If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
            If LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS) > 0 Then
              LivLen(Y1, 2) = (val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE)) * val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)))
            Else
              LivLen(Y1, 2) = val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE))
            End If
          Else
            LivLen(Y1, 2) = 0
          End If
          Exit For
        End If
      Next Y1
    End If
    If val(LstFldEdit.ListItems(K1)) = val(LstFldEdit.ListItems(K1 + 1)) Then
      If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
        For Y1 = 1 To 50
          If val(LivLen(Y1, 1)) = val(LstFldEdit.ListItems(K1).text) Then
            If val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)) > 0 Then
              LivLen(Y1, 2) = val(LivLen(Y1, 2)) + (val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE)) * val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)))
            Else
              LivLen(Y1, 2) = val(LivLen(Y1, 2)) + val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE))
            End If
            Exit For
          End If
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
            If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
              LivLen(Y1, 2) = LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
      End If
    End If
    If val(LstFldEdit.ListItems(K1)) < val(LstFldEdit.ListItems(K1 + 1)) Then
      wLen = 0
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) > val(LstFldEdit.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          wLen = wLen + val(LivLen(Y1, 2))
          LivLen(Y1, 2) = 0
        End If
      Next Y1
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) = val(LstFldEdit.ListItems(K1).text) Then
          If LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS) > 0 Then
            wLen = wLen * LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)
          End If
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE) = wLen  'non c'ho capito niente
          If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
            LivLen(Y1, 2) = val(LivLen(Y1, 2)) + wLen
            wLen = 0
          End If
'          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE) = val(LivLen(Y1, 2))
          wTot = 0
        End If
      Next Y1
      If wLen > 0 Then
        For Y1 = 1 To 50
          If Trim(LivLen(Y1, 1)) = LstFldEdit.ListItems(K1).text Then
            Exit For
          End If
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
            If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
              If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS)) > 0 Then
                wLen = wLen * val(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_OCCURS))
              End If
              LivLen(Y1, 2) = val(wLen)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
        LstFldEdit.ListItems(K1).SubItems(LstFldEdit_BYTE) = wLen
        wTot = 0
      End If
    End If
  Next K1
   
  K1 = 1
  LstFldEdit.ListItems(K1).SubItems(LstFldEdit_ORDINALE) = K1
  LstFldEdit.ListItems(K1).SubItems(LstFldEdit_START) = 1
  wLen = 1
   
  For K1 = 2 To LstFldEdit.ListItems.count
    LstFldEdit.ListItems(K1).SubItems(LstFldEdit_ORDINALE) = K1
    If Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) = "" Then
      If LstFldEdit.ListItems(K1 - 1).SubItems(LstFldEdit_LENGTH) <> "0" Then
        If Trim(LstFldEdit.ListItems(K1 - 1).SubItems(LstFldEdit_OCCURS)) > 0 Then
          wLen = wLen + (val(LstFldEdit.ListItems(K1 - 1).SubItems(LstFldEdit_OCCURS)) * val(LstFldEdit.ListItems(K1 - 1).SubItems(LstFldEdit_BYTE)))
        Else
          wLen = wLen + val(LstFldEdit.ListItems(K1 - 1).SubItems(LstFldEdit_BYTE))
        End If
      End If
      LstFldEdit.ListItems(K1).SubItems(LstFldEdit_START) = wLen
    Else
      For Y1 = 1 To K1
        If Trim(LstFldEdit.ListItems(Y1).SubItems(LstFldEdit_NOME)) = Trim(LstFldEdit.ListItems(K1).SubItems(LstFldEdit_NOMERED)) Then
          wLen = LstFldEdit.ListItems(Y1).SubItems(LstFldEdit_START)
          LstFldEdit.ListItems(K1).SubItems(LstFldEdit_START) = wLen
          Exit For
        End If
      Next Y1
    End If
  Next K1
End Sub

Sub SpostaCampo(Modo As String)
  Dim K1 As Integer
  Dim Ida As Integer
  Dim Ia As Integer
  Dim WsTR As String
  Dim swBold As Boolean
  Dim swColor As Variant
   
  Ida = LstFldEdit.SelectedItem.Index
   
  If Modo = "Up" Then
    Ia = Ida - 1
  Else
    Ia = Ida + 1
  End If
   
  WsTR = LstFldEdit.ListItems(Ida).text
  LstFldEdit.ListItems(Ida).text = LstFldEdit.ListItems(Ia).text
  LstFldEdit.ListItems(Ia).text = WsTR
   
  swBold = LstFldEdit.ListItems(Ida).ListSubItems(LstFldEdit_NOME).Bold
  swColor = LstFldEdit.ListItems(Ida).ListSubItems(LstFldEdit_NOME).ForeColor
   
  For K1 = 2 To 13
    If K1 <> 12 Then
      WsTR = LstFldEdit.ListItems(Ida).SubItems(K1)
      LstFldEdit.ListItems(Ida).SubItems(K1) = LstFldEdit.ListItems(Ia).SubItems(K1)
      LstFldEdit.ListItems(Ida).ListSubItems(K1).Bold = LstFldEdit.ListItems(Ia).ListSubItems(K1).Bold
      LstFldEdit.ListItems(Ida).ListSubItems(K1).ForeColor = LstFldEdit.ListItems(Ia).ListSubItems(K1).ForeColor
      
      LstFldEdit.ListItems(Ia).SubItems(K1) = WsTR
    End If
  Next K1
  
  LstFldEdit.ListItems(Ia).ListSubItems(LstFldEdit_NOME).Bold = swBold
  LstFldEdit.ListItems(Ia).ListSubItems(LstFldEdit_NOME).ForeColor = swColor
End Sub

Sub CaricaDMAreaSeg(seg As Long)
  Dim indCmp As Integer
  Dim indStr As Integer
  Dim App As String
  
  On Error GoTo ERR
  
  LstFldEdit.ListItems.Clear
  For indStr = 2 To UBound(gSegm(seg).Strutture)
    'If gSegm(seg).Strutture(indstr).IdStruttura = wIdStruttura Then
    'se non va bene idoggetto
    If gSegm(seg).Strutture(indStr).StrIdArea = cIdArea And gSegm(seg).Strutture(indStr).StrIdOggetto = cIdOggetto Then
      App = cIdOggetto & "/" & cIdArea
       
      For indCmp = 1 To UBound(gSegm(seg).Strutture(indStr).Campi)
        With LstFldEdit.ListItems.Add(indCmp, , gSegm(seg).Strutture(indStr).Campi(indCmp).livello)
          .SubItems(LstFldEdit_ORDINALE) = gSegm(seg).Strutture(indStr).Campi(indCmp).Ordinale
          .SubItems(LstFldEdit_NOME) = gSegm(seg).Strutture(indStr).Campi(indCmp).Nome
          .SubItems(LstFldEdit_BYTE) = gSegm(seg).Strutture(indStr).Campi(indCmp).bytes
          .SubItems(LstFldEdit_LENGTH) = gSegm(seg).Strutture(indStr).Campi(indCmp).lunghezza
          .SubItems(LstFldEdit_DEC) = gSegm(seg).Strutture(indStr).Campi(indCmp).Decimali
          .SubItems(LstFldEdit_START) = gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione
          .SubItems(LstFldEdit_TYPE) = gSegm(seg).Strutture(indStr).Campi(indCmp).Tipo
          .SubItems(LstFldEdit_OCCURS) = gSegm(seg).Strutture(indStr).Campi(indCmp).Occurs
          ' Mauro 15/01/2008
          If gSegm(seg).Strutture(indStr).Campi(indCmp).Occurs Then
            .SubItems(LstFldEdit_OCCURSFLAT) = gSegm(seg).Strutture(indStr).Campi(indCmp).OccursFlat
          Else
            .SubItems(LstFldEdit_OCCURSFLAT) = ""
          End If
          .SubItems(LstFldEdit_NOMERED) = gSegm(seg).Strutture(indStr).Campi(indCmp).NomeRed
          .SubItems(LstFldEdit_IDCMP) = gSegm(seg).Strutture(indStr).Campi(indCmp).Id_Cmp 'ALE 10/07/2006
          'Mauro 15/01/2008
          .SubItems(LstFldEdit_DAMIGR) = IIf(gSegm(seg).Strutture(indStr).Campi(indCmp).DaMigrare, "YES", "NO")
          If gSegm(seg).Strutture(indStr).Campi(indCmp).Index = True Then
            .ListSubItems(LstFldEdit_NOME).Bold = True
            .ListSubItems(LstFldEdit_NOME).ForeColor = vbBlue
          End If
          If gSegm(seg).Strutture(indStr).Campi(indCmp).Correzione = "S" Then
            .ListSubItems(LstFldEdit_NOME).ForeColor = vbRed
            .SubItems(LstFldEdit_SELECTOR) = gSegm(seg).Strutture(indStr).Campi(indCmp).valore
          End If
        End With
      Next indCmp
      Exit For
    End If
  Next indStr
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 9
      MsgBox "Load error:" & vbCrLf & "Oggetto = " & gSegm(seg).Strutture(indStr).IdOggetto & vbCrLf & _
             "Area = " & gSegm(seg).Strutture(indStr).StrIdArea & vbCrLf & "Segmento = " & gSegm(seg).IdSegm, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case Else
      Stop
  End Select
End Sub

Sub CaricaDMAreaSegUno()
  Dim Index As Integer
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim TbAreaSeg As Recordset, tbverifica As Recordset, TbApp As Recordset
  Dim Nome As String
  Dim wStrIdarea As Variant
  Dim wStrIdOggetto As Long
  Dim c As Long

  LstFldEdit.ListItems.Clear
  
  'SQ 13-11-06
  'TMP: COS'E' STA ROBA???!!!!!!!!!!!!!!!!!
  'Set tbverifica = m_fun.Open_Recordset("select * from MgRel_SegAree where idsegmento = " & gSegm(indseg).IdSegm & " and correzione = 'P' ")
  Set tbverifica = m_fun.Open_Recordset("select * from MgRel_SegAree where idsegmento = " & indexseg & " and correzione = 'P' ")
  If tbverifica.RecordCount = 0 Then
    MsgBox " There is no Structure associated ", vbInformation, "i-4.Migration"
    Exit Sub
  End If
    
  For indseg = 1 To UBound(gSegm)
    If indexseg = gSegm(indseg).IdSegm Then
      If UBound(gSegm(indseg).Strutture) Then
            For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
                LstFldEdit.ListItems.Add , , gSegm(indseg).Strutture(1).Campi(indCmp).livello
                c = LstFldEdit.ListItems.count
                With gSegm(indseg).Strutture(1).Campi(indCmp)
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_ORDINALE) = .Ordinale
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_NOME) = .Nome
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_BYTE) = .bytes
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_LENGTH) = .lunghezza
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_DEC) = .Decimali
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_START) = .Posizione
                If .segno = "S" Then
                  LstFldEdit.ListItems(c).SubItems(LstFldEdit_TYPE) = Trim(.segno & .Tipo & " - " & .Usage)
                Else
                  LstFldEdit.ListItems(c).SubItems(LstFldEdit_TYPE) = Trim(.Tipo & " - " & .Usage)
                End If
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_OCCURS) = .Occurs
                ' Mauro 15/01/2008
                If .Occurs Then
                  LstFldEdit.ListItems(c).SubItems(LstFldEdit_OCCURSFLAT) = IIf(.OccursFlat, "YES", "NO")
                Else
                  If .OccursFlat Then
                    LstFldEdit.ListItems(c).SubItems(LstFldEdit_OCCURSFLAT) = "YES"
                  Else
                    LstFldEdit.ListItems(c).SubItems(LstFldEdit_OCCURSFLAT) = ""
                  End If
                End If
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_NOMERED) = .NomeRed
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_IDCMP) = .Id_Cmp 'ALE 10/07/2006
                ' Mauro 15/01/2008
                LstFldEdit.ListItems(c).SubItems(LstFldEdit_DAMIGR) = IIf(.DaMigrare, "YES", "NO")
                If .Index = True Then
                  LstFldEdit.ListItems(c).ListSubItems(LstFldEdit_NOME).Bold = True
                  LstFldEdit.ListItems(c).ListSubItems(LstFldEdit_NOME).ForeColor = vbBlue
                End If
                If .Correzione = "S" Then
                  LstFldEdit.ListItems(c).ListSubItems(LstFldEdit_NOME).ForeColor = vbRed
                  LstFldEdit.ListItems(c).SubItems(LstFldEdit_SELECTOR) = .valore
                End If
                End With
            Next indCmp
          End If
    End If
  Next indseg
End Sub

Sub CaricaDMAreaSegZero()
  Dim Index As Integer
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim TbAreaSeg As Recordset
  Dim TbApp As Recordset
  Dim Nome As String
  Dim wStrIdarea As Variant
  Dim wStrIdOggetto As Long
  Dim IndS As Integer
  
  On Error GoTo ErrStr:
  
  IndS = UBound(gSegm(indexseg).Strutture)
  
  wStrIdOggetto = gSegm(indexseg).Strutture(2).StrIdOggetto
  wStrIdarea = gSegm(indexseg).Strutture(2).StrIdArea
  LstFldEdit.ListItems.Clear
  Set TbAreaSeg = m_fun.Open_Recordset("select * from psdata_cmp where " & _
                  "idoggetto = " & wStrIdOggetto & " and idarea = " & wStrIdarea & " and " & _
                  "livello <> 88 order by ordinale")
  While Not TbAreaSeg.EOF
    With LstFldEdit.ListItems.Add(, , TbAreaSeg!livello)
      .SubItems(LstFldEdit_ORDINALE) = TbAreaSeg!Ordinale
      .SubItems(LstFldEdit_NOME) = TbAreaSeg!Nome
      .SubItems(LstFldEdit_BYTE) = TbAreaSeg!Byte
      .SubItems(LstFldEdit_LENGTH) = TbAreaSeg!lunghezza
      .SubItems(LstFldEdit_DEC) = TbAreaSeg!Decimali
      .SubItems(LstFldEdit_START) = TbAreaSeg!Posizione
      .SubItems(LstFldEdit_TYPE) = TbAreaSeg!Tipo & "-" & TbAreaSeg!Usage
      .SubItems(LstFldEdit_OCCURS) = TbAreaSeg!Occurs
      .SubItems(LstFldEdit_OCCURSFLAT) = IIf(TbAreaSeg!OccursFlat, "YES", "NO")
      .SubItems(LstFldEdit_NOMERED) = TbAreaSeg!NomeRed
      .SubItems(LstFldEdit_DAMIGR) = IIf(TbAreaSeg!DaMigrare, "YES", "NO")
      If TbAreaSeg!Correzione = "R" Then
        .ListSubItems(LstFldEdit_NOME).Bold = True
        .ListSubItems(LstFldEdit_NOME).ForeColor = vbBlue
      End If
      If TbAreaSeg!Correzione = "I" Then
        .ListSubItems(2).ForeColor = vbRed
        .SubItems(10) = TbAreaSeg!valore
      End If
    End With
    TbAreaSeg.MoveNext
  Wend
ErrStr:
  If LstFldEdit.ListItems.count = 0 Then
    With LstFldEdit.ListItems.Add(, , "1")
      .SubItems(LstFldEdit_ORDINALE) = 1
      .SubItems(LstFldEdit_NOME) = "NomeCampo"
    End With
  End If
End Sub

Sub AggiornaDmAreaSeg()
  Dim indCmp As Integer
  Dim TbAreaSeg As Recordset
  Dim Index As Integer, indseg As Integer, indStr As Integer
  Dim Nome As String
  Dim wStrIdarea As Long, wStrIdOggetto As Long
  Dim k As Integer
  Dim wTipo As String, wUsage As String, wSegno As String
  
  On Error GoTo ErrNoStr:
  
  For k = 1 To UBound(gSegm)
     If indexseg = gSegm(k).IdSegm Then
        Exit For
     End If
  Next k
  If UBound(gSegm(k).Strutture) < 2 Then
     ReDim Preserve gSegm(k).Strutture(2)
     ReDim Preserve gSegm(k).Strutture(2).Campi(0)
  End If
  
  wStrIdOggetto = gSegm(k).Strutture(2).StrIdOggetto
  wStrIdarea = gSegm(k).Strutture(2).StrIdArea

  For indseg = 1 To UBound(gSegm)
    If indexseg = gSegm(indseg).IdSegm Then
      indStr = 1
      ReDim Preserve gSegm(indseg).Strutture(indStr).Campi(0)
      
      For indCmp = 1 To LstFldEdit.ListItems.count
        ReDim Preserve gSegm(indseg).Strutture(indStr).Campi(indCmp)
        k = InStr(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_TYPE), "-")
        wTipo = Trim(Mid$(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_TYPE), 1, k - 1)) 'ALE
        If Len(wTipo) = 2 Then
          wSegno = Mid(wTipo, 1, 1)
          wTipo = Mid(wTipo, 2, 1)
        Else
          If wTipo = "9" Then
            wSegno = "N"
          Else
            wSegno = " "
          End If
        End If
       
        wUsage = Trim(Mid$(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_TYPE), k + 1))
        
        If LstFldEdit.ListItems(indCmp).Key = "NEW" Then 'new
          Dim tb As New Recordset
          Set tb = m_fun.Open_Recordset("select max(IdCmp) as IDMax from MgData_Cmp where IdSegmento= " & gSegm(indseg).IdSegm)
          If tb.RecordCount Then
            gSegm(indseg).Strutture(indStr).Campi(indCmp).Id_Cmp = tb!idMax + 1
          Else
            gSegm(indseg).Strutture(indStr).Campi(indCmp).Id_Cmp = 0
          End If
          tb.Close
        Else
          gSegm(indseg).Strutture(indStr).Campi(indCmp).Id_Cmp = val(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_IDCMP))
        End If
        
        gSegm(indseg).Strutture(indStr).Campi(indCmp).livello = LstFldEdit.ListItems(indCmp).text
        gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes = val(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_BYTE))
        gSegm(indseg).Strutture(indStr).Campi(indCmp).idArea = wStrIdarea
        gSegm(indseg).Strutture(indStr).Campi(indCmp).IdOggetto = wStrIdOggetto
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Decimali = val(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_DEC))
        gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza = val(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_LENGTH))
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome = LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_NOME)
        gSegm(indseg).Strutture(indStr).Campi(indCmp).NomeRed = LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_NOMERED)
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Occurs = LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_OCCURS)
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Correzione = " "
        gSegm(indseg).Strutture(indStr).Campi(indCmp).IdSegm = gSegm(indseg).IdSegm
        gSegm(indseg).Strutture(indStr).Campi(indCmp).IdStruttura = " "
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Modificato = False
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Ordinale = indCmp
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione = val(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_START))
        ' Mauro 15/01/2008
        gSegm(indseg).Strutture(indStr).Campi(indCmp).OccursFlat = IIf(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_OCCURSFLAT) = "YES", True, False)
        gSegm(indseg).Strutture(indStr).Campi(indCmp).DaMigrare = IIf(LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_DAMIGR) = "YES", True, False)
        
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Tipo = wTipo
        gSegm(indseg).Strutture(indStr).Campi(indCmp).segno = wSegno
        
        gSegm(indseg).Strutture(indStr).Campi(indCmp).Usage = wUsage
        gSegm(indseg).Strutture(indStr).Campi(indCmp).valore = ""
        
        If LstFldEdit.ListItems(indCmp).ListSubItems(LstFldEdit_NOME).Bold = True Then
          gSegm(indseg).Strutture(indStr).Campi(indCmp).Index = True
        Else
          gSegm(indseg).Strutture(indStr).Campi(indCmp).Index = False
        End If
        If LstFldEdit.ListItems(indCmp).ListSubItems(LstFldEdit_NOME).ForeColor = vbRed Then
          gSegm(indseg).Strutture(indStr).Campi(indCmp).Correzione = "S"
          gSegm(indseg).Strutture(indStr).Campi(indCmp).valore = LstFldEdit.ListItems(indCmp).SubItems(LstFldEdit_SELECTOR)
        End If
      Next indCmp
    End If
  Next indseg
     
  Exit Sub
ErrNoStr:
  Resume Next
End Sub

Sub InserisciCampoEdit()
  Dim K1 As Integer
  Dim I1 As Integer
  Dim I2 As Integer
  Dim WsTR As String
  
  K1 = LstFldEdit.SelectedItem.Index
  
  With LstFldEdit.ListItems.Add(, , "5")
    For I2 = 1 To 8
      .SubItems(I2) = "0"
    Next I2
    .SubItems(7) = "X - ZND"
    '.SubItems(11) = 0
  End With
  
  LstFldEdit.ListItems(K1 + 1).Key = "NEW" 'ALE 10/06/2006
  
  For I1 = LstFldEdit.ListItems.count To K1 + 2 Step -1
    WsTR = LstFldEdit.ListItems(I1).text
    LstFldEdit.ListItems(I1).text = LstFldEdit.ListItems(I1 - 1).text
    LstFldEdit.ListItems(I1 - 1).text = WsTR

    For I2 = 1 To 11 '9
      WsTR = LstFldEdit.ListItems(I1).SubItems(I2)
      LstFldEdit.ListItems(I1).SubItems(I2) = LstFldEdit.ListItems(I1 - 1).SubItems(I2)
      LstFldEdit.ListItems(I1 - 1).SubItems(I2) = WsTR
    Next I2
  Next I1
End Sub

Sub Rinumera()
  Dim K1 As Integer
  For K1 = 1 To LstFldEdit.ListItems.count
    LstFldEdit.ListItems(K1).SubItems(LstFldEdit_ORDINALE) = K1
  Next K1
End Sub

Private Sub InserisciCampoStruttura(NomeCampo As String)
  Dim indseg As Double
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim indcmpAPP As Integer
  Dim k As Integer
  Dim Ordinale As Integer
  Dim livello As Integer
  Dim Posizione As Integer
  Dim tb As Recordset
  Dim App_indcmp As Integer
  Dim App_Bold As String
  Dim wIndCmp As Integer
  
  'SETTO SE C'E' IL CAMPO BOLD IN UNA VARIABILE DI APPOGGIO
  Modificato = True
  
  For indCmp = 1 To TreeViewMerge.Nodes.count
    If TreeViewMerge.Nodes(indCmp).Bold = True Then
      App_Bold = Trim(TreeViewMerge.Nodes(indCmp).text)
      Exit For
    End If
  Next indCmp
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      'Set tb = DbPrj.OpenRecordset("select * from DMAreaSeg where IdSegm= " & gSegm(indseg).IdSegm & " order by ordinale")
      Set tb = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & gSegm(indseg).IdSegm & " order by ordinale")
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        k = InStr(1, NomeCmp, " ")
          If gSegm(indseg).Strutture(1).Campi(indCmp).Nome = Trim(Mid$(NomeCmp, 1, k)) Then
            App_indcmp = indCmp + 1
            Exit For
          End If
      Next indCmp
      
      '***********************************************************************'
      '***** RICALCOLO PER L'INSERIMENTO NEL POSTO GIUSTO DEL NUOVO CAMPO ****'
      '***********************************************************************'
      
      ReDim Preserve gSegm(indseg).Strutture(1).Campi(UBound(gSegm(indseg).Strutture(1).Campi) + 1)
      
      For indcmpAPP = UBound(gSegm(indseg).Strutture(1).Campi) To (App_indcmp + 1) Step -1
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Ordinale = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Ordinale + 1
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Posizione = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Posizione + 5
      
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).IdSegm = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).IdSegm
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Nome = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Nome
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).NomeRed = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).NomeRed
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).livello = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).livello
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Tipo = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Tipo
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).bytes = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).bytes
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Occurs = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Occurs
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).lunghezza = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).lunghezza
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Decimali = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Decimali
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).segno = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).segno
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Usage = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Usage
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).valore = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).valore
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Modificato = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Modificato
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Correzione = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Correzione
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).Index = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Index
        ' Mauro 15/01/2008
        If gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).Occurs Then
          gSegm(indseg).Strutture(1).Campi(indcmpAPP).OccursFlat = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).OccursFlat
        Else
          gSegm(indseg).Strutture(1).Campi(indcmpAPP).OccursFlat = ""
        End If
        gSegm(indseg).Strutture(1).Campi(indcmpAPP).DaMigrare = gSegm(indseg).Strutture(1).Campi(indcmpAPP - 1).DaMigrare
      
      Next indcmpAPP
      wIndCmp = App_indcmp - 1
      If wIndCmp < 0 Then wIndCmp = 1
      
      gSegm(indseg).Strutture(1).Campi(App_indcmp).IdSegm = gSegm(indseg).IdSegm
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Ordinale = App_indcmp
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Nome = NomeCampo
      gSegm(indseg).Strutture(1).Campi(App_indcmp).NomeRed = ""
      gSegm(indseg).Strutture(1).Campi(App_indcmp).livello = gSegm(indseg).Strutture(1).Campi(wIndCmp).livello
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Tipo = "X"
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Posizione = 1
      gSegm(indseg).Strutture(1).Campi(App_indcmp).bytes = 5
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Occurs = 0
      gSegm(indseg).Strutture(1).Campi(App_indcmp).lunghezza = 5
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Decimali = 0
      gSegm(indseg).Strutture(1).Campi(App_indcmp).segno = "N"
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Usage = "ZND"
      gSegm(indseg).Strutture(1).Campi(App_indcmp).valore = ""
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Modificato = False
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Correzione = "I"
      gSegm(indseg).Strutture(1).Campi(App_indcmp).Index = False
      ' Mauro 15/01/2008
      gSegm(indseg).Strutture(1).Campi(App_indcmp).OccursFlat = False
      gSegm(indseg).Strutture(1).Campi(App_indcmp).DaMigrare = True

      Exit For
    End If
  Next indseg
  
  'RISETTO IL CAMPO CHE ERA BOLD
  For indCmp = 1 To TreeViewMerge.Nodes.count
    If App_Bold = Trim(TreeViewMerge.Nodes(indCmp).text) Then
      TreeViewMerge.Nodes(indCmp).Bold = True
    Else
      TreeViewMerge.Nodes(indCmp).Bold = False
    End If
  Next indCmp
  
  Ricalcola_Posizione_Lunghezza
  
  VisualizzaStruttura
End Sub
''''''''''''''''''''''''''''''''''''''''''
' Aggiunta SEGMENTO VIRTUALE
''''''''''''''''''''''''''''''''''''''''''
Private Sub CmdInsVirt_Click()
  Dim indseg As Long, indseg1 As Long
  Dim IndFld As Integer, indStr As Integer, indCmp As Integer, IdOrig As Long
  Dim r As Recordset
  Dim SQL As String, AppNodo As String
  Dim x As Integer
  
  Modificato = True
  indseg1 = UBound(gSegm) + 1
  For indseg = 1 To indseg1 - 1
    If gSegm(indseg).Name = Nomenodo Then
      IdOrig = gSegm(indseg).IdSegm
      ReDim Preserve gSegm(indseg1)
      ReDim Preserve gSegm(indseg1).gField(0)
      ReDim Preserve gSegm(indseg1).Strutture(0)
      ReDim Preserve gSegm(indseg1).Strutture(0).Campi(0)
      'SQ: utilizzare un parametro d'environment...
      gSegm(indseg1).Name = getVirtualSegmentName(gSegm(indseg))
      NomeVirt = gSegm(indseg1).Name
      '
      With gSegm(indseg1)
        .Parent = gSegm(indseg).Parent
        .Comprtn = gSegm(indseg).Comprtn
        .Correzione = "I"
        .IdOrigine = IdOrig
        .MaxLen = gSegm(indseg).MaxLen
        .MinLen = gSegm(indseg).MinLen
        '.IdSegm = gSegm(indseg).IdSegm & Format(indseg1, "00") 'ALE13/06/2006
        .IdSegm = gSegm(indseg).IdSegm & Mid(NomeVirt, Len(Nomenodo) + 1, Len(NomeVirt) - Len(Nomenodo))
        .IdOggetto = gSegm(indseg).IdOggetto
        .Pointer = gSegm(indseg).Pointer
        .Rules = gSegm(indseg).Rules
        .Redefines = True
      End With
      '
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        ReDim Preserve gSegm(indseg1).gField(IndFld)
        With gSegm(indseg1).gField(IndFld)
          .Name = gSegm(indseg).gField(IndFld).Name
          .MaxLen = gSegm(indseg).gField(IndFld).MaxLen
          .dbdName = gSegm(indseg).gField(IndFld).dbdName
          .Index = gSegm(indseg).gField(IndFld).Index
          .IdField = gSegm(indseg).gField(IndFld).IdField
          .NameIndPunSeg = gSegm(indseg).gField(IndFld).NameIndPunSeg
          .NullVal = gSegm(indseg).gField(IndFld).NullVal
          .Pointer = gSegm(indseg).gField(IndFld).Pointer
          .segment = gSegm(indseg).gField(IndFld).segment
          .Seq = gSegm(indseg).gField(IndFld).Seq
          .Srch = gSegm(indseg).gField(IndFld).Srch
          .Start = gSegm(indseg).gField(IndFld).Start
          .SubSeq = gSegm(indseg).gField(IndFld).SubSeq
          .type = gSegm(indseg).gField(IndFld).type
          .Unique = gSegm(indseg).gField(IndFld).Unique
          .Xname = gSegm(indseg).gField(IndFld).Xname
          'SQ
          .Correzione = "I"
        End With
      Next IndFld
      'SQ: partiva da 2!
      For indStr = 1 To UBound(gSegm(indseg).Strutture)
        ReDim Preserve gSegm(indseg1).Strutture(indStr)
        ReDim Preserve gSegm(indseg1).Strutture(indStr).Campi(0)
        With gSegm(indseg1).Strutture(indStr)
          'SQ
          '.Correzione = "I"
          .Correzione = gSegm(indseg).Strutture(indStr).Correzione
          .NomeCampo = gSegm(indseg).Strutture(indStr).NomeCampo
          .StrIdArea = gSegm(indseg).Strutture(indStr).StrIdArea
          .StrIdOggetto = gSegm(indseg).Strutture(indStr).StrIdOggetto
          .nomeStruttura = gSegm(indseg).Strutture(indStr).nomeStruttura
          .IdOggetto = gSegm(indseg).Strutture(indStr).IdOggetto
          .Primario = gSegm(indseg).Strutture(indStr).Primario
          '.Ordinale = gSegm(IndSeg).Strutture(IndStr).Ordinale
          For indCmp = 1 To UBound(gSegm(indseg).Strutture(indStr).Campi)
          ReDim Preserve .Campi(indCmp)
            .Campi(indCmp).bytes = gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes
            .Campi(indCmp).Ordinale = gSegm(indseg).Strutture(indStr).Campi(indCmp).Ordinale
            'SQ
            '.Campi(indcmp).Correzione = "I"
            .Campi(indCmp).Correzione = gSegm(indseg).Strutture(indStr).Campi(indCmp).Correzione
            .Campi(indCmp).Decimali = gSegm(indseg).Strutture(indStr).Campi(indCmp).Decimali
            .Campi(indCmp).livello = gSegm(indseg).Strutture(indStr).Campi(indCmp).livello
            .Campi(indCmp).lunghezza = gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza
            .Campi(indCmp).Nome = gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome
            .Campi(indCmp).NomeRed = gSegm(indseg).Strutture(indStr).Campi(indCmp).NomeRed
            .Campi(indCmp).Occurs = gSegm(indseg).Strutture(indStr).Campi(indCmp).Occurs
            .Campi(indCmp).Posizione = gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione
            .Campi(indCmp).segno = gSegm(indseg).Strutture(indStr).Campi(indCmp).segno
            .Campi(indCmp).Tipo = gSegm(indseg).Strutture(indStr).Campi(indCmp).Tipo
            .Campi(indCmp).Usage = gSegm(indseg).Strutture(indStr).Campi(indCmp).Usage
            .Campi(indCmp).Id_Cmp = gSegm(indseg).Strutture(indStr).Campi(indCmp).Id_Cmp
            ' Mauro 15/01/2008
            If gSegm(indseg).Strutture(indStr).Campi(indCmp).Occurs Then
              .Campi(indCmp).OccursFlat = gSegm(indseg).Strutture(indStr).Campi(indCmp).OccursFlat
            Else
              .Campi(indCmp).OccursFlat = False
            End If
            .Campi(indCmp).DaMigrare = gSegm(indseg).Strutture(indStr).Campi(indCmp).DaMigrare
          Next indCmp
        End With
      Next indStr
      Exit For
    End If
  Next indseg
  TitSeg.Caption = "Segment: " & Nomenodo
  AppNodo = Trim(Nomenodo)
  
  AggiungiRelazioni Nomenodo
  
  ReDim SegAss(0)
  For x = 1 To UBound(SegAssApp)
    ReDim Preserve SegAss(UBound(SegAss) + 1)
    SegAss(UBound(SegAss)).IdOggetto = SegAssApp(x).IdOggetto
    SegAss(UBound(SegAss)).NameChild = SegAssApp(x).NameChild
    SegAss(UBound(SegAss)).NameFather = SegAssApp(x).NameFather
    SegAss(UBound(SegAss)).Relazione = SegAssApp(x).Relazione
    SegAss(UBound(SegAss)).Tag = SegAssApp(x).Tag
  Next x
  
  CaricaTabSeg TreeView1
  CaricaSeg indseg
  CaricaField indseg
  CaricaStr ListStr, ImageList1, Nomenodo
  CaricaCampi indseg
  CaricaAreaSeg indseg
  TreeView1_NodeClick TreeView1.Nodes(1)
  'Set r = m_fun.Open_Recordset("SELECT * FROM DMTavole WHERE Nome = '" & NomeVirt & "'")
  Select Case Trim(UCase(DataMan.DmTipoMigrazione))
    Case "DB2"
      SQL = "SELECT * FROM DMDB2_Tabelle WHERE Nome = '" & NomeVirt & "'"
    Case "ORACLE"
      SQL = "SELECT * FROM DMORC_Tabelle WHERE Nome = '" & NomeVirt & "'"
    Case "VSAM"
      SQL = ""
  End Select
  If SQL <> "" Then
    Set r = m_fun.Open_Recordset(SQL)
    If r.RecordCount = 0 Then
      r.AddNew
      'r!IdSegm = gSegm(indseg1).IdSegm
      r!IdOrigine = gSegm(indseg1).IdSegm
      r!Nome = NomeVirt
      r.Update
      r.Close
    End If
  End If
  
  TreeView1.Nodes(1).Expanded = True
  'SQ 23-06-06
  'Condizione Redefines: Nome template di default
  Text1(6).text = NomeVirt
  MsgBox "Virtual Segment '" & NomeVirt & "' correctly created. Please save updates before managing areas.", vbInformation, "i-4.Migration - Virtual Segment"
End Sub

Public Sub ResettaStruttura_Check()
  Dim tb As Recordset
  Dim indseg As Long
  Dim indStr As Integer
  Dim indCmp As Integer
    
  For indseg = 1 To UBound(gSegm)
    If Nomenodo = gSegm(indseg).Name Then
      CaricaAreaSeg indseg
      VisualizzaStruttura
      cancellarelazioni IndOgg
      CaricaTreeRelazioni TreeRelazioni
      VisualizzaRelazioni TreeRelazioni
    End If
  Next indseg
End Sub

Public Sub VisualizzaStruttura()
  MousePointer = 11
  RichTextBox1.text = ""
  StruttureAssociate
  CaricaAreaCobol
  TrovaPrimario
'  If ListStruttAss.ListItems.Count <> 1 Then SovraCampi
  MousePointer = 0
  ControllaChiaveField
  ControllaChiaveImpostata
End Sub

Sub CancRipField_Click()
   Modificato = True
   CancRepField ListField
   
'  Modificato = True
'  ImpostaChiave ListField, ImageList1, Nomenodo ' Aggiorna la listview e il campo index della struttura gsegm...
'  AggiornaSpecField ' Aggiorna i valori del Field
End Sub

Private Sub Form_Activate()
  If StatusOK Then
      
    CancRipField.ToolTipText = "Delete Field's reference"
    CmdImpostaChiave.ToolTipText = "Set key"
    Cmd.ToolTipText = "Associate/Disassociate Segment area"
    
    CmdDelVirt.ToolTipText = "Delete Virtual Segment"
    CmdInsVirt.ToolTipText = "Create Virtual Segment"
    Check1.Caption = "Unuseful segment"
    Frame1.Caption = "Characteristics"
    TitSeg.Caption = "Segment: "
    Frame7.Caption = "DLI - Field's detail"
    'Frame3.Caption = "Utilized Cobol's Areas"
    Frame6.Caption = "Critical zones"
    'SQ - non esiste "Signalations"
    Frame8.Caption = "Notes:"
    'Frame2.Caption = "DB structure"
    Frame4.Caption = "Field's detail"
    Frame10.Caption = "Selected Field's characteristics:"
    Frame9.Caption = "Utilized Cobol's Areas"
    Frame16.Caption = "Resulting structure"
    Areaincidenza.Caption = "Incidence Area"
    SSTab1.TabCaption(0) = "Segments"
    SSTab1.TabCaption(1) = "Field particular"
    SSTab1.TabCaption(2) = "Structure composition"
    SSTab1.TabCaption(3) = "Relations"
    
    Label4.Caption = "Cobol's Area details"
    Label25.Caption = "Father - Child's relations"
    ListField.ColumnHeaders(1).text = "Field's name"
    ListField.ColumnHeaders(2).text = "S"
    ListField.ColumnHeaders(4).text = "Length"
    ListField.ColumnHeaders(2).Width = "310"
    ListField.ColumnHeaders(3).Width = "310"
    ListField.ColumnHeaders(4).Width = "670"
    ListField.ColumnHeaders(6).text = "Type"
    ListStr.ColumnHeaders(2).text = "Structure"
    ListStr.ColumnHeaders(3).text = "Field"
    ListCampi.ColumnHeaders(1).text = "Lev."
    ListCampi.ColumnHeaders(2).text = "Field's name"
    ListField2.ColumnHeaders(1).text = "Field's name"
    ListField2.ColumnHeaders(2).text = "S"
    ListField2.ColumnHeaders(4).text = "Length"
    ListField2.ColumnHeaders(6).text = "Type"
    ListStr2.ColumnHeaders(2).text = " Structure"
    ListStr2.ColumnHeaders(3).text = "Field"
    ListIncidenza.ColumnHeaders(1).text = "Lev."
    ListIncidenza.ColumnHeaders(2).text = "Field's name"
    
    Label18.Caption = "Numeric"
    Label22.Caption = "AlfaNumeric"
    Label20.Caption = "Field doesn't coincide with incident field"
  
    'SQ - sbagliato:
    'MadmdF_EditDli.Caption = UCase("Edit database's structure. Database name = ") & Trim(UCase(NomeDatabaseCorrente))
    MadmdF_EditDli.Caption = " STRUCTURE EDITING  -  " & "DB: """ & pNome_DBCorrente & """"
    
    HScroll1_GotFocus
    VScroll1.Value = 1
    ListStr.Refresh
    TreeView1_NodeClick TreeView1.Nodes(1)
    CreaRelazioniPF TreeView1
    TreeRelazioni.ImageList = ImageList1
  Else
    Unload Me
  End If
  TreeView1.Nodes(1).Selected = True
  TreeView1_NodeClick TreeView1.Nodes(TreeView1.SelectedItem.Index)
End Sub
Private Sub Form_Load()
  Dim k As Integer
  Screen.MousePointer = vbHourglass
  
  'SQ:
  'pacco dei tab che devono stare sull'ultimo senno' non funziona!
  SSTab1.Tab = 0
  Modificato = False
  
  'Setta il parent della form
  'SetParent Me.hwnd, DataMan.DmParent
  
  FraTab1.Left = 10
  For k = 0 To 279
    LblConfronto(k).Appearance = 1
  Next k
  
  ReDim gSegm(0)
  ReDim gSegm(0).Strutture(0)
  ReDim gSegm(0).Strutture(0).Campi(0)
  ReDim charris(0)
  
  'Carica tutta la struttura gSegm (compresi field...)
  StatusOK = MadmdM_DLI_Dati.CaricaStruttura
  
  'Passa tutti i controlli  ???!!
  StatusOK = True
  If StatusOK Then
    CaricaTabSeg TreeView1
    CaricaSeg 1
    CaricaField 1
    CaricaStr ListStr, ImageList1, Nomenodo
    CaricaCampi 1
    ConfrontaAreeCobol
    
    If UBound(gSegm) > 0 Then
       TitSeg.Caption = UCase("Segment: ") & gSegm(1).Name
    End If
    
    'Valore = Int((UBound(charris) / 280) + 1)
    HScroll1.Min = 1
    If valore = 0 Then valore = 1
    HScroll1.MAX = valore
    
    
    '*********inizio commento ALE***************
'    m_fun.FnWidth = MadmdF_Start.Width
'    'Memorizza in una variabile globale la height
'    m_HeightBefore = m_fun.FnHeight
'    m_fun.FnHeight = m_fun.FnHeightNoFinImm 'Ricontrollare
'    m_fun.ResizeControls Me
'
'    'Resize dei tab
'
'    FraTab2.Top = 30
'    FraTab2.Left = 60
'    FraTab3.Top = 30
'    FraTab3.Left = 60
'    FraTab4.Top = 30
'    FraTab4.Left = 60
'    FraTab5.Top = 30
'    FraTab5.Left = 60
'    FraTab1.Top = 30
'    FraTab1.Left = 60
'
'    lsw2ndIdxAccess.Top = 30
'    lsw2ndIdxAccess.Left = 60
    '*********fine commento ALE***************
      
    SSTab1.TabVisible(6) = False
  
  Else
    MsgBox "Missing some structures...", vbExclamation, DataMan.DmNomeProdotto
  End If
  
  'Load_PgmWithDLIAccess_IdxSecondary
  
 ' DoEvents ' ALE
  Screen.MousePointer = vbDefault
  'Me.Refresh ' ALE
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Sub CaricaCampi(seg As Long)
  
  If UBound(gSegm) < seg Then Exit Sub
  
  Dim indCmp As Integer
  Dim indStr As Integer
  Dim App As String
  
  On Error GoTo ERR
  
  ListCampi.ListItems.Clear
  For indStr = 2 To UBound(gSegm(seg).Strutture)
  'indstr = TrovaPrimario
    'If gSegm(seg).Strutture(indstr).IdStruttura = wIdStruttura Then
    'se non va bene idoggetto
    If gSegm(seg).Strutture(indStr).StrIdArea = wIdArea And gSegm(seg).Strutture(indStr).StrIdOggetto = wIdOggetto Then
       App = wIdOggetto & "/" & wIdArea
       Label4 = "Dettaglio Area Cobol: " & App & " - " & gSegm(seg).Strutture(indStr).nomeStruttura
       For indCmp = 1 To UBound(gSegm(seg).Strutture(indStr).Campi)
          With ListCampi.ListItems.Add(indCmp, , gSegm(seg).Strutture(indStr).Campi(indCmp).livello)
            .SubItems(1) = gSegm(seg).Strutture(indStr).Campi(indCmp).Nome
            .SubItems(2) = gSegm(seg).Strutture(indStr).Campi(indCmp).bytes
            .SubItems(3) = gSegm(seg).Strutture(indStr).Campi(indCmp).lunghezza
            .SubItems(4) = gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione
            .SubItems(5) = gSegm(seg).Strutture(indStr).Campi(indCmp).Tipo
            .SubItems(6) = gSegm(seg).Strutture(indStr).Campi(indCmp).Occurs
          End With
       Next indCmp
       Exit For
    End If
  Next indStr
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 9
      MsgBox "Load error:" & vbCrLf & "Oggetto = " & gSegm(seg).Strutture(indStr).IdOggetto & vbCrLf & _
             "Area = " & gSegm(seg).Strutture(indStr).StrIdArea & vbCrLf & "Segmento = " & gSegm(seg).IdSegm, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case Else
      Stop
  End Select
End Sub

Sub CaricaSeg(seg As Long)
  If UBound(gSegm) < seg Then Exit Sub
  
  Text1(0) = gSegm(seg).MaxLen
  Text1(1) = gSegm(seg).MinLen
  Text1(2) = gSegm(seg).Comprtn
  Text1(3) = gSegm(seg).Rules
  Text1(4) = gSegm(seg).Pointer
  Text1(5) = gSegm(seg).TbName
  'ALE
  Text1(6) = gSegm(seg).Condizione
  
  ChkRedefines.Visible = False
  ChkRedefines.Enabled = False
  'ChkRedefines.Value = False
  If gSegm(seg).Correzione = "I" Then
    CmdDelVirt.Enabled = True
    CmdInsVirt.Enabled = False
    ChkRedefines.Visible = True
    ChkRedefines.Enabled = True
    ChkRedefines.Value = IIf(gSegm(seg).Redefines, 1, 0)
    Check1.Value = 0
  ElseIf Trim(gSegm(seg).Correzione) = "" Then
    CmdDelVirt.Enabled = False
    ChkRedefines.Value = False
    'CmdInsVirt.Enabled = True
    Check1.Value = 0
  ElseIf gSegm(seg).Correzione = "O" Then
    CheckSegNT False
    Check1.Value = 1
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
   m_fun.FnHeight = m_HeightBefore
   m_fun.RemoveActiveWindows Me
   m_fun.FnActiveWindowsBool = False
   
   SetFocusWnd m_fun.FnParent
End Sub

Private Sub HScroll1_GotFocus()
  HScroll1.Min = 1
  HScroll1.MAX = valore
  Frame6.Caption = "Comparison from byte " & 280 * HScroll1.Value - 279 & " to byte " & 280 * HScroll1.Value
  ColoraConfronto HScroll1.Value
  ResettaField
  If ContrFiller = True Then
     DisegnaFiller
'  Else
'     ResettaFiller
  End If
  DisegnaSegmento (1)
  HScroll1.Enabled = False
  HScroll1.Enabled = True
End Sub

'Private Sub ImpostaCampoChiave_Click()
'  Dim indseg As Integer
'  Dim indcmp As Integer
'  Dim K As Integer
'  Dim j As Integer
'  Dim bool As Boolean
'  Dim Index As Integer
'  Dim Livello As Integer
'
'  For indseg = 1 To TreeViewMerge.Nodes.Count
'    If TreeViewMerge.Nodes(indseg).Selected = True Then
'       bool = True
'       Exit For
'    End If
'  Next indseg
'  If bool Then
'    Modificato = True
'
'    For indseg = 1 To UBound(gSegm)
'      If gSegm(indseg).Name = Nomenodo Then
'        For indcmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
'          K = InStr(TreeViewMerge.SelectedItem.Text, "  ")
'          If K = 0 Then K = Len(TreeViewMerge.SelectedItem.Text)
'          If Trim(Mid$(TreeViewMerge.SelectedItem.Text, 1, K)) = gSegm(indseg).Strutture(1).Campi(indcmp).Nome Then Exit For
'        Next indcmp
'        Exit For
'      End If
'    Next indseg
'
'
'    If gSegm(indseg).Strutture(1).Campi(indcmp).Lunghezza = 0 Then
'
'      Index = TreeViewMerge.SelectedItem.Index
'
'      If TreeViewMerge.SelectedItem.image = 10 Or TreeViewMerge.SelectedItem.ForeColor = vbRed Then
'        For j = indcmp To UBound(gSegm(indseg).Strutture(1).Campi)
'          If val(gSegm(indseg).Strutture(1).Campi(j).Livello) > val(gSegm(indseg).Strutture(1).Campi(indcmp).Livello) Or (indcmp = j) Then
'
'            If gSegm(indseg).Strutture(1).Campi(j).Lunghezza = 0 Then
'              TreeViewMerge.Nodes(j).image = 0
'            Else
'              TreeViewMerge.Nodes(j).image = 6
'            End If
'
'            TreeViewMerge.Nodes(j).ForeColor = vbBlack
'            gSegm(indseg).Strutture(1).Campi(j).Index = False
'            gSegm(indseg).Strutture(1).Campi(j).Correzione = ""
'
'          Else
'            Exit For
'          End If
'        Next j
'
'      Else
'
'        For j = indcmp To UBound(gSegm(indseg).Strutture(1).Campi)
'          If val(gSegm(indseg).Strutture(1).Campi(j).Livello) > val(gSegm(indseg).Strutture(1).Campi(indcmp).Livello) Or (indcmp = j) Then
'
'            'If gSegm(indseg).Strutture(1).Campi(j).Lunghezza = 0 Then
'            '  TreeViewMerge.Nodes(j).image = 0
'            'Else
'              TreeViewMerge.Nodes(j).image = 10
'            'End If
'
'            gSegm(indseg).Strutture(1).Campi(j).Index = True
'            gSegm(indseg).Strutture(1).Campi(j).Correzione = "R"
'            TreeViewMerge.Nodes(j).ForeColor = vbRed
'            Index = Index + 1
'
'          Else
'            Exit For
'          End If
'        Next j
'
'      End If
'
'    Else
'      If TreeViewMerge.SelectedItem.image = 10 Then
'        TreeViewMerge.SelectedItem.ForeColor = vbBlack
'        'TreeViewMerge.SelectedItem.image = 8
'        TreeViewMerge.SelectedItem.image = 6
'        gSegm(indseg).Strutture(1).Campi(indcmp).Index = False
'        gSegm(indseg).Strutture(1).Campi(indcmp).Correzione = ""
'
'      Else
'        TreeViewMerge.SelectedItem.image = 10
'        TreeViewMerge.SelectedItem.ForeColor = vbRed
'        gSegm(indseg).Strutture(1).Campi(indcmp).Index = True
'        gSegm(indseg).Strutture(1).Campi(indcmp).Correzione = "R"
'        'gSegm(indseg).Strutture(1).Campi(indcmp).SKey = False
'      End If
'
'    End If
'
'
'  End If
'End Sub

Private Sub LblConfronto_Click(Index As Integer)
  Dim indice As Integer
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim i As Integer
  
  On Error Resume Next
  
  Frame6.Caption = "Comparison from byte " & 280 * HScroll1.Value - 279 & " to byte " & 280 * HScroll1.Value & " - byte " & Index + 1 & " "
  indice = (280 * (HScroll1.Value - 1)) + (Index + 1)
  If LblConfronto(Index).BackColor = vbGrayText Then
    ResettaField
    For indseg = 1 To UBound(gSegm)
      If gSegm(indseg).Name = Nomenodo Then
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          If gSegm(indseg).Strutture(indStr).nomeStruttura = ListStr.SelectedItem.SubItems(1) Then
            For indCmp = 1 To UBound(gSegm(indseg).Strutture(indStr).Campi)
              If val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) <= indice And val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes) - 1 >= indice And gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza <> 0 And gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome = "FILLER" Then
                For i = val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) - 1 To val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes) - 2
                  indice = i - 280 * (HScroll1.Value - 1)
                  LblConfronto(indice).Appearance = 0
                  LblConfronto(indice).BackColor = vbGrayText
                Next i
                If ListCampi.ListItems.count <> 0 Then
                  ListCampi.ListItems(indCmp).EnsureVisible
                  ListCampi.SetFocus
                  ListCampi.ListItems(indCmp).Selected = True
                End If
                Exit Sub
              End If
            Next indCmp
          End If
        Next indStr
      End If
    Next indseg
  End If
End Sub

Private Sub ListField_Click()
  BiancoField
  If ListField.ListItems.count <> 0 Then
    'If ListField.SelectedItem.SmallIcon = 3 Then DisegnaSegmento (1)
    'If ListField.SelectedItem.SmallIcon = 10 Then DisegnaSegmento (1)
    If ListField.SelectedItem.SmallIcon = 11 Then
      DisegnaSegmento (2)
    Else
      DisegnaSegmento (1)
    End If
    'If ListField.SelectedItem.SmallIcon <> 3 And _
    'ListField.SelectedItem.SmallIcon <> 11 And _
    'ListField.SelectedItem.SmallIcon <> 10 Then ResettaField
  End If
End Sub

Private Sub ListField2_Click()
  If ListStr2.ListItems.count <> 0 And ListField2.ListItems.count <> 0 Then
    Label2 = "Area Cobol incidence " & ListStr2.SelectedItem.text & " " & ListStr2.SelectedItem.SubItems(1) & " on filed " & ListField2.SelectedItem.text
    CaricaListaIncidenze
  End If
  SpecCampo
  If valore <> 0 Then EsaminaField_Check
  If ListField2.ListItems.count <> 0 Then
  ColoraAreaIncidenza ListField2.SelectedItem.text
  Else
    ResetAreaIncidenza
  End If
End Sub

Private Sub ListStr_Click()
  Dim indseg As Long
  Dim App As String
    
  If ListStr.ListItems.count Then
    App = ListStr.SelectedItem.text
    wIdOggetto = Mid(App, 1, InStr(App, "/") - 1)
    wIdArea = Mid(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))
    If ListStr.SelectedItem.SmallIcon = 3 Or ListStr.SelectedItem.SmallIcon = 12 Then
      For indseg = 1 To UBound(gSegm)
        If gSegm(indseg).Name = Nomenodo Then
          CaricaCampi indseg
          Exit For
        End If
      Next indseg
    End If
    CaricaSemi ListCampi, ListStr
  End If
'''  ControllaFiller
End Sub

Private Sub SovraCampi()
'  Dim indseg As Integer
'  Dim indstr  As Integer
'  Dim indstr1 As Integer
'  Dim indcmp As Integer
'  Dim indcmp1 As Integer
'  Dim Nome As String
'
'  ' modificato da pallante
'  On Error Resume Next
'  '*****************
'
'  TreeViewMerge.ImageList = ImageList1
'  For indseg = 1 To UBound(gSegm)
'    If Nomenodo = gSegm(indseg).Name Then
'      If ListStr.ListItems.Count <> 0 Then
'        For indcmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
'          For indstr1 = indstr + 1 To UBound(gSegm(indseg).Strutture)
'            If gSegm(indseg).Strutture(indstr1).Correzione <> "C" Then
'              For indcmp1 = 1 To UBound(gSegm(indseg).Strutture(indstr1).Campi)
'                If TreeViewMerge.Nodes.Count < indcmp Then Exit For
'                If TreeViewMerge.Nodes(indcmp).image <> 6 Then
'                  Nome = gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Nome
'                  Nome = gSegm(indseg).Strutture(indstr1).Campi(indcmp).Nome
'                  If gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Lunghezza <> 0 And gSegm(indseg).Strutture(indstr1).Campi(indcmp).Lunghezza <> 0 Then
'                    If (gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Posizione = gSegm(indseg).Strutture(1).Campi(indcmp).Posizione And gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Bytes = gSegm(indseg).Strutture(1).Campi(indcmp).Bytes And val(gSegm(indseg).Strutture(1).Campi(indcmp).Lunghezza) <> 0) Or (ListStruttAss.ListItems.Count = 1) Then
'                      TreeViewMerge.Nodes(indcmp).image = 8
'                      Exit For
'                    Else
'                      TreeViewMerge.Nodes(indcmp).image = 7
'                    End If
'                  End If
'                End If
'              Next indcmp1
'            End If
'          Next indstr1
'        Next indcmp
'      End If
'    End If
'  Next indseg
End Sub
Private Sub ListStr2_Click()
  ListField2_Click
End Sub
Private Sub SSTab1_Click(PreviousTab As Integer)
  Dim indseg As Long
  Dim indStr As Integer
  Dim risp As Integer
  
  If StatusOK Then
    Select Case SSTab1.Tab
      Case 0
        FraTab1.Move 60, 30
      Case 1
        FraTab2.Move 60, 30
      Case 2
        FraTab3.Move 60, 30
        If ModoCancella Then
          For indseg = 1 To UBound(gSegm)
            If gSegm(indseg).Name = Nomenodo Then
              CaricaDMAreaSeg indseg
              Exit For
            End If
          Next indseg
        
        Else
          CaricaDMAreaSegUno
        End If
      Case 3
        FraTab4.Move 60, 30
      Case 4
        'FraTab5.Move 60, 30
      Case 5
        fraIdxDliPgm.Move 60, 30
    End Select
    
    If Check1.Value <> 1 Then
      AggiornaSpecField
      If SSTab1.Tab = 2 Then
        For indseg = 1 To UBound(gSegm)
          If gSegm(indseg).Name = Nomenodo Then
            For indStr = 2 To UBound(gSegm(indseg).Strutture)
              'If gSegm(indseg).Strutture(1).IdStruttura = gSegm(indseg).Strutture(indstr).IdStruttura And gSegm(indseg).Strutture(indstr).Correzione = "C" Then
              If gSegm(indseg).Strutture(1).StrIdArea = gSegm(indseg).Strutture(indStr).StrIdArea And gSegm(indseg).Strutture(1).StrIdOggetto = gSegm(indseg).Strutture(indStr).StrIdOggetto And gSegm(indseg).Strutture(indStr).Correzione = "C" Then
                risp = MsgBox("Warning, This structure was deleted! Associate a new structure?", vbYesNo)
                If risp = 6 Then CaricaAreaSeg indseg
                  VisualizzaStruttura
                Exit Sub
              End If
            Next indStr
          End If
        Next indseg
      End If
      WaitTime 0.1
      
      If SSTab1.Caption = "Relations" Then
        'Non piu caricarelazioni ma visualizzarelazioni
        VisualizzaRelazioni TreeRelazioni
      End If
'      If SSTab1.Caption = "Segments" Then
'        ListStr.Top = 200
'        ListStr.Left = 30
'        ListStr.Height = ((Frame3.Height - 450) / 10) * 3.5
'        Label4.Top = ListStr.Top + ListStr.Height + 50
'        ListCampi.Top = 80 + Label4.Top + Label4.Height
'        ListCampi.Height = ((Frame3.Height - 450) / 10) * 5
'        ListCampi.Height = Frame3.Height - (ListCampi.Top + 80)
'        CheckFiller.Top = 200
'        Cmd.Top = 200 + CheckFiller.Height + 1
'        CmdAreaPrim.Top = Cmd.Top + CheckFiller.Height + 1
'        'Label4.Top = CmdAreaPrim.Top + CheckFiller.Height + 10
'      End If
    End If
  End If
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim indseg As Double
Dim wResp As Integer

  m_fun.FnProcessRunning = True
  
  Select Case Trim(UCase(Button.Key))
  
    Case "SAVE"
      wResp = MsgBox("Do you want to save all updates on this DBD?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto)
      If wResp = vbYes Then
        MousePointer = 11
        SalvaModifiche
        MousePointer = 0
      End If
  
    ' Mauro 27/08/2007 : Questo pulsante � invisibile
''    Case "CREATE"
''      MousePointer = vbHourglass
''
''      DataMan.DmParam_MaxSize = 40
''      DataMan.DmParam_PercOut = DataMan.DmPathDb & "\Output-Prj"
''      'DataMan.DmParam_PercCpyStruc = DataMan.DmPathDb & "\input-prj\CPY_BASE\CPY_BASE_CPDD.cpy"
''      DataMan.DmParam_PercCpyStruc = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_CPDD"
''      'DataMan.DmParam_PercCpyField = DataMan.DmPathDb & "\input-prj\CPY_BASE\CPY_BASE_CPDR.cpy"
''      DataMan.DmParam_PercCpyField = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_CPDR"
''      'DataMan.DmParam_PercCpyExec = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\CPY_BASE_EXEC.cpy"
''      DataMan.DmParam_PercOutCpy = DataMan.DmPathDb & "\OutPut-Prj\Dli\"
''      DataMan.DmParam_PercOutField = DataMan.DmPathDb & "\OutPut\"
''      DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut\Db2\"
''      DataMan.DmParam_OutField_Orc = DataMan.DmPathDb & "\OutPut\Oracle\"
''      DataMan.DmParam_OutField_Vsam = DataMan.DmPathDb & "\OutPut\Vsam\"
''
''      wResp = MsgBox("Do you want to save the changes?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto)
''      If wResp = vbYes Then
''        SalvaModifiche
''        WaitTime 0.5
''      End If
''
''      GenCpyStruct
''
''      MousePointer = vbNormal
''      MsgBox "Copybooks Created.", vbInformation, DataMan.DmNomeProdotto
            
  End Select
  
  m_fun.FnProcessRunning = False
End Sub

Private Sub Toolbar2_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Dim wResp As Variant
Dim indseg As Double

  m_fun.FnProcessRunning = True
  Select Case ButtonMenu.Key
'   *********************
'   *      RESTORE      *
'   *********************
    Case "RESTDBD"
      'primo pulsante
      wResp = MsgBox("Restore initial Dbd?", vbYesNo, DataMan.DmNomeProdotto)
      If wResp = vbYes Then
        EliminaTbPF
        RipristinaDBD
        Form_Load
        CreaRelazioniPF TreeView1
        TreeRelazioni.ImageList = ImageList1
        
        Modificato = False
      End If
      
    Case "RESTSEG"
      'secondo pulsante
      If Nomenodo <> "" Then
        wResp = MsgBox("Restore Segment " & Nomenodo & "?", vbYesNo, DataMan.DmNomeProdotto)
      Else
        wResp = MsgBox("Restore Initial Segment?", vbYesNo, DataMan.DmNomeProdotto)
      End If
      If wResp = vbYes Then
        For indseg = 1 To UBound(gSegm)
          If Trim(UCase(Nomenodo)) = Trim(UCase(gSegm(indseg).Name)) Then
            MousePointer = 11
            RipristinaCondizioneIniziale indseg
            MousePointer = 0
            MsgBox "Segment Restored"
            ListStr.Refresh
            VisualizzaStruttura
            TreeView1.Nodes(1).Selected = True
            TreeView1_NodeClick TreeView1.SelectedItem
            Exit Sub
          End If
        Next indseg
      End If
      
    Case "RESTAREA"
      ' terzo pulsante
      wResp = MsgBox("Restore initial structure?", vbYesNo, DataMan.DmNomeProdotto)
      If wResp = vbYes Then
        ResettaStruttura_Check
        Pulisci_Txt_CampoStrut
      End If
        
'   *********************
'   *       CHECK       *
'   *********************
        
'''    Mauro 27/08/2007 : Questi pulsanti sono tutti invisibili
'''    Case "CONTRFILLER"
'''
'''      ' ********************************************
'''      ' * ROUTINE PER VISUALIZZARE O MENO I FILLER *
'''      ' ********************************************
'''      ContrFiller = Not ContrFiller
'''      If ContrFiller Then
'''        ChkFiller = vbChecked
'''      Else
'''        ChkFiller = vbUnchecked
'''      End If
'''      ControllaFiller
'''
'''    Case "SELAREA"
'''      EsaminaStruttura_Check
'''
'''    Case "SELFIELD"
'''      EsaminaField_Check
'''
'''    Case "SELSEGMENT"
'''      EsaminaSeg_Check
     
    Case "GLOBAL"
      ' Pulsante di men� del "fulminello"
      GlobalCheck
      MsgBox "Check Complete.", vbOKOnly + vbInformation, DataMan.DmNomeProdotto

  End Select
  m_fun.FnProcessRunning = False

End Sub

Private Sub ControllaFiller()
  If ListCampi.ListItems.count <> 0 Then
    ResettaFiller
    ConfrontaAreeCobol
    DisegnaFiller
  End If
End Sub

Private Sub TreeRelazioni_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim Child As String
  Dim Father As String
  Dim tb As Recordset
  Dim x As Integer
  Dim val As Boolean
  
  Modificato = True
  If InStr(Node.Key, "@") = 0 Then
    
    If Node.Checked = True Then
      val = True
    Else
      val = False
    End If
    
    For x = 1 To UBound(SegAss)
      SegAss(x).Relazione = val
      TreeRelazioni.Nodes(x).Checked = val
    Next x
    Exit Sub
  End If
  
  Father = Node.Parent
  Child = Node.text
  
  For x = 1 To UBound(SegAss)
    If SegAss(x).IdOggetto = IndOgg And SegAss(x).NameFather = Father And SegAss(x).NameChild = Child Then
      If Node.Checked = True Then
        SegAss(x).Relazione = True
      Else
        SegAss(x).Relazione = False
      End If
    End If
  Next x

End Sub

Public Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim indseg As Long
  MousePointer = 11
  BiancoField
  ResettaField
  ResettaFiller
  Nomenodo = Node.Key
  NomeFather = Node.Key
  For indseg = 1 To UBound(gSegm) 'Ale indseg = 0 invece di partire da 1
    If Trim(UCase(gSegm(indseg).Name)) = Trim(UCase(Nomenodo)) Then
      Exit For
    End If
  Next indseg
  TitSeg.Caption = "Segment: " & Nomenodo
  indsegSelected = indseg
  If UBound(gSegm) >= indseg Then
    CaricaSeg indseg
    indexseg = gSegm(indseg).IdSegm
    indicesegmento = indseg
    ' Mauro 27/08/2007 : Cancello le liste delle aree e dei campi
    '                    poi le ricarico solo se il segmento interessato non � obsoleto
    ListStr.ListItems.Clear
    ListCampi.ListItems.Clear
    If gSegm(indseg).Correzione <> "O" Then
      CaricaField indseg
      CaricaStr ListStr, ImageList1, Nomenodo 'carica aree associate
      CaricaCampi indseg 'carica i campi delle aree
      AggiornaSpecField
      CaricaListaIncidenze
      ConfrontaAreeCobol
      ControllaSovrapposizioneCampi
      VisualizzaStruttura
      ListStr.Refresh
      ListField2_Click
    End If
    'Stop
    'solo per adesso dopo implementare
    If Trim(gSegm(indseg).Correzione) <> "I" Then
      CmdInsVirt.Enabled = True
      'CmdDelVirt.Enabled = False
    Else
    End If
    If ListStr.ListItems.count > 0 Then
      'ListStr.ListItems(1).Selected = True
      ListStr_Click
    End If
    SSTab1.Tab = 0
    SSTab1_Click 0
  Else
    'tmp: non si deve verificare!
    MsgBox "Unconsistent structure. Please save structure updates before managing areas.", vbInformation
  End If
  MousePointer = 0
End Sub

Sub CaricaField(seg As Long)
  If UBound(gSegm) < seg Then Exit Sub
  
  Dim IndFld As Integer
  Dim KeyImage As String
  ListField.ListItems.Clear
  ListField.SmallIcons = ImageList1
  For IndFld = 1 To UBound(gSegm(seg).gField)
    If Left(gSegm(seg).gField(IndFld).Name, 1) <> "/" Then  'campi "fasulli"...
      'SQ-5b: ATTENZIONE - HO ANCHE GLI XDFLD:
      If Len(gSegm(seg).gField(IndFld).Srch) = 0 Then
'        If Trim(gSegm(seg).gField(IndFld).Correzione) = "C" Then
'          KeyImage = 4
'        ElseIf gSegm(seg).gField(IndFld).Index Then
'          KeyImage = 10
'        Else
'          KeyImage = 3
'        End If
        If gSegm(seg).gField(IndFld).Index Then   'fare le chiavi index = true!!!!!!!!!!!!!
          KeyImage = 10
        ElseIf gSegm(seg).gField(IndFld).Correzione = "M" Or gSegm(seg).gField(IndFld).Correzione = "A" Then
          KeyImage = 3
        Else
          KeyImage = 4
        End If
        With ListField.ListItems.Add(, , gSegm(seg).gField(IndFld).Name, , val(KeyImage))
          If gSegm(seg).gField(IndFld).Seq Then
            .SubItems(1) = "T"
          Else
            .SubItems(1) = "F"
          End If
          If gSegm(seg).gField(IndFld).Unique Then
            .SubItems(2) = "T"
          Else
            .SubItems(2) = "F"
          End If
          
          .SubItems(3) = gSegm(seg).gField(IndFld).MaxLen
          If .SubItems(3) > DataMan.DmParam_MaxSize Then
            .SmallIcon = (11)
          End If
          .SubItems(4) = gSegm(seg).gField(IndFld).Start
          .SubItems(5) = gSegm(seg).gField(IndFld).type
        End With
      Else
        ' � un XD-Field
        If gSegm(seg).gField(IndFld).Correzione = "" Then
          KeyImage = 3 ' Metto il baffetto
        ElseIf gSegm(seg).gField(IndFld).Index Then   'fare le chiavi index = true!!!!!!!!!!!!!
          KeyImage = 10 ' Metto la chiave
        ElseIf gSegm(seg).gField(IndFld).Correzione = "M" Or gSegm(seg).gField(IndFld).Correzione = "A" Then
          KeyImage = 3 ' Metto il baffetto
        Else
          KeyImage = 4 ' Metto il divieto
        End If
        With ListField.ListItems.Add(, , gSegm(seg).gField(IndFld).Name, , val(KeyImage))
          If gSegm(seg).gField(IndFld).Seq Then
            .SubItems(1) = "T"
          Else
            .SubItems(1) = "F"
          End If
          If gSegm(seg).gField(IndFld).Unique Then
            .SubItems(2) = "T"
          Else
            .SubItems(2) = "F"
          End If
          .SubItems(3) = gSegm(seg).gField(IndFld).MaxLen
          .SubItems(4) = gSegm(seg).gField(IndFld).Start
          .SubItems(5) = "X" ' Per distinguere Xd-Field da altri campi
        End With
      End If
    End If
  Next IndFld
End Sub

Sub ConfrontaAreeCobol()
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim Lungh1 As Integer
  Dim j As Integer
  
  On Error Resume Next
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      If UBound(gSegm(indseg).Strutture) Then
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          'ale 14-06-06
          'Non colorava! (a cosa serve 'sta if?!)
          'If Trim(gSegm(indseg).Strutture(indstr).Correzione) = "" Or Trim(gSegm(indseg).Strutture(indstr).Correzione) = "I" Then
            'SQ
            If UBound(gSegm(indseg).Strutture(indStr).Campi) Then
              Lungh1 = gSegm(indseg).Strutture(indStr).Campi(1).bytes
            ReDim charris(Lungh1)
              For indCmp = 1 To UBound(gSegm(indseg).Strutture(indStr).Campi)
                For j = val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) To val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza) - 1
                  If j <= UBound(charris) Then
                    charris(j) = gSegm(indseg).Strutture(indStr).Campi(indCmp).Tipo
                  End If
                Next j
              Next indCmp
              ConfrontaSucc indseg, indStr
              Exit Sub
            End If
          'End If
        Next indStr
      Else
        ColoraConfronto 1
        Exit Sub
      End If
    End If
  Next indseg
End Sub
    
Sub ConfrontaSucc(seg As Integer, str As Integer)

  Dim indStr As Integer
  Dim indCmp As Integer
  Dim Lungh As Double
  Dim j As Integer
  
  For indStr = str + 1 To UBound(gSegm(seg).Strutture)
    If gSegm(seg).Strutture(indStr).Correzione <> "C" Then
      ' Mauro 25/09/2007 : Controllo se c'� almeno un Field
      'Lungh = gSegm(seg).Strutture(indstr).Campi(1).bytes
      If UBound(gSegm(seg).Strutture(indStr).Campi) Then
        Lungh = gSegm(seg).Strutture(indStr).Campi(1).bytes
      Else
        Lungh = 0
      End If
      ReDim charatt(Lungh)
      If Lungh > UBound(charris) Then ReDim Preserve charris(UBound(charris) + (Lungh - UBound(charris)))
      For indCmp = 1 To UBound(gSegm(seg).Strutture(indStr).Campi)
        For j = val(gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione) To val(gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(seg).Strutture(indStr).Campi(indCmp).lunghezza) - 1
          If j <= UBound(charris) Then
            charatt(j) = gSegm(seg).Strutture(indStr).Campi(indCmp).Tipo
            If charris(j) <> charatt(j) Then
              If charris(j) = "" Then
                charris(j) = charatt(j)
              Else
                charris(j) = "-"
              End If
            End If
          End If
        Next j
      Next indCmp
    End If
  Next indStr
  'ColoraConfronto HScroll1.Value
End Sub
Sub ColoraConfronto(k As Integer)

  Dim i As Integer
  If k = 0 Then k = 1
  If ListStr.ListItems.count = 0 Then
    For i = (280 * k) - 279 To 280 * k
       LblConfronto((i - 1) - (280 * (k - 1))).BackColor = vbWhite
    Next i
    Exit Sub
  End If
  For i = (280 * k) - 279 To 280 * k
    LblConfronto((i - 1) - (280 * (k - 1))).Appearance = 1
    LblConfronto((i - 1) - (280 * (k - 1))).BackColor = vbWhite
    If i > UBound(charris) Then
      LblConfronto((i - 1) - (280 * (k - 1))).BackColor = vbBlack
    Else
      If charris(i) = "-" Then
        LblConfronto((i - 1) - (280 * (k - 1))).BackColor = vbRed
      End If
    End If
  Next i
End Sub
Sub CaricaListaIncidenze()
  Dim nomeStruttura As String
  Dim IdStruttura As String
  Dim indseg As Integer
  Dim indStr As Integer
  Dim IndFld As Integer
  Dim indCmp As Integer
  On Error Resume Next
  ListIncidenza.ListItems.Clear
  ListIncidenza.SmallIcons = ImageList1
  If ListStr2.ListItems.count <> 0 Then
    IdStruttura = ListStr2.SelectedItem.text
    nomeStruttura = ListStr2.SelectedItem.SubItems(1)
    For indseg = 1 To UBound(gSegm)
      If gSegm(indseg).Name = Nomenodo Then
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          'If gSegm(indseg).Strutture(indstr).IdStruttura = IdStruttura And gSegm(indseg).Strutture(indstr).nomeStruttura = nomeStruttura Then
          If gSegm(indseg).Strutture(indStr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea = IdStruttura And gSegm(indseg).Strutture(indStr).nomeStruttura = nomeStruttura Then
            'Ho trovato il tracciato: gSegm(indseg).Strutture(indstr).Campi
            For indCmp = 1 To UBound(gSegm(indseg).Strutture(indStr).Campi)
              If ListField2.ListItems.count <> 0 Then
                'Valido se :
                ' - inizio campo <= start Field & fine campo >= fine Field (campo completamente esterno)
                ' - .... (campo completamente interno!
                'SQ: attenzione: se lunghezza 0 non mi serve!!!!!!!!!!!!!
                If gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza <> "0" And _
                  ((val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) <= val(ListField2.SelectedItem.SubItems(4)) And val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes) - 1 >= val(ListField2.SelectedItem.SubItems(4))) Or (val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) >= val(ListField2.SelectedItem.SubItems(4)) And val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) <= val(ListField2.SelectedItem.SubItems(4)) + val(ListField2.SelectedItem.SubItems(3)) - 1)) Then
                  With ListIncidenza.ListItems.Add(, , gSegm(indseg).Strutture(indStr).Campi(indCmp).livello)
                    .SubItems(1) = gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome
                    .SubItems(2) = gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes
                    .SubItems(3) = gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza
                    .SubItems(4) = gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione
                    .SubItems(5) = gSegm(indseg).Strutture(indStr).Campi(indCmp).Tipo
                    .SubItems(6) = gSegm(indseg).Strutture(indStr).Campi(indCmp).Occurs
                  End With
                  If Trim(gSegm(indseg).Strutture(indStr).Campi(indCmp).Correzione) = "C" Then
                    ListIncidenza.ListItems(ListIncidenza.ListItems.count).SmallIcon = 4
                  Else
                    ListIncidenza.ListItems(ListIncidenza.ListItems.count).SmallIcon = 3
                  End If
                End If
              End If
            Next indCmp
          End If
        Next indStr
      End If
    Next indseg
  End If
  ListIncidenza.Refresh
End Sub
Sub AggiornaSpecField()
    
  Dim indseg As Integer
  Dim Appindseg As Integer
  Dim indStr As Integer
  Dim IndFld As Integer
  ListField2.ListItems.Clear
  ListStr2.ListItems.Clear
  ListField2.SmallIcons = ImageList1
  For indseg = 1 To UBound(gSegm)
      If Trim(UCase(gSegm(indseg).Name)) = Trim(UCase(Nomenodo)) Then
          Appindseg = indseg
          For IndFld = 1 To UBound(gSegm(indseg).gField)
              'If gSegm(indseg).gField(indfld).Correzione = "" And Mid$(gSegm(indseg).gField(indfld).Name, 1, 1) <> "/" And gSegm(indseg).gField(indfld).index = False Then
              If Mid$(gSegm(indseg).gField(IndFld).Name, 1, 1) <> "/" Then 'And gSegm(indseg).gField(IndFld).Index = False Then
                With ListField2.ListItems.Add(, , gSegm(indseg).gField(IndFld).Name, , 3)
                    If gSegm(indseg).gField(IndFld).Seq Then
                      .SubItems(1) = "T"
                    Else
                      .SubItems(1) = "F"
                    End If
                    If gSegm(indseg).gField(IndFld).Unique Then
                      .SubItems(2) = "T"
                    Else
                      .SubItems(2) = "F"
                    End If
                    .SubItems(3) = gSegm(indseg).gField(IndFld).MaxLen
                    If .SubItems(3) > 40 Then
                      .SmallIcon = (11)
                    End If
                    .SubItems(4) = gSegm(indseg).gField(IndFld).Start
                    .SubItems(5) = gSegm(indseg).gField(IndFld).type
                End With
               ElseIf gSegm(indseg).gField(IndFld).Correzione = "" And gSegm(indseg).gField(IndFld).Index = True Then
                With ListField2.ListItems.Add(, , gSegm(indseg).gField(IndFld).Name, , 10)
                    .SubItems(1) = gSegm(indseg).gField(IndFld).Seq
                    .SubItems(2) = gSegm(indseg).gField(IndFld).Unique
                    .SubItems(3) = gSegm(indseg).gField(IndFld).MaxLen
                    .SubItems(4) = gSegm(indseg).gField(IndFld).Start
                    .SubItems(5) = gSegm(indseg).gField(IndFld).type
                End With
              End If
          Next IndFld
          Exit For
      End If
  Next indseg
  For indseg = 1 To UBound(gSegm)
      If gSegm(indseg).Name = Nomenodo Then
          For indStr = 2 To UBound(gSegm(indseg).Strutture)
              If Trim(gSegm(indseg).Strutture(indStr).Correzione) = "" Then
                  'With ListStr2.ListItems.Add(, , gSegm(indseg).Strutture(indstr).IdStruttura)
                  With ListStr2.ListItems.Add(, , gSegm(indseg).Strutture(indStr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea)
                      .SubItems(1) = gSegm(indseg).Strutture(indStr).nomeStruttura
                  End With
              ElseIf gSegm(indseg).Strutture(indStr).Correzione = "P" Then
                'With ListStr2.ListItems.Add(, , gSegm(indseg).Strutture(indstr).IdStruttura)
                With ListStr2.ListItems.Add(, , gSegm(indseg).Strutture(indStr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea)
                      .SubItems(1) = gSegm(indseg).Strutture(indStr).nomeStruttura
                      .SubItems(2) = gSegm(indseg).Strutture(indStr).NomeCampo
                      ListStr2.Refresh
                End With
              End If
          Next indStr
      End If
  Next indseg
  SpecCampo
  TrovaPrimario

End Sub

Function TrovaPrimario() As Long
  Dim indseg As Double
  Dim indStr As Double
'  Dim App As String
'  Dim AppSelect As String
'  Dim wResp As Integer
'  Dim Bool As Integer
  Dim Appindseg As Double
'  Dim Appindstr As Double
  Dim k As Double
  
  bool = 1
  ListStr.SmallIcons = ImageList1
  ListStr2.SmallIcons = ImageList1
  If UBound(gSegm) = 0 Then Exit Function
'  For K = 1 To ListStr2.ListItems.Count
'    ListStr2.ListItems(K).SmallIcon = 3
'    ListStr.ListItems(K).SmallIcon = 3
'  Next K

  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      Appindseg = indseg
      Exit For
    End If
  Next indseg
  If indseg <= UBound(gSegm) Then
    For indStr = 2 To UBound(gSegm(indseg).Strutture)
      If gSegm(indseg).Strutture(indStr).Primario Then
        If ListStr2.ListItems.count Then
           For k = 1 To ListStr2.ListItems.count
            If gSegm(indseg).Strutture(indStr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea = ListStr2.ListItems(k) Then
              ListStr2.ListItems(k).SmallIcon = 12
              ' Mauro 19/11/2007 : Rendo visibile l'area associata
              ListStr2.ListItems(k).EnsureVisible
              ListStr2.ListItems(k).Selected = True
              TrovaPrimario = indStr
            End If
            'tmp: sistemare a monte...
            If k <= ListStr.ListItems.count Then
              If gSegm(indseg).Strutture(indStr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea = ListStr.ListItems(k) Then
                ListStr.ListItems(k).SmallIcon = 12
                ' Mauro 19/11/2007 : Rendo visibile l'area associata
                ListStr.ListItems(k).EnsureVisible
                ListStr.ListItems(k).Selected = True
                TrovaPrimario = indStr
              End If
            End If
           Next k
    '       Exit For
        End If
      End If
    Next indStr
  End If
End Function

Sub ResettaFiller()
    Dim i As Integer
    For i = 0 To 279
        If LblConfronto(i).BackColor = vbGrayText Then
          LblConfronto(i).BackColor = vbWhite
          LblConfronto(i).Appearance = 1
        End If
    Next i
End Sub
Sub ResettaField()
    
  Dim i As Integer
  Dim savcol As Variant
  For i = 0 To 279
    If LblConfronto(i).Appearance = 0 Then
      savcol = LblConfronto(i).BackColor
      LblConfronto(i).Appearance = 1
      LblConfronto(i).BackColor = savcol
    End If
  Next i
End Sub
Sub BiancoField()
  Dim i As Integer
  Dim savcol As Variant
  For i = 0 To 279
    'If LblConfronto(i).Appearance = 0 Then 'ALE
      LblConfronto(i).BackColor = vbWhite
   ' End If 'ALE
  Next i

End Sub
Sub SpecCampo()
  Dim indseg As Integer
  Dim IndFld As Integer
  If ListField2.ListItems.count <> 0 Then
  For indseg = 1 To UBound(gSegm)
    If Trim(UCase(gSegm(indseg).Name)) = Nomenodo Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        If gSegm(indseg).gField(IndFld).Name = ListField2.SelectedItem.text Then
          TxtNCampo = gSegm(indseg).gField(IndFld).Name
          Text8 = gSegm(indseg).gField(IndFld).NameIndPunSeg
          Text9 = gSegm(indseg).gField(IndFld).dbdName
          Text10 = gSegm(indseg).gField(IndFld).Pointer
          Text11 = gSegm(indseg).gField(IndFld).Srch
          Text12 = gSegm(indseg).gField(IndFld).segment
          Text13 = gSegm(indseg).gField(IndFld).Xname
          Text14 = gSegm(indseg).gField(IndFld).NullVal
          Text15 = gSegm(indseg).gField(IndFld).SubSeq
          Exit Sub
        End If
      Next IndFld
    End If
  Next indseg
  End If
End Sub

Sub DisegnaFiller()
    
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim i As Integer
  Dim check As Integer
  Dim App As String
  
  check = 0
  If ContrFiller = True Then
    For indseg = 1 To UBound(gSegm)
      If Nomenodo = gSegm(indseg).Name Then
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          App = ListStr.SelectedItem.text
    
          wIdOggetto = Mid$(App, 1, InStr(App, "/") - 1)
          wIdArea = Mid$(App, InStr(App, "/") + 1, Len(App) - InStr(App, "/"))
          If Trim(gSegm(indseg).Strutture(indStr).Correzione) = "" And wIdArea = gSegm(indseg).Strutture(indStr).StrIdArea And wIdOggetto = gSegm(indseg).Strutture(indStr).StrIdOggetto And ListStr.SelectedItem.SubItems(1) = gSegm(indseg).Strutture(indStr).nomeStruttura Then
            For indCmp = 1 To UBound(gSegm(indseg).Strutture(indStr).Campi)
              'If gSegm(indseg).Strutture(indstr).Campi(indcmp).nome = "FILLER" _
                 Or Mid(gSegm(indseg).Strutture(indstr).Campi(indcmp).nome, Len(gSegm(indseg).Strutture(indstr).Campi(indcmp).nome) - 5) = "FILLER" Then
              If InStr(1, gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome, "FILLER", vbTextCompare) > 0 Then
 '               CheckSeg(2).Value = 1
                For i = val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) To val(gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione) + val(gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes) - 1
                  On Error Resume Next
                  LblConfronto((i - 1) - ((HScroll1.Value - 1) * 280)).BackColor = vbGrayText
                  check = 1
                Next i
              End If
            Next indCmp
          End If
        Next indStr
      End If
    Next indseg
  Else
    ResettaFiller
  End If
End Sub

Sub DisegnaSegmento(x As Integer)
  Dim indseg As Integer
  Dim IndFld As Integer
  Dim i As Integer
  Dim j As Integer
  Dim savcol As Variant
  
  ResettaField
  
  If HScroll1.Value = 0 Then HScroll1.Value = 1
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        If gSegm(indseg).gField(IndFld).Name = ListField.SelectedItem.text Then
          If HScroll1.Value = Int(val(gSegm(indseg).gField(IndFld).Start) / 280) + 1 Then
            For i = val(gSegm(indseg).gField(IndFld).Start) To val(gSegm(indseg).gField(IndFld).Start) + val(gSegm(indseg).gField(IndFld).MaxLen - 1)
              j = (i - 1) - ((HScroll1.Value - 1) * 280)
              If j < 280 Then
                savcol = LblConfronto(j).BackColor
                LblConfronto(j).Appearance = 0
                Select Case x
                  Case 1
                    LblConfronto(j).BackColor = RGB(50, 140, 250)
                  Case 2
                    LblConfronto(j).BackColor = &H80C0FF
                End Select
              Else
                Exit For
              End If
            Next i
          End If
          Exit For
        End If
      Next IndFld
    End If
  Next indseg

End Sub

' Mauro 27/08/2007 : I controlli vanno bene, ma non c'� nessun ritorno o messaggio di errore per segnalare una qualche anomalia
Sub ControllaSovrapposizioneCampi()
Dim indseg As Integer
Dim IndFld As Integer
Dim indfld1 As Integer
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        ' Mauro 27/08/2007 : Perch� manca la correzione "P"? Non � compresa anche questa tra le strutture associate?
        'If Trim(gSegm(indseg).gField(IndFld).Correzione) = "" Or _
        '   Trim(gSegm(indseg).gField(IndFld).Correzione) = "M" Or _
        '   Trim(gSegm(indseg).gField(IndFld).Correzione) = "I" Then
        If Trim(gSegm(indseg).gField(IndFld).Correzione) = "" Or _
           Trim(gSegm(indseg).gField(IndFld).Correzione) = "M" Or _
           Trim(gSegm(indseg).gField(IndFld).Correzione) = "P" Or _
           Trim(gSegm(indseg).gField(IndFld).Correzione) = "I" Then
          For indfld1 = IndFld + 1 To UBound(gSegm(indseg).gField)
            If Trim(gSegm(indseg).gField(indfld1).Correzione) = "" Then
              If (val(gSegm(indseg).gField(indfld1).Start) >= val(gSegm(indseg).gField(IndFld).Start) And _
                  val(gSegm(indseg).gField(indfld1).Start) < val(gSegm(indseg).gField(IndFld).Start) + val(gSegm(indseg).gField(IndFld).MaxLen)) Or _
                 (val(gSegm(indseg).gField(indfld1).Start) <= val(gSegm(indseg).gField(IndFld).Start) And _
                  val(gSegm(indseg).gField(indfld1).Start) + val(gSegm(indseg).gField(indfld1).MaxLen) >= val(gSegm(indseg).gField(IndFld).Start)) Then
                'CheckSeg(0).Value = 1
                Exit Sub
              End If
            End If
          Next indfld1
        End If
      Next IndFld
    End If
  Next indseg
  'CheckSeg(0).Value = 0
End Sub

Sub ColoraAreaIncidenza(NomeField As String)
  Dim indseg As Integer
  Dim IndFld As Integer
  Dim i As Integer
  Dim Index As Integer
  ResetAreaIncidenza
  If ListStr.ListItems.count = 0 Then Exit Sub
  'If VScroll1.Value = 0 Then Exit Sub 'ALE??
  'ResetAreaIncidenza
  Index = 1
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        If gSegm(indseg).gField(IndFld).Name = NomeField Then
          VScroll1.Min = 1
          VScroll1.MAX = Int(gSegm(indseg).gField(IndFld).MaxLen / 35) + 1
          For i = (VScroll1.Value - 1) * 35 + (gSegm(indseg).gField(IndFld).Start) To val(gSegm(indseg).gField(IndFld).Start) + val(gSegm(indseg).gField(IndFld).MaxLen) - 1
            If i > UBound(charris) Then Exit Sub
            If charris(i) = "-" Then
              LblIncidenza(Index + 1).BackColor = vbBlue
              LblIncidenza(Index).BackColor = vbYellow
            ElseIf charris(i) = "X" Then
              LblIncidenza(Index + 1).BackColor = vbBlue
            ElseIf charris(i) = "9" Then
              LblIncidenza(Index).BackColor = vbYellow
            End If
            Index = Index + 4
            If Index > 137 Then
              Index = Index - 4
            End If
          Next i
          For i = 1 To ListIncidenza.ListItems.count
            If val(ListIncidenza.ListItems(i).SubItems(3)) <> 0 Then
              If val(gSegm(indseg).gField(IndFld).Start) > val(ListIncidenza.ListItems(i).SubItems(4)) And ListIncidenza.ListItems(i).SmallIcon <> 4 Then
                LblIncidenza(0).BackColor = vbRed
                Exit Sub
              ElseIf val(gSegm(indseg).gField(IndFld).Start) + val(gSegm(indseg).gField(IndFld).MaxLen) < val(ListIncidenza.ListItems(ListIncidenza.ListItems.count).SubItems(4)) + val(ListIncidenza.ListItems(ListIncidenza.ListItems.count).SubItems(2)) And ListIncidenza.ListItems(ListIncidenza.ListItems.count).SmallIcon <> 4 Then
                LblIncidenza(Index - 5).BackColor = vbRed
                Exit Sub
              End If
            End If
          Next i
        End If
      Next IndFld
    End If
  Next indseg
End Sub

Sub ResetAreaIncidenza()
  Dim i As Integer
  For i = i To 139
      LblIncidenza(i).BackColor = vbWhite
  Next i
End Sub

Sub CaricaAreaCobol()
  
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indstr1 As Integer
  Dim indCmp As Integer
  Dim cmp As Integer
  Dim indcmp1 As Integer
  Dim controllo As Boolean
  Dim indice As Integer
  Dim Chiavi(50, 2) As String
  Dim k As Integer
  Dim MaxLen As Long
  Dim testo As String
  Dim TestoT As String
  Dim Liv1 As Integer
  Dim Liv2 As Integer
  Dim IndS As Integer
    
  ' corretto da pallante
    On Error GoTo ERR:
  ' *************************
  IndS = 1
  
  MaxLen = 0
  TreeViewMerge.Nodes.Clear
  TreeViewMerge.ImageList = ImageList1
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo And UBound(gSegm(indseg).Strutture) Then
      If UBound(gSegm(indseg).Strutture) Then
        For indCmp = 1 To UBound(gSegm(indseg).Strutture(IndS).Campi)
          If Len(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome) > MaxLen Then
            MaxLen = Len(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome)
          End If
        Next
      End If
    End If
  Next indseg
  
  IndS = TrovaPrimario
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo And UBound(gSegm(indseg).Strutture) <> 0 Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(IndS).Campi)
        If Trim(gSegm(indseg).Strutture(IndS).Campi(indCmp).NomeRed) = "" Then
          For k = 1 To 50
            If Chiavi(k, 1) = "" Or _
               Chiavi(k, 1) = str(gSegm(indseg).Strutture(IndS).Campi(indCmp).livello) Then
               Chiavi(k, 1) = str(gSegm(indseg).Strutture(IndS).Campi(indCmp).livello)
               Chiavi(k, 2) = str(indCmp)
               Exit For
            End If
          Next k
          
          If indCmp = UBound(gSegm(indseg).Strutture(IndS).Campi) Then
            Liv1 = (gSegm(indseg).Strutture(IndS).Campi(indCmp).livello)
            Liv2 = (gSegm(indseg).Strutture(IndS).Campi(indCmp).livello) - 1
          Else
            Liv1 = (gSegm(indseg).Strutture(IndS).Campi(indCmp).livello)
            Liv2 = gSegm(indseg).Strutture(IndS).Campi(indCmp + 1).livello
          End If
          With TreeViewMerge.Nodes
            If gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome = "" Then Exit For
            If Trim(Chiavi(k, 1)) = "1" Or Trim(Chiavi(k, 1)) = "77" Then
              .Add , , "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome, MaxLen)
              
            ElseIf Liv1 < Liv2 Then
              .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome, MaxLen)
                            
            'ElseIf gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza = 0 Then
            '  .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome, MaxLen)
            
            ElseIf gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali <> 0 Then
              Select Case gSegm(indseg).Strutture(IndS).Campi(indCmp).Usage
                Case "PCK"
                  If gSegm(indseg).Strutture(IndS).Campi(indCmp).segno = "S" Then
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).segno & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  Else
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  End If
                  testo = "      Pic " & TestoT & "(" & val(gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza - gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali) & ")" & "V " & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali & ")" & "COMP-3"
                  .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & testo, MaxLen), 6
                Case "BNR"
                  If gSegm(indseg).Strutture(IndS).Campi(indCmp).segno = "S" Then
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).segno & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  Else
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  End If
                  testo = "      Pic " & TestoT & "(" & val(gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza - gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali) & ")" & "V " & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali & ")" & "COMP"
                  .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & testo, MaxLen), 6
                Case Else
                  If gSegm(indseg).Strutture(IndS).Campi(indCmp).segno = "S" Then
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).segno & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  Else
                    TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
                  End If
                  testo = "      Pic " & TestoT & "(" & val(gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza - gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali) & ")" & "V " & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).Decimali & ")"
                  .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & testo, MaxLen), 6
              End Select
            Else
              If gSegm(indseg).Strutture(IndS).Campi(indCmp).segno = "S" Then
                TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).segno & gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
              Else
                TestoT = gSegm(indseg).Strutture(IndS).Campi(indCmp).Tipo
              End If
              If gSegm(indseg).Strutture(IndS).Campi(indCmp).Usage = "BNR" And gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione <> "C" Then
                '.Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome & "      Pic " & gSegm(indseg).Strutture(inds).Campi(indcmp).Tipo & "(" & gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza & ") " & "COMP", MaxLen), 6
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") " & "COMP", MaxLen), 6
              ElseIf gSegm(indseg).Strutture(IndS).Campi(indCmp).Usage = "BNR" And gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione = "C" Then
                '.Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome & "      Pic " & gSegm(indseg).Strutture(inds).Campi(indcmp).Tipo & "(" & gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza & ") " & "COMP", MaxLen), 7
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") " & "COMP", MaxLen), 7
              ElseIf gSegm(indseg).Strutture(IndS).Campi(indCmp).Usage = "PCK" And gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione <> "C" Then
                '.Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome & "      Pic " & gSegm(indseg).Strutture(inds).Campi(indcmp).Tipo & "(" & gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza & ") " & "COMP-3", MaxLen), 6
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") " & "COMP-3", MaxLen), 6
              ElseIf gSegm(indseg).Strutture(IndS).Campi(indCmp).Usage = "PCK" And gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione = "C" Then
                '.Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome & "      Pic " & gSegm(indseg).Strutture(inds).Campi(indcmp).Tipo & "(" & gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza & ") " & "COMP-3", MaxLen), 7
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") " & "COMP-3", MaxLen), 7
              ElseIf gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione <> "C" Then
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") ", MaxLen), 6
              Else
                '.Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indcmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(inds).Campi(indcmp).nome & "      Pic " & gSegm(indseg).Strutture(inds).Campi(indcmp).Tipo & "(" & gSegm(indseg).Strutture(inds).Campi(indcmp).Lunghezza & ") ", MaxLen), 7
                .Add "C" & Trim(Chiavi(k - 1, 2)), tvwChild, "C" & Trim(str(indCmp)), Normalizza_TreeViewMerge(gSegm(indseg).Strutture(IndS).Campi(indCmp).Nome & "      Pic " & TestoT & "(" & gSegm(indseg).Strutture(IndS).Campi(indCmp).lunghezza & ") ", MaxLen), 7
              End If
              
            End If
          End With
          If Trim(gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione) = "S" Then TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).Bold = True
          If Trim(gSegm(indseg).Strutture(IndS).Campi(indCmp).Correzione) = "R" Then
             TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).ForeColor = vbRed
             TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).image = 10
             gSegm(indseg).Strutture(IndS).Campi(indCmp).Index = True
          End If
          If gSegm(indseg).Strutture(IndS).Campi(indCmp).Index Then TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).image = 10
          If gSegm(indseg).Strutture(IndS).Campi(indCmp).Index Then
            TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).ForeColor = vbRed
          Else
            TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).ForeColor = vbBlack
          End If
          TreeViewMerge.Nodes(TreeViewMerge.Nodes.count).Expanded = True
         Else
            cmp = indCmp + 1
            Do
              If cmp >= UBound(gSegm(indseg).Strutture(IndS).Campi) Then Exit Do
              If gSegm(indseg).Strutture(IndS).Campi(cmp).livello <= gSegm(indseg).Strutture(IndS).Campi(indCmp).livello Then Exit Do
              cmp = cmp + 1
            Loop
            If cmp = UBound(gSegm(indseg).Strutture(IndS).Campi) Then
              indCmp = cmp
            Else
              indCmp = cmp - 1
            End If
          End If
      Next indCmp
      Exit Sub
    End If
  Next indseg
  
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 9
      If IndS = 1 Then
        IndS = 2
        gSegm(indseg).Strutture(IndS).Primario = True
        gSegm(indseg).Strutture(1).Campi = gSegm(indseg).Strutture(IndS).Campi
        Resume Next
      Else
        MsgBox "Load error!!! - " & ERR.Description, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
      End If
    Case Else
  '    Stop
  End Select
End Sub

Private Function Normalizza_TreeViewMerge(NomeCampo As String, MAX As Long) As String
  Dim NomeFinale As String
  Dim Nome As String
  Dim Tipologia As String
  Dim k As Integer
  Dim Y As Integer

  Nome = Trim(NomeCampo)

  k = InStr(NomeCampo, "  ")
  If k <> 0 Then
     Nome = Trim(Mid$(NomeCampo, 1, k))
     Tipologia = Trim(Mid$(NomeCampo, k, Len(NomeCampo)))
     Y = MAX + 10 - Len(Nome)
     'SQ
     If Y > 0 Then
      NomeFinale = Nome & Space(Y) & "-" & Tipologia
     Else
      NomeFinale = Nome & "-" & Tipologia
     End If
  Else
    NomeFinale = Trim(Nome)
  End If

  Normalizza_TreeViewMerge = NomeFinale
End Function




Sub StruttureAssociate()
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim controllo As Boolean
  
'  ListStruttAss.ListItems.Clear
'  For indseg = 1 To UBound(gSegm)
'    If Nomenodo = gSegm(indseg).Name Then
'      controllo = True
'      For indstr = 2 To UBound(gSegm(indseg).Strutture)
'        If gSegm(indseg).Strutture(indstr).Correzione <> "C" Then
'          'With ListStruttAss.ListItems.Add(, , gSegm(indseg).Strutture(indstr).IdStruttura)
'          With ListStruttAss.ListItems.Add(, , gSegm(indseg).Strutture(indstr).StrIdOggetto & "/" & gSegm(indseg).Strutture(indstr).StrIdArea)
'            .SubItems(1) = gSegm(indseg).Strutture(indstr).nomeStruttura
'          End With
'        End If
'      Next indstr
'    End If
'    If controllo = True Then Exit Sub
'  Next indseg
End Sub

Sub CheckConfini()
  Dim indseg As Integer
  Dim indCmp As Integer
  Dim indcmp1 As Integer
  Dim k As Integer
  
  On Error GoTo ERR
  
  indcmp1 = 1
  For indseg = 1 To UBound(gSegm)
    If Nomenodo = gSegm(indseg).Name Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        k = InStr(1, NomeCmp, "  ")
        If k = 0 Then k = Len(NomeCmp)
        If gSegm(indseg).Strutture(1).Campi(indCmp).Nome = Trim(Mid$(NomeCmp, 1, k)) And TreeViewMerge.Nodes(indcmp1).image = 7 Then
          AggiungiTB "DMW00002", "Fields' s coincidence of the structure " & ListStr.SelectedItem.SubItems(2) & " with the field " & Trim(Mid$(NomeCmp, 1, k)) & " causes an error: control the cobol area's structure or ignore it ", RichTextBox1
          Exit Sub
        End If
        If Trim(gSegm(indseg).Strutture(1).Campi(indCmp).NomeRed) = "" Then indcmp1 = indcmp1 + 1
      Next indCmp
    End If
  Next indseg
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 9
      MsgBox "Load error:" & vbCrLf & "Oggetto = " & gSegm(indseg).Strutture(1).IdOggetto & vbCrLf & _
             "Area = " & gSegm(indseg).Strutture(1).StrIdArea & vbCrLf & "Segmento = " & gSegm(indseg).IdSegm, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case Else
  '    Stop
  End Select
End Sub

Sub AggiornaOrdinale()
  Dim indseg As Double
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim k As Integer
  Dim PosApp As Integer
  Dim CampoApp As wCampo
  Dim StringApp As String
  
  For indseg = 1 To UBound(gSegm)
    If Nomenodo = gSegm(indseg).Name Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        k = InStr(1, NomeCmp, " ")
        If gSegm(indseg).Strutture(1).Campi(indCmp).Nome = Trim(Mid$(NomeCmp, 1, k)) Then
          With gSegm(indseg).Strutture(1)
            If BottonePremuto = 2 Then
              CampoApp.bytes = gSegm(indseg).Strutture(1).Campi(indCmp).bytes
              CampoApp.Ordinale = gSegm(indseg).Strutture(1).Campi(indCmp).Ordinale
              CampoApp.Posizione = gSegm(indseg).Strutture(1).Campi(indCmp).Posizione
              CampoApp.Correzione = gSegm(indseg).Strutture(1).Campi(indCmp).Correzione
              CampoApp.Decimali = gSegm(indseg).Strutture(1).Campi(indCmp).Decimali
              CampoApp.IdSegm = gSegm(indseg).Strutture(1).Campi(indCmp).IdSegm
              CampoApp.IdStruttura = gSegm(indseg).Strutture(1).Campi(indCmp).IdStruttura
              CampoApp.Index = gSegm(indseg).Strutture(1).Campi(indCmp).Index
              CampoApp.livello = gSegm(indseg).Strutture(1).Campi(indCmp).livello
              CampoApp.lunghezza = gSegm(indseg).Strutture(1).Campi(indCmp).lunghezza
              CampoApp.Nome = gSegm(indseg).Strutture(1).Campi(indCmp).Nome
              CampoApp.NomeRed = gSegm(indseg).Strutture(1).Campi(indCmp).NomeRed
              CampoApp.Occurs = gSegm(indseg).Strutture(1).Campi(indCmp).Occurs
              ' Mauro 15/01/2008
              'If gSegm(indseg).Strutture(1).Campi(indcmp).Occurs Then
                CampoApp.OccursFlat = gSegm(indseg).Strutture(1).Campi(indCmp).OccursFlat
              'Else
              '  CampoApp.OccursFlat = ""
              'End If
              CampoApp.DaMigrare = gSegm(indseg).Strutture(1).Campi(indCmp).DaMigrare
              CampoApp.segno = gSegm(indseg).Strutture(1).Campi(indCmp).segno
              'CampoApp.SKey = gSegm(indseg).Strutture(1).Campi(indcmp).SKey
              CampoApp.Tipo = gSegm(indseg).Strutture(1).Campi(indCmp).Tipo
              CampoApp.Usage = gSegm(indseg).Strutture(1).Campi(indCmp).Usage
              CampoApp.valore = gSegm(indseg).Strutture(1).Campi(indCmp).valore
              
              .Campi(indCmp).Ordinale = .Campi(indCmp + 1).Ordinale
              .Campi(indCmp).Posizione = val(.Campi(indCmp).Posizione) + val(.Campi(indCmp + 1).bytes)
              .Campi(indCmp).bytes = gSegm(indseg).Strutture(1).Campi(indCmp + 1).bytes
              .Campi(indCmp).Correzione = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Correzione
              .Campi(indCmp).Decimali = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Decimali
              .Campi(indCmp).IdSegm = gSegm(indseg).Strutture(1).Campi(indCmp).IdSegm
              .Campi(indCmp).Index = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Index
              .Campi(indCmp).livello = gSegm(indseg).Strutture(1).Campi(indCmp + 1).livello
              .Campi(indCmp).lunghezza = gSegm(indseg).Strutture(1).Campi(indCmp + 1).lunghezza
              .Campi(indCmp).Nome = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Nome
              .Campi(indCmp).NomeRed = gSegm(indseg).Strutture(1).Campi(indCmp + 1).NomeRed
              .Campi(indCmp).Occurs = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Occurs
              ' Mauro 15/01/2008
              'If gSegm(indseg).Strutture(1).Campi(indcmp + 1).Occurs Then
                .Campi(indCmp).OccursFlat = gSegm(indseg).Strutture(1).Campi(indCmp + 1).OccursFlat
              'Else
              '  .Campi(indcmp).OccursFlat = ""
              'End If
              .Campi(indCmp).DaMigrare = gSegm(indseg).Strutture(1).Campi(indCmp + 1).DaMigrare
              .Campi(indCmp).segno = gSegm(indseg).Strutture(1).Campi(indCmp + 1).segno
              '.Campi(indcmp).SKey = gSegm(indseg).Strutture(1).Campi(indcmp + 1).SKey
              .Campi(indCmp).Tipo = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Tipo
              .Campi(indCmp).Usage = gSegm(indseg).Strutture(1).Campi(indCmp + 1).Usage
              .Campi(indCmp).valore = gSegm(indseg).Strutture(1).Campi(indCmp + 1).valore

              .Campi(indCmp + 1).Ordinale = CampoApp.Ordinale
              .Campi(indCmp + 1).Posizione = CampoApp.Posizione
              .Campi(indCmp + 1).bytes = CampoApp.bytes
              .Campi(indCmp + 1).Correzione = CampoApp.Correzione
              .Campi(indCmp + 1).Decimali = CampoApp.Decimali
              .Campi(indCmp + 1).IdSegm = CampoApp.IdSegm
              .Campi(indCmp + 1).IdStruttura = CampoApp.IdStruttura
              .Campi(indCmp + 1).Index = CampoApp.Index
              .Campi(indCmp + 1).livello = CampoApp.livello
              .Campi(indCmp + 1).lunghezza = CampoApp.lunghezza
              .Campi(indCmp + 1).Nome = CampoApp.Nome
              .Campi(indCmp + 1).NomeRed = CampoApp.NomeRed
              .Campi(indCmp + 1).Occurs = CampoApp.Occurs
              ' Mauro 15/01/2008
              'If CampoApp.Occurs Then
                .Campi(indCmp + 1).OccursFlat = CampoApp.OccursFlat
              'Else
              '  .Campi(indcmp + 1).OccursFlat = ""
              'End If
              .Campi(indCmp + 1).DaMigrare = CampoApp.DaMigrare
              .Campi(indCmp + 1).Ordinale = CampoApp.Ordinale
              .Campi(indCmp + 1).Posizione = CampoApp.Posizione
              .Campi(indCmp + 1).segno = CampoApp.segno
              '.Campi(indcmp + 1).SKey = CampoApp.SKey
              .Campi(indCmp + 1).Tipo = CampoApp.Tipo
              .Campi(indCmp + 1).Usage = CampoApp.Usage
              .Campi(indCmp + 1).valore = CampoApp.valore
            Else
              CampoApp.bytes = gSegm(indseg).Strutture(1).Campi(indCmp).bytes
              CampoApp.Ordinale = gSegm(indseg).Strutture(1).Campi(indCmp).Ordinale
              CampoApp.Posizione = gSegm(indseg).Strutture(1).Campi(indCmp).Posizione
              CampoApp.Correzione = gSegm(indseg).Strutture(1).Campi(indCmp).Correzione
              CampoApp.Decimali = gSegm(indseg).Strutture(1).Campi(indCmp).Decimali
              CampoApp.IdSegm = gSegm(indseg).Strutture(1).Campi(indCmp).IdSegm
              CampoApp.IdStruttura = gSegm(indseg).Strutture(1).Campi(indCmp).IdStruttura
              CampoApp.Index = gSegm(indseg).Strutture(1).Campi(indCmp).Index
              CampoApp.livello = gSegm(indseg).Strutture(1).Campi(indCmp).livello
              CampoApp.lunghezza = gSegm(indseg).Strutture(1).Campi(indCmp).lunghezza
              CampoApp.Nome = gSegm(indseg).Strutture(1).Campi(indCmp).Nome
              CampoApp.NomeRed = gSegm(indseg).Strutture(1).Campi(indCmp).NomeRed
              CampoApp.Occurs = gSegm(indseg).Strutture(1).Campi(indCmp).Occurs
              CampoApp.segno = gSegm(indseg).Strutture(1).Campi(indCmp).segno
              'CampoApp.SKey = gSegm(indseg).Strutture(1).Campi(indcmp).SKey
              CampoApp.Tipo = gSegm(indseg).Strutture(1).Campi(indCmp).Tipo
              CampoApp.Usage = gSegm(indseg).Strutture(1).Campi(indCmp).Usage
              CampoApp.valore = gSegm(indseg).Strutture(1).Campi(indCmp).valore
              ' Mauro 15/01/2008
              'If gSegm(indseg).Strutture(1).Campi(indcmp).Occurs Then
                CampoApp.OccursFlat = gSegm(indseg).Strutture(1).Campi(indCmp).OccursFlat
              'Else
              '  CampoApp.OccursFlat = ""
              'End If
              CampoApp.DaMigrare = gSegm(indseg).Strutture(1).Campi(indCmp).DaMigrare
              
              .Campi(indCmp).Ordinale = .Campi(indCmp - 1).Ordinale
              .Campi(indCmp).Posizione = val(.Campi(indCmp).Posizione) + val(.Campi(indCmp - 1).bytes)
              .Campi(indCmp).bytes = gSegm(indseg).Strutture(1).Campi(indCmp - 1).bytes
              .Campi(indCmp).Correzione = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Correzione
              .Campi(indCmp).Decimali = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Decimali
              .Campi(indCmp).IdSegm = gSegm(indseg).Strutture(1).Campi(indCmp).IdSegm
              .Campi(indCmp).Index = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Index
              .Campi(indCmp).livello = gSegm(indseg).Strutture(1).Campi(indCmp - 1).livello
              .Campi(indCmp).lunghezza = gSegm(indseg).Strutture(1).Campi(indCmp - 1).lunghezza
              .Campi(indCmp).Nome = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Nome
              .Campi(indCmp).NomeRed = gSegm(indseg).Strutture(1).Campi(indCmp - 1).NomeRed
              .Campi(indCmp).Occurs = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Occurs
              .Campi(indCmp).segno = gSegm(indseg).Strutture(1).Campi(indCmp - 1).segno
              '.Campi(indcmp).SKey = gSegm(indseg).Strutture(1).Campi(indcmp - 1).SKey
              .Campi(indCmp).Tipo = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Tipo
              .Campi(indCmp).Usage = gSegm(indseg).Strutture(1).Campi(indCmp - 1).Usage
              .Campi(indCmp).valore = gSegm(indseg).Strutture(1).Campi(indCmp - 1).valore
              ' Mauro 15/01/2008
              'If gSegm(indseg).Strutture(1).Campi(indcmp - 1).Occurs Then
                .Campi(indCmp).OccursFlat = gSegm(indseg).Strutture(1).Campi(indCmp - 1).OccursFlat
              'Else
              '  .Campi(indcmp).OccursFlat = ""
              'End If
              .Campi(indCmp).DaMigrare = gSegm(indseg).Strutture(1).Campi(indCmp - 1).DaMigrare
              
              .Campi(indCmp - 1).Ordinale = CampoApp.Ordinale
              .Campi(indCmp - 1).Posizione = CampoApp.Posizione
              .Campi(indCmp - 1).bytes = CampoApp.bytes
              .Campi(indCmp - 1).Correzione = CampoApp.Correzione
              .Campi(indCmp - 1).Decimali = CampoApp.Decimali
              .Campi(indCmp - 1).IdSegm = CampoApp.IdSegm
              .Campi(indCmp - 1).IdStruttura = CampoApp.IdStruttura
              .Campi(indCmp - 1).Index = CampoApp.Index
              .Campi(indCmp - 1).livello = CampoApp.livello
              .Campi(indCmp - 1).lunghezza = CampoApp.lunghezza
              .Campi(indCmp - 1).Nome = CampoApp.Nome
              .Campi(indCmp - 1).NomeRed = CampoApp.NomeRed
              .Campi(indCmp - 1).Occurs = CampoApp.Occurs
              .Campi(indCmp - 1).Ordinale = CampoApp.Ordinale
              .Campi(indCmp - 1).Posizione = CampoApp.Posizione
              .Campi(indCmp - 1).segno = CampoApp.segno
              '.Campi(indcmp - 1).SKey = CampoApp.SKey
              .Campi(indCmp - 1).Tipo = CampoApp.Tipo
              .Campi(indCmp - 1).Usage = CampoApp.Usage
              .Campi(indCmp - 1).valore = CampoApp.valore
              ' Mauro 15/01/2008
              'If CampoApp.Occurs Then
                .Campi(indCmp - 1).OccursFlat = CampoApp.OccursFlat
              'Else
              '  .Campi(indcmp - 1).OccursFlat = ""
              'End If
              .Campi(indCmp - 1).DaMigrare = CampoApp.DaMigrare
            End If
            Ricalcola_Posizione_Lunghezza
            Exit Sub
          End With
        End If
      Next indCmp
    End If
  Next indseg
End Sub

Sub AggiornaLivello()
  Dim indseg As Double
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim k As Integer
  Dim OrdApp As Integer
  For indseg = 1 To UBound(gSegm)
    If Nomenodo = gSegm(indseg).Name Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        k = InStr(1, NomeCmp, " ")
        If k = 0 Then k = Len(NomeCmp)
        With gSegm(indseg).Strutture(1)
          If .Campi(indCmp).Nome = Trim(Mid$(NomeCmp, 1, k)) Then
            If BottonePremuto = 1 Then
              .Campi(indCmp).livello = val(.Campi(indCmp).livello) + 1
              'If .Campi(indcmp - 1).Livello = val(.Campi(indcmp).Livello) - 1 Then
                '.Campi(indcmp).Posizione = val(.Campi(indcmp - 1).Posizione)
                '.Campi(indcmp - 1).Bytes = 0
                '.Campi(indcmp - 1).Lunghezza = 0
              'Else
              '  .Campi(indcmp).Posizione = val(.Campi(indcmp - 1).Posizione) + val(.Campi(indcmp).Bytes)
              'End If
            Else
              .Campi(indCmp).livello = val(.Campi(indCmp).livello) - 1
            End If
             
            Exit Sub
          End If
        End With
      Next indCmp
    End If
  Next indseg
End Sub

Sub InserisciCampo(Segm As Integer, Ordinale As Integer, livello As Integer, Posizione As Integer)
  Dim tb As Recordset
  Dim indCmp As Integer
  
  ReDim Preserve gSegm(Segm).Strutture(1).Campi(UBound(gSegm(Segm).Strutture(1).Campi) + 1)
  With gSegm(Segm).Strutture(1)
    For indCmp = UBound(.Campi) - 1 To Ordinale Step -1
      .Campi(indCmp + 1).Ordinale = .Campi(indCmp).Ordinale + 1
      .Campi(indCmp + 1).bytes = .Campi(indCmp).bytes
      .Campi(indCmp + 1).Decimali = .Campi(indCmp).Decimali
      .Campi(indCmp + 1).Occurs = .Campi(indCmp).Occurs
      .Campi(indCmp + 1).NomeRed = .Campi(indCmp).NomeRed
      .Campi(indCmp + 1).Nome = .Campi(indCmp).Nome
      .Campi(indCmp + 1).livello = .Campi(indCmp).livello
      .Campi(indCmp + 1).lunghezza = .Campi(indCmp).lunghezza
      .Campi(indCmp + 1).Posizione = .Campi(indCmp).Posizione
      .Campi(indCmp + 1).segno = .Campi(indCmp).segno
      .Campi(indCmp + 1).Tipo = .Campi(indCmp).Tipo
      .Campi(indCmp + 1).Usage = .Campi(indCmp).Usage
      .Campi(indCmp + 1).IdSegm = .Campi(indCmp).IdSegm
      .Campi(indCmp + 1).valore = .Campi(indCmp).valore
      ' Mauro 15/01/2008
      .Campi(indCmp + 1).OccursFlat = .Campi(indCmp).OccursFlat
      .Campi(indCmp + 1).DaMigrare = .Campi(indCmp).DaMigrare
    Next indCmp
  End With
  gSegm(Segm).Strutture(1).Campi(Ordinale).bytes = 0
  gSegm(Segm).Strutture(1).Campi(Ordinale).Decimali = 0
  gSegm(Segm).Strutture(1).Campi(Ordinale).Occurs = 0
  gSegm(Segm).Strutture(1).Campi(Ordinale).NomeRed = "NEW"
  gSegm(Segm).Strutture(1).Campi(Ordinale).Nome = "NUOVO CAMPO"
  gSegm(Segm).Strutture(1).Campi(Ordinale).Ordinale = Ordinale
  gSegm(Segm).Strutture(1).Campi(Ordinale).livello = livello
  gSegm(Segm).Strutture(1).Campi(Ordinale).lunghezza = 0
  gSegm(Segm).Strutture(1).Campi(Ordinale).Posizione = Posizione
  gSegm(Segm).Strutture(1).Campi(Ordinale).segno = "N"
  gSegm(Segm).Strutture(1).Campi(Ordinale).Tipo = "X"
  gSegm(Segm).Strutture(1).Campi(Ordinale).Usage = "NEW"
  gSegm(Segm).Strutture(1).Campi(Ordinale).IdSegm = gSegm(Segm).IdSegm
  gSegm(Segm).Strutture(1).Campi(Ordinale).valore = ""
  ' Mauro 15/01/2008
  gSegm(Segm).Strutture(1).Campi(Ordinale).OccursFlat = ""
  gSegm(Segm).Strutture(1).Campi(Ordinale).DaMigrare = True
  
  VisualizzaStruttura
  TxtNCampo.SetFocus
End Sub

Sub SalvaNuovoCampo(Segm As Integer)
  
  Dim tb As Recordset
  Dim indseg As Integer
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim k As Integer
  Dim bool As Boolean
  
'  bool = False
'  If TreeViewMerge.Nodes.Count <> 0 Then
'    If Frame19.Visible = True And TxtSwitchValue = "" Then
'      MsgBox "Insert value!", vbOKOnly
'    Else
'      For indcmp = 1 To UBound(gSegm(Segm).Strutture(1).Campi)
'        K = InStr(1, NomeCmp, "  ")
'        If K = 0 Then K = Len(NomeCmp)
'        If gSegm(Segm).Strutture(1).Campi(indcmp).Nome = Trim(Mid$(NomeCmp, 1, K)) Then
'            Exit For
'        ElseIf indcmp = UBound(gSegm(Segm).Strutture(1).Campi) Then Exit Sub
'        End If
'      Next indcmp
'      With gSegm(Segm).Strutture(1)
'        If Frame19.Visible = True Then .Campi(indcmp).Valore = Trim(UCase(TxtSwitchValue))
'        K = InStr(Combo2.Text, "S")
'        If K > 0 Then
'          .Campi(indcmp).Segno = "S"
'        Else
'          .Campi(indcmp).Segno = "N"
'        End If
'        K = InStr(Combo2.Text, "9")
'        If K > 0 Then
'          .Campi(indcmp).Tipo = "9"
'        Else
'          .Campi(indcmp).Tipo = "X"
'        End If
'
'        K = InStr(Combo2.Text, "COMP")
'        If K > 0 Then
'          If val(txtLen) >= 0 And val(txtLen) <= 5 Then
'            .Campi(indcmp).Bytes = 2
'            bool = True
'          ElseIf val(txtLen) >= 6 And val(txtLen) <= 9 Then
'            .Campi(indcmp).Bytes = 4
'            bool = True
'          ElseIf val(txtLen) > 9 Then
'            .Campi(indcmp).Bytes = 8
'            bool = True
'          End If
'        End If
'        K = InStr(Combo2.Text, "COMP-3")
'        If K > 0 Then
'          .Campi(indcmp).Bytes = Int(val(txtLen) / 2) + 1
'          bool = True
'        End If
'        .Campi(indcmp).Nome = Trim(UCase(TxtNomeCmp))
'        .Campi(indcmp).Lunghezza = txtLen
'        If bool = False Then .Campi(indcmp).Bytes = val(Trim(TxtLunghezza))
'        .Campi(indcmp).Posizione = TxtStart
'        .Campi(indcmp).Occurs = TxtOccurs
'        .Campi(indcmp).Decimali = TxtDecimali
'        .Campi(indcmp).Usage = TipoCampo(Combo2)
'
'        If Frame19.Visible = False Then .Campi(indcmp).Correzione = "M"
'        Frame19.Visible = False
'        VisualizzaStruttura
'      End With
'    End If
'  End If
End Sub

Function CheckRadice(Key As String) As Boolean
  Set nodX = TreeViewMerge.SelectedItem
  risp = 0
  If Key = "C1" Then
    risp = MsgBox("This field is area's root", vbOKOnly + vbSystemModal)
   CheckRadice = False
    Exit Function
  End If
  CheckRadice = True
End Function

'Private Sub TxtOccurs_Change()
'  If Len(TxtOccurs) Then
'    If CInt(TxtOccurs) = 0 Then
'      chkOccursFlat.Value = 0
'      chkOccursFlat.Enabled = False
'    Else
'      chkOccursFlat.Enabled = True
'    End If
'  End If
'End Sub

Private Sub VScroll1_Change()
  On Error Resume Next
  ColoraAreaIncidenza ListField2.SelectedItem.text
End Sub

Sub ControllaChiaveField()
  Dim indseg As Integer, IndFld As Integer, indCmp As Integer, indcmp1 As Integer
  Dim i As Integer
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      If UBound(gSegm(indseg).Strutture) Then
      With gSegm(indseg).Strutture(1)
        For IndFld = 1 To UBound(gSegm(indseg).gField)
          If gSegm(indseg).gField(IndFld).Correzione = "" Then
            For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
              'If gSegm(indseg).gField(indfld).Start = .Campi(indcmp).Posizione And gSegm(indseg).gField(indfld).MaxLen = .Campi(indcmp).Bytes And .Campi(indcmp).Lunghezza <> 0 Then
              If gSegm(indseg).gField(IndFld).Start = .Campi(indCmp).Posizione And gSegm(indseg).gField(IndFld).MaxLen = .Campi(indCmp).bytes And val(.Campi(indCmp).lunghezza) <> 0 Then
                If gSegm(indseg).gField(IndFld).Index = True Then
                  For indcmp1 = 1 To TreeViewMerge.Nodes.count
                    If Trim(Mid$(TreeViewMerge.Nodes(indcmp1).text, 1, InStr(1, TreeViewMerge.Nodes(indcmp1).text, " "))) = gSegm(indseg).Strutture(1).Campi(indCmp).Nome Then
                      TreeViewMerge.Nodes(indcmp1).ForeColor = vbRed
                      TreeViewMerge.Nodes(indcmp1).image = 10
                      Exit For
                    End If
                  Next indcmp1
                End If
              End If
            Next indCmp
          End If
        Next IndFld
      End With
      End If
    End If
  Next indseg
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      'For indcmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        'If gSegm(indseg).Strutture(1).Campi(indcmp).index = True Then
        '  For indcmp1 = 1 To TreeViewMerge.Nodes.Count
        '    If Trim(Mid$(TreeViewMerge.Nodes(indcmp1).text, 1, InStr(1, TreeViewMerge.Nodes(indcmp1).text, " "))) = gSegm(indseg).Strutture(1).Campi(indcmp).nome Then
        '      TreeViewMerge.Nodes(indcmp1).ForeColor = vbRed
        '      Exit For
        '    End If
        '  Next indcmp1
        'End If
      'Next indcmp
    End If
  Next indseg
End Sub

Sub ControllaChiaveImpostata()
  On Error Resume Next
  Dim indseg As Integer
  Dim indCmp As Integer
  If ListStr.ListItems.count = 0 Then Exit Sub
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        If gSegm(indseg).Strutture(1).Campi(indCmp).Index = True Then
          AggiungiTB "DMW0003", "Field " & gSegm(indseg).Strutture(1).Campi(indCmp).Nome & " is the segment's key ", RichTextBox1
        'ElseIf gSegm(indseg).Strutture(1).Campi(indcmp).SKey = True Then
        End If
      Next indCmp
    End If
  Next indseg
End Sub

Sub cancellarelazioni(ogg As Double)
  Dim tbField As Recordset
  Set tbField = m_fun.Open_Recordset("select * from MgRel_DLISeg where idoggetto = " & ogg)
  If tbField.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgRel_DLISeg where idoggetto = " & ogg)
  tbField.Close
End Sub

Public Sub Load_PgmWithDLIAccess_IdxSecondary(Optional ByVal idDbd As Long = -1)
  Dim rs1 As Recordset, rs2 As Recordset, rs3 As Recordset
  Dim wIdDBD As Long
  Dim wOldIdOggetto As Long
  
  Dim wData() As tDL1_PgmIndex_Type2
  Dim wContinue As Boolean
  
  ReDim wData(0)
  wOldIdOggetto = -1
  wIdDBD = idDbd
  lsw2ndIdxAccess.ListItems.Clear
  Screen.MousePointer = vbHourglass
  
  Set rs1 = m_fun.Open_Recordset("Select * From PsDLI_IstrDett_Key Where " & _
                                 "IdField <> 0  and Not isnull(IdField) Order by IdOggetto")
  While Not rs1.EOF
    
    wContinue = False
     'Controlla se il db � quello selezionato
    If idDbd <> -1 Then
      Set rs3 = m_fun.Open_Recordset("Select IdOggetto From PsDLI_Istruzioni Where " & _
                                     "IdOggetto = " & rs1!IdOggetto & " And Riga = " & rs1!Riga & " And IdDbd = " & wIdDBD)
      If rs3.RecordCount Then
        wContinue = True
      Else
        wContinue = False
      End If
      rs3.Close
    Else
      wContinue = True
    End If
    
    If wContinue Then
      'Recupera i field interessati
      Set rs2 = m_fun.Open_Recordset("Select * From PsDLI_Field Where " & _
                                     "IdField = " & rs1!IdField & " And (ptr_xNome <> '' and Not IsNull(ptr_xNome))")
      While Not rs2.EOF
        If rs1!IdOggetto <> wOldIdOggetto Then
          lsw2ndIdxAccess.ListItems.Add , , rs1!IdOggetto
          lsw2ndIdxAccess.ListItems(lsw2ndIdxAccess.ListItems.count).ListSubItems.Add , , m_fun.Restituisci_NomeOgg_Da_IdOggetto(rs1!IdOggetto)
          
          wOldIdOggetto = rs1!IdOggetto
        End If
        rs2.MoveNext
      Wend
      rs2.Close
    End If
    rs1.MoveNext
        
    DoEvents
  Wend
  rs1.Close
  Screen.MousePointer = vbDefault
End Sub

Public Sub AddAreagSeg(IdOggetto As Long, idArea As Long, nomeArea As String)
  Dim seg As Long
  Dim indStr As Integer
  Dim Newindstr As Integer
  Dim AppgSegm() As wSegm
  Dim i As Long
  
  seg = indsegSelected
  
  i = UBound(gSegm(seg).Strutture) + 1
  ReDim Preserve gSegm(seg).Strutture(i)
  gSegm(seg).Strutture(i).StrIdOggetto = IdOggetto
  gSegm(seg).Strutture(i).nomeStruttura = nomeArea
  gSegm(seg).Strutture(i).StrIdArea = idArea
  
  With ListStr.ListItems.Add(, , gSegm(seg).Strutture(indStr).IdOggetto & "/" & gSegm(seg).Strutture(i).StrIdArea, , 2)
    .SubItems(1) = gSegm(seg).Strutture(i).nomeStruttura
    .SubItems(2) = gSegm(seg).Strutture(i).NomeCampo
  End With
End Sub

'SQ 16-02-09
'Public Sub AddArea(idOggetto As Long, idArea As Long, nomeArea As String)
Public Sub AddArea(idSegment As Long, IdOggetto As Long, idArea As Long, nomeArea As String)
  Dim rsLogic As Recordset
 
  addArea_Segmento idSegment, IdOggetto, idArea, nomeArea
  
  '''''''''''''''''''''''''''''''''''''''''''
  ' Gestione DBD logici:
  ' associazione automatica area!
  '''''''''''''''''''''''''''''''''''''''''''
  Set rsLogic = m_fun.Open_Recordset("SELECT idSegmento,ordinale FROM PsDLI_Segmenti_Source WHERE Segmento='" & Nomenodo & "' AND DBD='" & pNome_DBCorrente & "'")
  While Not rsLogic.EOF
    'Chiederlo o stressa solo la vita?
    'msgbox "There are Logical DBD including This segment is the...
    
    addArea_Segmento rsLogic!IdSegmento, IdOggetto, idArea, nomeArea, rsLogic!Ordinale
    
    rsLogic.MoveNext
  Wend
  rsLogic.Close
  
  MadmdM_DLI_Dati.CaricaStruttura

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
' concatLevel: 0 su segmenti fisici
'              1 su primo segmento x DBD logici
'              2 su secondo segmento x DBD logici
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub addArea_Segmento(IdSegmento As Long, IdOggetto As Long, idArea As Long, nomeArea As String, Optional concatLevel As Integer = 0)
  Dim rs As Recordset, rsMg As Recordset, rsPs As Recordset
  Dim i As Long, IdCmp As Long

  'SQ - Verificare se anche per i concat...
  For i = 1 To UBound(gSegm)
    If gSegm(i).IdSegm = IdSegmento Then
      If gSegm(i).Correzione = "I" Then
        Set rs = m_fun.Open_Recordset("select * from MgDLI_segmenti where idSegmento = " & IdSegmento)
          If rs.RecordCount = 0 Then
            MsgBox " Save the Virtual Segment before adding a Structure ", vbInformation, "i-4.Migration"
            Exit Sub
          End If
          rs.Close
        End If
      Exit For
    End If
  Next
  
  Set rs = m_fun.Open_Recordset("SELECT * FROM MgRel_SegAree WHERE idSegmento=" & IdSegmento)
  'SQ - x DBD LOGICI
  'If rs.RecordCount Then
  If rs.RecordCount > 0 And concatLevel = 0 Then
    'una area associata esiste gi�, vado a controllare se � presente tra le aree del segmento
    Set rsPs = m_fun.Open_Recordset("SELECT * FROM PsRel_SegAree WHERE idSegmento=" & IdSegmento & " AND StrIdOggetto=" & IdOggetto & " AND StrIdArea=" & idArea)
    If rsPs.RecordCount Then
  ''    MsgBox " Relation already present! ", vbInformation, "i-4.Migration"
    Else
      'Chiarire 'sta cosa!?
      rsPs.AddNew
      rsPs!IdSegmento = IdSegmento
      rsPs!StrIdOggetto = IdOggetto
      rsPs!StrIdArea = idArea
      rsPs!NomeCmp = nomeArea
    
    End If
    rsPs.Update
    rsPs.Close
  Else
    'MgRel_SegAree non c'� un'area associata
    If concatLevel = 0 Then
      rs.AddNew
      rs!IdSegmento = IdSegmento
      rs!StrIdOggetto = IdOggetto
      rs!StrIdArea = idArea
      rs!NomeCmp = IIf(concatLevel = 2, "C-", "") & nomeArea
      rs!Correzione = "P"
      rs.Update
      
      'MgData_Area:
      Set rsPs = m_fun.Open_Recordset("SELECT * FROM PsData_Area WHERE IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea)
      Set rsMg = m_fun.Open_Recordset("SELECT * FROM MgData_Area WHERE idSegmento = " & IdSegmento)
      If rsMg.RecordCount > 0 Then rsMg.Delete
      'SQ - Boh?!
      Do Until rsPs.EOF
        rsMg.AddNew
        rsMg!IdSegmento = IdSegmento
        rsMg!Nome = rsPs!Nome
        rsMg!livello = rsPs!livello
        rsMg!NomeRed = rsPs!NomeRed
        rsMg!Correzione = " "
        rsMg.Update
        rsPs.MoveNext
      Loop
      rsPs.Close
      rsMg.Close
    Else
      'CONCATENATO
      If rs.RecordCount = 0 Then
        'Primo giro
        rs.AddNew
        rs!IdSegmento = IdSegmento
        rs!StrIdOggetto = IdOggetto
        rs!StrIdArea = idArea
        rs!NomeCmp = IIf(concatLevel = 2, "C-", "") & nomeArea
        rs!Correzione = "P"
        rs.Update
        
        'Data_Area e RelSeg_Aree devono andare di pari passo... ignoro il ciclo usato sotto (chiarire)
        Set rsPs = m_fun.Open_Recordset("select * from PsData_Area where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea)
        Set rsMg = m_fun.Open_Recordset("select * from MgData_Area where idSegmento = " & IdSegmento)
        If rsMg.RecordCount = 0 Then
          rsMg.AddNew
        Else
          'condizione sporca: sovrascrivo
        End If
        rsMg!IdSegmento = IdSegmento
        rsMg!Nome = rsPs!Nome
        rsMg!livello = rsPs!livello
        rsMg!NomeRed = rsPs!NomeRed
        rsMg!Correzione = " "
        rsMg.Update
        rsMg.Close
      Else
        'Concatenati: Area gi� assegnata
        If concatLevel = 2 Then
          '
        Else
          'Sostiture l'area 2 con l'area 1...
          'MgRel_SegAree
          rs!StrIdArea = idArea
          rs!NomeCmp = nomeArea
          rs.Update
          'MgData_Area
          DataMan.DmConnection.Execute "DELETE * FROM MgData_Area where idSegmento=" & IdSegmento
          'Data_Area e RelSeg_Aree devono andare di pari passo... ignoro il ciclo usato sotto (chiarire)
          Set rsPs = m_fun.Open_Recordset("select * from PsData_Area where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea)
          Set rsMg = m_fun.Open_Recordset("select * from MgData_Area where idSegmento = " & IdSegmento)
          If rsMg.RecordCount = 0 Then
            rsMg.AddNew
          Else
            'condizione sporca: sovrascrivo
          End If
          rsMg!IdSegmento = IdSegmento
          rsMg!Nome = rsPs!Nome
          rsMg!livello = rsPs!livello
          rsMg!NomeRed = rsPs!NomeRed
          rsMg!Correzione = " "
          rsMg.Update
          rsMg.Close
          '
          DataMan.DmConnection.Execute "DELETE * FROM MgData_Cmp where idSegmento=" & IdSegmento & " AND idCmp = 1001"  'Campo di gruppo (AREA)
        End If
      End If
    End If
    
    'MgData_Cmp:
    Select Case concatLevel
      Case 2
        'Concatenato DX
        'pulizia eventuale mondezza:
        DataMan.DmConnection.Execute "DELETE * FROM MgData_Cmp where idSegmento=" & IdSegmento & " AND idCmp > 1000"
        'Attenzione: butto via il livello "01" (attenzione: requisito tmp...)
        'Set rsPs = m_fun.Open_Recordset("SELECT * from PsData_Cmp where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea & " AND ordinale > 1")
        Set rsPs = m_fun.Open_Recordset("SELECT * from PsData_Cmp where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea & " AND ordinale > 0 and livello <> 88")
        IdCmp = 1001  'trucco
      Case 1
        'Concatenato SX
        'pulizia eventuale mondezza:
        DataMan.DmConnection.Execute "DELETE * FROM MgData_Cmp where idSegmento=" & IdSegmento & " AND idCmp < 1000"
        Set rsPs = m_fun.Open_Recordset("SELECT * from PsData_Cmp where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea & " and livello <> 88")
        IdCmp = 1
      Case Else
        'Casi normali
        'pulizia eventuale mondezza:
        DataMan.DmConnection.Execute "DELETE * FROM MgData_Cmp where idSegmento=" & IdSegmento
        Set rsPs = m_fun.Open_Recordset("SELECT * from PsData_Cmp where IdOggetto =  " & IdOggetto & " AND IdArea = " & idArea & " and livello <> 88")
        IdCmp = 1
    End Select
    
    Set rsMg = m_fun.Open_Recordset("SELECT * FROM MgData_Cmp where idSegmento = " & IdSegmento)
    For IdCmp = IdCmp To rsPs.RecordCount + IdCmp - 1
      rsMg.AddNew
      rsMg!IdSegmento = IdSegmento
      rsMg!IdCmp = IdCmp
      rsMg!Nome = IIf(concatLevel = 2, "C-", "") & rsPs!Nome
      rsMg!Ordinale = rsPs!Ordinale + IIf(concatLevel = 2, 1000, 0)
      rsMg!livello = rsPs!livello
      rsMg!Tipo = rsPs!Tipo
      rsMg!Posizione = rsPs!Posizione + IIf(concatLevel = 2, 1000, 0) 'TMP! sbagliata... ma serve?
      rsMg!Byte = rsPs!Byte
      rsMg!lunghezza = rsPs!lunghezza
      rsMg!Decimali = rsPs!Decimali
      rsMg!segno = rsPs!segno
      rsMg!Usage = rsPs!Usage
      rsMg!Picture = rsPs!Picture
      rsMg!Occurs = rsPs!Occurs
      rsMg!NomeRed = rsPs!NomeRed
      rsMg!Selettore = False
      rsMg!Selettore_Valore = ""
      rsMg!Correzione = " "
      ' Mauro 15/01/2008
      'rsMg!OccursFlat = rsPs!OccursFlat
      rsMg!OccursFlat = False
      '''silvia 14-3-2008 imposto il valore di default del campo DaMigrare nella MgData_Cmp (deve essere true)
      '''rsMg!DaMigrare = rsPs!DaMigrare
      rsMg!DaMigrare = True
      rsMg.Update
      rsPs.MoveNext
    Next
    rsMg.Close
    rsPs.Close
  End If
  rs.Close
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'SQ. Modificato il 13-06-05 (sostituire Nome con Id su tabella MgData_Area!!!!!!!!!!!!!!!)
'''''''
Public Sub removeArea(IdOggetto As String, idArea As String)
  Dim appIndex As Integer
  
  'Non sarebbero da eliminare le Ps, ma aggiungere il record in MgRel_SegAree con correzione...
  DataMan.DmConnection.Execute ("delete * from PsRel_SegAree where idSegmento = " & indexseg & " AND StrIdOggetto =  " & IdOggetto & " AND StrIdArea = " & idArea)
  DataMan.DmConnection.Execute ("delete * from MgRel_SegAree where idSegmento = " & indexseg & " AND StrIdOggetto =  " & IdOggetto & " AND StrIdArea = " & idArea)
  
  'SQ 5-1-07: in queste tabelle � presente solo l'area associata
  'Fa schifo!!!!!!!!!! non abbiamo un flag decente?!
  If ListStr.SelectedItem.SmallIcon = 12 Then
    DataMan.DmConnection.Execute ("delete * from MgData_Area where idSegmento = " & indexseg)
    DataMan.DmConnection.Execute ("delete * from MgData_Cmp where idSegmento = " & indexseg)
  End If
  appIndex = ListStr.SelectedItem.Index
  ListStr.ListItems.Remove appIndex
  'SQ: tmp
  On Error Resume Next
  ListStr2.ListItems.Remove appIndex
  'SQ
  ListCampi.ListItems.Clear
  Label4 = ""
   
  '?DataMan.DmConnection.Execute ("delete * from MgData_Area where idSegmento = " & indexseg & " AND Nome='" & nomeArea & "'")
  'ATTENZIONE: PER ADESSO NON AGGIORNO LE STRUTTURE IN MEMORIA... BISOGNA RICARICARLE...
   
End Sub


'SQ 16-02-09

Sub importAreaSegments()
  Dim i As Long
  Dim Line As String, dialogFileName As String
  Dim isLabel As Boolean, isOK As Boolean
  Dim cParam As Collection
  
  Dim Riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset, rs1 As Recordset, rs2 As Recordset
  Dim idOggettoC As Long, idOggettoR As Long
  Dim strIns As String
  Dim strPGMnf As String
  Dim strPSBnf As String
  Dim strlog As String
  Dim strlog1 As String
  Dim Componente As String
  Dim count As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Gestione inserimento lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  comDialog.ShowOpen

  'dialogFileName = LoadCommDialog(True, "Select a file", "All Files", "*")
  dialogFileName = comDialog.fileName
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
    Screen.MousePointer = vbHourglass
    m_fun.FnActiveWindowsBool = True
    m_fun.FnProcessRunning = True
       
    If MsgBox("Do you want to insert the list of the " & UBound(objList) & " associations?", vbQuestion + vbYesNo, "Import Segment-Area") = vbYes Then
      For i = 1 To UBound(objList)
        isOK = True
        Riga() = Split(objList(i), " ")
        If UBound(Riga()) >= 2 Then
          'count = count + 1
          m_fun.FnActiveWindowsBool = False
          
          'DBD
          Set rs = m_fun.Open_Recordset("SELECT idOggetto FROM Bs_Oggetti WHERE Nome = '" & Trim(Riga(0)) & "' and tipo='DBD'")
          If rs.RecordCount Then
            idOggettoC = rs!IdOggetto
          Else
            idOggettoC = 0
            strPGMnf = strPGMnf & Riga(0) & " (DBD not found)" & vbCrLf
            isOK = False
          End If
          rs.Close
          
          Dim IdSegmento As Long
          'Segmento
          Set rs = m_fun.Open_Recordset("SELECT idSegmento FROM PsDli_Segmenti WHERE Nome = '" & Trim(Riga(1)) & "' and idOggetto=" & idOggettoC)
          If rs.RecordCount = 0 Then
            'SQ VIRTUALE?
            Set rs = m_fun.Open_Recordset("SELECT idSegmento FROM MgDli_Segmenti WHERE correzione='I' AND Nome = '" & Trim(Riga(1)) & "' and idOggetto=" & idOggettoC)
          End If
          If rs.RecordCount Then
            IdSegmento = rs!IdSegmento
          Else
            IdSegmento = 0
            'sistemare
            strPGMnf = strPGMnf & Riga(1) & " (Segment not found in DBD: " & Riga(0) & ")" & vbCrLf
            isOK = False
          End If
          rs.Close
                    
          'Programma/Copy
          'TMP!!!
          Set rs = m_fun.Open_Recordset("SELECT idOggetto FROM Bs_oggetti WHERE Nome = '" & Trim(Riga(2)) & "' and livello2 like '%copybook%'")
          If rs.RecordCount Then
            idOggettoR = rs!IdOggetto
            'Componente = rs!Nome
          Else
            idOggettoR = 0
            strPSBnf = strPSBnf & Riga(1) & " (PGM: " & Riga(0) & ")" & vbCrLf
            isOK = False
          End If
          rs.Close
          
          'Area
          Dim areaName
          Dim idArea As Long
          Dim idOggettoArea As Long
          
          If UBound(Riga) = 2 Then
            'Si considera la prima area della copy/programma
            Set rs = m_fun.Open_Recordset("SELECT idOggetto,idArea,nome FROM PsData_Cmp WHERE idOggetto=" & idOggettoR & " AND idArea=1")
          Else
            'Ho il nome dell'area
            Set rs = m_fun.Open_Recordset("SELECT idOggetto,idArea,nome FROM PsData_Cmp WHERE idOggetto=" & idOggettoR & " AND Nome = '" & Trim(Riga(3)) & "'")
          End If
          If rs.RecordCount Then
            idOggettoArea = rs!IdOggetto
            areaName = rs!Nome
            idArea = rs!idArea
          Else
            idArea = 0
            strPSBnf = strPSBnf & Riga(1) & " (PGM: " & Riga(0) & ")" & vbCrLf
            isOK = False
          End If
          rs.Close
        
          If isOK Then
            'TILVIA
            Nomenodo = Trim(Riga(1))
            '''
            AddArea IdSegmento, idOggettoArea, idArea, areaName & ""
          End If
        Else
          strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
          'DLLFunzioni.WriteLog "!!!!!!!!!!LIST ERROR: WRONG LINE : " & objList(i)
        End If
                
      Next
      If strPGMnf <> "" Then
        strPGMnf = "---PGM NOT FOUND---" & vbCrLf & strPGMnf
      End If
      If strPSBnf <> "" Then
        strPSBnf = "---PSB NOT FOUND---" & vbCrLf & strPSBnf
      End If
      If strIns <> "" Then
        strIns = "INSERT RELATION PGM - PSB: " & vbCrLf & strIns
      End If
      If strlog1 <> "" Then
        strlog1 = "Relation already exist from PGM - PSB" & vbCrLf & strlog1
      End If
          
      'TILVIA 24112010
      Set rs = m_fun.Open_Recordset("Select Distinct Nome, idoggetto From BS_Oggetti where Tipo_DBD not in ('IND')")
      If rs.RecordCount Then
       Do Until rs.EOF
         Set rs1 = m_fun.Open_Recordset("Select * from PsDli_Segmenti where idoggetto = " & rs!IdOggetto & " and idsegmento not in (select idsegmento from PsRel_SegAree)")
         If rs1.RecordCount Then
           Do Until rs1.EOF
             Set rs2 = m_fun.Open_Recordset("select * from MgDli_Segmenti where IdSegmento = " & rs1!IdSegmento)
             If Not rs2.RecordCount Then
               rs2.AddNew
               rs2!IdSegmento = rs1!IdSegmento
               rs2!IdOggetto = rs1!IdOggetto
               rs2!Nome = rs1!Nome
               rs2!MaxLen = rs1!MaxLen
               rs2!MinLen = rs1!MinLen
               rs2!Parent = rs1!Parent
               rs2!Pointer = rs1!Pointer
               rs2!Rules = rs1!Rules
               rs2!Comprtn = rs1!Comprtn
               rs2!Correzione = "O"
               rs2.Update
             End If
             rs2.Close
             rs1.MoveNext
           Loop
         End If
         rs1.Close
         rs.MoveNext
      Loop
      End If
      rs.Close
      Screen.MousePointer = vbDefault
      m_fun.Show_ShowText strPGMnf & vbCrLf & strPSBnf & vbCrLf & strIns & vbCrLf & strlog1 & vbCrLf & strlog, "DBD Import Log"
      m_fun.FnActiveWindowsBool = True
      'Screen.MousePointer = vbDefault
      
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      
    End If
    Screen.MousePointer = vbDefault
  End If
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Import PSB", "InsertPSB_from_List", ERR.Number, ERR.Description, True
End Sub

