VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form MadmdF_VSAM 
   Appearance      =   0  'Flat
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Files Tools"
   ClientHeight    =   6675
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   9405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab SSTab1 
      Height          =   6525
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   11509
      _Version        =   393216
      TabOrientation  =   1
      TabHeight       =   520
      TabCaption(0)   =   "General Information"
      TabPicture(0)   =   "MadmdF_VSAM.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Areas Details"
      TabPicture(1)   =   "MadmdF_VSAM.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame8"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Structure Details"
      TabPicture(2)   =   "MadmdF_VSAM.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "LblModif"
      Tab(2).Control(1)=   "Toolbar1"
      Tab(2).Control(2)=   "imlToolbarIcons"
      Tab(2).Control(3)=   "LstFldEditVSAM"
      Tab(2).Control(4)=   "Frame7"
      Tab(2).ControlCount=   5
      Begin VB.Frame Frame8 
         Caption         =   "Record Types"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   5955
         Left            =   -74940
         TabIndex        =   69
         Top             =   120
         Width           =   3405
         Begin VB.TextBox txtCondition 
            Height          =   285
            Left            =   960
            TabIndex        =   84
            Top             =   5520
            Width           =   2295
         End
         Begin VB.CommandButton cmdSaveAcronym 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2760
            Picture         =   "MadmdF_VSAM.frx":0054
            Style           =   1  'Graphical
            TabIndex        =   79
            ToolTipText     =   "Save Changes"
            Top             =   5160
            Width           =   465
         End
         Begin VB.TextBox txtNameRT 
            Height          =   285
            Left            =   960
            TabIndex        =   75
            Top             =   4800
            Width           =   2295
         End
         Begin VB.TextBox txtAcronym 
            Height          =   285
            Left            =   960
            TabIndex        =   74
            Top             =   5160
            Width           =   1755
         End
         Begin VB.CommandButton cmdSaveRT 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   410
            Left            =   2790
            Picture         =   "MadmdF_VSAM.frx":019E
            Style           =   1  'Graphical
            TabIndex        =   73
            ToolTipText     =   "Save Changes"
            Top             =   1320
            Width           =   465
         End
         Begin VB.CommandButton cmdDelRT 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   2790
            Picture         =   "MadmdF_VSAM.frx":02E8
            Style           =   1  'Graphical
            TabIndex        =   71
            ToolTipText     =   "Delete Record Type"
            Top             =   720
            Width           =   465
         End
         Begin VB.CommandButton cmdAddRT 
            Height          =   405
            Left            =   2790
            Picture         =   "MadmdF_VSAM.frx":0432
            Style           =   1  'Graphical
            TabIndex        =   70
            ToolTipText     =   "Create new Record Type"
            Top             =   300
            Width           =   465
         End
         Begin MSComctlLib.ListView lswRecordType 
            Height          =   4425
            Left            =   120
            TabIndex        =   72
            Top             =   240
            Width           =   2595
            _ExtentX        =   4577
            _ExtentY        =   7805
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   0
            BackColor       =   16777215
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "RECT"
               Text            =   "Name"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "DES"
               Text            =   "Acronym"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Condition"
               Object.Width           =   0
            EndProperty
         End
         Begin VB.Label Label22 
            Caption         =   "Condition"
            Height          =   195
            Left            =   60
            TabIndex        =   85
            Top             =   5580
            Width           =   855
         End
         Begin VB.Label Label18 
            Caption         =   "Name"
            Height          =   195
            Left            =   60
            TabIndex        =   77
            Top             =   4860
            Width           =   855
         End
         Begin VB.Label Label17 
            Caption         =   "Acronym"
            Height          =   195
            Left            =   60
            TabIndex        =   76
            Top             =   5220
            Width           =   855
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Field's Details"
         ForeColor       =   &H8000000D&
         Height          =   1455
         Left            =   -74880
         TabIndex        =   45
         Top             =   4620
         Width           =   8925
         Begin VB.CheckBox chkDaMigrare 
            Alignment       =   1  'Right Justify
            Caption         =   "To Migrate"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   7020
            TabIndex        =   83
            Top             =   1020
            Width           =   1095
         End
         Begin VB.CheckBox chkOccursFlat 
            Alignment       =   1  'Right Justify
            Caption         =   "Occurs Flattened"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   6600
            TabIndex        =   82
            Top             =   660
            Width           =   1515
         End
         Begin VB.TextBox TxtSelTemplate 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   4500
            TabIndex        =   80
            Top             =   1020
            Width           =   1965
         End
         Begin VB.CommandButton CmdFldOk 
            Height          =   345
            Left            =   7620
            Picture         =   "MadmdF_VSAM.frx":16A4
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   180
            Width           =   1155
         End
         Begin VB.TextBox TxTDec 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   3570
            TabIndex        =   52
            Top             =   600
            Width           =   765
         End
         Begin VB.TextBox TxtLen 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   3570
            TabIndex        =   51
            Top             =   240
            Width           =   765
         End
         Begin VB.ComboBox CmbType 
            BackColor       =   &H00FFFFC0&
            Height          =   315
            ItemData        =   "MadmdF_VSAM.frx":1C2E
            Left            =   1620
            List            =   "MadmdF_VSAM.frx":1C41
            TabIndex        =   50
            Top             =   240
            Width           =   1395
         End
         Begin VB.TextBox TxtNomeRed 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   900
            TabIndex        =   49
            Top             =   1020
            Width           =   2145
         End
         Begin VB.TextBox TxtOccurs 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   5880
            TabIndex        =   48
            Top             =   540
            Width           =   555
         End
         Begin VB.TextBox TxtNome 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   630
            TabIndex        =   47
            Top             =   600
            Width           =   2385
         End
         Begin VB.TextBox TxtLiv 
            BackColor       =   &H00FFFFC0&
            Height          =   345
            Left            =   630
            TabIndex        =   46
            Top             =   240
            Width           =   465
         End
         Begin VB.Label Label20 
            Alignment       =   1  'Right Justify
            Caption         =   "Selector Template"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   3120
            TabIndex        =   81
            Top             =   1080
            Width           =   1305
         End
         Begin VB.Label Label23 
            Alignment       =   1  'Right Justify
            Caption         =   "Dec"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   3000
            TabIndex        =   60
            Top             =   630
            Width           =   465
         End
         Begin VB.Label Label21 
            Alignment       =   1  'Right Justify
            Caption         =   "Len"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   3000
            TabIndex        =   59
            Top             =   300
            Width           =   465
         End
         Begin VB.Label Label19 
            Alignment       =   1  'Right Justify
            Caption         =   "Name Red"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   60
            TabIndex        =   58
            Top             =   1080
            Width           =   795
         End
         Begin VB.Label Label15 
            Alignment       =   1  'Right Justify
            Caption         =   "Occurs"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   5040
            TabIndex        =   57
            Top             =   600
            Width           =   675
         End
         Begin VB.Label Label14 
            Caption         =   "Type"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   1170
            TabIndex        =   56
            Top             =   300
            Width           =   405
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            Caption         =   "Name"
            ForeColor       =   &H8000000D&
            Height          =   165
            Left            =   90
            TabIndex        =   55
            Top             =   660
            Width           =   465
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Level"
            ForeColor       =   &H8000000D&
            Height          =   165
            Index           =   4
            Left            =   90
            TabIndex        =   54
            Top             =   300
            Width           =   465
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Keys"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   3615
         Left            =   5460
         TabIndex        =   34
         Top             =   120
         Width           =   3645
         Begin VB.ComboBox CmbPrimary 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            ItemData        =   "MadmdF_VSAM.frx":1C72
            Left            =   2190
            List            =   "MadmdF_VSAM.frx":1C7C
            Style           =   2  'Dropdown List
            TabIndex        =   67
            Top             =   3060
            Width           =   705
         End
         Begin VB.CommandButton CmdDelkey 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   3060
            Picture         =   "MadmdF_VSAM.frx":1C89
            Style           =   1  'Graphical
            TabIndex        =   65
            ToolTipText     =   "Remove Key"
            Top             =   1140
            Width           =   465
         End
         Begin VB.CommandButton CmdAddkey 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   410
            Left            =   3060
            Picture         =   "MadmdF_VSAM.frx":1DD3
            Style           =   1  'Graphical
            TabIndex        =   64
            ToolTipText     =   "Add Key"
            Top             =   2940
            Width           =   465
         End
         Begin VB.CommandButton CmdIkey 
            Height          =   410
            Left            =   3060
            Picture         =   "MadmdF_VSAM.frx":1F1D
            Style           =   1  'Graphical
            TabIndex        =   63
            ToolTipText     =   "Associate as Primary Key"
            Top             =   300
            Width           =   465
         End
         Begin VB.CommandButton CmdIIkey 
            Height          =   405
            Left            =   3060
            Picture         =   "MadmdF_VSAM.frx":2067
            Style           =   1  'Graphical
            TabIndex        =   62
            ToolTipText     =   "Associate as Secondary Key"
            Top             =   720
            Width           =   465
         End
         Begin VB.TextBox TxtKLen 
            BackColor       =   &H00FFFFC0&
            Height          =   285
            Left            =   570
            TabIndex        =   37
            Top             =   3060
            Width           =   615
         End
         Begin VB.TextBox TxtKPos 
            BackColor       =   &H00FFFFC0&
            Height          =   285
            Left            =   2190
            TabIndex        =   36
            Top             =   2670
            Width           =   675
         End
         Begin VB.TextBox TxtKName 
            BackColor       =   &H00FFFFC0&
            Height          =   315
            Left            =   570
            TabIndex        =   35
            Top             =   2640
            Width           =   1125
         End
         Begin MSComctlLib.ListView LswKeys 
            Height          =   2265
            Left            =   120
            TabIndex        =   38
            Top             =   270
            Width           =   2895
            _ExtentX        =   5106
            _ExtentY        =   3995
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Name"
               Object.Width           =   2822
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Kpos"
               Object.Width           =   1236
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "KLen"
               Object.Width           =   1236
            EndProperty
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Primary Key"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   1230
            TabIndex        =   66
            Top             =   3090
            Width           =   885
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Caption         =   "Len"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   120
            TabIndex        =   41
            Top             =   3090
            Width           =   345
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Pos"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   1770
            TabIndex        =   40
            Top             =   2730
            Width           =   345
         End
         Begin VB.Label Label2 
            Caption         =   "Name"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   90
            TabIndex        =   39
            Top             =   2730
            Width           =   465
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Used Cobol Areas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   6015
         Left            =   -71460
         TabIndex        =   28
         Top             =   60
         Width           =   5565
         Begin VB.CommandButton CmdChek 
            Height          =   405
            Left            =   4950
            Picture         =   "MadmdF_VSAM.frx":21B1
            Style           =   1  'Graphical
            TabIndex        =   32
            ToolTipText     =   "Associate/Unassociate Area to segment"
            Top             =   300
            Width           =   525
         End
         Begin VB.CommandButton CmdAreaPrim 
            Height          =   405
            Left            =   4950
            Picture         =   "MadmdF_VSAM.frx":22FB
            Style           =   1  'Graphical
            TabIndex        =   31
            ToolTipText     =   "Associate/DissociatePrimary Area"
            Top             =   720
            Width           =   525
         End
         Begin VB.CommandButton cmdAddArea 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   4950
            Picture         =   "MadmdF_VSAM.frx":2445
            Style           =   1  'Graphical
            TabIndex        =   30
            ToolTipText     =   "Add association"
            Top             =   1140
            Width           =   525
         End
         Begin VB.CommandButton cmdRemoveArea 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   4950
            Picture         =   "MadmdF_VSAM.frx":36B7
            Style           =   1  'Graphical
            TabIndex        =   29
            ToolTipText     =   "Remove association"
            Top             =   1590
            Width           =   525
         End
         Begin MSComctlLib.ListView ListCampi 
            Height          =   3165
            Left            =   60
            TabIndex        =   33
            Top             =   2700
            Width           =   4785
            _ExtentX        =   8440
            _ExtentY        =   5583
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Livello"
               Text            =   "Level"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "NCampo"
               Text            =   "Name"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "Bytes"
               Text            =   "Bytes"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "Lunghezza"
               Text            =   "Length"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "Posizione"
               Text            =   "Start"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Key             =   "Tipo"
               Text            =   "Type"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Key             =   "Occurs"
               Text            =   "Occurs"
               Object.Width           =   1058
            EndProperty
         End
         Begin MSComctlLib.ListView LswArea 
            Height          =   2145
            Left            =   60
            TabIndex        =   42
            Top             =   270
            Width           =   4755
            _ExtentX        =   8387
            _ExtentY        =   3784
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Object"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Type"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Area"
               Object.Width           =   3528
            EndProperty
         End
         Begin VB.Label Label12 
            Caption         =   "Area Details"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   195
            Left            =   120
            TabIndex        =   78
            Top             =   2520
            Width           =   4635
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "JCL - Information"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2295
         Left            =   90
         TabIndex        =   26
         Top             =   3810
         Width           =   9015
         Begin MSComctlLib.ListView LswJcl 
            Height          =   1875
            Left            =   90
            TabIndex        =   27
            Top             =   300
            Width           =   8805
            _ExtentX        =   15531
            _ExtentY        =   3307
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   -2147483624
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Object"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Type"
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "nStep"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Program"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Link"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Text            =   "Use"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "File Information"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   3615
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   5235
         Begin VB.TextBox txtAcronymVSAM 
            BackColor       =   &H00FFFFC0&
            Height          =   285
            Left            =   1260
            TabIndex        =   88
            Top             =   1200
            Width           =   2625
         End
         Begin VB.TextBox txtNameVSAM 
            BackColor       =   &H00FFFFC0&
            Height          =   285
            Left            =   1260
            TabIndex        =   86
            Top             =   780
            Width           =   2625
         End
         Begin VB.CommandButton CmdSalva 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   410
            Left            =   4320
            Picture         =   "MadmdF_VSAM.frx":3801
            Style           =   1  'Graphical
            TabIndex        =   68
            ToolTipText     =   "Save Changes"
            Top             =   960
            Width           =   465
         End
         Begin VB.TextBox TxtName 
            BackColor       =   &H00FFFFC0&
            Height          =   315
            Left            =   1260
            TabIndex        =   24
            Top             =   330
            Width           =   3825
         End
         Begin VB.Frame Frame5 
            Caption         =   "Type"
            ForeColor       =   &H00000080&
            Height          =   1935
            Left            =   3720
            TabIndex        =   17
            Top             =   1560
            Width           =   1425
            Begin VB.OptionButton OptESDS 
               Caption         =   "ESDS"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   150
               TabIndex        =   23
               Top             =   300
               Width           =   1185
            End
            Begin VB.OptionButton OptKSDS 
               Caption         =   "KSDS"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   150
               TabIndex        =   22
               Top             =   540
               Width           =   1185
            End
            Begin VB.OptionButton OptRRDS 
               Caption         =   "RRDS"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   150
               TabIndex        =   21
               Top             =   780
               Width           =   1185
            End
            Begin VB.OptionButton OptSeq 
               Caption         =   "Sequential"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   150
               TabIndex        =   20
               Top             =   1020
               Width           =   1185
            End
            Begin VB.CheckBox ChkGDG 
               Alignment       =   1  'Right Justify
               Caption         =   "GDG"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   420
               TabIndex        =   19
               Top             =   1590
               Width           =   765
            End
            Begin VB.OptionButton Option1 
               Caption         =   "Unknown"
               ForeColor       =   &H00FF0000&
               Height          =   285
               Left            =   150
               TabIndex        =   18
               Top             =   1260
               Value           =   -1  'True
               Width           =   1185
            End
         End
         Begin VB.Frame Frame6 
            Height          =   1935
            Left            =   60
            TabIndex        =   2
            Top             =   1560
            Width           =   3585
            Begin VB.TextBox TxtMinLen 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   1290
               TabIndex        =   9
               Top             =   150
               Width           =   825
            End
            Begin VB.TextBox TxtMaxLen 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   2880
               TabIndex        =   8
               Top             =   180
               Width           =   615
            End
            Begin VB.TextBox TxtKeyPos 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   1290
               TabIndex        =   7
               Top             =   570
               Width           =   825
            End
            Begin VB.TextBox TxtKeyLen 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   2880
               TabIndex        =   6
               Top             =   570
               Width           =   615
            End
            Begin VB.TextBox TxtRS 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   2880
               TabIndex        =   5
               Top             =   990
               Width           =   615
            End
            Begin VB.TextBox TxtRF 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   1290
               TabIndex        =   4
               Top             =   990
               Width           =   825
            End
            Begin VB.TextBox txtTag 
               BackColor       =   &H00FFFFC0&
               Height          =   285
               Left            =   900
               TabIndex        =   3
               Top             =   1410
               Width           =   2625
            End
            Begin VB.Label Label5 
               Alignment       =   1  'Right Justify
               Caption         =   "MinLen"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   600
               TabIndex        =   16
               Top             =   210
               Width           =   555
            End
            Begin VB.Label Label6 
               Alignment       =   1  'Right Justify
               Caption         =   "MaxLen"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   2160
               TabIndex        =   15
               Top             =   240
               Width           =   615
            End
            Begin VB.Label Label7 
               Alignment       =   1  'Right Justify
               Caption         =   "KeyPos"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   600
               TabIndex        =   14
               Top             =   630
               Width           =   555
            End
            Begin VB.Label Label8 
               Alignment       =   1  'Right Justify
               Caption         =   "KeyLen"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   2160
               TabIndex        =   13
               Top             =   630
               Width           =   615
            End
            Begin VB.Label Label9 
               Alignment       =   1  'Right Justify
               Caption         =   "RecSize"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   2160
               TabIndex        =   12
               Top             =   1020
               Width           =   615
            End
            Begin VB.Label Label10 
               Alignment       =   1  'Right Justify
               Caption         =   "RecordFormat"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   150
               TabIndex        =   11
               Top             =   1020
               Width           =   1005
            End
            Begin VB.Label Label11 
               Alignment       =   1  'Right Justify
               Caption         =   "Tag"
               ForeColor       =   &H00FF0000&
               Height          =   225
               Left            =   270
               TabIndex        =   10
               Top             =   1440
               Width           =   555
            End
         End
         Begin VB.Label Label26 
            Alignment       =   1  'Right Justify
            Caption         =   "Acronym"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   420
            TabIndex        =   89
            Top             =   1200
            Width           =   735
         End
         Begin VB.Label Label25 
            Alignment       =   1  'Right Justify
            Caption         =   "VSAM Name"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Left            =   120
            TabIndex        =   87
            Top             =   780
            Width           =   1035
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Name"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Index           =   0
            Left            =   510
            TabIndex        =   25
            Top             =   360
            Width           =   645
         End
      End
      Begin MSComctlLib.ListView LstFldEditVSAM 
         Height          =   4005
         Left            =   -74880
         TabIndex        =   43
         Top             =   600
         Width           =   8985
         _ExtentX        =   15849
         _ExtentY        =   7064
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "Livello"
            Text            =   "Lv."
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "Ordinale"
            Text            =   "Ord."
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "NCampo"
            Text            =   "Nome campo"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "Bytes"
            Text            =   "Bytes"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "Lunghezza"
            Text            =   "Len."
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Text            =   "Dec."
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "Posizione"
            Text            =   "Start"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Key             =   "Tipo"
            Text            =   "Type"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "Occurs"
            Text            =   "Occurs"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Key             =   "OCCFLAT"
            Text            =   "Flattened"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Redefines"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Selector Template"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "To Migrate"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ImageList imlToolbarIcons 
         Left            =   -69420
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   11
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":394B
               Key             =   "New"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3A5D
               Key             =   "Copy"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3B6F
               Key             =   "Paste"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3C81
               Key             =   "Cut"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3D93
               Key             =   "Open"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3EA5
               Key             =   "Save"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":3FB7
               Key             =   "Help"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":40C9
               Key             =   "Close"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":4225
               Key             =   "Check"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":4685
               Key             =   "Up"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MadmdF_VSAM.frx":49A1
               Key             =   "Down"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   390
         Left            =   -74880
         TabIndex        =   61
         Tag             =   "fixed"
         Top             =   120
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "imlToolbarIcons"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   15
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "New"
               Object.ToolTipText     =   "New"
               ImageKey        =   "New"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Copy"
               Object.ToolTipText     =   "Copy"
               ImageKey        =   "Copy"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Paste"
               Object.ToolTipText     =   "Paste"
               ImageKey        =   "Paste"
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Cut"
               Object.ToolTipText     =   "Cut"
               ImageKey        =   "Cut"
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Up"
               Object.ToolTipText     =   "Up"
               ImageKey        =   "Up"
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Down"
               Object.ToolTipText     =   "Down"
               ImageKey        =   "Down"
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Open"
               Object.ToolTipText     =   "Open"
               ImageKey        =   "Open"
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Save"
               Object.ToolTipText     =   "Save"
               ImageKey        =   "Save"
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Check"
               Object.ToolTipText     =   "Check Area"
               ImageKey        =   "Check"
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Key             =   "Help"
               Object.ToolTipText     =   "Help"
               ImageKey        =   "Help"
            EndProperty
            BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
         EndProperty
      End
      Begin VB.Label LblModif 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   375
         Left            =   -68700
         TabIndex        =   44
         Top             =   120
         Width           =   2775
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8910
      Top             =   750
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":4CBD
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":4E17
            Key             =   "Edit"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":4F71
            Key             =   "Exit"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   8880
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":6CAB
            Key             =   "SegVero"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":7103
            Key             =   "SegVirt"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":755B
            Key             =   "ElemOk"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":79AF
            Key             =   "ElemKO"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":7E03
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":7F5D
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":83B1
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":8811
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":8C6D
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":8DC9
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":8F25
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_VSAM.frx":9377
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MadmdF_VSAM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SwMod As Boolean
Dim IndSel As Integer
Dim IndSelC As Integer
Dim varVSAM As String
Dim varIdArea As Long
Dim varIdOggetto As Long

Private Sub cmdAddArea_Click()
  MadmdF_AddArea.isVSAM = True
  MadmdF_AddArea.Show vbModal
End Sub

Public Function AddArea(varIdOggetto As Long, varIdArea As Long, varNomeArea As String)
  Dim rsAdd As Recordset, rsVSAM As Recordset
  Dim rs As Recordset, rsDMVSM As Recordset
  Dim i As Long

  Set rsVSAM = m_fun.Open_Recordset("select * from DMVSM_Archivio where idarchivio = " & pIndex_DBCorrente)
  If rsVSAM.RecordCount Then
    Set rsAdd = m_fun.Open_Recordset("select * from dmvsm_tracciato where " & _
                                     "vsam = '" & rsVSAM!VSAM & "' and IdOggetto =" & varIdOggetto & " and " & _
                                     "IdArea =" & varIdArea)
    If rsAdd.RecordCount = 0 Then
      rsAdd.AddNew
      rsAdd!VSAM = rsVSAM!VSAM
      rsAdd!Ordinale = getId("Ordinale", "DMVSM_tracciato") ' � sbagliato!!!!!!!!!
      rsAdd!IdOggetto = varIdOggetto
      rsAdd!idArea = varIdArea
      rsAdd!Tag = "Added"
      rsAdd.Update

      ' Update DMVSM_Data_Cmp
      DataMan.DmConnection.Execute ("delete * from DMVSM_Data_Cmp where " & _
                                    "vsam = '" & rsVSAM!VSAM & "' and idoggetto = " & varIdOggetto & _
                                    " and idarea = " & varIdArea)
      'Set rs = m_fun.Open_Recordset("select max(idcmp) from DMVSM_Data_Cmp")
      'If IsNull(rs.fields(0)) Then
      '  i = 0
      'Else
      '  i = rs.fields(0)
      'End If
      'rs.Close
      i = getId("idcmp", "DMVSM_Data_Cmp") - 1
      Set rs = m_fun.Open_Recordset("select * from PSData_Cmp where " & _
                                    "Idoggetto = " & varIdOggetto & " and IdArea = " & varIdArea)
      Set rsDMVSM = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp")
      Do Until rs.EOF
        'completare
        i = i + 1
        rsDMVSM.AddNew
        rsDMVSM!IdCmp = i
        rsDMVSM!VSAM = rsVSAM!VSAM
        rsDMVSM!IdOggetto = rs!IdOggetto
        rsDMVSM!idArea = rs!idArea
        rsDMVSM!Ordinale = rs!Ordinale
        rsDMVSM!Nome = rs!Nome
        rsDMVSM!livello = rs!livello
        rsDMVSM!Tipo = rs!Tipo
        rsDMVSM!Posizione = rs!Posizione
        rsDMVSM!Byte = rs!Byte
        rsDMVSM!lunghezza = rs!lunghezza
        rsDMVSM!Decimali = rs!Decimali
        rsDMVSM!segno = rs!segno
        rsDMVSM!Usage = rs!Usage
        rsDMVSM!Occurs = rs!Occurs
        rsDMVSM!OccursFlat = False
        rsDMVSM!NomeRed = rs!NomeRed
        rsDMVSM!condizionered = ""
        rsDMVSM!DaMigrare = True

        rsDMVSM.Update
        rs.MoveNext
      Loop
      rsDMVSM.Close
      rs.Close
    Else
      MsgBox "The record is already existing, the new record can not be saved", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
    End If
    rsAdd.Close
  End If
  rsVSAM.Close
  refreshLswArea
End Function
  
' Mauro 05/09/2007 : Nuova gestione delle aree con Record Type
Public Sub refreshLswRecordType()
  Dim tbRecordT As Recordset, tbArea As Recordset, tbVSAM As Recordset
  Dim listRT As ListItem

  lswRecordType.ListItems.Clear
  Set tbVSAM = m_fun.Open_Recordset("select vsam from DMVSM_archivio where idarchivio = " & pIndex_DBCorrente)
  If tbVSAM.RecordCount Then
    Set tbRecordT = m_fun.Open_Recordset("select * from DMVSM_RecordType where " & _
                                         "vsam = '" & tbVSAM!VSAM & "' order by idrecordtype")
    Do Until tbRecordT.EOF
      Set listRT = lswRecordType.ListItems.Add(, tbRecordT!VSAM & "/" & tbRecordT!IdRecordType, tbRecordT!Nome)
      listRT.Tag = ""
      If tbRecordT!idArea Then
        Set tbArea = m_fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                          "vsam = '" & tbRecordT!VSAM & "' and idarea = " & tbRecordT!idArea & _
                                          " and idoggetto = " & tbRecordT!IdOggetto)
        If tbArea.RecordCount Then
          listRT.Tag = tbArea!IdOggetto & "/" & tbArea!idArea & "/" & tbArea!VSAM
        End If
      End If
      listRT.SubItems(1) = tbRecordT!Acronimo & ""
      listRT.SubItems(2) = tbRecordT!Condizione & ""
      tbRecordT.MoveNext
    Loop
    tbRecordT.Close
  End If
  tbVSAM.Close
End Sub

Public Sub refreshLswArea()
  Dim Tba As Recordset, Tbo As Recordset
  Dim tbVSAM As Recordset
  Dim wNome As String, wTipo As String
  Dim wNomeArea As String
  
  LswArea.ListItems.Clear
  Set tbVSAM = m_fun.Open_Recordset("select vsam from DMVSM_Archivio where idarchivio = " & pIndex_DBCorrente)
  If Len(tbVSAM!VSAM & "") Then
    Set Tba = m_fun.Open_Recordset("select * from DMVSM_Tracciato where vsam = '" & tbVSAM!VSAM & "" & "'")
    Do Until Tba.EOF
      Set Tbo = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & Tba!IdOggetto)
      If Tbo.RecordCount Then
        wNome = Tbo!Nome
        wTipo = Tbo!Tipo
      End If
      Tbo.Close
      Set Tbo = m_fun.Open_Recordset("select * from psdata_area where " & _
                                     "idoggetto = " & Tba!IdOggetto & " and idarea = " & Tba!idArea)
      If Tbo.RecordCount Then
        wNomeArea = Trim(Tbo!Nome)
      End If
      Tbo.Close
      LswArea.SmallIcons = ImageList2
      With LswArea.ListItems.Add(, , wNome)
        .Tag = Tba!IdOggetto & "/" & Tba!idArea & "/" & Tba!VSAM
        .SubItems(1) = wTipo
        .SubItems(2) = wNomeArea 'Nome Area
''        If Tba!associata Then
''          .SmallIcon = 12
''        Else
          .SmallIcon = 3
''        End If
      End With
      Tba.MoveNext
    Loop
    Tba.Close
  End If
  tbVSAM.Close
  
  If LswArea.ListItems.Count Then
    LswArea_Click
  End If
  
  If lswRecordType.ListItems.Count Then
    lswRecordType_Click
  End If
End Sub

Public Sub refreshLswCampi()
  Dim a As Long
  Dim ids() As String

  For a = 1 To LswArea.ListItems.Count
    ids = Split(LswArea.ListItems(a).Tag, "/")
    aggiorna_Data_Cmp Int(ids(0)), Int(ids(1)), ids(2)
  Next a
End Sub


Private Sub CmdAddkey_Click()
  Dim rsAssociata As Recordset
  Dim ids() As String
  Dim i As Integer
  Dim boolAssocia As Boolean
  
  On Error GoTo ErrorHandler
  boolAssocia = True
  If LswKeys.ListItems.Count And CmbPrimary.ListIndex = 0 Then
    For i = 1 To LswArea.ListItems.Count
      If LswKeys.ListItems(i).SmallIcon = 5 Then '5 primaria
        boolAssocia = False
        Exit For
      End If
    Next i
  End If
   
  If Not (IsNumeric(TxtKLen.text) And IsNumeric(TxtKPos.text)) Then
    MsgBox "Insert numeric values for the fields Pos and Len", vbInformation, DataMan.DmNomeProdotto
    Exit Sub
  End If
   
  If boolAssocia Then
    Set rsAssociata = m_fun.Open_Recordset("select * from DMVSM_chiavi where idarchivio = " & pIndex_DBCorrente)
    If rsAssociata.RecordCount = 0 Or CmbPrimary.ListIndex = 1 Then
      rsAssociata.AddNew
      rsAssociata!Nome = TxtKName
      rsAssociata!keyStart = TxtKPos
      rsAssociata!keyLen = TxtKLen
      rsAssociata!idArchivio = pIndex_DBCorrente
      rsAssociata!IdKey = getId("IdKey", "DMVSM_Chiavi")
      rsAssociata!Primaria = IIf(CmbPrimary.ListIndex = 0, True, False)
      rsAssociata!Data_Creazione = Now
      rsAssociata!Data_Modifica = Now ' a cosa serve se non � possibile modificare la chiave?!?!
      rsAssociata.Update
    Else
      MsgBox "The Primary Key is already existing, the new key can not be saved", vbExclamation, DataMan.DmNomeProdotto
    End If
    rsAssociata.Close
  Else
    MsgBox "The Primary Key is already existing, the new key can not be saved", vbExclamation, DataMan.DmNomeProdotto
  End If
  refreshLswKeys
  
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " - " & ERR.Description, , DataMan.DmNomeProdotto
End Sub

Private Sub cmdAddRT_Click()
  Dim tbRecordT As Recordset
  Dim listRT As ListItem

  Set tbRecordT = m_fun.Open_Recordset("select * from DMVSM_RecordType")
  ' Se non c'� nessun Record Type, ne creo uno di default
  tbRecordT.AddNew
  tbRecordT!VSAM = UCase(txtNameVSAM) & ""
  tbRecordT!IdRecordType = getId("idRecordType", "DMVSM_RecordType")
  tbRecordT!Nome = "Record Type"
  tbRecordT!Acronimo = "RecType"
  tbRecordT!idArea = 0
  tbRecordT!IdOggetto = 0
  tbRecordT!Condizione = ""
  tbRecordT.Update
  
  Set listRT = lswRecordType.ListItems.Add(, UCase(txtNameVSAM) & "" & "/" & tbRecordT!IdRecordType, tbRecordT!Nome)
  listRT.SubItems(1) = tbRecordT!Acronimo & ""
  listRT.SubItems(2) = tbRecordT!Condizione & ""
End Sub

Private Sub CmdAreaPrim_Click()
  Dim i As Long
  Dim isGiaAssociata As Boolean

  For i = 1 To LswArea.ListItems.Count
    If LswArea.ListItems(i).SmallIcon = 12 And _
       LswArea.ListItems(i).Tag <> LswArea.SelectedItem.Tag Then
      isGiaAssociata = True
      Exit For
    End If
  Next i
  If LswArea.ListItems.Count Then
    If isGiaAssociata Then
      MsgBox "The associated area is already existing!", vbOKOnly + vbInformation, DataMan.DmNomeProdotto
    Else
      lswRecordType.SelectedItem.Tag = LswArea.SelectedItem.Tag
      LswArea.SelectedItem.SmallIcon = 12
    End If
  End If
End Sub

Private Sub CmdChek_Click()
  Dim i As Long
  Dim isAssociata As Boolean

  For i = 1 To LswArea.ListItems.Count
    If LswArea.ListItems(i).SmallIcon = 12 And _
       LswArea.ListItems(i).Tag = LswArea.SelectedItem.Tag Then
      isAssociata = True
      Exit For
    End If
  Next i
  If isAssociata Then
    lswRecordType.SelectedItem.Tag = ""
    LswArea.SelectedItem.SmallIcon = 3
  End If
End Sub

Private Sub CmdDelkey_Click()
  Dim ids() As String
  
  If LswKeys.ListItems.Count Then
    ids = Split(LswKeys.SelectedItem.Tag, "/")
    DataMan.DmConnection.Execute ("delete * from dmvsm_chiavi where " & _
                                  "idarchivio = " & ids(0) & " AND IdKey = " & ids(1))
  End If
  refreshLswKeys
End Sub

Private Sub cmdDelRT_Click()
  Dim rsRecordT As Recordset, rsTracciato As Recordset
  Dim ids() As String
  
  If lswRecordType.ListItems.Count Then
    ids = Split(lswRecordType.SelectedItem.Key, "/")
    DataMan.DmConnection.Execute ("delete * from dmvsm_recordtype where " & _
                                  "vsam = '" & ids(0) & "' AND Idrecordtype = " & ids(1))
  End If
  refreshLswRecordType
  refreshLswArea
End Sub

Private Sub CmdFldOk_Click()
  If IndSel = 0 Or IndSel > LstFldEditVSAM.ListItems.Count Then Exit Sub

  LstFldEditVSAM.ListItems(IndSel).text = TxtLiv
  LstFldEditVSAM.ListItems(IndSel).SubItems(2) = UCase(TxtNome)
  LstFldEditVSAM.ListItems(IndSel).SubItems(4) = UCase(TxtLen)
  LstFldEditVSAM.ListItems(IndSel).SubItems(5) = UCase(TxTDec)
  LstFldEditVSAM.ListItems(IndSel).SubItems(7) = UCase(CmbType.text)
  LstFldEditVSAM.ListItems(IndSel).SubItems(8) = UCase(TxtOccurs)
  If TxtOccurs = 0 Then
    LstFldEditVSAM.ListItems(IndSel).SubItems(9) = ""
  Else
    LstFldEditVSAM.ListItems(IndSel).SubItems(9) = IIf(chkOccursFlat.Value = 0, "NO", "YES")
  End If
  LstFldEditVSAM.ListItems(IndSel).SubItems(10) = UCase(TxtNomeRed)
  LstFldEditVSAM.ListItems(IndSel).SubItems(11) = UCase(TxtSelTemplate)
  LstFldEditVSAM.ListItems(IndSel).SubItems(12) = IIf(chkDaMigrare.Value = 0, "NO", "YES")
  SwMod = True
  LblModif = "Area modified"
  
  TxtLiv = ""
  TxtNome = ""
  TxtLen = ""
  TxTDec = ""
  CmbType.text = ""
  TxtOccurs = ""
  chkOccursFlat.Value = False
  TxtNomeRed = ""
  TxtSelTemplate = ""
  chkDaMigrare.Value = False
End Sub

Private Sub CmdIIkey_Click()
  Dim rsChek As Recordset
  Dim ids() As String
  
  If LswKeys.ListItems.Count Then
    ids = Split(LswKeys.SelectedItem.Tag, "/")
    Set rsChek = m_fun.Open_Recordset("select * from dmvsm_chiavi where " & _
                                      "idarchivio = " & ids(0) & " and idKey = " & ids(1))
    If rsChek.RecordCount Then
      rsChek!Primaria = False
      rsChek.Update
    End If
    rsChek.Close
  End If
  refreshLswKeys
End Sub

Private Sub CmdIkey_Click()
  Dim rsChek As Recordset
  Dim ids() As String
  Dim boolAssocia As Boolean
  Dim i As Long
  
  If LswKeys.ListItems.Count Then
    ids = Split(LswKeys.SelectedItem.Tag, "/")
    boolAssocia = True
    'For i = 1 To LswArea.ListItems.count
    For i = 1 To LswKeys.ListItems.Count
      If LswKeys.ListItems(i).SmallIcon = 5 Then
        boolAssocia = False
        Exit For
      End If
    Next i
    If boolAssocia Then
      Set rsChek = m_fun.Open_Recordset("select * from dmvsm_chiavi where " & _
                                        "idarchivio = " & ids(0) & " and idKey = " & ids(1))
      If rsChek.RecordCount Then
        rsChek!Primaria = True
        rsChek.Update
      End If
      rsChek.Close
    End If
  End If
  refreshLswKeys
End Sub

Private Sub cmdRemoveArea_Click()
  Dim ids() As String

  If LswArea.ListItems.Count Then
    ids = Split(LswArea.SelectedItem.Tag, "/")
    DataMan.DmConnection.Execute ("delete * from dmvsm_tracciato where " & _
                                  "vsam = '" & ids(2) & "' AND IdOggetto = " & ids(0) & " AND " & _
                                  "IdArea = " & ids(1))
  End If
  refreshLswArea
End Sub

Private Sub CmdSalva_Click()
  Dim Tba As Recordset, tbVSAM As Recordset
  
  If txtNameVSAM = "" Then
    MsgBox "Insert correct value for VSAM Name!", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
    Exit Sub
  End If
  If txtAcronymVSAM = "" Then
    MsgBox "Insert corect value for VSAM Acronym!", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
    Exit Sub
  End If

  Set tbVSAM = m_fun.Open_Recordset("select * from DMVSM_NomiVSAM where nomevsam = '" & UCase(txtNameVSAM) & "'")
  If tbVSAM.RecordCount Then
    tbVSAM!nomeVSAM = UCase(txtNameVSAM)
    tbVSAM!Acronimo = UCase(txtAcronymVSAM)
    tbVSAM.Update
  Else
    tbVSAM.AddNew
    tbVSAM!nomeVSAM = UCase(txtNameVSAM)
    tbVSAM!Acronimo = UCase(txtAcronymVSAM)
    tbVSAM!to_migrate = True
    tbVSAM!obsolete = False
    tbVSAM.Update
  End If
  tbVSAM.Close
  
  Set Tba = m_fun.Open_Recordset("select * from dmvsm_archivio where idarchivio = " & pIndex_DBCorrente)
  If Tba.RecordCount Then
    Tba!Nome = TxtName
    Tba!VSAM = txtNameVSAM
    Tba!recform = TxtRF
    Tba!recsize = IIf(Len(TxtRS), TxtRS, 0)
    Tba!MinLen = IIf(Len(TxtMinLen), TxtMinLen, 0)
    Tba!MaxLen = IIf(Len(TxtMaxLen), TxtMaxLen, 0)
    Tba!keyStart = IIf(Len(TxtKeyPos), TxtKeyPos, 0)
    Tba!keyLen = IIf(Len(TxtKeyLen), TxtKeyLen, 0)
    Tba!Tag = txtTag
    Select Case True
      Case OptRRDS
        Tba!type = "RRDS"
      Case OptESDS
        Tba!type = "ESDS"
      Case OptKSDS
        Tba!type = "KSDS"
      Case OptSeq
        Tba!type = "Seq"
    End Select
    If ChkGDG = vbChecked Then
      Tba!gdg = True
    Else
      Tba!gdg = False
    End If
    Tba.Update
    MsgBox "Files Saved!", vbOKOnly + vbInformation, DataMan.DmNomeProdotto
  End If
  Tba.Close
End Sub

Private Sub cmdSaveAcronym_Click()
  If Not lswRecordType.SelectedItem Is Nothing Then
    lswRecordType.SelectedItem.text = txtNameRT
    lswRecordType.SelectedItem.SubItems(1) = txtAcronym
    lswRecordType.SelectedItem.SubItems(2) = txtCondition
  End If
End Sub

Private Sub cmdSaveRT_Click()
  Dim rsRecordT As Recordset
  Dim ids() As String, idArea() As String
  
  If Not lswRecordType.SelectedItem Is Nothing Then
    If MsgBox("Do you want to save all updates on this Record Type?", vbYesNo + vbInformation, DataMan.DmNomeProdotto) = vbYes Then
      If Len(lswRecordType.SelectedItem.Tag) Then
        ids = Split(lswRecordType.SelectedItem.Key, "/")
        idArea = Split(lswRecordType.SelectedItem.Tag, "/")
        Set rsRecordT = m_fun.Open_Recordset("select * from dmvsm_recordtype where " & _
                                             "vsam = '" & ids(0) & "' and idrecordtype = " & ids(1))
        If rsRecordT.RecordCount Then
          rsRecordT!Nome = lswRecordType.SelectedItem
          rsRecordT!Acronimo = lswRecordType.SelectedItem.SubItems(1)
          rsRecordT!idArea = idArea(1)
          rsRecordT!IdOggetto = idArea(0)
          rsRecordT!Condizione = lswRecordType.SelectedItem.SubItems(2)
          rsRecordT.Update
          MsgBox "Record Type Saved!", vbOKOnly + vbInformation, DataMan.DmNomeProdotto
        End If
        rsRecordT.Close
      Else
        MsgBox "No associated area!", vbOKOnly + vbInformation, DataMan.DmNomeProdotto
      End If
    End If
  End If
End Sub

Private Sub Form_Load()
  Dim tbRecordT As Recordset, tbVSAM As Recordset
  
  Elimina_Flickering_Object Me.hwnd
  
  Set tbVSAM = m_fun.Open_Recordset("select vsam from DMVSM_Archivio where idarchivio = " & pIndex_DBCorrente)
  If tbVSAM.RecordCount Then
    Set tbRecordT = m_fun.Open_Recordset("select * from dmvsm_recordtype where vsam = '" & tbVSAM!VSAM & "" & "'")
    If tbRecordT.RecordCount = 0 Then
      ' Se non c'� nessun Record Type, ne creo uno di default
      tbRecordT.AddNew
      tbRecordT!VSAM = tbVSAM!VSAM & ""
      tbRecordT!IdRecordType = getId("idRecordType", "DMVSM_RecordType")
      tbRecordT!Nome = "Default Record Type"
      tbRecordT!Acronimo = "RT1"
      tbRecordT!idArea = 0
      tbRecordT!IdOggetto = 0
      tbRecordT!Condizione = ""
      tbRecordT.Update
    End If
    tbRecordT.Close
  End If
  tbVSAM.Close
  
  CaricaArchivio
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
   
  LswArea.ColumnHeaders(1).Width = LswArea.Width / 3
  LswArea.ColumnHeaders(2).Width = LswArea.Width / 3
  LswArea.ColumnHeaders(3).Width = LswArea.Width / 3
  ListCampi.ColumnHeaders(1).Width = ListCampi.Width / 9
  ListCampi.ColumnHeaders(2).Width = ListCampi.Width / 4
  ListCampi.ColumnHeaders(3).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(4).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(5).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(6).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(7).Width = ListCampi.Width / 7
  
  lswRecordType.ColumnHeaders(1).Width = lswRecordType.Width / 4 * 3
  lswRecordType.ColumnHeaders(2).Width = lswRecordType.Width / 4
  lswRecordType.ColumnHeaders(3).Width = 0
  
  Terminate_Flickering_Object
End Sub

Sub CaricaArchivio()
  Dim Tba As Recordset, Tbo As Recordset
     
  Set Tba = m_fun.Open_Recordset("select * from dmvsm_archivio where idarchivio = " & pIndex_DBCorrente)
  If Tba.RecordCount Then
    TxtName = Tba!Nome
    txtNameVSAM = Tba!VSAM & ""
    TxtRF = Tba!recform & ""
    TxtRS = Tba!recsize & ""
    TxtMinLen = Tba!MinLen & ""
    TxtMaxLen = Tba!MaxLen & ""
    TxtKeyPos = Tba!keyStart & ""
    TxtKeyLen = Tba!keyLen & ""
    txtTag = Tba!Tag & ""
    Select Case Tba!type & ""
      Case "ESDS"
        OptESDS = True
      Case "KSDS"
        OptKSDS = True
      Case "RRDS"
        OptRRDS = True
      Case "Seq"
        OptSeq = True
      Case Else
        Option1 = True
    End Select
    If Tba!gdg Then
      ChkGDG = vbChecked
    Else
      ChkGDG = vbUnchecked
    End If
  End If
  Tba.Close
  
  txtAcronymVSAM = ""
  If Len(txtNameVSAM) Then
    Set Tba = m_fun.Open_Recordset("select * from DMVSM_NomiVSAM where nomeVSAM = '" & txtNameVSAM & "'")
    If Tba.RecordCount Then
      txtAcronymVSAM = Tba!Acronimo
    End If
    Tba.Close
  End If
  
  Set Tba = m_fun.Open_Recordset("select * from psrel_file where dsnname = '" & TxtName & "'")
  Do Until Tba.EOF
    Set Tbo = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & Tba!IdOggetto)
    If Tbo.RecordCount Then
      With LswJcl.ListItems.Add(, , Tbo!Nome)
        .SubItems(1) = Tbo!Tipo
        .SubItems(2) = Tba!Step & " (" & Trim(str(Tba!nstep)) & ")"
        .SubItems(3) = Tba!program
        .SubItems(4) = Tba!link
        .SubItems(5) = Tba!use & ""
      End With
    End If
    Tbo.Close
    Tba.MoveNext
  Loop
  Tba.Close
  
  refreshLswKeys
  refreshLswRecordType
  refreshLswArea
  refreshLswCampi
  
  CmbPrimary.ListIndex = 1
End Sub

Public Sub refreshLswKeys()
  Dim Tba As Recordset
  LswKeys.ListItems.Clear
 
  Set Tba = m_fun.Open_Recordset("select * from dmvsm_chiavi where idarchivio = " & pIndex_DBCorrente)
  LswKeys.SmallIcons = ImageList2
  Do Until Tba.EOF
    With LswKeys.ListItems.Add(, , Tba!Nome)
      .Tag = Tba!idArchivio & "/" & Tba!IdKey
      .SubItems(1) = Tba!keyStart
      .SubItems(2) = Tba!keyLen
      If Tba!Primaria Then
        .SmallIcon = 5
      Else
        .SmallIcon = 10
      End If
    End With
    Tba.MoveNext
  Loop
  Tba.Close
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  LswArea.ColumnHeaders(1).Width = LswArea.Width / 3
  LswArea.ColumnHeaders(2).Width = LswArea.Width / 3
  LswArea.ColumnHeaders(3).Width = LswArea.Width / 3
  ListCampi.ColumnHeaders(1).Width = ListCampi.Width / 9
  ListCampi.ColumnHeaders(2).Width = ListCampi.Width / 4
  ListCampi.ColumnHeaders(3).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(4).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(5).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(6).Width = ListCampi.Width / 7
  ListCampi.ColumnHeaders(7).Width = ListCampi.Width / 7
  
  lswRecordType.ColumnHeaders(1).Width = lswRecordType.Width / 4 * 3
  lswRecordType.ColumnHeaders(2).Width = lswRecordType.Width / 4
  lswRecordType.ColumnHeaders(3).Width = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub LstFldEditVSAM_ItemClick(ByVal Item As MSComctlLib.ListItem)
  If LstFldEditVSAM.SelectedItem.SubItems(8) = 0 Then
    chkOccursFlat.Enabled = False
  Else
    chkOccursFlat.Enabled = True
  End If
End Sub

Private Sub LswArea_Click()
  Dim indCmp As Integer, indStr As Integer
  Dim rs As Recordset
  Dim ids() As String
  
  On Error GoTo ErrorHandler
  If LswArea.ListItems.Count Then
    'DA completare ALE
    ListCampi.ListItems.Clear
    ids = Split(LswArea.SelectedItem.Tag, "/")
    Set rs = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                  "idoggetto = " & ids(0) & " and idarea = " & ids(1) & _
                                  " and vsam = '" & ids(2) & "' order by ordinale")
    Do Until rs.EOF
      With ListCampi.ListItems.Add(, , rs!livello)
        .SubItems(1) = rs!Nome
        .SubItems(2) = rs!Byte
        .SubItems(3) = rs!lunghezza
        .SubItems(4) = rs!Posizione
        .SubItems(5) = rs!Tipo
        .SubItems(6) = rs!Occurs
        .Tag = rs!IdOggetto & "/" & rs!idArea
      End With
      rs.MoveNext
    Loop
    rs.Close
  End If
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " - " & ERR.Description
End Sub

Private Sub LswArea_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = LswArea.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LswArea.SortOrder = Order
  LswArea.SortKey = Key - 1
  LswArea.Sorted = True
End Sub

Private Sub LswJcl_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = LswJcl.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LswJcl.SortOrder = Order
  LswJcl.SortKey = Key - 1
  LswJcl.Sorted = True
End Sub

Private Sub LswJcl_DblClick()
  Dim nomeOggetto As String
  Dim IdOggetto As Long

  If LswJcl.ListItems.Count Then
    nomeOggetto = LswJcl.ListItems.Item(LswJcl.SelectedItem.Index)
    IdOggetto = m_fun.Restituisci_IdOggetto_Da_Nome(nomeOggetto)
         
    'm_fun.Show_CopyLoad IdOggetto
    m_fun.Show_ObjViewer IdOggetto
  End If
End Sub

Private Sub LswKeys_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = LswKeys.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LswKeys.SortOrder = Order
  LswKeys.SortKey = Key - 1
  LswKeys.Sorted = True
End Sub

Private Sub lswRecordType_Click()
  Dim i As Long
  
  txtNameRT = lswRecordType.SelectedItem
  txtAcronym = lswRecordType.SelectedItem.SubItems(1)
  txtCondition = lswRecordType.SelectedItem.SubItems(2)
    
  For i = 1 To LswArea.ListItems.Count
    'Tag = IdOggetto & "/" & idArea & "/" & IdArchivio
    If LswArea.ListItems(i).Tag = lswRecordType.SelectedItem.Tag Then
      LswArea.ListItems(i).SmallIcon = 12
    Else
      LswArea.ListItems(i).SmallIcon = 3
    End If
  Next i
End Sub

Private Sub lswRecordType_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswRecordType.SortOrder
  
  Key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  'ListObjCol = Key
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswRecordType.SortOrder = Order
  lswRecordType.SortKey = Key - 1
  lswRecordType.Sorted = True
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  Dim rs As Recordset, rsAssociata As Recordset
  Dim ids() As String
  
  Elimina_Flickering_Object Me.hwnd
  
  If SSTab1.Caption <> "General Information" And txtNameVSAM = "" Then
    SSTab1.Tab = PreviousTab
    MsgBox "Insert correct value for VSAM Name!", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
    Terminate_Flickering_Object
    Exit Sub
  End If
  If SSTab1.Caption <> "General Information" And txtAcronymVSAM = "" Then
    SSTab1.Tab = PreviousTab
    MsgBox "Insert correct value for VSAM Acronym!", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
    Terminate_Flickering_Object
    Exit Sub
  End If
  
  If SSTab1.Caption = "Structure Details" Then
    chkOccursFlat.Enabled = True
    LstFldEditVSAM.ListItems.Clear
    If lswRecordType.ListItems.Count Then
      ids = Split(lswRecordType.SelectedItem.Key, "/")
      Set rsAssociata = m_fun.Open_Recordset("select * from DMVSM_recordtype where " & _
                                             "vsam = '" & ids(0) & "' and idrecordtype = " & ids(1))
      If rsAssociata.RecordCount Then
        varVSAM = rsAssociata!VSAM
        varIdArea = rsAssociata!idArea
        varIdOggetto = rsAssociata!IdOggetto
        Set rs = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                      "Idoggetto = " & rsAssociata!IdOggetto & " and IdArea = " & rsAssociata!idArea & _
                                      " and vsam = '" & rsAssociata!VSAM & "'" & _
                                      " order by ordinale")
        Do Until rs.EOF
          With LstFldEditVSAM.ListItems.Add(, , rs!livello)
            .SubItems(1) = IIf(IsNull(rs!Ordinale), "", rs!Ordinale)
            .SubItems(2) = IIf(IsNull(rs!Nome), "", rs!Nome)
            .SubItems(3) = IIf(IsNull(rs!Byte), "", rs!Byte)
            .SubItems(4) = IIf(IsNull(rs!lunghezza), "", rs!lunghezza)
            .SubItems(5) = IIf(IsNull(rs!Decimali), "", rs!Decimali)
            .SubItems(6) = IIf(IsNull(rs!Posizione), "", rs!Posizione)
            .SubItems(7) = IIf(IsNull(rs!Tipo & rs!Usage), "", rs!Tipo & " - " & rs!Usage)
            .SubItems(8) = IIf(IsNull(rs!Occurs), "", rs!Occurs)
            If rs!Occurs = 0 Then
              .SubItems(9) = ""
            Else
              .SubItems(9) = IIf(rs!OccursFlat, "YES", "NO")
            End If
            .SubItems(10) = IIf(IsNull(rs!NomeRed), "", rs!NomeRed)
            .SubItems(11) = IIf(IsNull(rs!condizionered), "", rs!condizionered)
            .SubItems(12) = IIf(rs!DaMigrare, "YES", "NO")
          End With
          rs.MoveNext
        Loop
        rs.Close
      End If
      rsAssociata.Close
    End If
  End If
  Terminate_Flickering_Object
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim wResp As Variant
  Dim k As Integer
  Dim IndSel As Integer

  On Error Resume Next
  Select Case Button.Key
    Case "New"
      SwMod = True
      InserisciCampoEdit
      LblModif = "Area modified"
    Case "Up"
      SpostaCampo Button.Key
    Case "Down"
      SpostaCampo Button.Key
    Case "Copy"
      IndSelC = LstFldEditVSAM.SelectedItem.Index
    Case "Paste"
      IndSel = LstFldEditVSAM.SelectedItem.Index
      With LstFldEditVSAM.ListItems.Add(, , LstFldEditVSAM.ListItems(IndSelC).text)
        For k = 1 To 9
          .SubItems(k) = LstFldEditVSAM.ListItems(IndSelC).SubItems(k)
        Next k
      End With
      For k = LstFldEditVSAM.ListItems.Count To IndSel Step -1
        LstFldEditVSAM.ListItems(k).Selected = True
        SpostaCampo "Up"
      Next k
      SwMod = True
      LblModif = "Area modified"
    Case "Cut"
      SwMod = True
      LstFldEditVSAM.ListItems.Remove LstFldEditVSAM.SelectedItem.Index
      LstFldEditVSAM.Refresh
      LblModif = "Area modified"
    Case "Open"
      SwMod = True
      CaricaDMAreaSegZero
      LblModif = "Area modified"
    Case "Save"
       AggiornaDmAreaSeg
       SwMod = False
       LblModif = "Area saved"
    Case "Check"
       SwMod = True
       LblModif = "Area modified"
       VerificaArea
    Case "Close"
      If SwMod Then
        wResp = MsgBox("Save the new area?", vbYesNo, DataMan.DmNomeProdotto)
      End If
      If wResp = vbYes Then AggiornaDmAreaSeg
  End Select
End Sub

Sub InserisciCampoEdit()
  Dim K1 As Integer, I1 As Integer, I2 As Integer
  Dim WsTR As String
   
  K1 = LstFldEditVSAM.SelectedItem.Index
  With LstFldEditVSAM.ListItems.Add(, , "5")
    For I2 = 1 To 8
      .SubItems(I2) = "0"
    Next I2
    .SubItems(7) = "X - ZND"
  End With
   
  For I1 = LstFldEditVSAM.ListItems.Count To K1 + 2 Step -1
    WsTR = LstFldEditVSAM.ListItems(I1).text
    LstFldEditVSAM.ListItems(I1).text = LstFldEditVSAM.ListItems(I1 - 1).text
    LstFldEditVSAM.ListItems(I1 - 1).text = WsTR

    For I2 = 1 To 9
      WsTR = LstFldEditVSAM.ListItems(I1).SubItems(I2)
      LstFldEditVSAM.ListItems(I1).SubItems(I2) = LstFldEditVSAM.ListItems(I1 - 1).SubItems(I2)
      LstFldEditVSAM.ListItems(I1 - 1).SubItems(I2) = WsTR
    Next I2
  Next I1
End Sub

Sub SpostaCampo(Modo As String)
  Dim K1 As Integer
  Dim Ida As Integer
  Dim Ia As Integer
  Dim WsTR As String
  Dim swBold As Boolean
  Dim swColor As Variant
   
  Ida = LstFldEditVSAM.SelectedItem.Index
  
  If Modo = "Up" Then
    Ia = Ida - 1
  Else
    Ia = Ida + 1
  End If

  WsTR = LstFldEditVSAM.ListItems(Ida).text
  LstFldEditVSAM.ListItems(Ida).text = LstFldEditVSAM.ListItems(Ia).text
  LstFldEditVSAM.ListItems(Ia).text = WsTR
  
  swBold = LstFldEditVSAM.ListItems(Ida).ListSubItems(2).Bold
  swColor = LstFldEditVSAM.ListItems(Ida).ListSubItems(2).ForeColor
  
  For K1 = 1 To 9
    WsTR = LstFldEditVSAM.ListItems(Ida).SubItems(K1)
    LstFldEditVSAM.ListItems(Ida).SubItems(K1) = LstFldEditVSAM.ListItems(Ia).SubItems(K1)
    LstFldEditVSAM.ListItems(Ida).ListSubItems(K1).Bold = LstFldEditVSAM.ListItems(Ia).ListSubItems(K1).Bold
    LstFldEditVSAM.ListItems(Ida).ListSubItems(K1).ForeColor = LstFldEditVSAM.ListItems(Ia).ListSubItems(K1).ForeColor
    
    LstFldEditVSAM.ListItems(Ia).SubItems(K1) = WsTR
  Next K1
  
  LstFldEditVSAM.ListItems(Ia).ListSubItems(2).Bold = swBold
  LstFldEditVSAM.ListItems(Ia).ListSubItems(2).ForeColor = swColor
End Sub

Sub AggiornaDmAreaSeg()
  Dim indCmp As Integer
  Dim rsArea As Recordset
  Dim k As Integer
  Dim wTipo As String, wUsage As String
  
  On Error GoTo ErrNoStr:
  
  DataMan.DmConnection.Execute ("delete * from DMVSM_Data_Cmp where " & _
                                "vsam = '" & varVSAM & "' and IdArea = " & varIdArea & " and " & _
                                "Idoggetto = " & varIdOggetto)

  Set rsArea = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp") 'where idarchivio = " & pIndex_DBCorrente & " and IdArea=" & varIdArea & " and Idoggetto=" & varIdOggetto
  For indCmp = 1 To LstFldEditVSAM.ListItems.Count
    rsArea.AddNew
    rsArea!IdCmp = getId("IdCmp", "DMVSM_Data_Cmp")
    rsArea!livello = LstFldEditVSAM.ListItems(indCmp).text
    rsArea!idArea = varIdArea
    rsArea!IdOggetto = varIdOggetto
    rsArea!VSAM = varVSAM
    rsArea!Nome = LstFldEditVSAM.ListItems(indCmp).SubItems(2)
    rsArea!Byte = val(LstFldEditVSAM.ListItems(indCmp).SubItems(3))
    rsArea!lunghezza = val(LstFldEditVSAM.ListItems(indCmp).SubItems(4))
    rsArea!Decimali = val(LstFldEditVSAM.ListItems(indCmp).SubItems(5))
    rsArea!Posizione = val(LstFldEditVSAM.ListItems(indCmp).SubItems(6))
    k = InStr(LstFldEditVSAM.ListItems(indCmp).SubItems(7), "-")
    wTipo = Trim(Mid$(LstFldEditVSAM.ListItems(indCmp).SubItems(7), 1, k - 1))
    wUsage = Trim(Mid$(LstFldEditVSAM.ListItems(indCmp).SubItems(7), k + 1))
    rsArea!Occurs = LstFldEditVSAM.ListItems(indCmp).SubItems(8)
    If LstFldEditVSAM.ListItems(indCmp).SubItems(8) = 0 Then
      rsArea!OccursFlat = False
    Else
      rsArea!OccursFlat = IIf(LstFldEditVSAM.ListItems(indCmp).SubItems(9) = "YES", True, False)
    End If
    rsArea!NomeRed = LstFldEditVSAM.ListItems(indCmp).SubItems(10)
    rsArea!condizionered = LstFldEditVSAM.ListItems(indCmp).SubItems(11)
    rsArea!DaMigrare = IIf(LstFldEditVSAM.ListItems(indCmp).SubItems(12) = "YES", True, False)
    rsArea!Correzione = " "
    rsArea!Ordinale = indCmp
    rsArea!segno = " "
    rsArea!Tipo = wTipo
    rsArea!Usage = wUsage
    
    rsArea.Update
  Next indCmp
  rsArea.Close
  Exit Sub
ErrNoStr:
  Resume Next
End Sub

Sub VerificaArea()
  Dim K1 As Integer, Y1 As Integer
  Dim wTot As Integer, wLen As Integer
  Dim LivLen(50, 2)
  
  For K1 = 1 To LstFldEditVSAM.ListItems.Count
    If K1 < LstFldEditVSAM.ListItems.Count Then
      If val(LstFldEditVSAM.ListItems(K1).text) < val(LstFldEditVSAM.ListItems(K1 + 1).text) Then
        If LstFldEditVSAM.ListItems(K1 + 1) <> "88" Then
          LstFldEditVSAM.ListItems(K1).SubItems(4) = 0
        End If
      End If
    End If
    If LstFldEditVSAM.ListItems(K1).SubItems(4) Then
      LstFldEditVSAM.ListItems(K1).SubItems(5) = val(LstFldEditVSAM.ListItems(K1).SubItems(5))
      wLen = val(LstFldEditVSAM.ListItems(K1).SubItems(4)) + val(LstFldEditVSAM.ListItems(K1).SubItems(5))
      If InStr(LstFldEditVSAM.ListItems(K1).SubItems(7), "PCK") Then
        wLen = Int((wLen) / 2) + 1
      End If
      If InStr(LstFldEditVSAM.ListItems(K1).SubItems(7), "BIN") Then
        wLen = 2
        If LstFldEditVSAM.ListItems(K1).SubItems(4) > 4 Then wLen = 4
        If LstFldEditVSAM.ListItems(K1).SubItems(4) > 8 Then wLen = 8
      End If
      LstFldEditVSAM.ListItems(K1).SubItems(3) = wLen
    Else
      LstFldEditVSAM.ListItems(K1).SubItems(5) = 0
      LstFldEditVSAM.ListItems(K1).SubItems(3) = 0
    End If
  Next K1
  Y1 = 1
  K1 = LstFldEditVSAM.ListItems.Count
   
  LivLen(Y1, 1) = 1
  LivLen(Y1, 2) = 0
  Y1 = 2
  LivLen(Y1, 1) = LstFldEditVSAM.ListItems(K1).text
  LivLen(Y1, 2) = val(LstFldEditVSAM.ListItems(K1).SubItems(3))
   
  For K1 = LstFldEditVSAM.ListItems.Count - 1 To 1 Step -1
    If val(LstFldEditVSAM.ListItems(K1)) > val(LstFldEditVSAM.ListItems(K1 + 1)) Then
      wTot = val(LstFldEditVSAM.ListItems(K1).SubItems(3))
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) > val(LstFldEditVSAM.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          LivLen(Y1, 2) = 0
        End If
        If val(LivLen(Y1, 1)) = val(LstFldEditVSAM.ListItems(K1).text) Then
          If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
            LivLen(Y1, 2) = val(LivLen(Y1, 2)) + wTot
          End If
        End If
        If Trim(LivLen(Y1, 1)) = "" Then
          LivLen(Y1, 1) = LstFldEditVSAM.ListItems(K1).text
          If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
            LivLen(Y1, 2) = val(LstFldEditVSAM.ListItems(K1).SubItems(3))
          Else
            LivLen(Y1, 2) = 0
          End If
          Exit For
        End If
      Next Y1
    End If
    If val(LstFldEditVSAM.ListItems(K1)) = val(LstFldEditVSAM.ListItems(K1 + 1)) Then
      If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
        For Y1 = 1 To 50
          If val(LivLen(Y1, 1)) = val(LstFldEditVSAM.ListItems(K1).text) Then
            LivLen(Y1, 2) = val(LivLen(Y1, 2)) + val(LstFldEditVSAM.ListItems(K1).SubItems(3))
            Exit For
          End If
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEditVSAM.ListItems(K1).text
            If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
              LivLen(Y1, 2) = LstFldEditVSAM.ListItems(K1).SubItems(3)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
      End If
    End If
    If val(LstFldEditVSAM.ListItems(K1)) < val(LstFldEditVSAM.ListItems(K1 + 1)) Then
      wLen = 0
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) > val(LstFldEditVSAM.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          wLen = wLen + val(LivLen(Y1, 2))
          LivLen(Y1, 2) = 0
        End If
      Next Y1
      For Y1 = 1 To 50
        If val(LivLen(Y1, 1)) = val(LstFldEditVSAM.ListItems(K1).text) Then
          LstFldEditVSAM.ListItems(K1).SubItems(3) = wLen  'non c'ho capito niente
          If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
            LivLen(Y1, 2) = val(LivLen(Y1, 2)) + wLen
            wLen = 0
          End If
'          LstFldEditVSAM.ListItems(K1).SubItems(3) = val(LivLen(Y1, 2))
          wTot = 0
        End If
      Next Y1
      If wLen > 0 Then
        For Y1 = 1 To 50
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEditVSAM.ListItems(K1).text
            If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
              LivLen(Y1, 2) = val(wLen)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
        LstFldEditVSAM.ListItems(K1).SubItems(3) = wLen
        wTot = 0
      End If
    End If
  Next K1
   
  K1 = 1
  LstFldEditVSAM.ListItems(K1).SubItems(1) = K1
  LstFldEditVSAM.ListItems(K1).SubItems(6) = 1
  wLen = 1
   
  For K1 = 2 To LstFldEditVSAM.ListItems.Count
    LstFldEditVSAM.ListItems(K1).SubItems(1) = K1
    If Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) = "" Then
      If LstFldEditVSAM.ListItems(K1 - 1).SubItems(4) <> "0" Then
        wLen = wLen + val(LstFldEditVSAM.ListItems(K1 - 1).SubItems(3))
      End If
      LstFldEditVSAM.ListItems(K1).SubItems(6) = wLen
    Else
      For Y1 = 1 To K1
        If Trim(LstFldEditVSAM.ListItems(Y1).SubItems(2)) = Trim(LstFldEditVSAM.ListItems(K1).SubItems(10)) Then
          wLen = LstFldEditVSAM.ListItems(Y1).SubItems(6)
          LstFldEditVSAM.ListItems(K1).SubItems(6) = wLen
          Exit For
        End If
      Next Y1
    End If
  Next K1

End Sub

Sub CaricaDMAreaSegZero()
  Dim TbAreaSeg As Recordset
  Dim wStrIdarea As Variant
  Dim wStrIdOggetto As Long
  Dim IndS As Integer
  
  On Error GoTo ErrStr:
  
  IndS = UBound(gSegm(indexseg).Strutture)
  
  wStrIdOggetto = gSegm(indexseg).Strutture(2).StrIdOggetto
  wStrIdarea = gSegm(indexseg).Strutture(2).StrIdArea
  LstFldEditVSAM.ListItems.Clear
  Set TbAreaSeg = m_fun.Open_Recordset("select * from psdata_cmp where " & _
                                       "idoggetto = " & wStrIdOggetto & " and idarea = " & wStrIdarea & _
                                       " order by ordinale")
  Do Until TbAreaSeg.EOF
    With LstFldEditVSAM.ListItems.Add(, , TbAreaSeg!livello)
      .SubItems(1) = TbAreaSeg!Ordinale
      .SubItems(2) = TbAreaSeg!Nome
      .SubItems(3) = TbAreaSeg!Byte
      .SubItems(4) = TbAreaSeg!lunghezza
      .SubItems(5) = TbAreaSeg!Decimali
      .SubItems(6) = TbAreaSeg!Posizione
      .SubItems(7) = TbAreaSeg!Tipo & "-" & TbAreaSeg!Usage
      .SubItems(8) = TbAreaSeg!Occurs
      If TbAreaSeg!Occurs = 0 Then
        .SubItems(9) = ""
      Else
        .SubItems(9) = IIf(TbAreaSeg!OccursFlat, "YES", "NO")
      End If
      .SubItems(10) = TbAreaSeg!NomeRed
      .SubItems(11) = TbAreaSeg!condizionered
      .SubItems(12) = IIf(TbAreaSeg!DaMigrare, "YES", "NO")
      If TbAreaSeg!Correzione = "R" Then
        .ListSubItems(2).Bold = True
        .ListSubItems(2).ForeColor = vbBlue
      End If
      If TbAreaSeg!Correzione = "I" Then
        .ListSubItems(2).ForeColor = vbRed
        .SubItems(10) = TbAreaSeg!valore
      End If
    End With
    TbAreaSeg.MoveNext
  Loop
  TbAreaSeg.Close
ErrStr:
  If LstFldEditVSAM.ListItems.Count = 0 Then
    With LstFldEditVSAM.ListItems.Add(, , "1")
      .SubItems(1) = 1
      .SubItems(2) = "NomeCampo"
    End With
  End If
End Sub

Private Sub LstFldEditVSAM_DblClick()
  If LstFldEditVSAM.ListItems.Count Then
    IndSel = LstFldEditVSAM.SelectedItem.Index
    TxtLiv = LstFldEditVSAM.SelectedItem.text
    TxtNome = LstFldEditVSAM.SelectedItem.SubItems(2)
    TxtLen = LstFldEditVSAM.SelectedItem.SubItems(4)
    TxTDec = LstFldEditVSAM.SelectedItem.SubItems(5)
    CmbType.text = LstFldEditVSAM.SelectedItem.SubItems(7)
    TxtOccurs = LstFldEditVSAM.SelectedItem.SubItems(8)
    If TxtOccurs = 0 Then
      chkOccursFlat.Value = 0
    Else
      chkOccursFlat.Value = IIf(LstFldEditVSAM.SelectedItem.SubItems(9) = "YES", 1, 0)
    End If
    TxtNomeRed = LstFldEditVSAM.SelectedItem.SubItems(10)
    TxtSelTemplate = LstFldEditVSAM.SelectedItem.SubItems(11)
    chkDaMigrare = IIf(LstFldEditVSAM.SelectedItem.SubItems(12) = "YES", 1, 0)
  End If
End Sub

Private Sub TxtOccurs_Change()
  If Len(TxtOccurs) Then
    If CInt(TxtOccurs) = 0 Then
      chkOccursFlat.Value = 0
      chkOccursFlat.Enabled = False
    Else
      chkOccursFlat.Enabled = True
    End If
  End If
End Sub
