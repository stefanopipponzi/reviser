VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_ExtraColumns 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Extra DB Columns"
   ClientHeight    =   4980
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   11040
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4980
   ScaleWidth      =   11040
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Height          =   60
      Left            =   50
      TabIndex        =   20
      Top             =   4320
      Width           =   10960
   End
   Begin VB.Frame Frame2 
      Height          =   4155
      Left            =   6525
      TabIndex        =   7
      Top             =   90
      Width           =   4440
      Begin VB.CommandButton cmdstr 
         Caption         =   "+"
         Height          =   315
         Left            =   2475
         TabIndex        =   22
         Top             =   675
         Width           =   255
      End
      Begin VB.ComboBox cmbTag 
         Height          =   345
         ItemData        =   "MadmdF_ExtraColumns.frx":0000
         Left            =   2835
         List            =   "MadmdF_ExtraColumns.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   675
         Width           =   1575
      End
      Begin VB.TextBox txtNome 
         Height          =   330
         Left            =   90
         TabIndex        =   13
         Top             =   675
         Width           =   2295
      End
      Begin VB.TextBox txtLength 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   720
         TabIndex        =   12
         Text            =   "0"
         Top             =   1770
         Width           =   810
      End
      Begin VB.TextBox txtDecimali 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   2910
         TabIndex        =   11
         Text            =   "0"
         Top             =   1770
         Width           =   855
      End
      Begin VB.ComboBox cmbTipo 
         Height          =   345
         Left            =   690
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1260
         Width           =   1725
      End
      Begin VB.ComboBox cmbNull 
         Height          =   345
         ItemData        =   "MadmdF_ExtraColumns.frx":0028
         Left            =   720
         List            =   "MadmdF_ExtraColumns.frx":0032
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   2280
         Width           =   1725
      End
      Begin VB.ComboBox cmbDefault 
         Height          =   345
         ItemData        =   "MadmdF_ExtraColumns.frx":003F
         Left            =   750
         List            =   "MadmdF_ExtraColumns.frx":0049
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   2790
         Width           =   1725
      End
      Begin VB.Label Label8 
         Caption         =   "Dynamic String:"
         Height          =   285
         Left            =   2835
         TabIndex        =   23
         Top             =   315
         Width           =   1395
      End
      Begin VB.Label Label1 
         Caption         =   "Column Name:"
         Height          =   285
         Left            =   90
         TabIndex        =   19
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Type:"
         Height          =   285
         Left            =   90
         TabIndex        =   18
         Top             =   1320
         Width           =   555
      End
      Begin VB.Label Label3 
         Caption         =   "Length:"
         Height          =   285
         Left            =   90
         TabIndex        =   17
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "Decimals:"
         Height          =   285
         Left            =   2040
         TabIndex        =   16
         Top             =   1800
         Width           =   825
      End
      Begin VB.Label Label5 
         Caption         =   "Null:"
         Height          =   285
         Left            =   90
         TabIndex        =   15
         Top             =   2325
         Width           =   420
      End
      Begin VB.Label Label6 
         Caption         =   "Default:"
         Height          =   285
         Left            =   90
         TabIndex        =   14
         Top             =   2865
         Width           =   690
      End
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "SAVE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   9180
      TabIndex        =   4
      Top             =   4470
      Width           =   1605
   End
   Begin VB.ComboBox cmbColumns 
      Height          =   345
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   330
      Width           =   2895
   End
   Begin VB.CommandButton cmdUP 
      Caption         =   "/\"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5910
      TabIndex        =   2
      Top             =   1440
      Width           =   420
   End
   Begin VB.CommandButton cmdDOWN 
      Caption         =   "\/"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5910
      TabIndex        =   1
      Top             =   2070
      Width           =   420
   End
   Begin MSComctlLib.ListView lswColumns 
      Height          =   2430
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   5700
      _ExtentX        =   10054
      _ExtentY        =   4286
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList2"
      SmallIcons      =   "ImageList2"
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDropMode     =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Type"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Length"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Decimals"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Null"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Default"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   4155
      Left            =   50
      TabIndex        =   6
      Top             =   90
      Width           =   6420
      Begin VB.CommandButton cmdOK 
         Caption         =   "Add New Column"
         Height          =   360
         Left            =   120
         TabIndex        =   25
         Top             =   3600
         Width           =   1605
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel Column"
         Height          =   390
         Left            =   4170
         TabIndex        =   24
         Top             =   3570
         Width           =   1635
      End
   End
   Begin VB.Label Label7 
      Caption         =   "Selected Columns "
      Height          =   285
      Left            =   3195
      TabIndex        =   5
      Top             =   225
      Width           =   1455
   End
End
Attribute VB_Name = "MadmdF_ExtraColumns"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public TipoDB As String

Private Sub cmbColumns_Click()
  Dim rsColumns As Recordset
  Dim listx As ListItem
  
  lswColumns.ListItems.Clear
  If cmbColumns.ListIndex = 0 Then
    Set rsColumns = m_fun.Open_Recordset("Select * From DMheader_columns")
    Do Until rsColumns.EOF
      Set listx = lswColumns.ListItems.Add(, , rsColumns!Nome)
      listx.SubItems(1) = rsColumns!Tipo
      listx.SubItems(2) = rsColumns!lunghezza
      listx.SubItems(3) = rsColumns!Decimali
      listx.SubItems(4) = rsColumns!Null
      listx.SubItems(5) = IIf(IsNull(rsColumns!Default), False, rsColumns!Default)
      rsColumns.MoveNext
    Loop
  Else
    Set rsColumns = m_fun.Open_Recordset("Select * From DMfooter_columns")
    Do Until rsColumns.EOF
      Set listx = lswColumns.ListItems.Add(, , rsColumns!Nome)
      listx.SubItems(1) = rsColumns!Tipo
      listx.SubItems(2) = rsColumns!lunghezza
      listx.SubItems(3) = rsColumns!Decimali
      listx.SubItems(4) = rsColumns!Null
      listx.SubItems(5) = IIf(IsNull(rsColumns!Default), False, rsColumns!Default)
      rsColumns.MoveNext
    Loop
  End If
  rsColumns.Close
End Sub

Private Sub cmdCancel_Click()
  If lswColumns.ListItems.Count Then
    lswColumns.ListItems.Remove (lswColumns.SelectedItem.Index)
  End If
End Sub

Private Sub cmdOK_Click()
  Dim listx As ListItem

  If controllaTipoDati Then
    Set listx = lswColumns.ListItems.Add(, , TxtNome.text)
    listx.SubItems(1) = cmbTipo.list(cmbTipo.ListIndex)
    listx.SubItems(2) = txtLength.text
    listx.SubItems(3) = txtDecimali.text
    listx.SubItems(4) = CmbNull.list(CmbNull.ListIndex)
    listx.SubItems(5) = cmbDefault.list(cmbDefault.ListIndex)
  End If
End Sub

Public Function controllaTipoDati() As Boolean
  Dim i As Integer
    
  controllaTipoDati = True
  
  For i = 1 To lswColumns.ListItems.Count
    If TxtNome.text = lswColumns.ListItems(i).text Then
      controllaTipoDati = False
    End If
  Next i
  If Not IsNumeric(txtLength.text) Then
    MsgBox "Length is a numeric field", vbInformation, "i-4.Migration"
    controllaTipoDati = False
  End If
  If Not IsNumeric(txtDecimali.text) Then
    MsgBox "Decimal is a numeric field", vbInformation, "i-4.Migration"
    controllaTipoDati = False
  End If
End Function

Private Sub cmdSave_Click()
  Dim i As Integer
  Dim Tabella As String
  Dim rsColumns As Recordset, rsDel As Recordset
  
  If cmbColumns.ListIndex = 0 Then
    Tabella = "DMheader_columns"
  Else
    Tabella = "DMfooter_columns"
  End If
  
  Set rsDel = m_fun.Open_Recordset("DELETE FROM " & Tabella & " ;")
  For i = 1 To lswColumns.ListItems.Count
    Set rsColumns = m_fun.Open_Recordset("select * from " & Tabella)
    rsColumns.AddNew
    rsColumns!Nome = lswColumns.ListItems(i).text
    rsColumns!Ordinale = i
    rsColumns!Tipo = lswColumns.ListItems(i).SubItems(1)
    rsColumns!lunghezza = lswColumns.ListItems(i).SubItems(2)
    rsColumns!Decimali = lswColumns.ListItems(i).SubItems(3)
    rsColumns!Null = IIf((lswColumns.ListItems(i).SubItems(4) = "Yes"), 0, -1)
    rsColumns!Default = lswColumns.ListItems(i).SubItems(5)
    rsColumns!indice = i
    rsColumns.Update
    rsColumns.Close
  Next i
End Sub

Private Sub cmdstr_Click()
  TxtNome.text = TxtNome.text & cmbTag.list(cmbTag.ListIndex)
End Sub

Private Sub cmdUP_Click()
  Dim app1a As String, app1b As String
  Dim app2a As String, app2b As String
  Dim app3a As String, app3b As String
  Dim app4a As String, app4b As String
  Dim app5a As String, app5b As String
  Dim app6a As String, app6b As String

  If lswColumns.ListItems.Count Then
    If lswColumns.ListItems.Count > 1 And lswColumns.SelectedItem.Index <> 1 Then
      app1a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1)
      app1b = lswColumns.ListItems(lswColumns.SelectedItem.Index)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).text = app1a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).text = app1b
       
      app2a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(1)
      app2b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(1)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(1) = app2a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(1) = app2b
       
      app3a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(2)
      app3b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(2)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(2) = app3a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(2) = app3b
       
      app4a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(3)
      app4b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(3)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(3) = app4a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(3) = app4b
       
      app5a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(4)
      app5b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(4)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(4) = app5a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(4) = app5b
       
      app6a = lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(5)
      app6b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(5)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(5) = app6a
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).SubItems(5) = app6b
      lswColumns.ListItems(lswColumns.SelectedItem.Index - 1).Selected = True
      lswColumns.Refresh
    End If
  End If
End Sub

Private Sub cmdDOWN_Click()
  Dim app1a As String, app1b As String
  Dim app2a As String, app2b As String
  Dim app3a As String, app3b As String
  Dim app4a As String, app4b As String
  Dim app5a As String, app5b As String
  Dim app6a As String, app6b As String

  If lswColumns.ListItems.Count Then
    If lswColumns.ListItems.Count > 1 And lswColumns.SelectedItem <> lswColumns.ListItems(lswColumns.ListItems.Count) Then
      app1a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1)
      app1b = lswColumns.ListItems(lswColumns.SelectedItem.Index)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).text = app1a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).text = app1b
       
      app2a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(1)
      app2b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(1)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(1) = app2a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(1) = app2b
       
      app3a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(2)
      app3b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(2)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(2) = app3a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(2) = app3b
       
      app4a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(3)
      app4b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(3)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(3) = app4a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(3) = app4b
       
      app5a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(4)
      app5b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(4)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(4) = app5a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(4) = app5b
       
      app6a = lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(5)
      app6b = lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(5)
      lswColumns.ListItems(lswColumns.SelectedItem.Index).SubItems(5) = app6a
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).SubItems(5) = app6b
      lswColumns.ListItems(lswColumns.SelectedItem.Index + 1).Selected = True
      lswColumns.Refresh
    End If
  End If
End Sub

Private Sub Form_Load()
  Dim rsColumns As Recordset
  Dim listx As ListItem
  
  SetParent Me.hwnd, DataMan.DmParent
  
  Caption = "Extra " & TipoDB & "Columns "
  
  lswColumns.ColumnHeaders(1).Width = lswColumns.Width / 6 * 0.99
  lswColumns.ColumnHeaders(2).Width = lswColumns.Width / 6 * 0.99
  lswColumns.ColumnHeaders(3).Width = lswColumns.Width / 6 * 0.99
  lswColumns.ColumnHeaders(4).Width = lswColumns.Width / 6 * 0.99
  lswColumns.ColumnHeaders(5).Width = lswColumns.Width / 6 * 0.99
  lswColumns.ColumnHeaders(6).Width = lswColumns.Width / 6 * 0.99
  
  Carica_Combo_Tipo TipoDB, cmbTipo
  
  cmbColumns.AddItem "Header Columns"
  cmbColumns.AddItem "Footer Columns"
  
  cmbColumns.ListIndex = 0
  cmbDefault.ListIndex = 0
  CmbNull.ListIndex = 0
  cmbTag.ListIndex = 0
  cmbTipo.ListIndex = 0
  txtLength.text = 0
  txtDecimali = 0
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub lswColumns_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswColumns.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswColumns.SortOrder = Order
  lswColumns.SortKey = Key - 1
  lswColumns.Sorted = True
End Sub
