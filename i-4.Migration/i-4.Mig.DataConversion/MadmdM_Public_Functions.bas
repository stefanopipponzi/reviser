Attribute VB_Name = "MadmdM_Public_Functions"
Option Explicit

Type t_Copy
  IdOggetto As Long
  Nome As String
  Area As String
  Position As Long
  PhisicalPath As String
  Tipo As String
End Type

Global AddTab As Boolean
Global EditTab As Boolean
Global NomeCmp As String
'**************************
Global BoolPrimo As Boolean
Global AzionePossibile As Boolean
Global StatoKey As String
'**************************
Global Tipp_Nodo As String
Global Nodo_Select As String
Global BoolSelect As Boolean
Global TBSPC_N_E As Boolean
Global Cambiamenti As Boolean
Global Cambia_Col As Boolean
Global Cambia_Idx As Boolean

'**************************

Global m_Array_Char(35) As String

Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long

Global bool As Boolean 'SERVE A DEFINIRE LO STATO DEL BOTTONE APPLICA

'A.P.I. per disallocare il popup menu in memoria
Public Declare Function DestroyMenu Lib "user32" (ByVal HMenu As Long) As Long
Public Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

'A.P.I. per il resize dei font
Public Declare Function GetDesktopWindow Lib "user32" () As Long
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Long, ByVal nIndex As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hdc As Long) As Long
Public Const LOGPIXELSX = 88
Public Const LOGPIXELSY = 90

Sub ResizeControls(frmName As Form, Optional IsMenu As Boolean, Optional TopListView As Boolean)
  m_fun.ResizeControls frmName
End Sub

Sub WaitTime(Secondi)
  Dim wTimer As ValueConstants
  
  wTimer = Timer
  While Timer < wTimer + Secondi
    DoEvents
  Wend
End Sub

'***************************************************************************
'********************* Assegna un valore di default  ***********************
'***************************************************************************
Public Function NVLS(pv_vP As Variant, pv_vDefault As Variant) As Variant
  If (Len(Trim((pv_vP))) > 0) Then
    NVLS = pv_vP
  Else
    NVLS = pv_vDefault
  End If
End Function

Public Function Carica_Menu() As Boolean
  SwMenu = True
  
  'inizializza la dll rendendo visibili le variabili della classe anche alle form
  Carica_Menu = True
  
  'crea il menu in memoria
  ReDim Dmmenu(0)
  
  'Primo Bottone del menu a sinistra : dm
  ReDim Preserve Dmmenu(UBound(Dmmenu) + 1)
  Dmmenu(UBound(Dmmenu)).id = 200
  Dmmenu(UBound(Dmmenu)).Label = "Data Manager"
  Dmmenu(UBound(Dmmenu)).ToolTipText = ""
  Dmmenu(UBound(Dmmenu)).Picture = ""
  Dmmenu(UBound(Dmmenu)).M1Name = ""
  Dmmenu(UBound(Dmmenu)).M1SubName = ""
  Dmmenu(UBound(Dmmenu)).M2Name = "<Root>"
  Dmmenu(UBound(Dmmenu)).M3Name = ""
  Dmmenu(UBound(Dmmenu)).M4Name = ""
  Dmmenu(UBound(Dmmenu)).Funzione = ""
  Dmmenu(UBound(Dmmenu)).DllName = "MadmdP_00"
  Dmmenu(UBound(Dmmenu)).TipoFinIm = ""
    
  '1) Sottobottone : Data Manager
  ReDim Preserve Dmmenu(UBound(Dmmenu) + 1)
  Dmmenu(UBound(Dmmenu)).id = 203
  Dmmenu(UBound(Dmmenu)).Label = "Data Bases" 'SQ
  Dmmenu(UBound(Dmmenu)).ToolTipText = ""
  Dmmenu(UBound(Dmmenu)).Picture = DataMan.DmImgDir & "\DataManagerNew.ico"
  Dmmenu(UBound(Dmmenu)).PictureEn = DataMan.DmImgDir & "\DataManagerNew.ico"
  Dmmenu(UBound(Dmmenu)).M1Name = ""
  Dmmenu(UBound(Dmmenu)).M1SubName = ""
  Dmmenu(UBound(Dmmenu)).M2Name = "Data Manager"
  Dmmenu(UBound(Dmmenu)).M3Name = ""
  Dmmenu(UBound(Dmmenu)).M4Name = ""
  Dmmenu(UBound(Dmmenu)).Funzione = "Show_DataManager"
  Dmmenu(UBound(Dmmenu)).DllName = "MadmdP_00"
  Dmmenu(UBound(Dmmenu)).TipoFinIm = ""

  If m_fun.Type_License <> LIC_REV_NODLIIMS And _
     m_fun.Type_License <> LIC_REV_NOIMS And _
     m_fun.Type_License <> LIC_REV_NOAGG Then
    '2) Sottobottone : DLI-->RELAZIONALE o VSAM
    ReDim Preserve Dmmenu(UBound(Dmmenu) + 1)
    Dmmenu(UBound(Dmmenu)).id = 212
    Dmmenu(UBound(Dmmenu)).Label = "IMS-DB --> RDBMS"
    Dmmenu(UBound(Dmmenu)).ToolTipText = ""
    Dmmenu(UBound(Dmmenu)).Picture = DataMan.DmImgDir & "\control panel.ico"
    Dmmenu(UBound(Dmmenu)).PictureEn = DataMan.DmImgDir & "\control panel.ico"
    Dmmenu(UBound(Dmmenu)).M1Name = ""
    Dmmenu(UBound(Dmmenu)).M1SubName = ""
    Dmmenu(UBound(Dmmenu)).M2Name = "Data Manager"
    Dmmenu(UBound(Dmmenu)).M3Name = ""
    Dmmenu(UBound(Dmmenu)).M4Name = ""
    Dmmenu(UBound(Dmmenu)).Funzione = "Show_DLIMigration"
    Dmmenu(UBound(Dmmenu)).DllName = "MadmdP_00"
    Dmmenu(UBound(Dmmenu)).TipoFinIm = ""
    
    'ALE
    ReDim Preserve Dmmenu(UBound(Dmmenu) + 1)
    Dmmenu(UBound(Dmmenu)).id = 213
    Dmmenu(UBound(Dmmenu)).Label = "VSAM --> RDBMS"
    Dmmenu(UBound(Dmmenu)).ToolTipText = ""
    Dmmenu(UBound(Dmmenu)).Picture = DataMan.DmImgDir & "\documents2.ico"
    Dmmenu(UBound(Dmmenu)).PictureEn = DataMan.DmImgDir & "\documents2.ico"
    Dmmenu(UBound(Dmmenu)).M1Name = ""
    Dmmenu(UBound(Dmmenu)).M1SubName = ""
    Dmmenu(UBound(Dmmenu)).M2Name = "Data Manager"
    Dmmenu(UBound(Dmmenu)).M3Name = ""
    Dmmenu(UBound(Dmmenu)).M4Name = ""
    Dmmenu(UBound(Dmmenu)).Funzione = "Show_VSAM_Migration"
    Dmmenu(UBound(Dmmenu)).DllName = "MadmdP_00"
    Dmmenu(UBound(Dmmenu)).TipoFinIm = ""
  End If
    
  '3) Sottobottone : DB2 --> ORACLE
  ReDim Preserve Dmmenu(UBound(Dmmenu) + 1)
  Dmmenu(UBound(Dmmenu)).id = 214
  Dmmenu(UBound(Dmmenu)).Label = "DB2 --> Oracle"
  Dmmenu(UBound(Dmmenu)).ToolTipText = ""
  Dmmenu(UBound(Dmmenu)).Picture = DataMan.DmImgDir & "\Group.ico"
  Dmmenu(UBound(Dmmenu)).PictureEn = DataMan.DmImgDir & "\Group.ico"
  Dmmenu(UBound(Dmmenu)).M1Name = ""
  Dmmenu(UBound(Dmmenu)).M1SubName = ""
  Dmmenu(UBound(Dmmenu)).M2Name = "Data Manager"
  Dmmenu(UBound(Dmmenu)).M3Name = ""
  Dmmenu(UBound(Dmmenu)).M4Name = ""
  Dmmenu(UBound(Dmmenu)).Funzione = "Show_RDBMS_CONVERTER"
  Dmmenu(UBound(Dmmenu)).DllName = "MadmdP_00"
  Dmmenu(UBound(Dmmenu)).TipoFinIm = "Immediata"
End Function

Public Function NormalizzaRiga(testo As String) As String
  Dim Ind As Integer
  Dim PosInit As Integer
  Dim Spazio As Boolean
  Dim TestoAppoggio As String
  
  PosInit = 1
  TestoAppoggio = ""
  
  For Ind = 1 To Len(testo)
    If Mid(testo, Ind, 1) <> " " And Len(testo) > Ind Then
      Spazio = False
    Else
      If Spazio = False Then
        TestoAppoggio = TestoAppoggio & Mid(testo, PosInit, Ind + 1 - PosInit)
        Spazio = True
      End If
      PosInit = Ind + 1
    End If
  Next Ind
  NormalizzaRiga = TestoAppoggio
End Function

Public Sub Carica_Lista_DB(Lsw As ListView, TipoDataB As String)
  Dim r As Recordset
  Dim SQL As String
  Dim MaxNum As Long
  Dim i As Long
  
  Lsw.ListItems.Clear
  NomeDatabaseCorrente = ""
  IDDatabaseCorrente = 0
  
  'carica la lista della form per la creazione di un nuovo DB
  Select Case Trim(UCase(TipoDataB))
    Case "DB2"
      SQL = "Select Distinct IdDb, Nome From DMDB2_DB Order By Nome"
    Case "ORACLE"
      SQL = "Select Distinct IdDb, Nome From DMORC_DB Order By Nome"
    Case "VSAM"
  End Select

  Set r = m_fun.Open_Recordset(SQL)
  While Not r.EOF
    Lsw.ListItems.Add , , r!idDB, , 1
    Lsw.ListItems(Lsw.ListItems.count).ListSubItems.Add , , r!Nome
    
    MaxNum = MaxNum + 1
    
    r.MoveNext
  Wend
  r.Close

 'alloca le righe rimanenti alle 100 previste
  For i = 1 To 100 - MaxNum
    Lsw.ListItems.Add , , ""
    Lsw.ListItems(Lsw.ListItems.count).ListSubItems.Add , , ""
  Next i
End Sub

Sub SettaFileBase()
  ' PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'VARIABILI DA RIMUOVERE DOPO AVER FATTO MAPPA PARAMETRI
  DataMan.DmParam_MaxSize = 40
  ' DataMan.DmParam_PercCpyStruc = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & DbType & "\CPY_BASE_CPDD.cpy"
  DataMan.DmParam_PercCpyStruc = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_CPDD"
  ' DataMan.DmParam_PercCpyField = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & DbType & "\CPY_BASE_CPDR.cpy"
  DataMan.DmParam_PercCpyField = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_CPDR"
  ''' DataMan.DmParam_PercCpyProgCob = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & wPref & "\CBL_BASE.cbl"
  ' DataMan.DmParam_PercCpyExec = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & DbType & "\CPY_BASE_EXEC.cpy"
  DataMan.DmParam_PercCpyExec = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_EXEC"
  '' DataMan.DmParam_PercJclLoad = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & wPref & "\JCL_BASE_LOAD.jcl"
  '' DataMan.DmParam_PercCpyMove = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & wPref & "\CPY_BASE_MOVE.cpy"
  '' DataMan.DmParam_PercCpyKey = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & wPref & "\CPY_BASE_MOVE_KEY.cpy"
  ' DataMan.DmParam_PercSQLExec = DataMan.DmPathDb & "\input-prj\CPY_BASE\" & DbType & "\DEC_BASE_EXEC.sql"
  DataMan.DmParam_PercSQLExec = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_TABLE"
End Sub

Sub CancellaDB()
  Dim wSql As String
  Dim wIdTable As Variant
  Dim TbTab1 As Recordset
  Dim PrefTab As String
   
  Select Case UCase(TipoDB)
    Case "DB2"
      PrefTab = "DMDB2_"
    Case "ORACLE"
      PrefTab = "DMORC_"
    Case "VSAM"
      PrefTab = "DMVSAM_"
  End Select
   
   Set TbTab1 = m_fun.Open_Recordset("select * from " & PrefTab & "Tabelle where IdDB = " & IDDatabaseCorrente)
  While Not TbTab1.EOF
    wIdTable = TbTab1!IdTable
    
    wSql = " Delete * from " & PrefTab & "IdxCol where idtable = " & wIdTable
    DataMan.DmConnection.Execute wSql
    
    wSql = " Delete * from " & PrefTab & "Index where idtable = " & wIdTable
    DataMan.DmConnection.Execute wSql
    
    wSql = " Delete * from " & PrefTab & "Colonne where idtable = " & wIdTable
    DataMan.DmConnection.Execute wSql
    
    TbTab1.MoveNext
  Wend
  TbTab1.Close
          
  wSql = " Delete * from " & PrefTab & "Tabelle where IdDb = " & IDDatabaseCorrente
  DataMan.DmConnection.Execute wSql
  
  wSql = " Delete * from " & PrefTab & "TableSpaces where IdDb = " & IDDatabaseCorrente
  DataMan.DmConnection.Execute wSql
  
  wSql = " Delete * from " & PrefTab & "DB where IdDb = " & IDDatabaseCorrente
  DataMan.DmConnection.Execute wSql
End Sub

Public Sub SoloNumeri(testo As TextBox)
  If Len(testo) <> 0 Then
    If Len(testo) <> 1 Then
      If IsNumeric(testo) = False Then
        'Select Case Mid$(Testo, Len(Testo), 1)
        '  Case ",", ";", ".", ":", "+", "-", "*", "/", "^"
        testo = Mid(testo, 1, Len(testo) - 1)
        '  Case Else
        '    Testo = Testo
        'End Select
      Else
        Select Case Mid$(testo, Len(testo), 1)
          Case ",", ";", ".", ":", "+", "-", "*", "/", "^"
            testo = Mid(testo, 1, Len(testo) - 1)
          Case Else
            testo = testo
        End Select
      End If
    Else
      If IsNumeric(testo) = False Then
        testo = ""
      Else
        testo = testo
      End If
    End If
  End If
  If Len(testo) <> 0 Then
    testo.SelStart = Len(testo)
  End If
End Sub

Public Sub Load_Array_Char()
  m_Array_Char(0) = "0"
  m_Array_Char(1) = "1"
  m_Array_Char(2) = "2"
  m_Array_Char(3) = "3"
  m_Array_Char(4) = "4"
  m_Array_Char(5) = "5"
  m_Array_Char(6) = "6"
  m_Array_Char(7) = "7"
  m_Array_Char(8) = "8"
  m_Array_Char(9) = "9"
  m_Array_Char(10) = "A"
  m_Array_Char(11) = "B"
  m_Array_Char(12) = "C"
  m_Array_Char(13) = "D"
  m_Array_Char(14) = "E"
  m_Array_Char(15) = "F"
  m_Array_Char(16) = "G"
  m_Array_Char(17) = "H"
  m_Array_Char(18) = "I"
  m_Array_Char(19) = "J"
  m_Array_Char(20) = "K"
  m_Array_Char(21) = "L"
  m_Array_Char(22) = "M"
  m_Array_Char(23) = "N"
  m_Array_Char(24) = "O"
  m_Array_Char(25) = "P"
  m_Array_Char(26) = "Q"
  m_Array_Char(27) = "R"
  m_Array_Char(28) = "S"
  m_Array_Char(29) = "T"
  m_Array_Char(30) = "U"
  m_Array_Char(31) = "V"
  m_Array_Char(32) = "Z"
  m_Array_Char(33) = "X"
  m_Array_Char(34) = "Y"
  m_Array_Char(35) = "W"
End Sub

Public Function Applica_PatternString(Stringa_Origine As String, cPattern As String) As String
  'Applica un pattern a una stringa
  Dim wString() As String, wPattern() As String
  Dim i As Long, j As Long
  Dim wApp As String
  
  'Stringa
  ReDim wString(Len(Stringa_Origine) - 1)
  For i = 0 To Len(Stringa_Origine) - 1
    wString(i) = Mid(Stringa_Origine, i + 1, 1)
  Next i
  
  'Pattern
  ReDim wPattern(Len(cPattern) - 1)
  For i = 0 To Len(cPattern) - 1
    wPattern(i) = Mid(cPattern, i + 1, 1)
  Next i
  
  wApp = ""
  'Elabora la stringa in base al pattern
  For i = 0 To UBound(wPattern)
    If i + 1 <= UBound(wString) Then
      Select Case wPattern(i)
        Case "?"
          'Prende il carattere di origine
          wApp = wApp & wString(i)
        Case "*"
          'Incolla il resto della stringa di origine ed esce
          For j = i To UBound(wString)
            wApp = wApp & wString(j)
          Next j
        Case Else
          wApp = wApp & wPattern(i)
      End Select
    Else
      Select Case wPattern(i)
        Case "?"
        Case "*"
        Case Else
          wApp = wApp & wPattern(i)
      End Select
    End If
  Next i
  Applica_PatternString = wApp
End Function
