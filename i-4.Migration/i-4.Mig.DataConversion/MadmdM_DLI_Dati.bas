Attribute VB_Name = "MadmdM_DLI_Dati"
Option Explicit

Global Contr_Lun As Boolean
Global GbFinMigr As Boolean
Global indexseg As Long
Global indicesegmento As Double
Global Tot_Byte_Sottocampati As Long

Public Type Filler
  Start As Integer
  Fin As Integer
End Type

Public Type Indenta_Liv
  livello As Integer
  Indet As Integer
End Type

Public Type wColor
  Red As Boolean
  Blue As Boolean
  Yellow As Boolean
  grey As Boolean
End Type

Type wField
  IdField As Double
  Name As String
  type As String
  Start As Integer
  MaxLen As Integer
  Seq As Boolean
  Unique As Boolean
  NameIndPunSeg As String
  dbdName As String
  Pointer As String
  Xname As String
  segment As String
  Srch As String
  SubSeq As String
  NullVal As String
  Correzione As String
  Index As Boolean
  extRtn As String
  LChild As String
End Type

Public Type wCampo
  Id_Cmp As Long
  IdStruttura As String
  IdOggetto As Long
  idArea As Long
  Ordinale As Integer
  IdSegm As Double
  livello As Integer
  Nome As String
  lunghezza As String
  Posizione As Long
  bytes As Long
  Tipo As String
  Decimali As Long
  segno As String
  Occurs As Long
  OccursFlat As Boolean ' Mauro 15/01/2008
  NomeRed As String
  Usage As String
  Correzione As String
  Index As Boolean
  'SKey As Boolean
  valore As Variant
  Modificato As Boolean
  DaMigrare As Boolean ' Mauro 15/01/2008
End Type

Public Type Stato_iniziale_Campo
  Nome As String
  lunghezza As String
  bytes As Long
  Tipo As String
  Usage As String
End Type

Public Type wStrutture
  IdOggetto As Double
  StrIdOggetto As Double
  StrIdArea As Double
  nomeStruttura As String
  NomeCampo As String
  Ordinale As Integer
  Tipo As String
  Correzione As String
  Campi() As wCampo
  Primario As Boolean
End Type

Public Type wSegm
  Disassociazione As Boolean
  IdSegm As Double
  IdOggetto As Double
  IdOrigine As Double
  IdRelSeg As Double
  RoutineIMS As String
  Name As String
  NameLChild As String
  DbdLchild As String
  PointerLchild As String
  MaxLen As Integer
  MinLen As Integer
  Parent As String
  Pointer As String
  Rules As String
  Comprtn As String
  TbName As String
  gField() As wField
  Correzione As String
  Strutture() As wStrutture
  Condizione As String
  Redefines As Boolean
  'tilvia 30/09/2009
  AliasSegm As String
End Type

Public Type Associazioni
  IdOggetto As Long
  NameFather As String
  NameChild As String
  Relazione As Boolean
  Tag As String
End Type

Public Type Byte_Livello
  livello As Integer
  Byte As Integer
End Type
  
  Global Ind_Liv() As Indenta_Liv
  Global SegAss() As Associazioni
  Global SegAssApp() As Associazioni
  Global BLiv() As Byte_Livello
  
  Global IdStruttura As Double
  
  Global cIdOggetto As Double
  Global cIdArea As Double
  
  Global wIdOggetto As Double
  Global wIdArea As Double

  Global Nomenodo As String
  Global NomeFather As String
  Global NomeVirt As String
  Global IndOgg As Double
  Global Nodo As String
  Global gIdStrutt() As Integer
  Global gSegm() As wSegm
  Global ColoreByte() As wColor
  Global tNodo As String
  Global nomeDbd As String
  Global IdRich As Integer
  Global DBD As String
  Global wFin As String
  Global wStart As String
  Global ChiaveP() As wCampo
  Global ChiaveS() As wCampo
  Global Segmento As String
  Global DataType As String
  Global wIdStruttura As Double
  Global ControlloAreaSeg As Boolean
  Global NomeTavola As String
  Dim wNumRec As Variant
  Dim wIdOgg As Variant
  Dim Start As Integer
  Dim indfld1 As Integer

Public Function CaricaDbD(Database As String)
    
  Dim wRecIn As String
  Dim wIstr As String
  Dim wCmd As String
  Dim k As Integer
  Dim Y As Integer
  Dim wParm As String
  Dim NfileDbd As Variant
  Dim wNome As String
  Dim wAccess As String
  Dim p As Integer
     
  ReDim gSegm(0)
      
   wIstr = ""
   NfileDbd = FreeFile
   Open Database For Input As NfileDbd
   While Not EOF(NfileDbd)
     Line Input #NfileDbd, wRecIn
     wRecIn = Mid$(wRecIn & Space$(80), 1, 80)
     If Mid$(wRecIn, 1, 1) <> "*" Then
        wIstr = Mid$(wRecIn, 1, 71)
        While Mid$(wRecIn, 72, 1) <> Space$(1)
          Line Input #NfileDbd, wRecIn
          wRecIn = Mid$(wRecIn & Space$(80), 1, 80)
          wIstr = wIstr & Mid$(wRecIn, 1, 71)
        Wend
        wIstr = LTrim(wIstr)
        k = InStr(wIstr, " ")
        If k = 0 Then k = 1
        wCmd = Mid$(wIstr, 1, k - 1)
        
        Select Case wCmd
            
                Case "SEGM"
                   
                   Dim nSegm As Integer
                   nSegm = 0
                   ReDim Preserve gSegm(UBound(gSegm) + 1)
                        
                        k = InStr(wIstr, "NAME=")
                        wParm = Mid$(wIstr, k + 5)
                        k = InStr(wParm, ",")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        gSegm(UBound(gSegm)).Name = wNome
                        
                        k = InStr(wIstr, "PARENT=")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 7)
                            If Mid$(wIstr, k + 7, 1) <> "(" Then
                                k = InStr(wParm, ",")
                                If k = 0 Then k = Len(wParm) + 1
                                wNome = Trim(Mid$(wParm, 1, k - 1))
                                While Mid$(wNome, 1, 1) = "("
                                  wNome = Mid$(wNome, 2)
                                Wend
                                gSegm(UBound(gSegm)).Parent = wNome
                            Else
                                k = InStr(wIstr, "PARENT=((")
                                wParm = Mid$(wIstr, k + 9)
                                k = InStr(wParm, ",")
                                If k = 0 Then k = Len(wParm) + 1
                                wNome = Trim(Mid$(wParm, 1, k - 1))
                                While Mid$(wNome, 1, 1) = "("
                                  wNome = Mid$(wNome, 2)
                                Wend
                                gSegm(UBound(gSegm)).Parent = wNome
                            End If
                        End If
                    
                        k = InStr(wIstr, "BYTES=(")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 7)
                            k = InStr(wParm, ",")
                            p = InStr(wParm, ")")
                            If k > p Or k = 0 Then
                                k = p
                            End If
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            gSegm(UBound(gSegm)).MaxLen = wNome
                            wParm = Mid$(wParm, k + 1)
                            k = InStr(wParm, ")")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            If wNome = "" Then wNome = "0"
                            gSegm(UBound(gSegm)).MinLen = wNome
                        Else
                            k = InStr(wIstr, "BYTES=")
                            If k > 0 Then
                                wParm = Mid$(wIstr, k + 6)
                                k = InStr(wParm, ",")
                                If k = 0 Then k = Len(wParm) + 1
                                wNome = Trim(Mid$(wParm, 1, k - 1))
                                gSegm(UBound(gSegm)).MaxLen = wNome
                                gSegm(UBound(gSegm)).MinLen = 0
                            End If
                        End If
                    
                        k = InStr(wIstr, "COMPRTN=")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 8)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            gSegm(UBound(gSegm)).Comprtn = wNome
                        End If
                   
                        k = InStr(wIstr, "RULES=(,")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 8)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 2))
                            k = InStr(wNome, ")")
                            If k > 0 Then wNome = Mid$(wNome, 1, k - 1)
                            gSegm(UBound(gSegm)).Rules = wNome
                        End If
                        
                        k = InStr(wIstr, "POINTER=(")
                        If k > 0 Then
                             wParm = Mid$(wIstr, k + 9)
                             k = InStr(wParm, ",")
                             If k = 0 Then k = Len(Trim(wParm)) + 1
                             wNome = Trim(Mid$(wParm, 1, k - 2))
                             gSegm(UBound(gSegm)).Pointer = wNome
                        Else
                            k = InStr(wIstr, "POINTER=")
                            If k > 0 Then
                                wParm = Mid$(wIstr, k + 8)
                                k = InStr(wParm, ",")
                                If k = 0 Then k = Len(Trim(wParm)) + 1
                                wNome = Trim(Mid$(wParm, 1, k - 1))
                                gSegm(UBound(gSegm)).Pointer = wNome
                            End If
                        End If
                    
                        k = InStr(wIstr, "PTR=")
                        If k > 0 Then
                             wParm = Mid$(wIstr, k + 4)
                             k = InStr(wParm, ",")
                             If k = 0 Then k = Len(Trim(wParm)) + 1
                             wNome = Trim(Mid$(wParm, 1, k - 1))
                             gSegm(UBound(gSegm)).Pointer = wNome
                        End If
                        
                        If gSegm(UBound(gSegm)).Comprtn = "" Then gSegm(UBound(gSegm)).Comprtn = "None"
                        If gSegm(UBound(gSegm)).Rules = "" Then gSegm(UBound(gSegm)).Rules = "None"
                        If gSegm(UBound(gSegm)).Pointer = "" Then gSegm(UBound(gSegm)).Pointer = "None"
                        
                        nSegm = UBound(gSegm)
                        ReDim Preserve gSegm(UBound(gSegm)).gField(0)
                    
                Case "DBD"
                       k = InStr(wIstr, "NAME=")
                       wParm = Mid$(wIstr, k + 5)
                       k = InStr(wParm, ",")
                       If k = 0 Then k = Len(wParm) + 1
                       wNome = Trim(Mid$(wParm, 1, k - 1))
                       k = InStr(wIstr, "ACCESS=")
                       wParm = Mid$(wIstr, k + 7)
                       k = InStr(wParm, ",")
                       If k = 0 Then k = Len(wParm) + 1
                       wAccess = Trim(Mid$(wParm, 1, k - 1))
                
                Case "FIELD"
        
                    With gSegm(UBound(gSegm))
                    ReDim Preserve .gField(UBound(.gField) + 1)
                   
                    k = InStr(wIstr, "NAME=(")
                     If k > 0 Then
                        wParm = Mid$(wIstr, k + 6)
                        k = InStr(wParm, ")")
                        If k > 0 Then wParm = Mid$(wParm, 1, k - 1)
                        k = InStr(wParm, ",")
                        If k > 0 Then
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            .gField(UBound(.gField)).Name = wNome
                            wParm = Mid$(wParm, k + 1)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            If wNome = "SEQ" Then .gField(UBound(.gField)).Seq = True
                            wParm = Mid$(wParm, k + 1)
                            If Trim(wParm) = "" Then .gField(UBound(.gField)).Unique = True
                            If Trim(wParm) <> "" Then
                                k = InStr(wParm, ")")
                                If k = 0 Then k = Len(wParm) + 1
                                wNome = Trim(Mid$(wParm, 1, k - 1))
                                If wNome = "U" Or wNome <> "M" Then .gField(UBound(.gField)).Unique = True
                            End If
                        Else
                            k = InStr(wIstr, "NAME=")
                            wParm = Mid$(wIstr, k + 5)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            .gField(UBound(.gField)).Name = wNome
                        End If
                     Else
                        k = InStr(wIstr, "NAME=")

                        wParm = Mid$(wIstr, k + 5)
                        k = InStr(wParm, ",")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        .gField(UBound(.gField)).Name = wNome
                     End If
                    
                    If Mid$(wNome, 1, 1) = "/" Then
                        .gField(UBound(.gField)).Name = wNome
                        .gField(UBound(.gField)).Seq = False
                        .gField(UBound(.gField)).Unique = False
                        .gField(UBound(.gField)).type = "(None)"
                        .gField(UBound(.gField)).MaxLen = 0
                        .gField(UBound(.gField)).Start = 0
                    End If
                                      
                    If Mid$(wNome, 1, 1) <> "/" Then
                        k = InStr(wIstr, "TYPE=")
                        If k <> 0 Then
                            wParm = Mid$(wIstr, k + 5)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            .gField(UBound(.gField)).type = wNome
                        Else
                            .gField(UBound(.gField)).type = "C"
                        End If
                    
                        k = InStr(wIstr, "START=")
                        
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 6)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            .gField(UBound(.gField)).Start = wNome
                        End If
                        
                        k = InStr(wIstr, "BYTES=")
                        
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 6)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            .gField(UBound(.gField)).MaxLen = wNome
                        End If
                    End If
                End With
        
        Case "LCHILD"
            
                With gSegm(UBound(gSegm))
                    With .gField(UBound(.gField))
                    If UBound(gSegm) = 1 And UBound(gSegm(UBound(gSegm)).gField) = 1 Then
                        If (gSegm(UBound(gSegm)).gField(1).dbdName) = "" Then
                            k = InStr(wIstr, "NAME=(")
                                If k > 0 Then
                                     wParm = Mid$(wIstr, k + 6)
                                     k = InStr(wParm, ",")
                                     If k = 0 Then k = Len(wParm) + 1
                                     wNome = Trim(Mid$(wParm, 1, k - 1))
                                     .NameIndPunSeg = wNome
                                 End If
                                 
                                 wParm = Mid$(wParm, k + 1)
                                 k = InStr(wParm, ")")
                                 If k = 0 Then k = Len(wParm) + 1
                                 wNome = Trim(Mid$(wParm, 1, k - 1))
                                 .dbdName = wNome
                             
                                 k = InStr(wIstr, "POINTER=")
                                 If k > 0 Then
                                     wParm = Mid$(wIstr, k + 8)
                                     k = InStr(wParm, " ")
                                     If k = 0 Then k = Len(wParm) + 1
                                     wNome = Trim(Mid$(wParm, 1, k - 1))
                                     .Pointer = wNome
                                 End If
                                 
                                 k = InStr(wIstr, "PTR=")
                                 If k > 0 Then
                                     wParm = Mid$(wIstr, k + 4)
                                     k = InStr(wParm, " ")
                                     If k = 0 Then k = Len(wParm) + 1
                                     wNome = Trim(Mid$(wParm, 1, k - 1))
                                     .Pointer = wNome
                                 End If
                        End If
                    End If
                                        
                    If UBound(gSegm) = 1 And UBound(gSegm(UBound(gSegm)).gField) = 0 And gSegm(1).NameLChild = "" Then
                        k = InStr(wIstr, "NAME=(")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 6)
                            k = InStr(wParm, ",")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            gSegm(1).NameLChild = wNome
                        End If
                        
                        wParm = Mid$(wParm, k + 1)
                        k = InStr(wParm, ")")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        gSegm(1).DbdLchild = wNome
                    
                        k = InStr(wIstr, "POINTER=")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 8)
                            k = InStr(wParm, " ")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            gSegm(1).PointerLchild = wNome
                        End If
                        
                        k = InStr(wIstr, "PTR=")
                        If k > 0 Then
                            wParm = Mid$(wIstr, k + 4)
                            k = InStr(wParm, " ")
                            If k = 0 Then k = Len(wParm) + 1
                            wNome = Trim(Mid$(wParm, 1, k - 1))
                            gSegm(1).PointerLchild = wNome
                        End If
                    End If
                    End With
                End With
        End Select
     End If
  Wend
Close #NfileDbd
End Function

Public Sub Figli(Database As String)

  Dim Afield As wField
  Dim wRecIn As String
  Dim wIstr As String
  Dim wCmd As String
  Dim k As Integer
  Dim Y As Integer
  Dim wParm As String
  Dim NfileDbd As Variant
  Dim nLchild As Integer
  Dim wNome As String
  Dim wAccess As String
  
  nLchild = 0
   
  Afield.NameIndPunSeg = ""
  Afield.Xname = ""
   
   wIstr = ""
   NfileDbd = FreeFile
   Open Database For Input As NfileDbd
  
   While Not EOF(NfileDbd)
     Line Input #NfileDbd, wRecIn
     wRecIn = Mid$(wRecIn & Space$(80), 1, 80)
     If Mid$(wRecIn, 1, 1) <> "*" Then
        wIstr = Mid$(wRecIn, 1, 71)
        While Mid$(wRecIn, 72, 1) <> Space$(1)
          Line Input #NfileDbd, wRecIn
          wRecIn = Mid$(wRecIn & Space$(80), 1, 80)
          wIstr = wIstr & Mid$(wRecIn, 1, 71)
        Wend
        wIstr = LTrim(wIstr)
        k = InStr(wIstr, " ")
        If k = 0 Then k = 1
        wCmd = Mid$(wIstr, 1, k - 1)
        
        Select Case wCmd

            Case "LCHILD"
                
                If Afield.NameIndPunSeg <> "" Then
                    CarLchild Afield
                    ResettaXfield Afield ' RICHIAMA ROUT DI ABBLENCAMENTO XFIELD
                End If
                If nLchild <> 0 Then
                    k = InStr(wIstr, "NAME=(")
                    If k > 0 Then
                        wParm = Mid$(wIstr, k + 6)
                        k = InStr(wParm, ",")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        Afield.NameIndPunSeg = wNome
                    End If
                    wParm = Mid$(wParm, k + 1)
                    k = InStr(wParm, ")")
                    If k = 0 Then k = Len(wParm) + 1
                    wNome = Trim(Mid$(wParm, 1, k - 1))
                    Afield.dbdName = wNome
                
                    k = InStr(wIstr, "POINTER=")
                    If k > 0 Then
                        wParm = Mid$(wIstr, k + 8)
                        k = InStr(wParm, " ")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        Afield.Pointer = wNome
                    End If
                                        
                    k = InStr(wIstr, "PTR=")
                    If k > 0 Then
                        wParm = Mid$(wIstr, k + 4)
                        k = InStr(wParm, " ")
                        If k = 0 Then k = Len(wParm) + 1
                        wNome = Trim(Mid$(wParm, 1, k - 1))
                        Afield.Pointer = wNome
                    End If
                    
                                        
                 End If
                 nLchild = 1
                     
            Case "XDFLD"
                     
                If Afield.Xname <> "" Then
                    CarLchild Afield
                    ResettaXfield Afield ' RICHIAMA ROUT DI ABBLENCAMENTO XFIELD
                End If
                k = InStr(wIstr, "NAME=")
                If k > 0 Then
                    wParm = Mid$(wIstr, k + 5)
                    k = InStr(wParm, ",")
                    If k = 0 Then k = Len(wParm) + 1
                    wNome = Trim(Mid$(wParm, 1, k - 1))
                    Afield.Xname = wNome
                Else
                    Afield.Xname = ""
                End If
                
                k = InStr(wIstr, "SEGMENT=")
                If k > 0 Then
                    wParm = Mid$(wIstr, k + 8)
                    k = InStr(wParm, ",")
                    If k = 0 Then k = Len(wParm) + 1
                    wNome = Trim(Mid$(wParm, 1, k - 1))
                    Afield.segment = wNome
                Else
                    Afield.segment = ""
                End If
                
                k = InStr(wIstr, "SRCH=(")
                If k > 0 Then
                    wParm = Mid$(wIstr, k + 6)
                    k = InStr(wParm, ",")
                    If k = 0 Then k = Len(Trim(wParm)) + 1
                    wNome = Trim(Mid$(Trim(wParm), 1, k - 2))
                    Afield.Srch = wNome
                Else
                    k = InStr(wIstr, "SRCH=")
                    If k > 0 Then
                       wParm = Mid$(wIstr, k + 5)
                       k = InStr(wParm, ",")
                       If k = 0 Then k = Len(Trim(wParm)) + 1
                       wNome = Trim(Mid$(wParm, 1, k - 1))
                       Afield.Srch = wNome
                    Else
                        Afield.Srch = ""
                    End If
                End If
                
                k = InStr(wIstr, "SUBSEQ=")
                If k > 0 Then
                    wParm = Mid$(wIstr, k + 7)
                    k = InStr(wParm, ",")
                    If k = 0 Then k = Len(wParm) + 1
                    wNome = Trim(Mid$(wParm, 1, k - 1))
                    Afield.SubSeq = wNome
                Else
                    Afield.SubSeq = ""
                End If
                
                k = InStr(wIstr, "NULLVAL=")
                If k > 0 Then
                    wParm = Mid$(wIstr, k + 8)
                    k = InStr(wParm, " ")
                    If k = 0 Then k = Len(wParm) + 1
                    wNome = Trim(Mid$(wParm, 1, k - 1))
                    Afield.NullVal = wNome
                Else
                    Afield.NullVal = ""
                End If
                If Afield.Xname <> "" Or Afield.NameIndPunSeg <> "" Then
                  CarLchild Afield
                  ResettaXfield Afield ' RICHIAMA ROUT DI ABBLENCAMENTO XFIELD
                End If
            End Select
      End If
    Wend
'    If Afield.xName <> "" Or Afield.NameIndPunSeg <> "" Then
'        CarLchild Afield
'        ResettaXfield Afield ' RICHIAMA ROUT DI ABBLENCAMENTO XFIELD
'    End If
    Close #NfileDbd
End Sub
Sub CarLchild(Cfield As wField)

  Dim K1 As Integer
  Dim k2 As Integer
  
  For K1 = 1 To UBound(gSegm)
  If gSegm(K1).Name = Cfield.segment Then
    For k2 = 1 To UBound(gSegm(K1).gField)
      If gSegm(K1).gField(k2).Name = Cfield.Srch Then
      ' CARICA VALORI DI XFIELD IN GSEGM.GFIELD
          With gSegm(K1).gField(k2)
            .NameIndPunSeg = Cfield.NameIndPunSeg
            .dbdName = Cfield.dbdName
            .Pointer = Cfield.Pointer
            .Xname = Cfield.Xname
            .segment = Cfield.segment
            .Srch = Cfield.Srch
            .NullVal = Cfield.NullVal
            .SubSeq = Cfield.SubSeq
            Exit Sub
          End With
      End If
    Next k2
   End If
  Next K1
    
End Sub


Sub ResettaXfield(Xfield As wField)
  With Xfield
    .dbdName = ""
    .Name = ""
    .NameIndPunSeg = ""
    .NullVal = ""
    .Pointer = ""
    .segment = ""
    .Srch = ""
    .Xname = ""
  End With
End Sub

Public Function recordsetAree(idSeg As Double) As String()
  Dim tbmg As Recordset, tbps As Recordset
  Dim App() As String
  Dim i As Long
  
  On Error GoTo errorh
  
  ReDim App(0)
  i = 0
  
  Set tbmg = m_fun.Open_Recordset("select * from MgRel_SegAree where idsegmento = " & idSeg)
  Do Until tbmg.EOF
    Set tbps = m_fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & idSeg & _
                                    " AND StrIdOggetto=" & tbmg!StrIdOggetto & _
                                    " AND StrIdArea = " & tbmg!StrIdArea & _
                                    " AND NomeCmp='" & Replace(tbmg!NomeCmp, "'", "''") & "'")
    If tbps.RecordCount = 0 Then
      tbps.AddNew
      tbps!IdSegmento = tbmg!IdSegmento
      tbps!StrIdOggetto = tbmg!StrIdOggetto
      tbps!StrIdArea = tbmg!StrIdArea
      tbps!NomeCmp = tbmg!NomeCmp
      tbps.Update
    End If
    tbps.Close
    i = i + 1
    ReDim Preserve App(i)
    App(i) = tbmg!IdSegmento & tbmg!StrIdOggetto & tbmg!StrIdArea & tbmg!NomeCmp & tbmg!Correzione
    tbmg.MoveNext
  Loop
  tbmg.Close
  
  recordsetAree = App
  Exit Function
errorh:
'  Set tbcorr = m_fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & idseg & " And strIdArea= " & tbmg!StrIdArea)
'  tbcorr!Correzione = tbmg!Correzione
'  tbcorr.Update
'If ERR.Number = -2147217887 Then
'    i = i + 1
'    ReDim Preserve app(i)
'    app(i) = tbmg!IdSegmento & tbmg!StrIdOggetto & tbmg!StrIdArea & tbmg!NomeCmp & tbmg!Correzione
' End If
  'Resume Next
  MsgBox ERR.Description
End Function

Public Function CaricaStruttura() As Boolean
  Dim tb As Recordset, rs As Recordset, Tb2 As Recordset, Tb4 As Recordset, rsMg As Recordset
  Dim indseg As Long, IdOggetto As Long, bytes As Long
  Dim IndFld As Integer, indStr As Integer, indCmp As Integer, IdSegV As Integer
  Dim pos As Long, liv As Long 'VIRGILIO
  Dim Ind As Integer, INDC As Integer
  Dim SQL As String, SQL1 As String
  Dim x As Integer
  Dim wCorr As String, wNomeFile As String
  Dim rsCmp As Recordset
  
  On Error GoTo ERR
    
  ReDim gSegm(0)
  IdOggetto = pIndex_DBCorrente 'Variabile globale dell'idoggetto proveniente dal chiamante
  IndOgg = IdOggetto  'SQ quante ne usiamo?!
  
  'Sorgente DBD per visualizzazione in richTextBox
  Set rs = m_fun.Open_Recordset("select nome,estensione,directory_input from bs_oggetti where " & _
                                "idoggetto = " & IdOggetto)
  If rs.RecordCount Then
     If rs!Estensione <> "" Then
        wNomeFile = rs!Directory_Input & "\" & rs!Nome & "." & rs!Estensione
     Else
        wNomeFile = rs!Directory_Input & "\" & rs!Nome
     End If
     wNomeFile = Replace(wNomeFile, "$", DataMan.DmPathDef)
     MadmdF_EditDli.RTBDBD.fileName = wNomeFile
  End If
  rs.Close
  
  'SEGMENTI "ORIGINALI"
  Set rs = m_fun.Open_Recordset("select * from PSDLI_segmenti where " & _
                                "idoggetto = " & IdOggetto & " order by idsegmento")
  If rs.RecordCount = 0 Then Exit Function  '???
  While Not rs.EOF
    indseg = UBound(gSegm) + 1
    ReDim Preserve gSegm(indseg)
    ReDim Preserve gSegm(indseg).gField(0)
    ReDim Preserve gSegm(indseg).Strutture(0)
    ReDim Preserve gSegm(indseg).Strutture(0).Campi(0)

    gSegm(indseg).Name = rs!Nome
    gSegm(indseg).MaxLen = rs!MaxLen
    gSegm(indseg).Parent = rs!Parent
    gSegm(indseg).MinLen = rs!MinLen
    gSegm(indseg).Comprtn = rs!Comprtn
    gSegm(indseg).TbName = rs!rdbms_tbname & ""
    gSegm(indseg).Pointer = rs!Pointer
    gSegm(indseg).Rules = rs!Rules
    gSegm(indseg).IdSegm = rs!IdSegmento
    gSegm(indseg).IdOggetto = rs!IdOggetto
    gSegm(indseg).RoutineIMS = " "
    gSegm(indseg).Condizione = IIf(IsNull(rs!Condizione), "", rs!Condizione) 'ALE 08/06/2006
    gSegm(indseg).Redefines = False
    
    'Correzione:
    'SQ 17-07-07 CHIARIRE UNA VOLTA PER TUTTE, I VARI TIPI DI CORREZIONE...
    'Set rsMg = m_fun.Open_Recordset("select * from MgDLI_segmenti where idoggetto = " & idOggetto & " and nome = '" & rs!Nome & "'order by IdSegmento")
    'TMP: per ora escludo i virtuali... ma?! (poi devo andare per nome?)
    Set rsMg = m_fun.Open_Recordset("select * from MgDLI_segmenti where " & _
                                    "idoggetto = " & IdOggetto & " and nome = '" & rs!Nome & "' AND correzione <> 'I' " & _
                                    "order by IdSegmento")
    If rsMg.RecordCount Then
      gSegm(indseg).Correzione = rsMg!Correzione
      'gSegm(indseg).Condizione = IIf(IsNull(tb1!Condizione), "", tb1!Condizione) 'ALE 08/06/2006
    End If
    rsMg.Close
    'SQ: Non so a cosa serva, ma c'era sotto un ciclo orrendo per questo!
    If gSegm(INDC).Correzione = "" Then gSegm(INDC).Correzione = " "
    
    rs.MoveNext
  Wend
  rs.Close
  
  'SEGMENTI VIRTUALI/CORRETTI (tipo?)
  'SQ 10-05-06
  'Set Tb = m_fun.Open_Recordset("select * from MgDLI_segmenti where idoggetto = " & idOggetto & " order by IdSegmento")
  Set rsMg = m_fun.Open_Recordset("select * from MgDLI_segmenti where " & _
                                  "idoggetto = " & IdOggetto & " and correzione <> 'C' and " & _
                                  "IdSegmentoOrigine <> 0 " & _
                                  "order by IdSegmento")
  While Not rsMg.EOF
    'SQ 10-05-06
    'If Tb!IdSegmentoOrigine <> 0 Then
      'If Tb!Correzione <> "C" Then
      indseg = UBound(gSegm) + 1
      ReDim Preserve gSegm(indseg)
      ReDim Preserve gSegm(indseg).gField(0)
      ReDim Preserve gSegm(indseg).Strutture(0)
      ReDim Preserve gSegm(indseg).Strutture(0).Campi(0)
      gSegm(indseg).Name = rsMg!Nome
      gSegm(indseg).MaxLen = rsMg!MaxLen
      gSegm(indseg).Parent = rsMg!Parent
      gSegm(indseg).MinLen = rsMg!MinLen
      gSegm(indseg).Comprtn = rsMg!Comprtn
      gSegm(indseg).TbName = rsMg!rdbms_tbname & ""
      gSegm(indseg).Pointer = rsMg!Pointer
      gSegm(indseg).Rules = rsMg!Rules
      gSegm(indseg).IdSegm = rsMg!IdSegmento
      gSegm(indseg).IdOggetto = rsMg!IdOggetto
      gSegm(indseg).RoutineIMS = " "
      gSegm(indseg).IdOrigine = rsMg!IdSegmentoOrigine
      gSegm(indseg).Correzione = IIf(rsMg!Correzione = "", "I", rsMg!Correzione)
      gSegm(indseg).Condizione = IIf(IsNull(rsMg!Condizione), "", rsMg!Condizione)  'ALE 08/06/2006
      gSegm(indseg).Redefines = rsMg!Redefines  'SQ serve?! - Mauro: Certo ke serve!!!!
    rsMg.MoveNext
  Wend
  rsMg.Close
  
  'SQ ?????????????????????????????
  'For INDC = 1 To UBound(gSegm)
  '  If gSegm(INDC).Correzione = "" Then
  '    gSegm(INDC).Correzione = " "
  '  End If
  'Next INDC
   
  '''''''''''''''''''''''''''''''''''
  ' FIELD:
  '''''''''''''''''''''''''''''''''''
  For indseg = 1 To UBound(gSegm) 'ciclo per tutti i segmenti
    'ALE manca il caricamento dei dati del seg virtuale da MgDLI_field 09/05/2006
    If gSegm(indseg).Correzione <> "I" Then
      Set tb = m_fun.Open_Recordset("select * from PSDLI_field where idsegmento = " & gSegm(indseg).IdSegm)
    Else
      Set tb = m_fun.Open_Recordset("select * from MgDLI_field where idsegmento = " & gSegm(indseg).IdSegm)
    End If
    IndFld = 0
    While Not tb.EOF
      IndFld = IndFld + 1
      ReDim Preserve gSegm(indseg).gField(IndFld)
      gSegm(indseg).gField(IndFld).Name = tb!Nome
      gSegm(indseg).gField(IndFld).type = tb!Tipo
      gSegm(indseg).gField(IndFld).Start = tb!Posizione
      gSegm(indseg).gField(IndFld).MaxLen = tb!lunghezza
      gSegm(indseg).gField(IndFld).Seq = tb!Seq
      gSegm(indseg).gField(IndFld).Unique = tb!Unique
      gSegm(indseg).gField(IndFld).NameIndPunSeg = tb!Ptr_Punseq & ""
      gSegm(indseg).gField(IndFld).dbdName = tb!Ptr_DBDNome & ""
      gSegm(indseg).gField(IndFld).Pointer = tb!Ptr_Pointer & ""
      gSegm(indseg).gField(IndFld).Xname = tb!Ptr_Xnome & ""
      gSegm(indseg).gField(IndFld).segment = tb!Ptr_Segmento & ""
      'SQ - 28-11-05
      'SOLO XDFIELD (VERIFICARE)
      'gSegm(indseg).gField(IndFld).SubSeq = Tb!Ptr_SubSeq
      gSegm(indseg).gField(IndFld).NullVal = tb!Ptr_NullVal & ""
      gSegm(indseg).gField(IndFld).IdField = tb!IdField
      gSegm(indseg).gField(IndFld).Index = tb!Seq         'SQ Tb!Ptr_index   '????Ptr???
      
      'Correzione:
      Set rs = m_fun.Open_Recordset("select correzione,Seq from MGDLI_field where idsegmento = " & gSegm(indseg).IdSegm & " and idfield = " & tb!IdField)
      If rs.RecordCount Then
        gSegm(indseg).gField(IndFld).Correzione = rs!Correzione
        gSegm(indseg).gField(IndFld).Index = rs!Seq
      Else
        gSegm(indseg).gField(IndFld).Correzione = " "
      End If
      rs.Close
      
      tb.MoveNext
    Wend
    tb.Close
    
    '''''''''''''''''''''''''''''''''''''''''
    ' XDFIELD:
    '''''''''''''''''''''''''''''''''''''''''
    If gSegm(indseg).Correzione <> "I" Then
      Set tb = m_fun.Open_Recordset("select * from PSDLI_XDfield where idsegmento = " & gSegm(indseg).IdSegm)
    Else
      Set tb = m_fun.Open_Recordset("select * from MgDLI_XDfield where idsegmento = " & gSegm(indseg).IdSegm)
    End If
    While Not tb.EOF
      IndFld = IndFld + 1
      ReDim Preserve gSegm(indseg).gField(IndFld)
      gSegm(indseg).gField(IndFld).IdField = tb!IdxdField * -1
      gSegm(indseg).gField(IndFld).Name = tb!Nome
      gSegm(indseg).gField(IndFld).Srch = tb!srchFields
      gSegm(indseg).gField(IndFld).SubSeq = tb!SubSeq
      gSegm(indseg).gField(IndFld).extRtn = tb!extRtn
      gSegm(indseg).gField(IndFld).LChild = tb!LChild
      'Correzione:
      Set rs = m_fun.Open_Recordset("select correzione from MGDLI_xdfield where idsegmento = " & gSegm(indseg).IdSegm & " and idxdfield = " & tb!IdxdField)
      If rs.RecordCount Then
        gSegm(indseg).gField(IndFld).Correzione = rs!Correzione
      Else
        gSegm(indseg).gField(IndFld).Correzione = ""
      End If
      rs.Close
      
      tb.MoveNext
  Wend
  tb.Close
    
    ''''''''''''''''''''''''''''''''''''''''''''''
    'INIZIO AREE ASSOCIATE: OLD
    ''''''''''''''''''''''''''''''''''''''''''''''
'    Dim mgVersion As Boolean
'...
    ''''''''''''''''''''''''''''''''''''''''''''''
    Dim mgVersion As Boolean, mgVersionA As Boolean, boolAree As Boolean
    Dim areeAss() As String
    Dim i As Long
    
    areeAss = recordsetAree(gSegm(indseg).IdSegm)
    Set tb = m_fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & gSegm(indseg).IdSegm)
    If tb.RecordCount Then
      indStr = 1  'PARTE DA 2...
      'indstr = 0
      While Not tb.EOF
        indStr = indStr + 1
        ReDim Preserve gSegm(indseg).Strutture(indStr)
        ReDim Preserve gSegm(indseg).Strutture(indStr).Campi(0)
        
        gSegm(indseg).Strutture(indStr).StrIdOggetto = tb!StrIdOggetto  ' = (IdOggetto_contenente l'area + idarea)
        gSegm(indseg).Strutture(indStr).StrIdArea = tb!StrIdArea
        gSegm(indseg).Strutture(indStr).IdOggetto = tb!StrIdOggetto
        
        If UBound(areeAss) > 0 Then
          For i = 1 To UBound(areeAss)
            Dim strApp As String
            strApp = tb!IdSegmento & tb!StrIdOggetto & tb!StrIdArea & tb!NomeCmp
            If strApp = Mid(areeAss(i), 1, Len(areeAss(i)) - 1) Then
              gSegm(indseg).Strutture(indStr).Primario = True
              If Len(areeAss(i)) > Len(strApp) Then
                gSegm(indseg).Strutture(indStr).Correzione = Mid(areeAss(i), Len(areeAss(i)), 1) & "" '14/06/2006
              Else
                gSegm(indseg).Strutture(indStr).Correzione = ""
              End If
              Exit For
            Else
  '            If Mid(areeAss(i), Len(areeAss(i)) - 1, 1) > strApp Then
  '              gSegm(indseg).Strutture(indstr).Correzione = Mid(areeAss(i), Len(areeAss(i)), 1) & "" '14/06/2006
  '            End If
              gSegm(indseg).Strutture(indStr).Primario = False
              gSegm(indseg).Strutture(indStr).Correzione = ""
            End If
          Next
        Else
          gSegm(indseg).Strutture(indStr).Primario = False
          gSegm(indseg).Strutture(indStr).Correzione = ""
        End If
        gSegm(indseg).Strutture(indStr).nomeStruttura = Trim(tb!NomeCmp)
        gSegm(indseg).Strutture(indStr).Tipo = "X"
        gSegm(indseg).Strutture(indStr).NomeCampo = Trim(tb!NomeCmp)

' MAURO 02/01/2007
''''''        ''''''''''''''''''
''''''        ' Data_Area:
''''''        ''''''''''''''''''
'...
''''''        rs.Close
        
        ''''''''''''''''''
        ' Data_Cmp:
        ''''''''''''''''''
        indCmp = 0
        If Len(Trim(tb!NomeCmp)) Then
          'SQ - 11-05-06
'          If Trim(gSegm(indseg).Correzione) = "I" Then
'            SQL = "select * from MgData_cmp where idsegmento = " & gSegm(indseg).IdSegm & " and nome= '" & Trim(Tb!NomeCmp) & "'"
'          Else
'            'SQ chiarire VC...
'            'VC: � una modifica che ho dovuto apportare per i campi che hanno apici utilizzati nella redefines all"interno dei programmi
'            SQL = "select * from PsData_cmp where idoggetto = " & Tb!StrIdOggetto & " and idarea = " & Tb!StrIdArea & " and nome= '" & Trim(Tb!NomeCmp) & "'"
'            'SQL = "select * from PsData_cmp where idoggetto = " & Tb!StrIdOggetto '& " and idarea = " & Tb!StrIdArea & " and nome= " & """" & Trim(Tb!NomeCmp) & """"
'          End If
          If gSegm(indseg).Strutture(indStr).Correzione = "P" Then
            Set rs = m_fun.Open_Recordset("select * from MgData_cmp where idsegmento=" & gSegm(indseg).IdSegm & " and nome= '" & Replace(Trim(tb!NomeCmp), "'", "''") & "'") 'serve solo per recuperare l'ordinale da cui partire
            mgVersion = True
            'SQ
            'SQL1 = "select * from MgData_cmp where idsegmento = " & gSegm(indseg).IdSegm & " and Ordinale>= " & rs!Ordinale & " order by ordinale"
            ' Mauro-SQ 31/01/2007
            If Not rs.EOF Then
              Set Tb2 = m_fun.Open_Recordset("select * from MgData_cmp where idsegmento = " & gSegm(indseg).IdSegm & " and Ordinale>= " & rs!Ordinale & " order by ordinale")
            Else
              ' Casi di segmento concatenato (se inserito solo quello di destra) (oppure errore!)
              Set Tb2 = m_fun.Open_Recordset("select * from MgData_cmp where idsegmento = " & gSegm(indseg).IdSegm & " and Ordinale>=1000 order by ordinale")
            End If
          Else
            Set rs = m_fun.Open_Recordset("select * from PsData_cmp where idoggetto = " & tb!StrIdOggetto & " and idarea = " & tb!StrIdArea & " and nome= '" & Replace(Trim(tb!NomeCmp), "'", "''") & "'")
            mgVersion = False
            'SQL1 = "select * from PsData_cmp where idoggetto = " & Tb!StrIdOggetto & " and idarea = " & Tb!StrIdArea & " and Ordinale>= " & rs!Ordinale & " order by ordinale"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SQ - 4-04-06: gestione strutture tipo:   01 PIPPO.
            '                                            COPY BLUTO.
            'N.B.: l'ordinale di BLUTO parte da 1, come il livello 01... aggiungo "AUX" nella select per fare un ordinamento corretto... (order by UNICO sulla UNION)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SQL1 = "SELECT '0' as AUX,* From PsData_Cmp Where idOggetto = " & Tb!StrIdOggetto & " And IdArea = " & Tb!StrIdArea & " And Ordinale >= " & rs!Ordinale & _
            '      " UNION " & _
            '      "SELECT '1' as AUX,a.* From PsData_Cmp as a,PsData_RelCpy as b WHERE " & _
            '      "b.idOggetto = " & Tb!StrIdOggetto & " And b.IdArea = " & Tb!StrIdArea & " And a.IdOggetto=b.IdCopy order by AUX,ordinale"
            'SQ 17-07-07 pezza volante... chiarire a monte...
            If Not rs.EOF Then
              If Not IsNull(rs!IdCopy) Then
                SQL1 = " UNION SELECT 1 as AUX,* From PsData_Cmp where idOggetto = " & rs!IdCopy & " order by AUX,ORDINALE"
              End If
              Set Tb2 = m_fun.Open_Recordset("SELECT '0' as AUX,* From PsData_Cmp Where idOggetto = " & tb!StrIdOggetto & " And IdArea = " & tb!StrIdArea & " And Ordinale >= " & rs!Ordinale & SQL1)
            Else
              'ma de che?!
              'gestire...
            End If
          End If
          ' Mauro 25/09/2007 : Se tb2 non � stato valorizzato...
          If Not Tb2 Is Nothing Then
            'SQ - PORTATA SOPRA PER MODIFICA UNION
            'Set Tb2 = m_fun.Open_Recordset(SQL1)
            If Tb2.RecordCount = 0 Then
              CaricaStruttura = False
            End If
            
            pos = Tb2!Posizione - 1
            liv = Tb2!livello - 1
            
            '?????????????????????
            If gSegm(indseg).Strutture(indStr).Correzione <> "C" Then
              'gSegm(indseg).Strutture(indstr).Correzione = gSegm(indseg).Strutture(indstr).Correzione & " "
              gSegm(indseg).Strutture(indStr).Correzione = gSegm(indseg).Strutture(indStr).Correzione & ""
            End If
            
            While Not Tb2.EOF
              If Tb2!livello > rs!livello Or (Tb2!livello = rs!livello And rs!Nome = Tb2!Nome) Then
                indCmp = indCmp + 1
                ReDim Preserve gSegm(indseg).Strutture(indStr).Campi(indCmp)
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Nome = Tb2!Nome
                'SQ
                '''''''If Trim(gSegm(indseg).Correzione) <> "I" Then
                If Trim(gSegm(indseg).Strutture(indStr).Correzione) <> "P" Then
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).IdOggetto = Tb2!IdOggetto
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).idArea = Tb2!idArea
                Else
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).IdOggetto = tb!StrIdOggetto
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).idArea = tb!StrIdArea
                End If
                
                
                gSegm(indseg).Strutture(indStr).Campi(indCmp).bytes = Tb2!Byte
                bytes = bytes + Tb2!Byte
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Decimali = TN(Tb2!Decimali, "DECIMAL") 'VIRGILIO
                
                gSegm(indseg).Strutture(indStr).Campi(indCmp).livello = Tb2!livello - liv
                gSegm(indseg).Strutture(indStr).Campi(indCmp).lunghezza = Tb2!lunghezza
                gSegm(indseg).Strutture(indStr).Campi(indCmp).NomeRed = TN(Tb2!NomeRed)
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Occurs = Tb2!Occurs
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Posizione = Tb2!Posizione - pos
                gSegm(indseg).Strutture(indStr).Campi(indCmp).segno = Tb2!segno & ""
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Tipo = Tb2!Tipo
                gSegm(indseg).Strutture(indStr).Campi(indCmp).OccursFlat = False
                gSegm(indseg).Strutture(indStr).Campi(indCmp).DaMigrare = True
                
                If Tb2!Usage = "" Then
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).Usage = ""
                Else
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).Usage = Tb2!Usage & ""
                End If
                
                gSegm(indseg).Strutture(indStr).Campi(indCmp).Ordinale = Tb2!Ordinale
                'SQ
                'If Trim(gSegm(indseg).Correzione) = "I" Then
                If mgVersion Then
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).Correzione = TN(Tb2!Correzione)
                  gSegm(indseg).Strutture(indStr).Campi(indCmp).Id_Cmp = Tb2!IdCmp 'ALE  10/07/2006
                End If
                
              End If
              Tb2.MoveNext
            Wend
          End If
          
          ' Mauro 28/04/06:Mi carico la lunghezza della struttura se i byte del livello 1 sono a 0
          If UBound(gSegm(indseg).Strutture(indStr).Campi) Then
            If gSegm(indseg).Strutture(indStr).Campi(1).bytes = 0 And _
               gSegm(indseg).Strutture(indStr).Campi(1).Tipo = "A" Then
              gSegm(indseg).Strutture(indStr).Campi(1).bytes = bytes
              Set rsCmp = m_fun.Open_Recordset("select * from psdata_cmp where idoggetto =" & tb!StrIdOggetto & " and idarea = " & tb!StrIdArea)
              If rsCmp.RecordCount Then
                rsCmp!Byte = bytes
                rsCmp.Update
              End If
              rsCmp.Close
            End If
          End If
        End If
''''''        ''''''''''''''''''''''''''''''''''''''''''''''''''
''''''        ' Cosa sta facendo?????????????????????????????????
''''''        ' Aggiunge campi?
''''''        ''''''''''''''''''''''''''''''''''''''''''''''''''
''''''        ' Mauro 28-03-2007 : commentato perch� sembra che sia un giro inutile
''''''        If gSegm(indseg).Correzione = "I" Then
''''''          SQL = "select * from MgData_cmp where IdSegmento = " & gSegm(indseg).IdSegm & " order  by ordinale"
''''''          Set rs = m_fun.Open_Recordset(SQL)
''''''          While Not rs.EOF
''''''              INDC = 0
''''''              For Ind = 1 To UBound(gSegm(indseg).Strutture(indstr).Campi)
''''''                If rs!Nome = gSegm(indseg).Strutture(indstr).Campi(Ind).Nome And rs!IdSegmento = gSegm(indseg).IdSegm Then
''''''                  INDC = Ind
''''''                  Exit For
''''''                End If
''''''              Next Ind
''''''              If INDC = 0 Then
''''''                indcmp = indcmp + 1
''''''                ReDim Preserve gSegm(indseg).Strutture(indstr).Campi(indcmp)
''''''                INDC = indcmp
''''''              End If
''''''              gSegm(indseg).Strutture(indstr).Campi(INDC).Nome = rs!Nome
''''''              With gSegm(indseg).Strutture(indstr).Campi(INDC)
''''''                 .Id_Cmp = rs!IdCmp 'ALE 10/07/2006
''''''                 .Bytes = rs!Byte
''''''                 .Decimali = rs!Decimali
''''''                 .Livello = rs!Livello
''''''                 .lunghezza = rs!lunghezza
''''''                 .NomeRed = rs!NomeRed
''''''                 .Occurs = rs!Occurs
''''''                 .Posizione = rs!Posizione
''''''                 .segno = rs!segno
''''''                 .Tipo = rs!Tipo
''''''                 .Usage = rs!Usage
''''''                 .Correzione = rs!Correzione
''''''                 .IdSegm = rs!IdSegmento
''''''''                 gSegm(indseg).Strutture(indstr).Campi(indcmp).Ordinale = rs!Ordinale
''''''''                 gSegm(indseg).Strutture(indstr).Campi(indcmp).valore = rs!Selettore_Valore
''''''                 .Ordinale = rs!Ordinale
''''''                 .valore = rs!Selettore_Valore
''''''                 rs.MoveNext
''''''              End With
''''''          Wend
''''''        End If
        
        tb.MoveNext
      Wend
      'CaricaAreaSeg indseg
      CaricaAreaAssociata indseg

    Else
      'Non ci sono aree associate nella Ps-MgRel_SegAree:
      'Cosa vengo a fare qui????????????????????!!!!!!!!!!!!
      'UNICA AREA (in Strutture(1)) ; le altre le mette in Strutture(2)!!!!
      Set Tb4 = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & gSegm(indseg).IdSegm)
      If Tb4.RecordCount Then
         ReDim Preserve gSegm(indseg).Strutture(1)
         indCmp = 0
         pos = Tb4!Posizione - 1
         liv = Tb4!livello - 1
         While Not Tb4.EOF
              indCmp = indCmp + 1
              ReDim Preserve gSegm(indseg).Strutture(1).Campi(indCmp)
              gSegm(indseg).Strutture(1).Campi(indCmp).Id_Cmp = Tb4!IdCmp 'ALE 10/07/2006
              gSegm(indseg).Strutture(1).Campi(indCmp).Nome = Tb4!Nome
              gSegm(indseg).Strutture(1).Campi(indCmp).bytes = Tb4!Byte
              gSegm(indseg).Strutture(1).Campi(indCmp).Decimali = Tb4!Decimali
              gSegm(indseg).Strutture(1).Campi(indCmp).livello = Tb4!livello - liv
              gSegm(indseg).Strutture(1).Campi(indCmp).lunghezza = Tb4!lunghezza
              gSegm(indseg).Strutture(1).Campi(indCmp).NomeRed = Tb4!NomeRed
              gSegm(indseg).Strutture(1).Campi(indCmp).Occurs = Tb4!Occurs
              gSegm(indseg).Strutture(1).Campi(indCmp).Posizione = Tb4!Posizione - pos
              gSegm(indseg).Strutture(1).Campi(indCmp).segno = Tb4!segno
              gSegm(indseg).Strutture(1).Campi(indCmp).Tipo = Tb4!Tipo
              ' Mauro 15/01/2008
              gSegm(indseg).Strutture(1).Campi(indCmp).OccursFlat = Tb4!OccursFlat
              gSegm(indseg).Strutture(1).Campi(indCmp).DaMigrare = Tb4!DaMigrare

              If Tb4!Usage = "" Then
                gSegm(indseg).Strutture(1).Campi(indCmp).Usage = ""
              Else
                gSegm(indseg).Strutture(1).Campi(indCmp).Usage = Tb4!Usage
              End If

              gSegm(indseg).Strutture(1).Campi(indCmp).Ordinale = Tb4!Ordinale
              gSegm(indseg).Strutture(1).Campi(indCmp).Correzione = Tb4!Correzione

            Tb4.MoveNext
          Wend
      Else
        ''''''''''''''''''''''''''''''''''
        ' CHIARIRE
        ''''''''''''''''''''''''''''''''''
        ''''''''''ReDim gSegm(indseg).Strutture(1)
        ''''''''''AssegnaAreaseg indseg
      End If
    End If
  Next indseg
  
  CaricaTreeRelazioni MadmdF_EditDli.TreeRelazioni
  
  ReDim SegAssApp(0)
  
  For x = 1 To UBound(SegAss)
    ReDim Preserve SegAssApp(UBound(SegAssApp) + 1)
    SegAssApp(UBound(SegAssApp)).IdOggetto = SegAss(x).IdOggetto
    SegAssApp(UBound(SegAssApp)).NameChild = SegAss(x).NameChild
    SegAssApp(UBound(SegAssApp)).NameFather = SegAss(x).NameFather
    SegAssApp(UBound(SegAssApp)).Relazione = SegAss(x).Relazione
    SegAssApp(UBound(SegAssApp)).Tag = SegAss(x).Tag
  Next x
  
  For x = 1 To UBound(SegAss)
    If SegAss(x).Tag <> "Reale" Then
      AggiungiRelazioni SegAss(x).Tag, SegAss(x).NameChild
    End If
  Next x
  
  ReDim SegAss(0)
  
  For x = 1 To UBound(SegAssApp)
    ReDim Preserve SegAss(UBound(SegAss) + 1)
    SegAss(UBound(SegAss)).IdOggetto = SegAssApp(x).IdOggetto
    SegAss(UBound(SegAss)).NameChild = SegAssApp(x).NameChild
    SegAss(UBound(SegAss)).NameFather = SegAssApp(x).NameFather
    SegAss(UBound(SegAss)).Relazione = SegAssApp(x).Relazione
    SegAss(UBound(SegAss)).Tag = SegAssApp(x).Tag
  Next x
  
  
  CaricaStruttura = True
  'Tb.Close
  Exit Function
  
ERR:
  Select Case ERR.Number
    Case 3021
      MsgBox ERR.Description, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case 75
      'OK: non ho il sorgente del DBD da caricare nell'editor
      MsgBox "DBD source file " & wNomeFile & " not found.", vbExclamation, DataMan.DmNomeProdotto
      Resume Next
    Case Else
      MsgBox ERR.Description, vbCritical + vbOKOnly, DataMan.DmNomeProdotto
  End Select
  'Resume
End Function



Sub CaricaColori(lista1 As ListView, lista2 As ListView, val As Integer, val2)
    
    Dim k As Integer
    Dim Index As Integer
    Dim wOver As Integer
    Dim i As Integer
    Dim j As Integer
    
    ReDim ColoreByte(0)
    ReDim Preserve ColoreByte(UBound(ColoreByte) + 1)
    lista1.Refresh

    For i = 1 To lista1.ListItems.count
        wOver = 0
        If lista1.ListItems(i).SubItems(2) <> "0" Then
           Index = Int(lista1.ListItems(i).SubItems(3)) - val + 1
        
        ' Controlla che l' incidenza non sfori a sinistra
           If Index < 1 Then
              wOver = (Index * -1) + 1
              Index = 1
              ColoreByte(UBound(ColoreByte)).Red = True
           End If
        End If
        
        For k = Int(lista1.ListItems(i).SubItems(3)) + wOver To (Int(lista1.ListItems(i).SubItems(3)) + Int(lista1.ListItems(i).SubItems(1)) - 1)
            j = (k - Int(lista2.SelectedItem.SubItems(4))) + 1
            If lista1.ListItems(i).SubItems(2) = "0" Then Exit For
            If UBound(ColoreByte) < j Then
               ReDim Preserve ColoreByte(j)
            End If
            If k = val + val2 Then
                ColoreByte(j - 1).Red = True
                Exit Sub
            End If
            
            If lista1.ListItems(i).SubItems(4) = "X" And lista1.ListItems(i).text = "FILLER" Then
                ColoreByte(j).grey = True
            ElseIf lista1.ListItems(i).SubItems(4) = "X" And lista1.ListItems(i).text <> "FILLER" Then
                ColoreByte(j).Blue = True
            Else
                ColoreByte(j).Yellow = True
            End If
         Next k
    Next i

End Sub

Function Sovrapposizione(Lung As String, pos1 As Integer, bytes1 As Integer, pos2 As Integer, bytes2 As Integer) As Boolean
  If Lung <> "0" _
    And ((((pos1 >= pos2) And (pos1 <= bytes2 + pos2 - 1)) _
    Or ((pos1 <= pos2) And (pos1 + bytes1 >= bytes2 + pos2))) _
    Or (((pos1 + bytes1 - 1 >= pos2) And (pos1 + bytes1 <= bytes2 + pos2))) _
    Or (((pos1 >= pos2) And (pos1 + bytes1 <= bytes2 + pos2)))) Then
    Sovrapposizione = True
  Else
    Sovrapposizione = False
  End If
End Function

Sub CaricaAreaAssociata(seg As Long)
  Dim indCmp As Integer
  Dim indStr As Integer
  Dim indstr2 As Integer
  Dim tb As Recordset
  Dim Tb1 As Recordset
  Dim bool As Boolean
  
  indCmp = 1
  Set tb = m_fun.Open_Recordset("select * from MgData_Area where IdSegmento = " & gSegm(seg).IdSegm & " order by Nome")
  Set Tb1 = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & gSegm(seg).IdSegm & " order by ordinale")
  'controllare se devi controllare anche il record count di tb1
  ReDim Preserve gSegm(seg).Strutture(1).Campi(0)
  If tb.RecordCount Then
    While Not tb.EOF
      ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
      
      gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = tb!IdSegmento
      gSegm(seg).Strutture(1).Campi(indCmp).Nome = tb!Nome
      gSegm(seg).Strutture(1).Campi(indCmp).livello = tb!livello
      gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = Trim(tb!NomeRed & " ")
      'tilvia 30/09/2009
      gSegm(seg).AliasSegm = Trim(tb!Alias_Segm & " ")
      
      'Stop
      If Tb1.RecordCount <> 0 Then
        Tb1.MoveFirst
        While Tb1.EOF = False
        'manca campo picture
        If indCmp > 1 Then
          ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
        End If
        gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = Tb1!IdSegmento
        gSegm(seg).Strutture(1).Campi(indCmp).Nome = Tb1!Nome
        gSegm(seg).Strutture(1).Campi(indCmp).livello = Tb1!livello
        gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = Trim(Tb1!NomeRed & "")
        gSegm(seg).Strutture(1).Campi(indCmp).Id_Cmp = Tb1!IdCmp 'ALE 10/07/2006
        'gSegm(seg).Strutture(1).Campi(indcmp).Modificato = tb1!Modificato
        gSegm(seg).Strutture(1).Campi(indCmp).Ordinale = Tb1!Ordinale
        gSegm(seg).Strutture(1).Campi(indCmp).bytes = Tb1!Byte
        gSegm(seg).Strutture(1).Campi(indCmp).Decimali = Tb1!Decimali
        gSegm(seg).Strutture(1).Campi(indCmp).lunghezza = Tb1!lunghezza
        gSegm(seg).Strutture(1).Campi(indCmp).Occurs = Tb1!Occurs
        gSegm(seg).Strutture(1).Campi(indCmp).Posizione = Tb1!Posizione
        gSegm(seg).Strutture(1).Campi(indCmp).segno = Tb1!segno
        gSegm(seg).Strutture(1).Campi(indCmp).Tipo = Tb1!Tipo
        gSegm(seg).Strutture(1).Campi(indCmp).Usage = Tb1!Usage & ""
        gSegm(seg).Strutture(1).Campi(indCmp).Correzione = TN(Tb1!Correzione)
        gSegm(seg).Strutture(1).Campi(indCmp).valore = Tb1!Selettore_Valore
        ' Mauro 15/01/2008
        gSegm(seg).Strutture(1).Campi(indCmp).OccursFlat = Tb1!OccursFlat
        gSegm(seg).Strutture(1).Campi(indCmp).DaMigrare = Tb1!DaMigrare
        If Trim(Tb1!Selettore_Valore) <> "" Then gSegm(seg).Strutture(1).Campi(indCmp).Correzione = "S"
      
        indCmp = indCmp + 1
        
        Tb1.MoveNext
      
        Wend
      End If
      tb.MoveNext
    Wend
  End If
 
End Sub

Sub CaricaAreaSeg(seg As Long)
  Dim indCmp As Integer
  Dim indStr As Integer
  Dim indstr2 As Integer
  Dim tb As Recordset
  Dim Tb1 As Recordset
  Dim bool As Boolean
  
  indCmp = 1
  Set tb = m_fun.Open_Recordset("select * from MgData_Area where IdSegmento = " & gSegm(seg).IdSegm & " order by Nome")
  Set Tb1 = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & gSegm(seg).IdSegm & " order by ordinale")
  'controllare se devi controllare anche il record count di tb1
  If tb.RecordCount Then
      While Not tb.EOF
        ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
          gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = tb!IdSegmento
          gSegm(seg).Strutture(1).Campi(indCmp).Nome = tb!Nome
          gSegm(seg).Strutture(1).Campi(indCmp).livello = tb!livello
          gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = Trim(tb!NomeRed & " ")
          'tilvia 30/09/2009
          gSegm(seg).AliasSegm = Trim(tb!Alias_Segm & " ")
         
          'Stop
          If Tb1.RecordCount <> 0 Then
            Tb1.MoveFirst
            While Tb1.EOF = False
  
            'manca campo picture
            If indCmp > 1 Then
              ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
            End If
            
            gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = Tb1!IdSegmento
            gSegm(seg).Strutture(1).Campi(indCmp).Nome = Tb1!Nome
            gSegm(seg).Strutture(1).Campi(indCmp).livello = Tb1!livello
            gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = Trim(Tb1!NomeRed & "")
            
            
            gSegm(seg).Strutture(1).Campi(indCmp).Id_Cmp = Tb1!IdCmp 'ALE 10/07/2006
            'gSegm(seg).Strutture(1).Campi(indcmp).Modificato = tb1!Modificato
            gSegm(seg).Strutture(1).Campi(indCmp).Ordinale = Tb1!Ordinale
            gSegm(seg).Strutture(1).Campi(indCmp).bytes = Tb1!Byte
            gSegm(seg).Strutture(1).Campi(indCmp).Decimali = Tb1!Decimali
            gSegm(seg).Strutture(1).Campi(indCmp).lunghezza = Tb1!lunghezza
            gSegm(seg).Strutture(1).Campi(indCmp).Occurs = Tb1!Occurs
            gSegm(seg).Strutture(1).Campi(indCmp).Posizione = Tb1!Posizione
            gSegm(seg).Strutture(1).Campi(indCmp).segno = Tb1!segno
            gSegm(seg).Strutture(1).Campi(indCmp).Tipo = Tb1!Tipo
            gSegm(seg).Strutture(1).Campi(indCmp).Usage = Tb1!Usage
            gSegm(seg).Strutture(1).Campi(indCmp).Correzione = Tb1!Correzione
            gSegm(seg).Strutture(1).Campi(indCmp).valore = Tb1!Selettore_Valore
            ' Mauro 15/01/2008
            gSegm(seg).Strutture(1).Campi(indCmp).OccursFlat = Tb1!OccursFlat
            gSegm(seg).Strutture(1).Campi(indCmp).DaMigrare = Tb1!DaMigrare
            If Trim(Tb1!Selettore_Valore) <> "" Then gSegm(seg).Strutture(1).Campi(indCmp).Correzione = "S"
          
            indCmp = indCmp + 1
            
            Tb1.MoveNext
          
            Wend
          End If
          tb.MoveNext
      Wend
  Else
      '???????????????????????????????????????????????????????????????????????????????
    'SQ 11-05-06
    If UBound(gSegm(seg).Strutture) >= indStr Then
      For indStr = 2 To UBound(gSegm(seg).Strutture)
        If gSegm(seg).Strutture(indStr).Primario Then
          indstr2 = indStr
          bool = True
          Exit For
        End If
      Next
'''''''''''''      '  aggiunta da pallante
'''''''''''''      If Not bool Then
'''''''''''''        indstr = 2
'''''''''''''        indstr2 = indstr
'''''''''''''        bool = True
'''''''''''''        gSegm(seg).Strutture(indstr).Primario = True
'''''''''''''      End If
      '************************
      If bool Then
        If gSegm(seg).Strutture(indStr).Correzione <> "C" Then
          For indCmp = 1 To UBound(gSegm(seg).Strutture(indstr2).Campi)
            ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
            gSegm(seg).Strutture(1).Campi(indCmp).Ordinale = indCmp
            gSegm(seg).Strutture(1).Campi(indCmp).Modificato = gSegm(seg).Strutture(indstr2).Campi(indCmp).Modificato
            gSegm(seg).Strutture(1).Campi(indCmp).Nome = gSegm(seg).Strutture(indstr2).Campi(indCmp).Nome
            gSegm(seg).Strutture(1).Campi(indCmp).bytes = gSegm(seg).Strutture(indstr2).Campi(indCmp).bytes
            gSegm(seg).Strutture(1).Campi(indCmp).Decimali = gSegm(seg).Strutture(indstr2).Campi(indCmp).Decimali
            gSegm(seg).Strutture(1).Campi(indCmp).livello = gSegm(seg).Strutture(indstr2).Campi(indCmp).livello
            gSegm(seg).Strutture(1).Campi(indCmp).lunghezza = gSegm(seg).Strutture(indstr2).Campi(indCmp).lunghezza
            gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = gSegm(seg).Strutture(indstr2).Campi(indCmp).NomeRed
            gSegm(seg).Strutture(1).Campi(indCmp).Occurs = gSegm(seg).Strutture(indstr2).Campi(indCmp).Occurs
            gSegm(seg).Strutture(1).Campi(indCmp).Posizione = gSegm(seg).Strutture(indstr2).Campi(indCmp).Posizione
            gSegm(seg).Strutture(1).Campi(indCmp).segno = gSegm(seg).Strutture(indstr2).Campi(indCmp).segno
            gSegm(seg).Strutture(1).Campi(indCmp).Tipo = gSegm(seg).Strutture(indstr2).Campi(indCmp).Tipo
            gSegm(seg).Strutture(1).Campi(indCmp).Usage = gSegm(seg).Strutture(indstr2).Campi(indCmp).Usage
            gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = gSegm(seg).IdSegm
            gSegm(seg).Strutture(1).Campi(indCmp).Correzione = ""
            gSegm(seg).Strutture(1).Campi(indCmp).valore = gSegm(seg).Strutture(indstr2).Campi(indCmp).valore
            ' Mauro 15/01/2008
            gSegm(seg).Strutture(1).Campi(indCmp).OccursFlat = gSegm(seg).Strutture(indstr2).Campi(indCmp).OccursFlat
            gSegm(seg).Strutture(1).Campi(indCmp).DaMigrare = gSegm(seg).Strutture(indstr2).Campi(indCmp).DaMigrare
          Next indCmp
        End If
      Else
        For indStr = 2 To UBound(gSegm(seg).Strutture)
          If gSegm(seg).Strutture(indStr).Correzione <> "C" Then
            For indCmp = 1 To UBound(gSegm(seg).Strutture(indStr).Campi)
              ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
              gSegm(seg).Strutture(1).Campi(indCmp).Ordinale = indCmp
              gSegm(seg).Strutture(1).Campi(indCmp).Modificato = gSegm(seg).Strutture(indStr).Campi(indCmp).Modificato
              gSegm(seg).Strutture(1).Campi(indCmp).Nome = gSegm(seg).Strutture(indStr).Campi(indCmp).Nome
              gSegm(seg).Strutture(1).Campi(indCmp).bytes = gSegm(seg).Strutture(indStr).Campi(indCmp).bytes
              gSegm(seg).Strutture(1).Campi(indCmp).Decimali = gSegm(seg).Strutture(indStr).Campi(indCmp).Decimali
              gSegm(seg).Strutture(1).Campi(indCmp).livello = gSegm(seg).Strutture(indStr).Campi(indCmp).livello
              gSegm(seg).Strutture(1).Campi(indCmp).lunghezza = gSegm(seg).Strutture(indStr).Campi(indCmp).lunghezza
              gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = gSegm(seg).Strutture(indStr).Campi(indCmp).NomeRed
              gSegm(seg).Strutture(1).Campi(indCmp).Occurs = gSegm(seg).Strutture(indStr).Campi(indCmp).Occurs
              gSegm(seg).Strutture(1).Campi(indCmp).Posizione = gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione
              gSegm(seg).Strutture(1).Campi(indCmp).segno = gSegm(seg).Strutture(indStr).Campi(indCmp).segno
              gSegm(seg).Strutture(1).Campi(indCmp).Tipo = gSegm(seg).Strutture(indStr).Campi(indCmp).Tipo
              gSegm(seg).Strutture(1).Campi(indCmp).Usage = gSegm(seg).Strutture(indStr).Campi(indCmp).Usage
              gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = gSegm(seg).IdSegm
              gSegm(seg).Strutture(1).Campi(indCmp).Correzione = ""
              gSegm(seg).Strutture(1).Campi(indCmp).valore = gSegm(seg).Strutture(indStr).Campi(indCmp).valore
              ' Mauro 15/01/2008
              gSegm(seg).Strutture(1).Campi(indCmp).OccursFlat = gSegm(seg).Strutture(indStr).Campi(indCmp).OccursFlat
              gSegm(seg).Strutture(1).Campi(indCmp).DaMigrare = gSegm(seg).Strutture(indStr).Campi(indCmp).DaMigrare
            Next indCmp
            tb.Close
            Exit For
          End If
        Next indStr
      End If
    End If
  End If
End Sub

Sub CaricaStr(list As ListView, image As ImageList, Nodo)
  Dim indseg As Integer
  Dim indStr As Integer
  Dim KeyImage As String
  
  list.ListItems.Clear
  list.Icons = image
  list.SmallIcons = image
  For indseg = 1 To UBound(gSegm)
    If Trim(Nodo) = Trim(gSegm(indseg).Name) Then
      If UBound(gSegm(indseg).Strutture) Then
        wIdOggetto = gSegm(indseg).Strutture(1).StrIdOggetto
        wIdArea = gSegm(indseg).Strutture(1).StrIdArea
        For indStr = 2 To UBound(gSegm(indseg).Strutture)
          If gSegm(indseg).Strutture(indStr).Primario Then
            KeyImage = 12
          Else
            If Trim(gSegm(indseg).Strutture(indStr).Correzione) = "C" Then
             KeyImage = 4
            Else
             KeyImage = 3
            End If
          End If
          With list.ListItems.Add(indStr - 1, , gSegm(indseg).Strutture(indStr).IdOggetto & "/" & gSegm(indseg).Strutture(indStr).StrIdArea, , val(KeyImage))
            .SubItems(1) = gSegm(indseg).Strutture(indStr).nomeStruttura
            .SubItems(2) = gSegm(indseg).Strutture(indStr).NomeCampo
          End With
        Next indStr
      End If
      Exit For
    End If
   Next indseg
End Sub

Function tipoCampo(combo As ComboBox) As String
  Dim k As String
  k = InStr(combo.text, "COMP-3")
  If k > 0 Then
    tipoCampo = "PCK"
    Exit Function
  End If
  k = InStr(combo.text, "COMP")
  If k > 0 Then
    tipoCampo = "BNR"
    Exit Function
  End If
  tipoCampo = "ZND"
End Function

Sub FillComboTipo(seg As Integer, Struttura As Integer, Campo As Integer, combo As ComboBox)
  With gSegm(seg).Strutture(1).Campi(Campo)
    If .segno = "N" And .Tipo = "X" And .Usage = "ZND" Then
      combo.text = combo.list(0)
  
    ElseIf .segno = "N" And .Tipo = "9" And .Usage = "ZND" Then
     combo.text = combo.list(1)
    ElseIf .segno = "S" And .Tipo = "9" And .Usage = "ZND" Then
      combo.text = combo.list(2)
    ElseIf .segno = "N" And .Tipo = "9" And .Usage = "BNR" Then
      combo.text = combo.list(3)
    ElseIf .segno = "S" And .Tipo = "9" And .Usage = "BNR" Then
      combo.text = combo.list(4)
    ElseIf .segno = "N" And .Tipo = "9" And .Usage = "PCK" Then
      combo.text = combo.list(5)
    ElseIf .segno = "S" And .Tipo = "9" And .Usage = "PCK" Then
      combo.text = combo.list(6)
    End If
    'controllo sull'usage uguale a ""
    '  ElseIf .Segno = "N" And .Tipo = "X" And .Usage = "" Then
    ' combo.text = combo.list(0)
  End With
End Sub

Sub SalvaModifiche()
  Dim cmp As Integer
  Dim tbSegm As Recordset, tbField As Recordset, tbStrutt As Recordset, tbStrutt1 As Recordset
  Dim TbCampi As Recordset, tbRelSeg As Recordset
  Dim tb As Recordset, TbColonne As Recordset, TbAreaSeg As Recordset, TbAreaCmp As Recordset
  Dim indseg As Integer
  Dim cont As Long
  Dim Xcont As Integer, IndFld As Integer, indStr As Integer, indCmp As Integer, indcmpStr As Integer
  Dim SQL As String
  Dim IdVirtSegm As Integer, IdOggetto As Long
    
  m_fun.FnConnection.BeginTrans
  
  On Error GoTo SalvaModificheError
  
  'Setto il campo "rdbms_tbname" della PsDli_Segmenti
  For indseg = 1 To UBound(gSegm)
    If Len(gSegm(indseg).TbName) Then
      Set tbSegm = m_fun.Open_Recordset("select * from PsDli_Segmenti where IdSegmento = " & gSegm(indseg).IdSegm & " and IdOggetto = " & gSegm(indseg).IdOggetto)
      tbSegm!rdbms_tbname = gSegm(indseg).TbName
      tbSegm.Update
    End If
  Next indseg
  
  'Elimino tutti i record della MgDli_Segmenti tranne quelli "O" che aggiorno?!
  For indseg = 1 To UBound(gSegm)
    IdVirtSegm = 0
    Set tbSegm = m_fun.Open_Recordset("select * from MgDli_Segmenti where IdSegmento = " & gSegm(indseg).IdSegm & " and IdOggetto = " & gSegm(indseg).IdOggetto)
    If gSegm(indseg).Correzione = "O" Then
      If tbSegm.RecordCount Then tbSegm.Delete
      tbSegm.AddNew
      tbSegm!IdSegmento = gSegm(indseg).IdSegm
      tbSegm!IdOggetto = gSegm(indseg).IdOggetto
      tbSegm!Nome = gSegm(indseg).Name
      tbSegm!MaxLen = gSegm(indseg).MaxLen
      tbSegm!MinLen = gSegm(indseg).MinLen
      tbSegm!Parent = gSegm(indseg).Parent
      tbSegm!Pointer = gSegm(indseg).Pointer
      tbSegm!Rules = gSegm(indseg).Rules
      tbSegm!Comprtn = gSegm(indseg).Comprtn
      tbSegm!Correzione = gSegm(indseg).Correzione
      tbSegm!Redefines = IIf(gSegm(indseg).Redefines, -1, 0)
      'tbSegm!Condizione = gSegm(indseg).Condizione 'ALE 08/06/2006
      tbSegm.Update
      Exit For
    Else
      If tbSegm.RecordCount Then
        tbSegm.Delete
      End If
      Exit For
    End If
  Next indseg
  
  '???????????? Ho appena inserito sopra quelli con "O"... chiarire
  m_fun.FnConnection.Execute "DELETE * from MgDli_Segmenti where Idoggetto = " & gSegm(indseg).IdOggetto
 'DataMan.DmConnection.Execute "delete * from MgDli_Segmenti where Idoggetto = " & gSegm(indseg).IdOggetto
  
  For indseg = 1 To UBound(gSegm)
    IdOggetto = gSegm(indseg).IdOggetto
    If gSegm(indseg).Correzione = "I" Or gSegm(indseg).Correzione = "D" Or gSegm(indseg).Correzione = "M" Or gSegm(indseg).Correzione = "O" Then
      'SQ ?? vado in chiave duplicata!?
      'Set tbSegm = m_fun.Open_Recordset("select * from MgDli_Segmenti where IdSegmentoOrigine = " & gSegm(indseg).IdOrigine & " and IdOggetto = " & gSegm(indseg).IdOggetto & " and Nome = '" & gSegm(indseg).Name & "'")
      Set tbSegm = m_fun.Open_Recordset("select * from MgDli_Segmenti where IdSegmento = " & gSegm(indseg).IdSegm & " and IdOggetto = " & gSegm(indseg).IdOggetto)
      If tbSegm.RecordCount = 0 Then
        tbSegm.AddNew
      End If
      tbSegm!IdSegmento = gSegm(indseg).IdSegm
      tbSegm!IdOggetto = gSegm(indseg).IdOggetto
      tbSegm!IdSegmentoOrigine = gSegm(indseg).IdOrigine
      tbSegm!Nome = gSegm(indseg).Name
      tbSegm!MaxLen = gSegm(indseg).MaxLen
      tbSegm!MinLen = gSegm(indseg).MinLen
      tbSegm!Parent = gSegm(indseg).Parent
      tbSegm!Pointer = gSegm(indseg).Pointer
      tbSegm!Rules = gSegm(indseg).Rules
      tbSegm!Comprtn = gSegm(indseg).Comprtn
      tbSegm!Correzione = gSegm(indseg).Correzione
      tbSegm!Condizione = gSegm(indseg).Condizione 'ALE 08/06/2006
      tbSegm!Redefines = IIf(gSegm(indseg).Redefines, -1, 0)
      tbSegm.Update
      IdVirtSegm = 1
    End If
        
    'Inserimento Strutture con Correzione = "" o "Primaria" in MgRel_SegAree
    'ATTENZIONE: parte da 2... in 1 ci sono quelle della MgData_Area
    For indStr = 2 To UBound(gSegm(indseg).Strutture)
      Set tbStrutt = m_fun.Open_Recordset("select * from MgRel_SegAree where StrIdOggetto= " & gSegm(indseg).Strutture(indStr).StrIdOggetto & " and StrIdArea = " & gSegm(indseg).Strutture(indStr).StrIdArea & " and IdSegmento = " & gSegm(indseg).IdSegm)
      If tbStrutt.RecordCount Then tbStrutt.Delete
      
      If Trim(gSegm(indseg).Strutture(indStr).Correzione) <> "" Or gSegm(indseg).Strutture(indStr).Primario Then
        tbStrutt.AddNew
        tbStrutt!IdSegmento = gSegm(indseg).IdSegm
        tbStrutt!StrIdOggetto = gSegm(indseg).Strutture(indStr).StrIdOggetto
        tbStrutt!StrIdArea = gSegm(indseg).Strutture(indStr).StrIdArea
        tbStrutt!NomeCmp = gSegm(indseg).Strutture(indStr).NomeCampo '& " "  'perche' questi bianchi!!!!!!!!!?????????????
        
        'If IdVirtSegm Then
        '  tbStrutt!Correzione = "I"
        'Else
          If gSegm(indseg).Strutture(indStr).Primario Then
            tbStrutt!Correzione = "P"
          Else
            tbStrutt!Correzione = Trim(gSegm(indseg).Strutture(indStr).Correzione & "")
          End If
        'End If
        tbStrutt.Update
      End If
    Next indStr
        
    'STRUTTURE(1): MgData_Area
    If UBound(gSegm(indseg).Strutture) > 0 Then
      'DataMan.DmConnection
      m_fun.FnConnection.Execute "Delete * from MgData_Area where Idsegmento = " & gSegm(indseg).IdSegm
      Set tbStrutt = m_fun.Open_Recordset("select * from MgData_Area")
'      tbStrutt.AddNew
'      tbStrutt!IdSegmento = gSegm(indseg).IdSegm
      
      If UBound(gSegm(indseg).Strutture(1).Campi) > 0 Then
        tbStrutt.AddNew 'ale 14/07/2006
        tbStrutt!IdSegmento = gSegm(indseg).IdSegm
        
        tbStrutt!Nome = gSegm(indseg).Strutture(1).Campi(1).Nome
        tbStrutt!livello = gSegm(indseg).Strutture(1).Campi(1).livello
        tbStrutt!NomeRed = gSegm(indseg).Strutture(1).Campi(1).NomeRed & ""
        'If IdVirtSegm = 0 Then
          tbStrutt!Correzione = gSegm(indseg).Strutture(1).Campi(1).Correzione & ""
        'Else
        '  tbStrutt!Correzione = "I"
        'End If
        'tilvia 30/09/2009
        tbStrutt!Alias_Segm = gSegm(indseg).AliasSegm
       
        tbStrutt.Update
      End If
      tbStrutt.Close
    
'      Set Tb = m_fun.Open_Recordset("select * from MgData_Cmp order by IdCmp") 'un bel max(*)
'      If Tb.RecordCount Then
'        Tb.MoveLast
'        cont = Tb!IdCmp
'      Else
'        cont = 0
'      End If
      
      'MgData_Cmp
      'DataMan.DmConnection
      m_fun.FnConnection.Execute "Delete * from MgData_Cmp  where Idsegmento = " & gSegm(indseg).IdSegm
      
      Set tbStrutt = m_fun.Open_Recordset("select * from MgData_Cmp where Idsegmento = " & gSegm(indseg).IdSegm)
      ' AGGIUNGO I CAMPI
      ' Aggiungo un nuovo contare per gestire le Occurs flattened
      Dim IdCmp As Long
      IdCmp = 0
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
'        If Trim(gSegm(indseg).Strutture(1).Campi(indcmp).NomeRed) = "" Then
'          cont = cont + 1
        ' Mauro 12/02/2008: Se l'Occurs � Flattened, lo spiattello nella MgData_Cmp
        If gSegm(indseg).Strutture(1).Campi(indCmp).OccursFlat = True And _
           gSegm(indseg).Strutture(1).Campi(indCmp).Occurs Then
          Dim Occurs As Long, occursLevel As Integer
          Dim OccursFld() As String
          Dim k As Long, i As Long
           
          ' Espando l'occurs
          Occurs = gSegm(indseg).Strutture(1).Campi(indCmp).Occurs
          i = 1
          ReDim OccursFld(i)
          OccursFld(i) = indCmp 'gSegm(indseg).Strutture(1).Campi(indcmp).Id_Cmp
          i = i + 1
          occursLevel = gSegm(indseg).Strutture(1).Campi(indCmp).livello
          indCmp = indCmp + 1
          Do Until indCmp > UBound(gSegm(indseg).Strutture(1).Campi)
            If gSegm(indseg).Strutture(1).Campi(indCmp).livello > occursLevel Then
              ReDim Preserve OccursFld(i)
              OccursFld(i) = indCmp 'gSegm(indseg).Strutture(1).Campi(indcmp).Id_Cmp
              i = i + 1
              indCmp = indCmp + 1
            Else
              indCmp = indCmp - 1
              Exit Do
            End If
          Loop
          'SQ tmp
          Dim lenGroup As Integer
          If gSegm(indseg).Strutture(1).Campi(OccursFld(1)).Tipo = "A" Then
            lenGroup = gSegm(indseg).Strutture(1).Campi(OccursFld(1)).bytes / Occurs
          Else
            If UBound(OccursFld) = 1 Then
              lenGroup = gSegm(indseg).Strutture(1).Campi(OccursFld(1)).bytes
            Else
              'SQ verificare: non ho il caso...
              lenGroup = gSegm(indseg).Strutture(1).Campi(OccursFld(1)).bytes
            End If
          End If
          For i = 1 To Occurs
            For k = 1 To UBound(OccursFld)
              With gSegm(indseg).Strutture(1).Campi(OccursFld(k))
              IdCmp = IdCmp + 1
              'SQ
              If i > 1 Then
                .Posizione = val(.Posizione) + lenGroup
              End If
              
              tbStrutt.AddNew
              tbStrutt!IdSegmento = gSegm(indseg).IdSegm
              tbStrutt!Nome = .Nome & Format(i, Switch(Occurs > 99, "000", Occurs > 999, "0000", True, "00"))
              tbStrutt!livello = .livello
              tbStrutt!NomeRed = .NomeRed & " "
              tbStrutt!Ordinale = IdCmp '.Ordinale
              tbStrutt!IdCmp = IdCmp '.Id_Cmp 'cont
              'SQ TMP
              tbStrutt!Byte = IIf(.Tipo = "A", val(.bytes) / Occurs, val(.bytes))
              tbStrutt!Decimali = val(.Decimali)
              tbStrutt!lunghezza = val(.lunghezza)
              tbStrutt!Occurs = 0
              tbStrutt!Posizione = .Posizione
              tbStrutt!segno = .segno
              tbStrutt!Tipo = .Tipo
              If Len(Trim(.Usage)) Then
                tbStrutt!Usage = .Usage
              Else
                tbStrutt!Usage = ""
              End If
              tbStrutt!Selettore_Valore = .valore & ""
              If Trim(.valore) = "" Then
                tbStrutt!Selettore = False
              Else
                tbStrutt!Selettore = True
              End If
              tbStrutt!OccursFlat = True
              tbStrutt!DaMigrare = .DaMigrare
              If Trim(.Correzione) = "" Then
                tbStrutt!Correzione = " "
              Else
                tbStrutt!Correzione = .Correzione
              End If
              If .Index Then
                tbStrutt!Correzione = "R"
              Else
                If Trim(.Correzione) = "" Then
                  tbStrutt!Correzione = " "
                Else
                  tbStrutt!Correzione = Trim(.Correzione)
                End If
              End If
              End With
              tbStrutt.Update
            Next k
          Next i
        Else
          With gSegm(indseg).Strutture(1).Campi(indCmp)
          IdCmp = IdCmp + 1
          tbStrutt.AddNew
          tbStrutt!IdSegmento = gSegm(indseg).IdSegm
          tbStrutt!Nome = .Nome
          tbStrutt!livello = .livello
          tbStrutt!NomeRed = .NomeRed & " "
          tbStrutt!Ordinale = IdCmp '.Ordinale
          tbStrutt!IdCmp = IdCmp '.Id_Cmp 'cont
          tbStrutt!Byte = val(.bytes)
          tbStrutt!Decimali = val(.Decimali)
          tbStrutt!lunghezza = val(.lunghezza)
          tbStrutt!Occurs = .Occurs
          tbStrutt!Posizione = val(.Posizione)
          tbStrutt!segno = .segno
          tbStrutt!Tipo = .Tipo
          If Trim(.Usage) = "" Then
            tbStrutt!Usage = Trim(.Usage) & ""
          Else
            tbStrutt!Usage = .Usage
          End If
          tbStrutt!Selettore_Valore = .valore & ""
          If Trim(.valore) = "" Then
            tbStrutt!Selettore = False
          Else
            tbStrutt!Selettore = True
          End If
          ' Mauro 15/01/2008
          tbStrutt!OccursFlat = .OccursFlat
          tbStrutt!DaMigrare = .DaMigrare
          'If IdVirtSegm = 0 Then
            If Trim(.Correzione) = "" Then
              tbStrutt!Correzione = " "
            Else
              tbStrutt!Correzione = .Correzione
            End If
           'Else
           '  tbStrutt!Correzione = "I"
          'End If
          If .Index Then
            tbStrutt!Correzione = "R"
          Else
            If Trim(.Correzione) = "" Then
              tbStrutt!Correzione = " "
            Else
              tbStrutt!Correzione = Trim(.Correzione)
            End If
          End If
          End With
          tbStrutt.Update
        End If
'        Else
'          cmp = indcmp + 1
'          Do
'            If cmp >= UBound(gSegm(indseg).Strutture(1).Campi) Then Exit Do
'            If gSegm(indseg).Strutture(1).Campi(cmp).Livello <= .Livello Then Exit Do
'            cmp = cmp + 1
'          Loop
'          If cmp = UBound(gSegm(indseg).Strutture(1).Campi) Then
'            indcmp = cmp
'          Else
'            indcmp = cmp - 1
'          End If
'        End If
      Next indCmp
      tbStrutt.Close
    End If
    
    'FIELD:
    'DataMan.DmConnection
    m_fun.FnConnection.Execute "delete * from MgDli_Field where Idsegmento = " & gSegm(indseg).IdSegm
    For IndFld = 1 To UBound(gSegm(indseg).gField)
      If gSegm(indseg).gField(IndFld).Correzione = "C" Or _
         gSegm(indseg).gField(IndFld).Correzione = "I" Or _
         gSegm(indseg).gField(IndFld).Correzione = "M" Or _
         gSegm(indseg).gField(IndFld).Correzione = "F" Or _
         gSegm(indseg).gField(IndFld).Correzione = "A" Then
        If gSegm(indseg).gField(IndFld).Srch = "" Then
          Set tbField = m_fun.Open_Recordset("select * from MgDli_Field where Nome= '" & gSegm(indseg).gField(IndFld).Name & "' and Idsegmento = " & gSegm(indseg).IdSegm & " and IdField = " & gSegm(indseg).gField(IndFld).IdField)
          If tbField.RecordCount <> 0 Then tbField.Delete
          'deve essere scritto solo quello che � stato cancellato o inserito
          tbField.AddNew
          tbField!IdField = gSegm(indseg).gField(IndFld).IdField
          tbField!IdSegmento = gSegm(indseg).IdSegm
          tbField!Nome = gSegm(indseg).gField(IndFld).Name
          tbField!Tipo = gSegm(indseg).gField(IndFld).type
          tbField!Posizione = gSegm(indseg).gField(IndFld).Start
          tbField!lunghezza = gSegm(indseg).gField(IndFld).MaxLen
          'tbField!Seq = gSegm(indseg).gField(IndFld).Seq
          tbField!Unique = gSegm(indseg).gField(IndFld).Unique
          tbField!Ptr_Punseq = gSegm(indseg).gField(IndFld).NameIndPunSeg
          tbField!Ptr_DBDNome = gSegm(indseg).gField(IndFld).dbdName
          tbField!Ptr_Pointer = gSegm(indseg).gField(IndFld).Pointer
          tbField!Ptr_Xnome = gSegm(indseg).gField(IndFld).Xname
          tbField!Ptr_Segmento = gSegm(indseg).gField(IndFld).segment
          tbField!Ptr_Search = gSegm(indseg).gField(IndFld).Srch
          'SQ - 28-11-05
          'tbField!Ptr_SubSeq = gSegm(indseg).gField(IndFld).SubSeq
          tbField!Ptr_NullVal = gSegm(indseg).gField(IndFld).NullVal
          If Trim(gSegm(indseg).gField(IndFld).Correzione) = "" Then
            tbField!Correzione = " "
          Else
            tbField!Correzione = gSegm(indseg).gField(IndFld).Correzione
          End If
          tbField!Ptr_index = gSegm(indseg).gField(IndFld).Index
          tbField!Seq = gSegm(indseg).gField(IndFld).Index
          tbField.Update
        End If
      End If
    Next IndFld
    
    'XD-FIELD:
    'DataMan.DmConnection
    m_fun.FnConnection.Execute "delete * from MgDli_xdField where Idsegmento = " & gSegm(indseg).IdSegm
    For IndFld = 1 To UBound(gSegm(indseg).gField)
      If gSegm(indseg).gField(IndFld).Correzione = "C" Or _
         gSegm(indseg).gField(IndFld).Correzione = "I" Or _
         gSegm(indseg).gField(IndFld).Correzione = "M" Or _
         gSegm(indseg).gField(IndFld).Correzione = "F" Or _
         gSegm(indseg).gField(IndFld).Correzione = "A" Then
        If gSegm(indseg).gField(IndFld).Srch <> "" Then
          Set tbField = m_fun.Open_Recordset("select * from MgDli_xdField where Nome= '" & gSegm(indseg).gField(IndFld).Name & "' and Idsegmento = " & gSegm(indseg).IdSegm & " and IdxdField = " & gSegm(indseg).gField(IndFld).IdField)
          If tbField.RecordCount <> 0 Then tbField.Delete
          'deve essere scritto solo quello che � stato cancellato o inserito
          tbField.AddNew
          tbField!IdxdField = gSegm(indseg).gField(IndFld).IdField * -1
          tbField!IdSegmento = gSegm(indseg).IdSegm
          tbField!Nome = gSegm(indseg).gField(IndFld).Name
          tbField!srchFields = gSegm(indseg).gField(IndFld).Srch
          tbField!SubSeq = gSegm(indseg).gField(IndFld).SubSeq
          tbField!extRtn = gSegm(indseg).gField(IndFld).extRtn
          tbField!LChild = gSegm(indseg).gField(IndFld).LChild
          If Trim(gSegm(indseg).gField(IndFld).Correzione) = "" Then
            tbField!Correzione = ""
          Else
            tbField!Correzione = gSegm(indseg).gField(IndFld).Correzione
          End If
          tbField.Update
        End If
      End If
    Next IndFld
  Next indseg
    
  'SALVATAGGIO DELLE RELAZIONI
  'DataMan.DmConnection
  m_fun.FnConnection.Execute "delete * from MgRel_DLISeg where idoggetto = " & IdOggetto
  Set tbField = m_fun.Open_Recordset("select * from MgRel_DLISeg where idoggetto = " & IdOggetto)
  For indseg = 1 To UBound(SegAss)
    If SegAss(indseg).IdOggetto = IdOggetto Then
      If SegAss(indseg).Tag <> "Virtuale" Then
        'tmp: pezza temporanea (per SEGM VIRT)
        On Error Resume Next
        tbField.AddNew
        tbField!IdOggetto = SegAss(indseg).IdOggetto
        tbField!Child = SegAss(indseg).NameChild
        tbField!Father = SegAss(indseg).NameFather
        tbField!Relazione = SegAss(indseg).Relazione
        tbField.Update
      End If
    End If
  Next
  tbField.Close
  tbSegm.Close
  
  m_fun.FnConnection.CommitTrans
  
  MsgBox "DBD correctly updated.", vbInformation, DataMan.DmNomeProdotto
  Exit Sub
SalvaModificheError:
  MsgBox ERR.Description, vbCritical, "i-4.Migration"
  m_fun.FnConnection.RollbackTrans
End Sub

Function CheckLivello(Nome As String, bottone As Integer, treeview As treeview) As Integer
  Dim indseg As Integer
  Dim indCmp As Integer
  Dim k As Integer
  CheckLivello = 0
  For indseg = 1 To UBound(gSegm)
    If Trim(Nomenodo) = Trim(gSegm(indseg).Name) Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        k = InStr(1, Nome, "  ")
        If k = 0 Then k = Len(Nome)
        If Trim(gSegm(indseg).Strutture(1).Campi(indCmp).Nome) = Trim(Mid$(Nome, 1, k)) Then
          If treeview.Nodes(indCmp).Index = UBound(gSegm(indseg).Strutture(1).Campi) And bottone = 2 Then CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
          If (treeview.Nodes(indCmp).Parent = treeview.Nodes(1) Or gSegm(indseg).Strutture(1).Campi(indCmp).livello = 1) And bottone = 0 Then
            CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
            Exit Function
          End If
          If indCmp <> UBound(gSegm(indseg).Strutture(1).Campi) Then
            If gSegm(indseg).Strutture(1).Campi(indCmp).livello < gSegm(indseg).Strutture(1).Campi(indCmp + 1).livello And bottone = 0 Then
              CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
              Exit Function
            End If
          End If
          If gSegm(indseg).Strutture(1).Campi(indCmp - 1).livello < gSegm(indseg).Strutture(1).Campi(indCmp).livello And bottone = 1 Then
            CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
            Exit Function
          End If
          If gSegm(indseg).Strutture(1).Campi(indCmp - 1).livello <> gSegm(indseg).Strutture(1).Campi(indCmp).livello And bottone = 3 Then
            CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
            Exit Function
          End If
          If UBound(gSegm(indseg).Strutture(1).Campi) <> treeview.Nodes.count Then
            If gSegm(indseg).Strutture(1).Campi(indCmp + 1).livello <> gSegm(indseg).Strutture(1).Campi(indCmp).livello And bottone = 2 Then
              CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
              Exit Function
            End If
          End If
          If indCmp <> UBound(gSegm(indseg).Strutture(1).Campi) Then
            If gSegm(indseg).Strutture(1).Campi(indCmp + 1).livello < gSegm(indseg).Strutture(1).Campi(indCmp).livello And bottone = 2 Then
              CheckLivello = MsgBox("Impossible change", vbOKOnly + vbSystemModal)
              Exit Function
            End If
          End If
        End If
      Next indCmp
    End If
  Next indseg

End Function

Sub CheckStruttura(Tree As treeview)
  Dim indseg As Integer
  Dim indCmp As Integer
  Dim livello As Integer
  Dim indcmp1 As Integer
  Dim a As String
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        If Tree.Nodes(indCmp).Children <> 0 Then
          gSegm(indseg).Strutture(1).Campi(indCmp).bytes = 0
          gSegm(indseg).Strutture(1).Campi(indCmp).Tipo = "X"
          Exit For
        End If
      Next indCmp
    End If
    Exit For
  Next indseg
  For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
    If gSegm(indseg).Strutture(1).Campi(indCmp).lunghezza = 0 Then
      gSegm(indseg).Strutture(1).Campi(indCmp).bytes = 0
      livello = gSegm(indseg).Strutture(1).Campi(indCmp).livello
      For indcmp1 = indCmp + 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        If gSegm(indseg).Strutture(1).Campi(indcmp1).livello = livello Then Exit For
        If gSegm(indseg).Strutture(1).Campi(indcmp1).lunghezza <> 0 Then gSegm(indseg).Strutture(1).Campi(indCmp).bytes = val(gSegm(indseg).Strutture(1).Campi(indCmp).bytes) + val(gSegm(indseg).Strutture(1).Campi(indcmp1).bytes)
      Next indcmp1
    End If
  Next indCmp
End Sub

Sub RipristinaCondizioneIniziale(seg As Double)
  
  Dim TbApp As Recordset
    
  'Set TbApp = m_fun.Open_Recordset("select * from DmCampi where IdSegm= " & gSegm(seg).IdSegm, TbApp)
  Set TbApp = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & gSegm(seg).IdSegm)
  'If TbApp.RecordCount <> 0 Then DbPrj.Execute ("delete * from DmCampi where IdSegm= " & gSegm(seg).IdSegm)
  If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Cmp where IdSegmento = " & gSegm(seg).IdSegm)
  
  'Set TbApp = m_fun.Open_Recordset("select * from DmAreaSeg where IdSegm= " & gSegm(seg).IdSegm, TbApp)
  Set TbApp = m_fun.Open_Recordset("select * from MgData_Area where IdSegmento= " & gSegm(seg).IdSegm)
  'If TbApp.RecordCount <> 0 Then DbPrj.Execute ("delete * from DmAreaSeg where IdSegm= " & gSegm(seg).IdSegm)
  If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Area where IdSegmento= " & gSegm(seg).IdSegm)
  
  'Set TbApp = m_fun.Open_Recordset("select * from DmField where IdSegm= " & gSegm(seg).IdSegm, TbApp)
  Set TbApp = m_fun.Open_Recordset("select * from MgDLI_Field where IdSegmento= " & gSegm(seg).IdSegm)
  'If TbApp.RecordCount <> 0 Then DbPrj.Execute ("delete * from Dmfield where IdSegm= " & gSegm(seg).IdSegm)
  If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgDLI_Field where IdSegmento= " & gSegm(seg).IdSegm)
  
  'Set TbApp = m_fun.Open_Recordset("select * from DmRelSeg where IdSegm= " & gSegm(seg).IdSegm & " and Correzione ='C'", TbApp)
  Set TbApp = m_fun.Open_Recordset("select * from MgRel_SegAree where IdSegmento= " & gSegm(seg).IdSegm & " and Correzione ='C'")
  If TbApp.RecordCount <> 0 Then
    TbApp.MoveFirst
    While TbApp.EOF = False
      
      TbApp!Correzione = ""
      TbApp.Update
      TbApp.MoveNext
    
    Wend
  End If
  CaricaStruttura
End Sub

Sub Relazioni(NomeSeg As String, list As ListView)

'  Dim tb As Recordset
'  Dim Tb1 As Recordset
'  Dim Tb2 As Recordset
'
'  Dim indseg As Integer
'  Dim indcmp As Integer
'
'  Set tb = DbPrj.OpenRecordset("select * from DMTavole where Nome ='" & NomeSeg & "'")
'  If tb.RecordCount <> 0 Then
'    For indseg = 1 To UBound(gSegm)
'      If gSegm(indseg).IdSegm = tb!IdSegm Then
'        For indcmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
'          With list.ListItems.Add(, , gSegm(indseg).Strutture(1).Campi(indcmp).Livello)
'            .SubItems(1) = gSegm(indseg).Strutture(1).Campi(indcmp).Nome
'            .SubItems(2) = gSegm(indseg).Strutture(1).Campi(indcmp).Bytes
'            .SubItems(3) = gSegm(indseg).Strutture(1).Campi(indcmp).Lunghezza
'            .SubItems(4) = gSegm(indseg).Strutture(1).Campi(indcmp).Posizione
'            .SubItems(5) = gSegm(indseg).Strutture(1).Campi(indcmp).Tipo
'            .SubItems(6) = gSegm(indseg).Strutture(1).Campi(indcmp).Occurs
'          End With
'        Next indcmp
'      End If
'    Next indseg
'  End If
End Sub

Sub ImpostaChiave(list As ListView, image As ImageList, NomeSeg As String)
  
  Dim i As Integer
  Dim indseg As Integer
  Dim IndFld As Integer
  Dim tb As Recordset
  Dim bool As Boolean
  
  list.SmallIcons = image
  For indseg = 1 To UBound(gSegm) 'trovo 'indice del segmento modificato e l'indice del field
    If gSegm(indseg).Name = NomeSeg Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        If gSegm(indseg).gField(IndFld).Name = list.SelectedItem.text Then Exit For
      Next IndFld
      Exit For
    End If
  Next indseg
  
  If list.SelectedItem.SmallIcon = 3 Then
    bool = Trova_Chiave(MadmdF_EditDli.ListField, 10)
    If bool Then
      list.SelectedItem.SmallIcon = 10
      gSegm(indseg).gField(IndFld).Index = True
      gSegm(indseg).gField(IndFld).Correzione = "M"
    Else
      list.SelectedItem.SmallIcon = list.SelectedItem.SmallIcon
    End If
    ElseIf list.SelectedItem.SmallIcon = 10 Then
    If list.SelectedItem.SubItems(3) > 40 Then
      list.SelectedItem.SmallIcon = 11
    Else
      list.SelectedItem.SmallIcon = 3
    End If
    gSegm(indseg).gField(IndFld).Index = False
    gSegm(indseg).gField(IndFld).Correzione = ""
'    '******ALE
'                list.SelectedItem.SmallIcon = 4
'            gSegm(indseg).gField(IndFld).Correzione = "C"
'            gSegm(indseg).gField(IndFld).Index = False
'    '****************
    
    ElseIf list.SelectedItem.SmallIcon = 11 Then
    bool = Trova_Chiave(MadmdF_EditDli.ListField, 10)
    If bool Then
      list.SelectedItem.SmallIcon = 10
      gSegm(indseg).gField(IndFld).Index = True
      gSegm(indseg).gField(IndFld).Correzione = "M"
    Else
      list.SelectedItem.SmallIcon = list.SelectedItem.SmallIcon
    End If
    
    ElseIf list.SelectedItem.SmallIcon = 4 Then
    bool = Trova_Chiave(MadmdF_EditDli.ListField, 10) 'controllo se c'� un'altra chiave
    If bool Then
      list.SelectedItem.SmallIcon = 10
      gSegm(indseg).gField(IndFld).Index = True
      gSegm(indseg).gField(IndFld).Correzione = "M"
    Else
      list.SelectedItem.SmallIcon = list.SelectedItem.SmallIcon
    End If
  
  End If

End Sub

Sub RipristinaDBD()
  Dim TbApp As Recordset
  Dim TbSegmenti As Recordset
  Dim indStr As Integer
  Dim indseg As Integer
  Dim SQL As String
    
  For indseg = 1 To UBound(gSegm)
    'stringa precedente
    'Sql = "select * from DmSegmenti where IdSegmento = " & gSegm(indseg).IdSegm
    '******************
    'Sql = "select * from PsDLI_Segmenti where IdSegmento = " & gSegm(indseg).IdSegm
    SQL = "select * from MgDLI_Segmenti where IdSegmento = " & gSegm(indseg).IdSegm
    Set TbSegmenti = m_fun.Open_Recordset(SQL)
    If TbSegmenti.RecordCount <> 0 Then
      TbSegmenti.MoveFirst
      While TbSegmenti.EOF = False
        'stringa precedente
        'Sql = "select * from DmRelSeg where IdSegm= " & TbSegmenti!IdSegm
        '******************
        SQL = "select * from MgRel_SegAree where IdSegmento = " & TbSegmenti!IdSegmento
        Set TbApp = m_fun.Open_Recordset(SQL)
        'If TbApp.RecordCount <> 0 Then DbPrj.Execute ("delete * from DmRelSeg where IdSegm= " & TbSegmenti!IdSegm)
        If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from PsRel_SegAree where IdSegmento = " & TbSegmenti!IdSegmento)
        
        'stringa precedente
        'Sql = "select * from DmAreaSeg where IdSegm= " & TbSegmenti!IdSegm
        '******************
        SQL = "select * from MgData_Area where IdSegmento = " & TbSegmenti!IdSegmento
        Set TbApp = m_fun.Open_Recordset(SQL)
        'If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from DmAreaSeg where IdSegm= " & TbSegmenti!IdSegm)
        If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Area where IdSegmento= " & TbSegmenti!IdSegmento)
        
        'stringa precedente
        'Sql = "select * from DmCampi where IdSegm= " & TbSegmenti!IdSegm
        '******************
        SQL = "select * from MgData_Cmp where IdSegmento = " & TbSegmenti!IdSegmento
        Set TbApp = m_fun.Open_Recordset(SQL)
        'If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from DmCampi where IdSegm= " & TbSegmenti!IdSegm)
        If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Cmp where IdSegmento = " & TbSegmenti!IdSegmento)
        
        'stringa precedente
        'Sql = "select * from DmField where IdSegm= " & TbSegmenti!IdSegm
        '******************
        SQL = "select * from MgDli_Field where IdSegmento = " & TbSegmenti!IdSegmento
        Set TbApp = m_fun.Open_Recordset(SQL)
        'If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from DmField where IdSegm= " & TbSegmenti!IdSegm)
        If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgDli_Field where IdSegmento= " & TbSegmenti!IdSegmento)
        
        TbSegmenti.Delete
        TbSegmenti.MoveNext
      Wend
    End If
      'Set TbApp = m_fun.Open_Recordset("select * from DmRelSeg where IdSegm= " & gSegm(indseg).IdSegm, TbApp)
      SQL = "select * from mgRel_SegAree where IdSegmento = " & gSegm(indseg).IdSegm
      Set TbApp = m_fun.Open_Recordset(SQL)
      If TbApp.RecordCount <> 0 Then
        TbApp.MoveFirst
        While TbApp.EOF = False
          
          TbApp!Correzione = " "
          TbApp.Update
          TbApp.MoveNext
        
        Wend
      End If
      
      SQL = "select * from MgDli_Field where IdSegmento = " & gSegm(indseg).IdSegm
      Set TbApp = m_fun.Open_Recordset(SQL)
      If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("Delete * from MgDli_Field where IdSegmento = " & gSegm(indseg).IdSegm)
      
      SQL = "select * from MgData_Cmp where IdSegmento = " & gSegm(indseg).IdSegm
      Set TbApp = m_fun.Open_Recordset(SQL)
      If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Cmp where IdSegmento = " & gSegm(indseg).IdSegm)
      
      SQL = "select * from MgRel_SegAree where IdSegmento = " & gSegm(indseg).IdSegm
      Set TbApp = m_fun.Open_Recordset(SQL)
      If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgRel_SegAree where IdSegmento = " & gSegm(indseg).IdSegm)

      SQL = "select * from MgData_Area where IdSegmento = " & gSegm(indseg).IdSegm
      Set TbApp = m_fun.Open_Recordset(SQL)
      If TbApp.RecordCount <> 0 Then DataMan.DmConnection.Execute ("delete * from MgData_Area where IdSegmento = " & gSegm(indseg).IdSegm)
      
  Next indseg
  
  MsgBox "DBD restored"
  
End Sub

Public Function Calcola_Bytes(Usa As String, x As Integer, indseg As Integer) As Long
  
  With gSegm(indseg).Strutture(1)
    Select Case Trim(Usa)
      Case "BNR"
      'caso comp
        If .Campi(x).lunghezza >= 1 And .Campi(x).lunghezza <= 4 Then
          Calcola_Bytes = 2
        'SQ il 9 ERA SBAGLIATO!
        'ElseIf .Campi(x).lunghezza >= 5 And .Campi(x).lunghezza <= 8 Then
        ElseIf .Campi(x).lunghezza >= 5 And .Campi(x).lunghezza <= 9 Then
          Calcola_Bytes = 4
        'ElseIf .Campi(x).lunghezza >= 9 Then
        ElseIf .Campi(x).lunghezza > 9 Then
          Calcola_Bytes = 8
        End If
      Case "PCK"
        'caso comp3
        Calcola_Bytes = Int(val(.Campi(x).lunghezza) / 2) + 1
      Case Else
        Calcola_Bytes = Int(.Campi(x).lunghezza)
        
    End Select
  End With
End Function

Sub Ricalcola_Posizione_Lunghezza()
  Dim indseg As Integer
  Dim indCmp As Integer
  Dim indliv As Integer
  Dim indbyte As Integer
  Dim Trovato As Boolean
  Dim Nome As String
  
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      ReDim BLiv(0)
      With gSegm(indseg).Strutture(1)
        For indCmp = UBound(.Campi) To 1 Step -1
          Nome = .Campi(indCmp).Nome
          Trovato = False
          ' Verifico se il livello che tratto esiste altrimenti lo creo
          For indliv = 0 To UBound(BLiv)
            If BLiv(indliv).livello = (.Campi(indCmp).livello) Then
              Trovato = True
              Exit For
            End If
          Next indliv
          If Trovato = False Then
            ReDim Preserve BLiv(UBound(BLiv) + 1)
          End If
          If .Campi(indCmp).lunghezza <> 0 Then
            .Campi(indCmp).bytes = Calcola_Bytes(.Campi(indCmp).Usage, indCmp, indseg)
            BLiv(indliv).Byte = BLiv(indliv).Byte + (.Campi(indCmp).bytes)
            BLiv(indliv).livello = (.Campi(indCmp).livello)
          Else
            For indbyte = 0 To UBound(BLiv)
              If BLiv(indbyte).livello > (.Campi(indCmp).livello) Then
                .Campi(indCmp).bytes = BLiv(indbyte).Byte
                BLiv(indbyte).Byte = 0
                BLiv(indliv).Byte = BLiv(indliv).Byte + (.Campi(indCmp).bytes)
              End If
            Next indbyte
          End If
        Next indCmp
        .Campi(1).Posizione = 1
        Nome = .Campi(1).Nome
        For indCmp = 2 To UBound(.Campi)
          Nome = .Campi(indCmp).Nome
          If .Campi(indCmp).livello > .Campi(indCmp - 1).livello Then
            .Campi(indCmp).Posizione = .Campi(indCmp - 1).Posizione
          Else
            .Campi(indCmp).Posizione = .Campi(indCmp - 1).Posizione + .Campi(indCmp - 1).bytes
          End If
        Next indCmp
      End With
    End If
    Exit For
  Next indseg
End Sub

Sub InserisciArea(rsSegmenti As Recordset, RtbFile As RichTextBox)
  Dim TbAreaSeg As Recordset, tbSegVirt As Recordset, TbOg As Recordset
  Dim testo As String, Struttura As String
  Dim PosTesto As Long, PosInit As Long
  Dim Nome As String, str As String, Str1 As String, Field As String, wStr1 As String
  Dim LenCmp As Integer
  Dim Decimali As String
  Dim LenRiga As Integer, LenR As Integer, k As Integer, x As Integer, i As Integer, MaxLen As Integer
  Dim cont As String
  Dim bol As Boolean
  ' Mauro 05/03/2008: I numerici, in conversione DLI2RDBMS, potrebbero diventare CHAR
  Dim tipoCmp As String, segnoCmp As String, rsColonne As Recordset
  'tilvia
  Dim livello As Integer
  ''
       
  'If Trim(rsSegmenti!Comprtn) <> "None" Then rsSegmenti!nome = rsSegmenti!Comprtn
  
  MaxLen = 0
  ' Tratto i segmenti reali
  ReDim Ind_Liv(0)
  LenRiga = 72
  Set TbAreaSeg = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento= " & rsSegmenti!IdSegmento & " order by ordinale")
  If TbAreaSeg.RecordCount Then
    Nome = rsSegmenti!Nome
    '''''''''''''''''''''''''''''''
    'Calcolo indentazione: una per livello
    '''''''''''''''''''''''''''''''
    For k = 1 To TbAreaSeg.RecordCount
      If Len(TbAreaSeg!Nome) > MaxLen Then
          MaxLen = Len(TbAreaSeg!Nome)
          testo = TbAreaSeg!Nome
      End If
      If UBound(Ind_Liv) = 0 Then
         ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
         Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
         Ind_Liv(UBound(Ind_Liv)).Indet = 0
      Else
        For x = 1 To UBound(Ind_Liv)
          If Ind_Liv(x).livello = Int(TbAreaSeg!livello) Then
            bol = True
            Exit For
          End If
        Next x
        If Not bol Then
           ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
           Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
           Ind_Liv(UBound(Ind_Liv)).Indet = Ind_Liv(UBound(Ind_Liv) - 1).Indet + 3
        End If
      End If
      bol = False
      TbAreaSeg.MoveNext
    Next k
    
    TbAreaSeg.MoveFirst
    NmSegCpy = rsSegmenti!Nome
    '''''''''''''''''''''
    ' Livelli 01:
    '''''''''''''''''''''
    'Nome parametrico:
    str = LeggiParam("T-COPY-AREA")
    If Len(str) Then
      Dim env As New Collection   'resetto;
      env.Add Null
      env.Add rsSegmenti
    Else
      'esco!
      ERR.Raise 7676, , "No valid value for ""T-COPY-AREA"" environment parameter."
    End If
    
    If Not Virtuale Then
    'ALPITOUR 5/8/2005
      If m_fun.FnNomeDB = "banca.mty" Then
         Struttura = "01 AREA-" & rsSegmenti!Nome & "  PIC X(" & rsSegmenti!MaxLen & ")." & vbCrLf & _
                  "01 " & getEnvironmentParam(str, "tabelle", env) & " REDEFINES AREA-" & rsSegmenti!Nome & "." & vbCrLf
      Else
         Struttura = "01 AREA-" & rsSegmenti!Nome & "  PIC X(" & rsSegmenti!MaxLen & ")." & vbCrLf & _
                  "01 RD-" & rsSegmenti!Nome & " REDEFINES AREA-" & rsSegmenti!Nome & "." & vbCrLf
      End If
    Else
      Struttura = "01 RD-" & getEnvironmentParam(str, "tabelle", env) & "." & vbCrLf
    End If
    
    'salto il vero livello 01:
    'TILVIA 05-05-2009 : gestione caso di un solo campo
    livello = 0
    If TbAreaSeg.RecordCount > 1 Then
      TbAreaSeg.MoveNext
      livello = TbAreaSeg!livello
    Else
      livello = TbAreaSeg!livello + 1
    End If
    
    While Not TbAreaSeg.EOF
      tipoCmp = TbAreaSeg!Tipo
      segnoCmp = TbAreaSeg!segno
      LenCmp = TbAreaSeg!lunghezza
      If TbAreaSeg!Decimali <> 0 Then
        Decimali = "V9(" & TbAreaSeg!Decimali & ")"
      Else
        Decimali = ""
      End If
      '& Space(20 - Len(TbColonne!Colonna)) &
      If LenCmp + TbAreaSeg!Decimali = "0" Then
        If TbAreaSeg!Occurs = "1" Or TbAreaSeg!Occurs = "0" Then
          If Trim(TbAreaSeg!NomeRed) <> "" Then
             Field = Space(Indenta(Int(livello))) & Format(livello, "00") & " " & TbAreaSeg!Nome & " REDEFINES " & TbAreaSeg!NomeRed & "."
          Else
             Field = Space(Indenta(Int(livello))) & Format(livello, "00") & " " & TbAreaSeg!Nome & "."
          End If
        Else
          Field = Space(Indenta(Int(livello))) & Format(livello, "00") & " " & TbAreaSeg!Nome & " OCCURS " & TbAreaSeg!Occurs & " TIMES."
        End If
      Else
        ' Mauro 05/03/2008:
        Set rsColonne = m_fun.Open_Recordset("select a.* from " & PrefDb & "_colonne as a, " & PrefDb & "_tabelle as b " & _
                                             "where b.idorigine = " & rsSegmenti!IdSegmento & " and a.idtable = b.idtable and " & _
                                             "a.idcmp = " & TbAreaSeg!IdCmp & " and a.nomecmp = '" & TbAreaSeg!Nome & "'")
        If rsColonne.RecordCount Then
          If tipoCmp = "9" And TbAreaSeg!Usage = "ZND" And rsColonne!Tipo = "CHAR" Then
            tipoCmp = "X"
            segnoCmp = ""
            LenCmp = rsColonne!lunghezza
          End If
        End If
        rsColonne.Close
        
        If segnoCmp = "S" Then
        'stefano
           'If Len(TbAreaSeg!Nome) > 30 Then
           If Len(TbAreaSeg!Nome) + Indenta(Int(TbAreaSeg!livello)) > 30 Then
             Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & _
                     "  PIC S" & IIf(LenCmp > 0, tipoCmp & "(" & LenCmp & ")", "")
           Else
           'stefano
             'Field = Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & Space(30 - Len(TbAreaSeg!Nome)) & "  PIC S" & TbAreaSeg!Tipo & "(" & LenCmp & ")"
             Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & _
                     Space(31 - Len(TbAreaSeg!Nome) - Indenta(Int(TbAreaSeg!livello))) & "  PIC S" & _
                     IIf(LenCmp > 0, tipoCmp & "(" & LenCmp & ")", "")
           End If
        Else
        'stefano
           'If Len(TbAreaSeg!Nome) > 30 Then
           If Len(TbAreaSeg!Nome) + Indenta(Int(TbAreaSeg!livello)) > 30 Then
             Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & _
                     "  PIC " & IIf(LenCmp > 0, tipoCmp & "(" & LenCmp & ")", "")
           Else
           'stefano
             'Field = Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & Space(30 - Len(TbAreaSeg!Nome)) & "  PIC " & TbAreaSeg!Tipo & "(" & LenCmp & ")"
             Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & _
                     Space(31 - Len(TbAreaSeg!Nome) - Indenta(Int(TbAreaSeg!livello))) & "  PIC " & _
                     IIf(LenCmp > 0, tipoCmp & "(" & LenCmp & ")", "")
           End If
        End If
        Select Case TbAreaSeg!Usage
           Case "BNR"
              Field = Field & Decimali & " COMP"
           Case "PCK"
              Field = Field & Decimali & " COMP-3"
           Case "ZND"
              If Trim(tipoCmp) = "9" Then Field = Field & Decimali
        End Select
        If TbAreaSeg!Occurs = "0" Or TbAreaSeg!Occurs = "1" Then
           'field = field & "." & vbCrLf
           Field = Field & "."
           'field = field & Space(LenRiga - Len(field)) & cont & vbCrLf
        Else
          Field = Field & " OCCURS " & TbAreaSeg!Occurs & " TIMES." & vbCrLf
        End If
      End If
      'cont = Format(val(cont) + 10, "00000000")
      If Len(Field) > LenRiga Then
        k = InStr(Field, "PIC")
        If k <> 0 Then
         str = RTrim(Mid(Field, 1, k - 1))
         Str1 = RTrim(Mid(Field, k, Len(Field)))
         'field = str & Space(LenRiga - Len(str)) & cont & vbCrLf
         Field = str & Space(LenRiga - Len(str)) & vbCrLf
         'cont = Format(val(cont) + 10, "00000000")
         For x = 1 To Len(str)
          If Mid(str, 1, 1) <> " " Then
            Str1 = Space(x + 2) & Str1
            'field = field & Str1 & Space(LenRiga - Len(Str1)) & cont & vbCrLf
            Field = Field & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
            Exit For
          Else
            str = Mid(str, 2)
          End If
         Next x
        End If
      Else
        Struttura = Struttura & Field & vbCrLf
      '  field = field & Space(LenRiga - Len(field)) & vbCrLf
      End If
      TbAreaSeg.MoveNext
    Wend
    changeTag RtbFile, "<STRUTTURA-ASSOCIATA>", Replace(Struttura, "_", "-")
    changeTag RtbFile, "<NOME-SEGM>", rsSegmenti!Nome
  End If
End Sub

Sub InserisciArea_PLI(rsSegmenti As Recordset, RtbFile As RichTextBox)
  Dim TbAreaSeg As Recordset
  Dim testo As String, Struttura As String
  Dim Nome As String, str As String, Str1 As String, Field As String
  Dim LenCmp As Integer
  Dim LenRiga As Integer, k As Integer, x As Integer, MaxLen As Integer
  ''
  Dim bol As Boolean, kind As String, tipoCampo As String
  ' Mauro 06/10/2010: I numerici, in conversione DLI2RDBMS, potrebbero diventare CHAR
  Dim rsColonne As Recordset
         
  MaxLen = 0
  ' Tratto i segmenti reali
  ReDim Ind_Liv(0)
  LenRiga = 72
  Set TbAreaSeg = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento= " & rsSegmenti!IdSegmento & " order by ordinale")
  If TbAreaSeg.RecordCount > 0 Then
    Nome = rsSegmenti!Nome
    '''''''''''''''''''''''''''''''
    'Calcolo indentazione: una per livello
    '''''''''''''''''''''''''''''''
    For k = 1 To TbAreaSeg.RecordCount
      If Len(TbAreaSeg!Nome) > MaxLen Then
        MaxLen = Len(TbAreaSeg!Nome)
        testo = TbAreaSeg!Nome
      End If
      If UBound(Ind_Liv) = 0 Then
        ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
        Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
        Ind_Liv(UBound(Ind_Liv)).Indet = 0
      Else
        For x = 1 To UBound(Ind_Liv)
          If Ind_Liv(x).livello = Int(TbAreaSeg!livello) Then
            bol = True
            Exit For
          End If
        Next x
        If Not bol Then
          ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
          Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
          Ind_Liv(UBound(Ind_Liv)).Indet = Ind_Liv(UBound(Ind_Liv) - 1).Indet + 3
        End If
      End If
      bol = False
      TbAreaSeg.MoveNext
    Next k
    
    TbAreaSeg.MoveFirst
    NmSegCpy = rsSegmenti!Nome
    '''''''''''''''''''''
    ' Livelli 01:
    '''''''''''''''''''''
    'Nome parametrico:
    str = LeggiParam("T-COPY-AREA")
    If Len(str) Then
      Dim env As New Collection   'resetto;
      env.Add Null
      env.Add rsSegmenti
    Else
      'esco!
      ERR.Raise 7676, , "No valid value for ""T-COPY-AREA"" environment parameter."
    End If
    
    If Not Virtuale Then
      Struttura = "DCL AREA_" & rsSegmenti!Nome & Space(30 - Len(rsSegmenti!Nome) - 9) & "  CHAR(" & rsSegmenti!MaxLen & ");" & vbCrLf & _
                  "DCL 1 RD_" & rsSegmenti!Nome & Space(31 - Len(rsSegmenti!Nome) - 9) & " BASED(ADDR(AREA_" & rsSegmenti!Nome & "))," & vbCrLf
'    Else
'      Struttura = "01 RD-" & getEnvironmentParam(str, "tabelle", env) & "." & vbCrLf
    End If
    
    'salto il vero livello 01:
    'TILVIA 05-05-2009 : gestione caso di un solo campo
    If TbAreaSeg.RecordCount > 1 Then
      TbAreaSeg.MoveNext
    Else
      TbAreaSeg!livello = TbAreaSeg!livello + 1
    End If
    While Not TbAreaSeg.EOF
      LenCmp = TbAreaSeg!lunghezza
      Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "0") & " " & TbAreaSeg!Nome & Space(31 - Len(TbAreaSeg!Nome) - Indenta(Int(TbAreaSeg!livello)))
      
      kind = ""
      Select Case TbAreaSeg!Usage
        Case "BNR"
          kind = " COMP"
        Case "PCK"
          kind = " COMP-3"
        Case "ZND"
          kind = ""
        'tilvia 28-04-2009
        Case "BIT"
          kind = "DECIMAL"
      End Select
      
      tipoCampo = getPLIData(TbAreaSeg!Tipo, LenCmp, kind, TbAreaSeg!Decimali, TbAreaSeg!segno)
      
      ' Mauro 06/10/2010:
      Set rsColonne = m_fun.Open_Recordset("select a.* from " & PrefDb & "_colonne as a, " & PrefDb & "_tabelle as b " & _
                                           "where b.idorigine = " & rsSegmenti!IdSegmento & " and a.idtable = b.idtable and " & _
                                           "a.idcmp = " & TbAreaSeg!IdCmp & " and a.nomecmp = '" & TbAreaSeg!Nome & "'")
      If rsColonne.RecordCount Then
        If TbAreaSeg!Tipo = "9" And (TbAreaSeg!Usage = "ZND" Or TbAreaSeg!Usage = "") And rsColonne!Tipo = "CHAR" Then
          tipoCampo = Replace(Replace(tipoCampo, "PIC '", "CHAR"), "9'", "")
        End If
      End If
      rsColonne.Close
      
      'tilvia
      Field = IIf(TbAreaSeg!Occurs <> 0, RTrim(Field) & " (" & TbAreaSeg!Occurs & ") ", Field) & tipoCampo & ","
      
      If Len(Field) > LenRiga Then
        k = InStr(Field, "PIC")
        If k <> 0 Then
          str = RTrim(Mid(Field, 1, k - 1))
          Str1 = RTrim(Mid(Field, k, Len(Field)))
          Field = str & Space(LenRiga - Len(str)) & vbCrLf
          For x = 1 To Len(str)
            If Mid(str, 1, 1) <> " " Then
              Str1 = Space(x + 2) & Str1
              Field = Field & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
              Exit For
            Else
              str = Mid(str, 2)
            End If
          Next x
        End If
      Else
        Struttura = Struttura & Field & vbCrLf
      '  field = field & Space(LenRiga - Len(field)) & vbCrLf
      End If
      If TbAreaSeg.RecordCount = 1 Then
        TbAreaSeg!livello = TbAreaSeg!livello - 1
      End If
      TbAreaSeg.MoveNext
    Wend

    TbAreaSeg.Close
    Struttura = Mid(Struttura, 1, InStrRev(Struttura, ",") - 1) & ";"
    'Struttura = Struttura & vbCrLf & "  RD_" & rsSegmenti!Nome & " = '' ;"
    
    changeTag RtbFile, "<STRUTTURA-ASSOCIATA>", Struttura
    changeTag RtbFile, "<NOME-SEGM>", rsSegmenti!Nome
  End If
End Sub

Public Function getPLIData(tipoDato As String, lunghezza As Integer, Tipo As String, _
                           Decimali As Integer, segno As String) As String

''''''''''''''''''''''''''''''
'segno al momento non gestito'
''''''''''''''''''''''''''''''

On Error GoTo ErrorHandler
  
  Tipo = Trim(Tipo)
  
  Select Case tipoDato
    Case "9"
      If Tipo = "COMP-3" Then
        If Decimali = 0 Then
          If lunghezza = 7 Then
            getPLIData = "FIXED DEC(7)"
          Else
            getPLIData = "FIXED DEC(" & lunghezza & ")"
          End If
        Else
          getPLIData = "FIXED DEC (" & lunghezza + Decimali & "," & Decimali & ")"
        End If
      ElseIf Tipo = "COMP" Then
        If Decimali = 0 Then
          If lunghezza <= 4 Then
            getPLIData = "FIXED BIN(15)"
          ElseIf lunghezza <= 9 Then
            getPLIData = "FIXED BIN(31)"
          End If
        Else
          If lunghezza + Decimali <= 4 Then
            getPLIData = "FIXED BIN (15," & Decimali & ")"
          ElseIf lunghezza + Decimali <= 9 Then
            getPLIData = "FIXED BIN (31," & Decimali & ")"
          End If
        End If
       ElseIf Tipo = "" Or Tipo = "0" Then
         getPLIData = "PIC '(" & lunghezza & ")9'"
      End If
    Case "X"
      If Tipo = "DECIMAL" Then
         getPLIData = "BIT(" & lunghezza & ")"
      Else
      getPLIData = "CHAR(" & lunghezza & ")"
''      If lunghezza > 255 Then
''        getPLIData = getPLIData & " VAR"
''      End If
      End If
  End Select

  Exit Function
ErrorHandler:
  m_fun.WriteLog "Errore in getPLIData: " & ERR.Description
  getPLIData = ""
End Function

Function Indenta(liv As Integer) As Long
  Dim x As Integer
  
  For x = 1 To UBound(Ind_Liv)
    If Ind_Liv(x).livello = Int(liv) Then
      Indenta = Ind_Liv(x).Indet
      Exit For
    End If
  Next x
End Function

'Sub InserisciArea_prova(tb As Recordset, rtb As RichTextBox)
'  Dim TbAreaSeg As Recordset, tbSegVirt As Recordset, TbOg As Recordset
'  Dim testo As String
'  Dim PosTesto As Long
'  Dim PosInit As Long
'  Dim Nome As String
'  Dim str As String
'  Dim Str1 As String
'  Dim WsTR As String
'  Dim wStr1 As String
'  Dim LenCmp As Integer
'  Dim Decimali As String
'  Dim LenRiga As Integer
'  Dim LenR As Integer
'  Dim k As Integer
'  Dim x As Integer
'  Dim i As Integer
'  Dim MaxLen As Integer
'  Dim wNomeSeg As String
'  Dim cont As String
'  Dim bol As Boolean
'
'  wNomeSeg = tb!Nome
'  'If Trim(Tb!Comprtn) <> "None" Then wNomeSeg = Tb!Comprtn
'
'  MaxLen = 0
'  ' Tratto i segmenti reali
'  ReDim Ind_Liv(0)
'  LenRiga = 72
'  Set TbAreaSeg = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento= " & tb!IdSegmento & " order by ordinale")
'  If TbAreaSeg.RecordCount > 0 Then
'    Nome = tb!Nome
'    '''''''''''''''''''''''''''''''
'    'routine calcola indentazione:
'    ' ogni livello ha la sua.
'    '''''''''''''''''''''''''''''''
'    For k = 1 To TbAreaSeg.RecordCount
'      If Len(TbAreaSeg!Nome) > MaxLen Then
'          MaxLen = Len(TbAreaSeg!Nome)
'          testo = TbAreaSeg!Nome
'      End If
'      If UBound(Ind_Liv) = 0 Then
'         ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
'         Ind_Liv(UBound(Ind_Liv)).Livello = Int(TbAreaSeg!Livello)
'         Ind_Liv(UBound(Ind_Liv)).Indet = 0
'      Else
'        For x = 1 To UBound(Ind_Liv)
'          If Ind_Liv(x).Livello = Int(TbAreaSeg!Livello) Then
'            bol = True
'            Exit For
'          End If
'        Next x
'        If Not bol Then
'           ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
'           Ind_Liv(UBound(Ind_Liv)).Livello = Int(TbAreaSeg!Livello)
'           Ind_Liv(UBound(Ind_Liv)).Indet = Ind_Liv(UBound(Ind_Liv) - 1).Indet + 3
'        End If
'      End If
'      bol = False
'      TbAreaSeg.MoveNext
'    Next k
'    TbAreaSeg.MoveFirst
'    NmSegCpy = tb!Nome
'
'    If Not Virtuale Then
'      '
'      'WsTR = Space(7) & "01 AREA-" & wNomeSeg & "  PIC X(" & Tb!MaxLen & ")."
'      'wStr1 = Space(7) & "01 RD-" & wNomeSeg & " REDEFINES AREA-" & wNomeSeg & "."
'      '...
'      'InsertParola RTB, "<STRUTTURA-ASSOCIATA>.", vbCrLf & WsTR & wStr1
'
'    Else
'      WsTR = Space(7) & "01 RD-" & wNomeSeg & "."
'      'cont = Format(val(cont) + 10, "00000000")
'      'wstr = wstr & Space(LenRiga - Len(wstr)) & cont & vbCrLf
'      WsTR = WsTR & Space(LenRiga - Len(WsTR)) & vbCrLf
'      InsertParola RTB, "<STRUTTURA-ASSOCIATA>.", vbCrLf & WsTR
'    End If
    
'    changeTag rtb, "<NOME-SEGM>", wNomeSeg
'    changeTag rtb, "<MAX-SEGLEN>", tb!MaxLen
'
'    'Inserisco la struttura
'    TbAreaSeg.MoveNext
'
'    While Not TbAreaSeg.EOF
'      LenCmp = TbAreaSeg!lunghezza
'      If TbAreaSeg!Decimali <> 0 Then
'        Decimali = "V9(" & TbAreaSeg!Decimali & ")"
'      Else
'        Decimali = ""
'      End If
'      '& Space(20 - Len(TbColonne!Colonna)) &
'      If LenCmp = "0" Then
'        If TbAreaSeg!Occurs = "1" Or TbAreaSeg!Occurs = "0" Then
'          If Trim(TbAreaSeg!NomeRed) <> "" Then
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & " REDEFINES " & TbAreaSeg!NomeRed & "."
'          Else
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & "."
'          End If
'        Else
'          WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & Space(3) & "OCCURS " & TbAreaSeg!Occurs & " TIMES."
'        End If
'      Else
'        If TbAreaSeg!segno = "S" Then
'           If Len(TbAreaSeg!Nome) > 30 Then
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & "  PIC S" & TbAreaSeg!Tipo & "(" & LenCmp & ")"
'           Else
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & Space(30 - Len(TbAreaSeg!Nome)) & "  PIC S" & TbAreaSeg!Tipo & "(" & LenCmp & ")"
'           End If
'        Else
'           If Len(TbAreaSeg!Nome) > 30 Then
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & "  PIC " & TbAreaSeg!Tipo & "(" & LenCmp & ")"
'           Else
'             WsTR = Space(7) & Space(Indenta(Int(TbAreaSeg!Livello))) & Format(TbAreaSeg!Livello, "00") & " " & TbAreaSeg!Nome & Space(30 - Len(TbAreaSeg!Nome)) & "  PIC " & TbAreaSeg!Tipo & "(" & LenCmp & ")"
'           End If
'        End If
'        Select Case TbAreaSeg!Usage
'           Case "BNR"
'              WsTR = WsTR & Decimali & " COMP"
'           Case "PCK"
'              WsTR = WsTR & Decimali & " COMP-3"
'           Case "ZND"
'              If Trim(TbAreaSeg!Tipo) = "9" Then WsTR = WsTR & Decimali
'        End Select
'        If TbAreaSeg!Occurs = "0" Or TbAreaSeg!Occurs = "1" Then
'           'wStr = wStr & "." & vbCrLf
'           WsTR = WsTR & "."
'
'           'wStr = wStr & Space(LenRiga - Len(wStr)) & cont & vbCrLf
'        Else
'          WsTR = WsTR & Space(5) & "OCCURS " & TbAreaSeg!Occurs & " TIMES." & vbCrLf
'        End If
'      End If
'      'cont = Format(val(cont) + 10, "00000000")
'      If Len(WsTR) > LenRiga Then
'        k = InStr(WsTR, "PIC")
'        If k <> 0 Then
'         str = RTrim(Mid(WsTR, 1, k - 1))
'         Str1 = RTrim(Mid(WsTR, k, Len(WsTR)))
'         'wstr = str & Space(LenRiga - Len(str)) & cont & vbCrLf
'         WsTR = str & Space(LenRiga - Len(str)) & vbCrLf
'         'cont = Format(val(cont) + 10, "00000000")
'         For x = 1 To Len(str)
'          If Mid(str, 1, 1) <> " " Then
'            Str1 = Space(x + 2) & Str1
'            'wstr = wstr & Str1 & Space(LenRiga - Len(Str1)) & cont & vbCrLf
'            WsTR = WsTR & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
'            Exit For
'          Else
'            str = Mid(str, 2)
'          End If
'         Next x
'        End If
'      Else
'        WsTR = WsTR & Space(LenRiga - Len(WsTR)) & cont & vbCrLf
'      '  wstr = wstr & Space(LenRiga - Len(wstr)) & vbCrLf
'      End If
'      rtb.SelText = WsTR
'      TbAreaSeg.MoveNext
'    Wend
'  End If
'End Sub

Sub CheckSegNT(Visible As Boolean)
  With MadmdF_EditDli
    .Frame1.Visible = Visible
    '.Frame3.Visible = Visible
    .Frame4.Visible = Visible
    .Frame6.Visible = Visible
    .Frame7.Visible = Visible
    ' Mauro 27/08/2007 : � la lista delle note! Perch� devo "eliminare" anche questa?!?!
    '.Frame8.Visible = Visible
    .Frame9.Visible = Visible
    .Frame10.Visible = Visible
    .Frame16.Visible = Visible
   ' .Frame17.Visible = Visible
    .Areaincidenza.Visible = Visible
    .FrmRelazioni.Visible = Visible
  End With
End Sub

Sub AssegnaAreaseg(seg As Double)
  Dim indCmp As Integer, Start As Integer, Lun As Integer, IndFld As Integer, fldins As Integer
  Dim Flag As Boolean
    
  indCmp = 2
  
  AggiungiStruttura seg
  
  For IndFld = 1 To UBound(gSegm(seg).gField)
    If Left(gSegm(seg).gField(IndFld).Name, 1) <> "/" Then
      If Not Flag Then
        AggiungiChiavePrimaria seg, indCmp, IndFld
        indCmp = indCmp + 1
        fldins = IndFld
        Flag = True
      Else
        If IndFld = 1 And Flag = False Then
          AggiungiPrimoCampo seg, indCmp, IndFld
          fldins = IndFld
          indCmp = indCmp + 1
        Else
          Lun = gSegm(seg).gField(IndFld).MaxLen
          Start = gSegm(seg).gField(IndFld).Start
          If (Start) > (gSegm(seg).gField(fldins).Start + gSegm(seg).gField(fldins).MaxLen) - 1 Then
            If (Start) = gSegm(seg).gField(fldins).Start + gSegm(seg).gField(fldins).MaxLen Then
              AggiungiChiaveSecondaria seg, indCmp, IndFld, fldins
              indCmp = indCmp + 1
              fldins = IndFld
            Else
              AggiungiCampo seg, indCmp, IndFld, fldins
              indCmp = indCmp + 1
              fldins = IndFld
              AggiungiChiaveSecondaria seg, indCmp, IndFld, fldins
              indCmp = indCmp + 1
              fldins = IndFld
            End If
          End If
        End If
      End If
    End If
  Next IndFld
  If (gSegm(seg).gField(fldins).Start + gSegm(seg).gField(fldins).MaxLen) - 1 <> gSegm(seg).MaxLen Then AggiungiUltimoCampo seg, indCmp, IndFld, fldins
End Sub
  
Sub AggiungiStruttura(seg As Double)
  ReDim Preserve gSegm(seg).Strutture(1).Campi(1)
  With gSegm(seg).Strutture(1).Campi(1)
    .NomeRed = ""
    .Ordinale = 1
    .bytes = gSegm(seg).MaxLen
    .Decimali = 0
    .livello = 1
    .lunghezza = 0
    .Occurs = 0
    .Posizione = 1
    .segno = "N"
    .Tipo = "X"
    .Usage = "ZND"
    .IdSegm = seg
    .Nome = "SEG" & Format(seg, "0000")
    .valore = ""
    '.index = 1
    '.index = False
    '.SKey = False
    .Correzione = "N"
    ' Mauro 15/01/2008
    .OccursFlat = False
    .DaMigrare = True
  End With
  
End Sub

Sub AggiungiCampo(indseg As Double, indCmp As Integer, IndFld As Integer, indsec As Integer)
  ReDim Preserve gSegm(indseg).Strutture(1).Campi(indCmp)
  With gSegm(indseg).Strutture(1).Campi(indCmp)
    .NomeRed = ""
    .Ordinale = indCmp
    .bytes = gSegm(indseg).gField(IndFld).Start - (gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen)
    .Decimali = 0
    .livello = 3
    .lunghezza = gSegm(indseg).gField(IndFld).Start - (gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen)
    .Occurs = 0
    .Posizione = gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen
    .segno = "N"
    .Tipo = "X"
    .Usage = ""
    .IdSegm = indseg
    .Nome = "DATI" & Format(indCmp, "000")
    .valore = ""
    ''.index = False
    ''.SKey = False
    .Correzione = "N"
    ' Mauro 15/01/2008
    .OccursFlat = False
    .DaMigrare = True
  End With
End Sub
Sub AggiungiPrimoCampo(indseg As Double, indCmp As Integer, IndFld As Integer)
'  ReDim Preserve gSegm(indseg).Strutture(1).Campi(indcmp)
'  With gSegm(indseg).Strutture(1).Campi(indcmp)
'    .NomeRed = ""
'    .Ordinale = indcmp
'    .Bytes = gSegm(indseg).gField(IndFld).Start - 1
'    .Decimali = 0
'    .Livello = 3
'    .Lunghezza = gSegm(indseg).gField(IndFld).Start - 1
'    .Occurs = 0
'    .Posizione = 1
'    .Segno = "N"
'    .Tipo = "X"
'    .Usage = ""
'    .IdSegm = indseg
'    .Nome = "DATI" & Format(indcmp, "000")
'    .Valore = ""
'    .Index = False
'    .SKey = False
'    .Correzione = "N"
'  End With
End Sub
Sub AggiungiUltimoCampo(indseg As Double, indCmp As Integer, IndFld As Integer, indsec As Integer)
'  ReDim Preserve gSegm(indseg).Strutture(1).Campi(indcmp)
'  With gSegm(indseg).Strutture(1).Campi(indcmp)
'    .NomeRed = ""
'    .Ordinale = indcmp
'    .Bytes = gSegm(indseg).MaxLen - (gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen - 1)
'    .Decimali = 0
'    .Livello = 3
'    .Lunghezza = gSegm(indseg).MaxLen - (gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen - 1)
'    .Occurs = 0
'    .Posizione = gSegm(indseg).gField(indsec).Start + gSegm(indseg).gField(indsec).MaxLen
'    .Segno = "N"
'    .Tipo = "X"
'    .Usage = ""
'    .IdSegm = indseg
'    .Nome = "DATI" & Format(indcmp, "000")
'    .Valore = ""
'    '.index = False
'    '.SKey = False
'    .Correzione = "N"
'  End With
End Sub

Sub AggiungiChiavePrimaria(indseg As Double, indCmp As Integer, IndFld As Integer)
  ReDim Preserve gSegm(indseg).Strutture(1).Campi(indCmp)
  With gSegm(indseg).Strutture(1).Campi(indCmp)
    .NomeRed = ""
    .Ordinale = indCmp
    .bytes = gSegm(indseg).gField(IndFld).MaxLen
    .Decimali = 0
    .livello = 3
    .lunghezza = gSegm(indseg).gField(IndFld).MaxLen
    .Occurs = 0
    .Posizione = gSegm(indseg).gField(IndFld).Start
    .segno = "N"
    .Tipo = "X"
    .Usage = ""
    .IdSegm = indseg
    .Nome = gSegm(indseg).gField(IndFld).Name
    .valore = ""
    '.index = True
    '.SKey = False
    .Correzione = "N"
    ' Mauro 15/01/2008
    .OccursFlat = False
    .DaMigrare = True
  End With
End Sub

Sub AggiungiChiaveSecondaria(indseg As Double, indCmp As Integer, IndFld As Integer, indsec As Integer)
  ReDim Preserve gSegm(indseg).Strutture(1).Campi(indCmp)
  With gSegm(indseg).Strutture(1).Campi(indCmp)
    .NomeRed = ""
    .Ordinale = indCmp
    .bytes = gSegm(indseg).gField(IndFld).MaxLen
    .Decimali = 0
    .livello = 3
    .lunghezza = gSegm(indseg).gField(IndFld).MaxLen
    .Occurs = 0
    .Posizione = gSegm(indseg).gField(IndFld).Start
    .segno = "N"
    .Tipo = "X"
    .Usage = ""
    .IdSegm = indseg
    .Nome = gSegm(indseg).gField(IndFld).Name
    .valore = ""
    '.index = False
    '.SKey = True
    .Correzione = "N"
    ' Mauro 15/01/2008
    .OccursFlat = False
    .DaMigrare = True
  End With
End Sub

Function VerificaAssociazioneSemi()
  
  Dim tbDMAS As Recordset
  Dim tbDMRS As Recordset
  Dim tbDMColonne As Recordset
  Dim tbSemi As Recordset
  
  Dim IdSegm As Integer
  Dim IdStrutt As Integer
  Dim Ordinale As Integer
  
  IdSegm = 0
  
  Screen.MousePointer = 11
  Set tbDMAS = m_fun.Open_Recordset("select * from DmAreaSeg where Modificato = false order by IdSegm ,ordinale")
  If tbDMAS.RecordCount <> 0 Then
    tbDMAS.MoveFirst
    While tbDMAS.EOF = False
      Set tbSemi = m_fun.Open_Recordset("select * from IMPSemi where Campo = '" & Replace(tbDMAS!NomeCmp, "'", "''") & "' and tiposel ='S' and Valido = true")
      If tbSemi.RecordCount <> 0 Then
         
         tbDMAS!Modificato = True
         tbDMAS.Update
      
      End If
      tbDMAS.MoveNext
    Wend
  End If
  Screen.MousePointer = 0
End Function

Sub EliminaTbPF()
  Dim tb As Recordset
  ''Stop
  'Set tb = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto = " & IndOgg)
  Set tb = m_fun.Open_Recordset("select * from MgRel_DLISeg where IdOggetto = " & IndOgg)
  If tb.RecordCount <> 0 Then
    tb.MoveFirst
    While tb.EOF = False
      tb.Delete
      tb.MoveNext
    Wend
  End If
End Sub

Sub EliminaRelazioni(SegAssApp() As Associazioni)
  Dim tb As Recordset
  'Dim SegAssApp() As Associazioni
  Dim i As Integer
  
  ReDim SegAssApp(0)
  
  For i = 1 To UBound(SegAss)
    If (SegAss(i).NameFather <> Nomenodo) And (SegAss(i).NameChild <> Nomenodo) Then
      ReDim Preserve SegAssApp(UBound(SegAssApp) + 1)
      SegAssApp(UBound(SegAssApp)).IdOggetto = SegAss(i).IdOggetto
      SegAssApp(UBound(SegAssApp)).NameChild = SegAss(i).NameChild
      SegAssApp(UBound(SegAssApp)).NameFather = SegAss(i).NameFather
      SegAssApp(UBound(SegAssApp)).Relazione = SegAss(i).Relazione
      SegAssApp(UBound(SegAssApp)).Tag = SegAss(i).Tag
      
    End If
  Next i
  
  ReDim SegAss(0)
  
  For i = 1 To UBound(SegAssApp)
      ReDim Preserve SegAss(UBound(SegAss) + 1)
      SegAss(UBound(SegAss)).IdOggetto = SegAssApp(i).IdOggetto
      SegAss(UBound(SegAss)).NameChild = SegAssApp(i).NameChild
      SegAss(UBound(SegAss)).NameFather = SegAssApp(i).NameFather
      SegAss(UBound(SegAss)).Relazione = SegAssApp(i).Relazione
      SegAss(UBound(SegAss)).Tag = SegAssApp(i).Tag
  Next i

End Sub

Sub CaricaTreeRelazioni(Tree As treeview)
  Dim tb As Recordset
  Dim TbVirt As Recordset
  Dim TbVirtOrig As Recordset
  Dim TbApp As Recordset
  Dim VirtIdOr As Integer
  Dim KeyF As String
  Dim indtree As Integer
  Dim Flag As Boolean
  Dim SegA As String
  
  ReDim SegAss(0)
  Tree.Nodes.Clear
  
  Set tb = m_fun.Open_Recordset("select * from MgRel_DLISeg where IdOggetto= " & IndOgg & " and Father = '0'")
  If tb.RecordCount <> 0 Then
      tb.MoveFirst
      While tb.EOF = False
        Tree.Nodes.Add , , tb!Child, tb!Child
        'HO SCRITTO DA QUI
        ReDim Preserve SegAss(UBound(SegAss) + 1)
        SegAss(UBound(SegAss)).IdOggetto = IndOgg
        SegAss(UBound(SegAss)).NameFather = tb!Father
        SegAss(UBound(SegAss)).NameChild = tb!Child
        SegAss(UBound(SegAss)).Relazione = tb!Relazione
        Set TbVirt = m_fun.Open_Recordset("select * from MgDLI_Segmenti where nome='" & tb!Child & "'")
        If TbVirt.RecordCount <> 0 Then
           If Trim(TbVirt!Correzione) = "I" Then
              VirtIdOr = TbVirt!IdSegmentoOrigine
              Set TbVirtOrig = m_fun.Open_Recordset("select * from PsDLI_Segmenti where idsegmento = " & VirtIdOr)
              If TbVirtOrig.RecordCount <> 0 Then
                 SegAss(UBound(SegAss)).Tag = TbVirtOrig!Nome
              End If
           Else
              SegAss(UBound(SegAss)).Tag = "Reale"
           End If
        Else
           SegAss(UBound(SegAss)).Tag = "Reale"
        End If
        tb.MoveNext
      Wend
    
    Flag = True
    indtree = 0
    While Flag
       indtree = indtree + 1
       If indtree > Tree.Nodes.count Then
          Flag = False
       Else
         KeyF = Tree.Nodes(indtree).Key
         SegA = Tree.Nodes(indtree).text
         Set tb = m_fun.Open_Recordset("select * from MgRel_DLISeg where IdOggetto= " & IndOgg & " and Father = '" & SegA & "'")
         If tb.RecordCount > 0 Then
           tb.MoveFirst
           While Not tb.EOF
              Tree.Nodes.Add KeyF, tvwChild, KeyF & "@" & Trim(tb!Child), Trim(tb!Child)
                ReDim Preserve SegAss(UBound(SegAss) + 1)
                SegAss(UBound(SegAss)).IdOggetto = IndOgg
                SegAss(UBound(SegAss)).NameFather = tb!Father
                SegAss(UBound(SegAss)).NameChild = tb!Child
                SegAss(UBound(SegAss)).Relazione = tb!Relazione
                Set TbVirt = m_fun.Open_Recordset("select * from MgDLI_Segmenti where nome='" & tb!Child & "'")
                If TbVirt.RecordCount <> 0 Then
                  If Trim(TbVirt!Correzione) = "I" Then
                    VirtIdOr = TbVirt!IdSegmentoOrigine
                    Set TbVirtOrig = m_fun.Open_Recordset("select * from PsDLI_Segmenti where idsegmento = " & VirtIdOr)
                    If TbVirtOrig.RecordCount <> 0 Then
                      SegAss(UBound(SegAss)).Tag = TbVirtOrig!Nome
                    End If
                  Else
                     SegAss(UBound(SegAss)).Tag = "Reale"
                  End If
                Else
                  SegAss(UBound(SegAss)).Tag = "Reale"
                End If
              If tb!Relazione = True Then Tree.Nodes(KeyF & "@" & Trim(tb!Child)).Checked = True
              tb.MoveNext
           Wend
          Else
          End If
       End If
    Wend
  Else
    Set tb = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto= " & IndOgg & " and Father = '0'")
    If tb.RecordCount <> 0 Then
      tb.MoveFirst
      While tb.EOF = False
      Tree.Nodes.Add , , tb!Child, tb!Child
      'HO SCRITTO DA QUI
      ReDim Preserve SegAss(UBound(SegAss) + 1)
      SegAss(UBound(SegAss)).IdOggetto = IndOgg
      SegAss(UBound(SegAss)).NameFather = tb!Father
      SegAss(UBound(SegAss)).NameChild = tb!Child
      SegAss(UBound(SegAss)).Relazione = tb!Relazione
      SegAss(UBound(SegAss)).Tag = "Reale"
      tb.MoveNext
      Wend
    End If
    
    Flag = True
    indtree = 0
    
    While Flag
      indtree = indtree + 1
      If indtree > Tree.Nodes.count Then
        Flag = False
      Else
        KeyF = Tree.Nodes(indtree).Key
        SegA = Tree.Nodes(indtree).text
        Set tb = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto= " & IndOgg & " and Father = '" & SegA & "'")
        If tb.RecordCount > 0 Then
          tb.MoveFirst
          While Not tb.EOF
            Tree.Nodes.Add KeyF, tvwChild, KeyF & "@" & Trim(tb!Child), Trim(tb!Child)
            ReDim Preserve SegAss(UBound(SegAss) + 1)
            SegAss(UBound(SegAss)).IdOggetto = IndOgg
            SegAss(UBound(SegAss)).NameFather = tb!Father
            SegAss(UBound(SegAss)).NameChild = tb!Child
            SegAss(UBound(SegAss)).Relazione = tb!Relazione
            SegAss(UBound(SegAss)).Tag = "Reale"
            
            If tb!Relazione = True Then Tree.Nodes(KeyF & "@" & Trim(tb!Child)).Checked = True
            tb.MoveNext
          Wend
        End If
      End If
    Wend
  End If
End Sub

Sub VisualizzaRelazioni(Tree As treeview)

  Dim tb As Recordset
  Dim TbApp As Recordset
  Dim KeyF As String
  Dim indtree As Integer
  Dim Flag As Boolean
  Dim SegA As String
  Dim x As Integer
  
  On Error GoTo ERR:
  
  Tree.Nodes.Clear
  For x = 1 To UBound(SegAss)
    If SegAss(x).IdOggetto = IndOgg And SegAss(x).NameFather = "0" Then
      If SegAss(x).Tag = "Reale" Then
        Tree.Nodes.Add , , SegAss(x).NameChild, SegAss(x).NameChild
        If SegAss(x).Relazione = True Then Tree.Nodes(Tree.Nodes.count).Checked = True
      Else
        Tree.Nodes.Add , , SegAss(x).NameChild, SegAss(x).NameChild
        Tree.Nodes(Tree.Nodes.count).ForeColor = ColoraNodo(Tree.Nodes(Tree.Nodes.count).FullPath)
        If SegAss(x).Relazione = True Then Tree.Nodes(Tree.Nodes.count).Checked = True
      End If
      'Exit For
    End If
  Next x
    
  Flag = True
  indtree = 0
  While Flag
    indtree = indtree + 1
    If indtree > Tree.Nodes.count Then
      Flag = False
    Else
      KeyF = Tree.Nodes(indtree).Key
      SegA = Tree.Nodes(indtree).text
      For x = 1 To UBound(SegAss)
        If SegAss(x).IdOggetto = IndOgg And Trim(SegAss(x).NameFather) = Trim(SegA) Then
          If SegAss(x).Tag = "Reale" Then
            Tree.Nodes.Add KeyF, tvwChild, KeyF & "@" & SegAss(x).NameChild, Trim(SegAss(x).NameChild)
            If SegAss(x).Relazione = True Then Tree.Nodes(KeyF & "@" & Trim(SegAss(x).NameChild)).Checked = True
            Tree.Nodes(KeyF & "@" & Trim(SegAss(x).NameChild)).ForeColor = ColoraNodo(Tree.Nodes(KeyF & "@" & Trim(SegAss(x).NameChild)).FullPath)
          Else
            Tree.Nodes.Add KeyF, tvwChild, "VIRT" & "@" & KeyF & "@" & SegAss(x).NameChild, Trim(SegAss(x).NameChild)
            If SegAss(x).Relazione = True Then Tree.Nodes("VIRT" & "@" & KeyF & "@" & Trim(SegAss(x).NameChild)).Checked = True
            Tree.Nodes("VIRT" & "@" & KeyF & "@" & Trim(SegAss(x).NameChild)).ForeColor = ColoraNodo(Tree.Nodes("VIRT" & "@" & KeyF & "@" & Trim(SegAss(x).NameChild)).FullPath)
          End If
        End If
      Next x
    End If
  Wend
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 9
      MsgBox "Load error!!!", vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case 3021
      MsgBox "Load error!!!", vbCritical + vbOKOnly, DataMan.DmNomeProdotto
    Case Else
      Stop
  End Select

End Sub

Function ColoraNodo(NodePath As String) As Integer
  If InStr(UCase(NodePath), "VIRT") <> 0 Then
    If IsNumeric(Mid(NodePath, InStr(1, UCase(NodePath), "VIRT") + 4, 3)) Then
    ColoraNodo = vbRed
    End If
  Else
    ColoraNodo = vbBlack
  End If
End Function

Sub CheckRelazioniPresenti(IOgg As Double, Tree As treeview)
  Dim indseg As Integer
  Dim seg As String
  Dim tb As Recordset
  Dim TbApp As Recordset
  Dim Flag As Boolean
  
  Set tb = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto = " & IOgg)
  If tb.RecordCount <> 0 Then
    tb.MoveLast
    tb.MoveFirst
    While tb.EOF = False
      seg = tb!Child
      Flag = False
      For indseg = 1 To Tree.Nodes.count
        If seg = Tree.Nodes(indseg).text Then
          Flag = True
          Exit For
        End If
      Next indseg
      If Flag = False Then
        'Set TbApp = m_fun.Open_Recordset("select * from RelazioniPF where IdOgg = " & IndOgg _
        & " and ( Father= '" & seg & "' or child= '" & seg & "')")
        Set TbApp = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto = " & IndOgg _
        & " and ( Father= '" & seg & "' or child= '" & seg & "')")
        If TbApp.RecordCount <> 0 Then
          TbApp.MoveFirst
          While TbApp.EOF = False
            TbApp.Delete
            TbApp.MoveNext
          Wend
        End If
        tb.Requery
    End If
    tb.MoveNext
    Wend
  End If
End Sub

Sub AssociaAnalisiImpatto(Campo As String, Ordinale As Integer, ListaStrutture As ListView, ListaCampi As ListView)
'  Dim tbcampo As Recordset
'  Dim TbApp As Recordset
'  Dim tbStrutt As Recordset
'
'  If ListaCampi.SelectedItem.Selected = True Then
'    If ListaCampi.SelectedItem.ListSubItems(1).ForeColor <> vbRed Then
'      Set tbcampo = DbPrj.OpenRecordset("select * from IMPSemi where Campo= '" & Campo & "'")
'      If tbcampo.RecordCount = 0 Then
'        Set TbApp = DbPrj.OpenRecordset("select * from campi where nomecmp= '" & Campo & "' and Ordinale = " & Ordinale & " and IdStruttura= " & Int(ListaStrutture.SelectedItem.Text))
'        If TbApp.RecordCount <> 0 Then
'          Set tbStrutt = DbPrj.OpenRecordset("select * from Strutture where IdStruttura= " & TbApp!IdStruttura)
'          tbcampo.AddNew
'          tbcampo!Codice = TbApp!IdStruttura & "/" & TbApp!Ordinale
'          tbcampo!Struttura = tbStrutt!Struttura
'          tbcampo!Campo = Campo
'          tbcampo!TipoSel = "M"
'          tbcampo!Caratteristica = TbApp!Tipo & "," & TbApp!Usage & "," & TbApp!Lunghezza & "," & TbApp!Decimali & "," & TbApp!Segno
'          tbcampo!IdOggetto = tbStrutt!IdOggetto
'          tbcampo.Update
'        End If
'      End If
'      ListaCampi.SelectedItem.ListSubItems(1).ForeColor = vbRed
'      ListaCampi.Refresh
'      MsgBox "Field Inserted"
'    Else
'      Set tbcampo = DbPrj.OpenRecordset("select * from IMPSemi where Campo= '" & Campo & "'")
'      If tbcampo.RecordCount <> 0 Then tbcampo.Delete
'      ListaCampi.SelectedItem.ListSubItems(1).ForeColor = vbBlack
'      ListaCampi.Refresh
'      MsgBox "Field deleted"
'    End If
'  End If
End Sub

Sub CreaRelazioniPF(Tree As treeview)
  Dim indchild As Integer
  Dim indseg As Integer
  Dim Node As Node
  Dim tb As Recordset
  Dim TbApp As Recordset
    
    Set tb = m_fun.Open_Recordset("select * from PsRel_DLISeg where IdOggetto = " & IndOgg)
    CheckRelazioniPresenti IndOgg, Tree
    If tb.RecordCount = 0 Then
      For indseg = 1 To Tree.Nodes.count
        If indseg = 1 Then
          tb.AddNew
          tb!IdOggetto = gSegm(indseg).IdOggetto
          tb!Father = "ROOT"
          tb!Child = Tree.Nodes(1).text
          tb.Update
        End If
        If Tree.Nodes(indseg).Children <> 0 Then
          Set Node = Tree.Nodes(indseg).Child
          For indchild = 1 To Tree.Nodes(indseg).Children
            tb.AddNew
            tb!IdOggetto = gSegm(indseg).IdOggetto
            tb!Father = Tree.Nodes(indseg).text
            tb!Child = Node.text
            Set Node = Node.Next
            tb.Update
          Next indchild
        Else
        If gSegm(indseg).Correzione = "I" Then
            Set tb = m_fun.Open_Recordset("select * from RelazioniPF where IdOgg= " & gSegm(indseg).IdOggetto)
            Set TbApp = m_fun.Open_Recordset("select * from RelazioniPF")
            tb.MoveFirst
            While tb.EOF = False
              If tb!Father = NomeFather Then
                TbApp.AddNew
                TbApp!IdOgg = tb!IdOgg
                TbApp!Father = Tree.Nodes(indseg).text
                TbApp!Child = tb!Child
                TbApp.Update
              End If
              If tb!Child = NomeFather Then
                TbApp.AddNew
                TbApp!IdOgg = tb!IdOgg
                TbApp!Father = tb!Father
                TbApp!Child = Nomenodo
                TbApp.Update
              End If
              tb.MoveNext
            Wend
          End If
        End If
      Next indseg
    End If
End Sub

Sub AggiungiRelazioni(Nome As String, Optional Virtuale As Variant)
  
  Dim tb As Recordset
  Dim TbApp As Recordset
  Dim i As Integer
  
  For i = 1 To UBound(SegAssApp)
    If Not IsMissing(Virtuale) Then
      If SegAssApp(i).NameFather = Nome Then
        ReDim Preserve SegAssApp(UBound(SegAssApp) + 1)
        SegAssApp(UBound(SegAssApp)).IdOggetto = SegAssApp(i).IdOggetto
        SegAssApp(UBound(SegAssApp)).NameChild = SegAssApp(i).NameChild
        If IsMissing(Virtuale) Then
          SegAssApp(UBound(SegAssApp)).NameFather = Trim(UCase(NomeVirt))
        Else
          SegAssApp(UBound(SegAssApp)).NameFather = Trim(UCase(Virtuale))
        End If
        SegAssApp(UBound(SegAssApp)).Relazione = SegAssApp(i).Relazione
        SegAssApp(UBound(SegAssApp)).Tag = "Virtuale"
      End If
    Else
      If SegAssApp(i).NameChild = Nome Then
        ReDim Preserve SegAssApp(UBound(SegAssApp) + 1)
        SegAssApp(UBound(SegAssApp)).IdOggetto = SegAssApp(i).IdOggetto
        If IsMissing(Virtuale) Then
          SegAssApp(UBound(SegAssApp)).NameChild = Trim(UCase(NomeVirt))
        Else
          SegAssApp(UBound(SegAssApp)).NameChild = Trim(UCase(Virtuale))
        End If
        SegAssApp(UBound(SegAssApp)).NameFather = SegAssApp(i).NameFather
        SegAssApp(UBound(SegAssApp)).Relazione = SegAssApp(i).Relazione
        SegAssApp(UBound(SegAssApp)).Tag = "FiglioVirtuale"
      ElseIf SegAssApp(i).NameFather = Nome Then
        ReDim Preserve SegAssApp(UBound(SegAssApp) + 1)
        SegAssApp(UBound(SegAssApp)).IdOggetto = SegAssApp(i).IdOggetto
        SegAssApp(UBound(SegAssApp)).NameChild = SegAssApp(i).NameChild
        If IsMissing(Virtuale) Then
          SegAssApp(UBound(SegAssApp)).NameFather = Trim(UCase(NomeVirt))
        Else
          SegAssApp(UBound(SegAssApp)).NameFather = Trim(UCase(Virtuale))
        End If
        SegAssApp(UBound(SegAssApp)).Relazione = SegAssApp(i).Relazione
        SegAssApp(UBound(SegAssApp)).Tag = "Virtuale"
      End If
    End If
  Next i
End Sub

Sub AggiungiTB(errore, Stringa, rich As RichTextBox)
  Dim vLen As Double
  Dim testo As String
  
  testo = Trim(errore) & " - " & Stringa & vbCrLf
  vLen = Len(rich.text)
  rich.SelStart = vLen
  rich.SelLength = 0
  rich.SelText = testo
End Sub

''Public Sub CaratteristicheCampo(seg As Integer, Struttura As Integer, Campo As Integer, Tree As treeview, TVal As TextBox, lbl As Label, Tlen As TextBox, Tbytes As TextBox, TNomeCmp As TextBox, tDec As TextBox, tOcc As TextBox, Tstart As TextBox, combotipo As ComboBox, Optional TUsage As TextBox)
''  Dim K As Integer
''  With gSegm(seg).Strutture(1).Campi(Campo)
''    If Tree.SelectedItem.Bold = True Then
''      TVal.Visible = True
''      lbl.Visible = True
''    Else
''      TVal.Visible = False
''      lbl.Visible = False
''    End If
''    If Not (IsNull(.valore)) Then TVal = .valore
''    Tlen = .lunghezza
''    Tbytes = .bytes
''    TNomeCmp = .Nome
''    tDec = .Decimali
''    tOcc = .Occurs
''    Tstart = .Posizione
''    FillComboTipo seg, Struttura, Campo, combotipo
''    TUsage = .Usage
''  End With
''End Sub
''
''Sub TreeViewMergeSel(Tree As treeview, list As ListView, StruttAss As ListView, NomeCampo As String, Ind As Integer, TVal As TextBox, lbl As Label, Tlen As TextBox, Tbytes As TextBox, TNomeCmp As TextBox, tDec As TextBox, tOcc As TextBox, Tstart As TextBox, combotipo As ComboBox, Optional TUsage As TextBox)
''  Dim indseg As Integer
''  Dim indstr As Integer
''  Dim indcmp As Integer
''  Dim indstr1 As Integer
''  Dim indcmp1 As Integer
''  Dim K As Integer
''  Dim j As Integer
''  Dim z As Integer
''  Dim Lun As Integer
''  Dim App As String
''  On Error Resume Next
''  list.ListItems.Clear
''  For indseg = 1 To UBound(gSegm)
''    If Nomenodo = gSegm(indseg).Name Then
''      For indcmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
''        K = InStr(1, NomeCampo, "  ")
''        If K = 0 Then K = Len(NomeCampo)
''        j = InStr(1, NomeCampo, "(")
''        z = InStr(1, NomeCampo, ")")
''        If gSegm(indseg).Strutture(1).Campi(indcmp).lunghezza <> 0 Then
''          If gSegm(indseg).Strutture(1).Campi(indcmp).Decimali <> 0 Then
''            Lun = val(Mid$(NomeCampo, j + 1, z - j - 1) + gSegm(indseg).Strutture(1).Campi(indcmp).Decimali)
''          Else
''            Lun = gSegm(indseg).Strutture(1).Campi(indcmp).lunghezza
''          End If
''        Else
''          Lun = val(gSegm(indseg).Strutture(1).Campi(indcmp).bytes)
''        End If
''        If gSegm(indseg).Strutture(1).Campi(indcmp).Nome = Trim(Mid$(NomeCampo, 1, K)) And Lun = val(gSegm(indseg).Strutture(1).Campi(indcmp).lunghezza) Then
''
''          CaratteristicheCampo indseg, 1, indcmp, Tree, TVal, lbl, Tlen, Tbytes, TNomeCmp, tDec, tOcc, Tstart, combotipo, TUsage
''
''          If Tree.SelectedItem.Children <> 0 Then
''            Exit Sub
''          End If
''          Contr_Lun = True
''          For indstr1 = 2 To UBound(gSegm(indseg).Strutture)
''            If gSegm(indseg).Strutture(indstr1).Correzione <> "C" Then
''              For indcmp1 = 1 To UBound(gSegm(indseg).Strutture(indstr1).Campi)
''                With gSegm(indseg).Strutture(indstr1).Campi(indcmp1)
''                App = .IdOggetto & "/" & .idArea
''                  If gSegm(indseg).Strutture(1).Campi(indcmp).Posizione = .Posizione And gSegm(indseg).Strutture(1).Campi(indcmp).bytes = .bytes And gSegm(indseg).Strutture(1).Campi(indcmp).Usage = .Usage And .lunghezza <> "0" And App = StruttAss.SelectedItem.text Then ' .IdStruttura = StruttAss.SelectedItem.text Then
''                    list.ListItems.Add , , .Livello
''                    list.ListItems(list.ListItems.count).SubItems(1) = .Nome
''                    list.ListItems(list.ListItems.count).SubItems(2) = "Pic " & .Tipo & " (" & .bytes & ") "
''                    list.ListItems(list.ListItems.count).SubItems(3) = .bytes
''                    list.ListItems(list.ListItems.count).SubItems(4) = .Posizione
''                    list.ListItems(list.ListItems.count).SubItems(5) = .lunghezza
''                    list.ListItems(list.ListItems.count).SubItems(6) = .Occurs
''                  Else
''                    If Sovrapposizione(gSegm(indseg).Strutture(indstr1).Campi(indcmp1).lunghezza, val(gSegm(indseg).Strutture(1).Campi(indcmp).Posizione), val(gSegm(indseg).Strutture(1).Campi(indcmp).bytes), val(gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Posizione), val(gSegm(indseg).Strutture(indstr1).Campi(indcmp1).bytes)) And gSegm(indseg).Strutture(indstr1).Campi(indcmp1).Nome <> gSegm(indseg).Strutture(1).Campi(indcmp).Nome And gSegm(indseg).Strutture(indstr1).Campi(indcmp1).IdStruttura = StruttAss.SelectedItem.text Then
''                      list.ListItems.Add , , .Livello
''                      list.ListItems(list.ListItems.count).SubItems(1) = .Nome
''                      list.ListItems(list.ListItems.count).SubItems(2) = "Pic " & .Tipo & " (" & .bytes & ") "
''                      list.ListItems(list.ListItems.count).SubItems(3) = .bytes
''                      list.ListItems(list.ListItems.count).SubItems(4) = .Posizione
''                      list.ListItems(list.ListItems.count).SubItems(5) = .lunghezza
''                      list.ListItems(list.ListItems.count).SubItems(6) = .Occurs
''                    End If
''                  End If
''                End With
''              Next indcmp1
''            End If
''          Next indstr1
''          Exit Sub
''        End If
''      Next indcmp
''    End If
''  Next indseg
''End Sub

Sub CaricaTabSeg(Tree As treeview)
  Dim indseg As Integer
  Dim i As Long
  Dim ExistKey As Boolean
  Dim MsgBool As Boolean
  Dim arrayIndseg() As Long
  
  ReDim arrayIndseg(0)
  Tree.Nodes.Clear
  
  For indseg = 1 To UBound(gSegm)
    ExistKey = False
    'If Trim(gSegm(indseg).Parent) = "0" And gSegm(indseg).Correzione <> "IC" Then
    For i = 1 To Tree.Nodes.count
     If Trim(UCase(Tree.Nodes(i).Key)) = UCase(Trim(gSegm(indseg).Name)) Then
       ExistKey = True
       MsgBool = True
       Exit For
     End If
    Next i
    
    If Trim(gSegm(indseg).Parent) = "0" And gSegm(indseg).Correzione <> "C" Then
      If gSegm(indseg).Correzione <> "I" Then
         
        If Not ExistKey Then
          Tree.Nodes.Add , , Trim(gSegm(indseg).Name), Trim(gSegm(indseg).Name), "SegVero"
        End If
      Else
         
        If Not ExistKey Then
          ReDim Preserve arrayIndseg(UBound(arrayIndseg) + 1)
          arrayIndseg(UBound(arrayIndseg) - 1) = indseg
          'Tree.Nodes.Add , , Trim(gSegm(indseg).Name), Trim(gSegm(indseg).Name), "SegVirt" 'ALE
        End If
      End If
      If ExistKey = False Then
        Nomenodo = Trim(gSegm(indseg).Name)
      End If
    Else
      'If Trim(gSegm(indseg).Correzione) = "I" And gSegm(indseg).Correzione <> "IC" Then
      If Trim(gSegm(indseg).Correzione) = "I" And gSegm(indseg).Correzione <> "C" Then
        If ExistKey = False Then
          ReDim Preserve arrayIndseg(UBound(arrayIndseg) + 1)
          arrayIndseg(UBound(arrayIndseg) - 1) = indseg
         'Tree.Nodes.Add Trim(gSegm(indseg).Parent), tvwChild, Trim(gSegm(indseg).Name), Trim(gSegm(indseg).Name), "SegVirt" 'ALE
        End If
     Else
        'If gSegm(indseg).Correzione <> "IC" Then
        If gSegm(indseg).Correzione <> "C" Then
          If ExistKey = False Then
            Tree.Nodes.Add Trim(gSegm(indseg).Parent), tvwChild, Trim(gSegm(indseg).Name), Trim(gSegm(indseg).Name), "SegVero"
          End If
        End If
      End If
    End If
  Next indseg
  
  For indseg = 0 To UBound(arrayIndseg) - 1
    'SQ 20-04-07: NO.
    'xstr = gSegm(arrayIndseg(indseg)).Name
    'xstr = Left(xstr, Len(xstr) - 2)
    For i = 1 To UBound(gSegm)
      If gSegm(i).IdSegm = gSegm(arrayIndseg(indseg)).IdOrigine Then
        Tree.Nodes.Add Trim(gSegm(i).Name), tvwNext, Trim(gSegm(arrayIndseg(indseg)).Name), Trim(gSegm(arrayIndseg(indseg)).Name), "SegVirt" 'ALE
        Exit For
      End If
    Next
    If i > UBound(gSegm) Then
      m_fun.WriteLog "###TMP: segment parent not found for: " & gSegm(arrayIndseg(indseg)).Name
    End If
  Next

  ' Mauro 02/08/2007 : Reimposto il nome del primo nodo,
  ' senn� poi tutti i controlli vengono effettuati sull'ultimo nodo
  If Tree.Nodes.count Then
    Nomenodo = Tree.Nodes(1)
  End If
End Sub

Sub CaricaSemi(ListaCmp As ListView, ListaStrutt As ListView)
'  Dim tbSemi As Recordset
'  Dim indcmp As Integer
'
'  'Set tbSemi = m_fun.Open_Recordset("select * from IMPsemi where Codice like '" & ListaStrutt.SelectedItem.text & "*'", tbSemi)
'  Set tbSemi = m_fun.Open_Recordset("select * from IMPsemi where Codice like '" & ListaStrutt.SelectedItem.text & "*'", tbSemi)
'  If tbSemi.RecordCount <> 0 Then
'    tbSemi.MoveFirst
'    While tbSemi.EOF = False
'      For indcmp = 1 To ListaCmp.ListItems.Count
'        If ListaCmp.ListItems(indcmp).SubItems(1) = tbSemi!Campo Then
'          ListaCmp.ListItems(indcmp).ListSubItems(1).ForeColor = vbRed
'          ListaCmp.Refresh
'          Exit For
'        End If
'      Next indcmp
'      tbSemi.MoveNext
'    Wend
'  End If
End Sub
Sub CancRepField(ListaField As ListView)
  Dim indseg As Integer
  Dim IndFld As Integer
  For indseg = 1 To UBound(gSegm)
    If gSegm(indseg).Name = Nomenodo Then
      For IndFld = 1 To UBound(gSegm(indseg).gField)
        If gSegm(indseg).gField(IndFld).Name = ListaField.SelectedItem.text Then
          'Discrimino dall'icona!
          If ListaField.SelectedItem.SmallIcon = 3 Then
            ListaField.SelectedItem.SmallIcon = 4
            gSegm(indseg).gField(IndFld).Correzione = "C"
            gSegm(indseg).gField(IndFld).Index = False
          ElseIf ListaField.SelectedItem.SmallIcon = 4 Then
            If ListaField.SelectedItem.SubItems(3) > 40 Then  '???????????????????????
              ListaField.SelectedItem.SmallIcon = 11
            Else
              ListaField.SelectedItem.SmallIcon = 3
            End If
            gSegm(indseg).gField(IndFld).Correzione = "M" 'SQ! l'ho messo io!!!!!!!!!!
            gSegm(indseg).gField(IndFld).Index = False
          ElseIf ListaField.SelectedItem.SmallIcon = 11 Then
            ListaField.SelectedItem.SmallIcon = 4
            gSegm(indseg).gField(IndFld).Correzione = "C"
            gSegm(indseg).gField(IndFld).Index = False
          ElseIf ListaField.SelectedItem.SmallIcon = 10 Then
            ListaField.SelectedItem.SmallIcon = 4
            gSegm(indseg).gField(IndFld).Correzione = "C"
            gSegm(indseg).gField(IndFld).Index = False
          End If
          Exit For
        End If
      Next IndFld
    Exit For
    End If
  Next indseg
End Sub

Sub StampaCoda()
  Printer.Line (1000, 14500)-(9000 + Printer.TextWidth("STRUTTURE"), 14500)
  Printer.CurrentX = 5925
  Printer.CurrentY = 14600
  Printer.Print Printer.Page
  Printer.CurrentX = 1000
  Printer.CurrentY = 14600
  Printer.Print "i-4 s.r.l."
  Printer.CurrentX = 1000
  Printer.Print "Coriano"
  Printer.CurrentX = 1000
  Printer.Print ""
  Printer.CurrentX = 1000
  Printer.Print "http://www.i-4.it"
End Sub

Sub Testata()
    
  Printer.ForeColor = vbRed
  Printer.CurrentX = 1000
  Printer.CurrentY = 1900
  Printer.Print "SEGMENTO"
  
  Printer.CurrentX = 5000
  Printer.CurrentY = 1900
  Printer.Print "FIELD"
  Printer.CurrentX = 6550
  Printer.CurrentY = 1900
  Printer.Print "P."
  Printer.CurrentX = 7350
  Printer.CurrentY = 1900
  Printer.Print "L."
  
  Printer.CurrentX = 9000
  Printer.CurrentY = 1900
  Printer.Print "STRUTTURE"
  Printer.ForeColor = vbBlack
End Sub

Sub Cancella_Campo_Da_Memoria(Campo As String, NumOrdiNale As Long, NumLivello As Long)
  Dim indseg As Double
  Dim indStr As Integer
  Dim indCmp As Integer
  Dim k As Integer
  Dim PosApp As Integer
  Dim CampoApp As wCampo
  Dim StringApp As String

  For indseg = 1 To UBound(gSegm)
    If Nomenodo = gSegm(indseg).Name Then
      For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
        If gSegm(indseg).Strutture(1).Campi(indCmp).Nome = Trim(Campo) And gSegm(indseg).Strutture(1).Campi(indCmp).Ordinale = Trim(NumOrdiNale) And gSegm(indseg).Strutture(1).Campi(indCmp).livello = Trim(NumLivello) Then
          For k = indCmp To UBound(gSegm(indseg).Strutture(1).Campi) - 1
            With gSegm(indseg).Strutture(1)
              .Campi(k).IdSegm = gSegm(indseg).Strutture(1).Campi(k).IdSegm
              .Campi(k).Nome = gSegm(indseg).Strutture(1).Campi(k + 1).Nome
              .Campi(k).Ordinale = .Campi(k + 1).Ordinale
              .Campi(k).Posizione = val(gSegm(indseg).Strutture(1).Campi(k + 1).Posizione)
              .Campi(k).bytes = gSegm(indseg).Strutture(1).Campi(k + 1).bytes
              .Campi(k).Correzione = gSegm(indseg).Strutture(1).Campi(k + 1).Correzione
              .Campi(k).Decimali = gSegm(indseg).Strutture(1).Campi(k + 1).Decimali
              .Campi(k).livello = gSegm(indseg).Strutture(1).Campi(k + 1).livello
              .Campi(k).lunghezza = gSegm(indseg).Strutture(1).Campi(k + 1).lunghezza
              .Campi(k).NomeRed = gSegm(indseg).Strutture(1).Campi(k + 1).NomeRed
              .Campi(k).Occurs = gSegm(indseg).Strutture(1).Campi(k + 1).Occurs
              .Campi(k).segno = gSegm(indseg).Strutture(1).Campi(k + 1).segno
              .Campi(k).Tipo = gSegm(indseg).Strutture(1).Campi(k + 1).Tipo
              .Campi(k).Usage = gSegm(indseg).Strutture(1).Campi(k + 1).Usage
              .Campi(k).valore = gSegm(indseg).Strutture(1).Campi(k + 1).valore
              .Campi(k).Index = gSegm(indseg).Strutture(1).Campi(k + 1).Index
            End With
          Next k
          Ricalcola_Posizione_Lunghezza
          ReDim Preserve gSegm(indseg).Strutture(1).Campi(UBound(gSegm(indseg).Strutture(1).Campi) - 1)
          Exit Sub
        End If
      Next indCmp
    End If
  Next indseg

End Sub

Function Trova_Chiave(Lista As ListView, IndImage As Integer) As Boolean

  Dim x As Integer
  
  For x = 1 To Lista.ListItems.count
    If Lista.ListItems(x).SmallIcon = IndImage Then
      Trova_Chiave = False
      Exit Function
    End If
  Next x
  
  Trova_Chiave = True
  
End Function
Public Sub Pulisci_Txt_CampoStrut()
  'MadmdF_EditDli.TxtDecimali = ""
'  MadmdF_EditDli.txtLen = ""
'  MadmdF_EditDli.TxtLunghezza = ""
'  MadmdF_EditDli.TxtOccurs = ""
'  MadmdF_EditDli.TxtStart = ""
'  MadmdF_EditDli.TxtUsage = ""
'  MadmdF_EditDli.TxtNomeCmp = ""
'  MadmdF_EditDli.TxtValore = ""
'  MadmdF_EditDli.Combo2 = ""
End Sub

Public Sub Area_Primaria(seg As Double, indStr As Double)
  Dim indCmp As Integer
  
  For indStr = 2 To UBound(gSegm(seg).Strutture)
    If Trim(gSegm(seg).Strutture(indStr).Correzione) <> "C" Then
      For indCmp = 1 To UBound(gSegm(seg).Strutture(indStr).Campi)
        
        ReDim Preserve gSegm(seg).Strutture(1).Campi(indCmp)
        gSegm(seg).Strutture(1).Campi(indCmp).Ordinale = indCmp
        gSegm(seg).Strutture(1).Campi(indCmp).Modificato = gSegm(seg).Strutture(indStr).Campi(indCmp).Modificato
        gSegm(seg).Strutture(1).Campi(indCmp).Nome = gSegm(seg).Strutture(indStr).Campi(indCmp).Nome
        gSegm(seg).Strutture(1).Campi(indCmp).bytes = gSegm(seg).Strutture(indStr).Campi(indCmp).bytes
        gSegm(seg).Strutture(1).Campi(indCmp).Decimali = gSegm(seg).Strutture(indStr).Campi(indCmp).Decimali
        gSegm(seg).Strutture(1).Campi(indCmp).livello = gSegm(seg).Strutture(indStr).Campi(indCmp).livello
        gSegm(seg).Strutture(1).Campi(indCmp).lunghezza = gSegm(seg).Strutture(indStr).Campi(indCmp).lunghezza
        gSegm(seg).Strutture(1).Campi(indCmp).NomeRed = gSegm(seg).Strutture(indStr).Campi(indCmp).NomeRed
        gSegm(seg).Strutture(1).Campi(indCmp).Occurs = gSegm(seg).Strutture(indStr).Campi(indCmp).Occurs
        gSegm(seg).Strutture(1).Campi(indCmp).Posizione = gSegm(seg).Strutture(indStr).Campi(indCmp).Posizione
        gSegm(seg).Strutture(1).Campi(indCmp).segno = gSegm(seg).Strutture(indStr).Campi(indCmp).segno
        gSegm(seg).Strutture(1).Campi(indCmp).Tipo = gSegm(seg).Strutture(indStr).Campi(indCmp).Tipo
        gSegm(seg).Strutture(1).Campi(indCmp).Usage = gSegm(seg).Strutture(indStr).Campi(indCmp).Usage
        gSegm(seg).Strutture(1).Campi(indCmp).IdSegm = gSegm(seg).IdSegm
        gSegm(seg).Strutture(1).Campi(indCmp).Correzione = ""
        gSegm(seg).Strutture(1).Campi(indCmp).valore = gSegm(seg).Strutture(indStr).Campi(indCmp).valore
      
      Next indCmp
      Exit For
    End If
  Next indStr
End Sub
  
' Sostituisce una parola in una RTB
Sub InsertParola(rtb As RichTextBox, FindWord As String, ReplaceWord As String)
  Dim k As Long
  On Error GoTo errFind
  k = rtb.Find(FindWord, 1)
  If k > 0 Then
    rtb.SelStart = k
    rtb.SelLength = Len(FindWord)
    rtb.SelText = ReplaceWord
  End If
  Exit Sub
errFind:
  'gestire...
End Sub

' Cancella una parola in una RTB
Sub DelParola(rtb As RichTextBox, Parola As String)
  Dim k As Long
  On Error GoTo errFind
  
  k = rtb.Find(Parola, 1)
  If k > 0 Then
    rtb.SelStart = k
    rtb.SelLength = Len(Parola)
    rtb.SelText = ""
  End If
  Exit Sub
errFind:
  'la parola non c'e': a posto cosi'!
End Sub
