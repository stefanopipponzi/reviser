VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_DLI2RDBMS 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " IMS-DB to RDBMS Conversion"
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   7575
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdExpLog 
      Caption         =   "Export Log"
      Enabled         =   0   'False
      Height          =   555
      Left            =   2280
      Picture         =   "MadmdF_DLI2RDBMS.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "fixed"
      ToolTipText     =   "export log in text"
      Top             =   30
      Width           =   975
   End
   Begin VB.CommandButton CmdExtraColums 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1680
      Picture         =   "MadmdF_DLI2RDBMS.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "fixed"
      ToolTipText     =   "Extra Columns"
      Top             =   30
      Width           =   550
   End
   Begin VB.CommandButton CmdChangeAtt 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1140
      Picture         =   "MadmdF_DLI2RDBMS.frx":0A14
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "fixed"
      ToolTipText     =   "Change Column Attributes"
      Top             =   30
      Width           =   550
   End
   Begin VB.CommandButton CmdErase 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   600
      Picture         =   "MadmdF_DLI2RDBMS.frx":16DE
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "fixed"
      ToolTipText     =   "Erase"
      Top             =   30
      Width           =   550
   End
   Begin VB.CommandButton CmdConvert 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   45
      Picture         =   "MadmdF_DLI2RDBMS.frx":19E8
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "fixed"
      ToolTipText     =   "Convert"
      Top             =   30
      Width           =   550
   End
   Begin VB.Frame Frame7 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   3390
      TabIndex        =   11
      Top             =   1680
      Width           =   4125
      Begin MSComctlLib.ListView LswLog 
         Height          =   4515
         Left            =   120
         TabIndex        =   12
         Top             =   270
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "DBD List"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   60
      TabIndex        =   9
      Top             =   1680
      Width           =   3285
      Begin MSComctlLib.ListView lswDBSel 
         Height          =   4515
         Left            =   120
         TabIndex        =   10
         Top             =   270
         Width           =   3045
         _ExtentX        =   5371
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "DBD"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "Saved"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   5100
      TabIndex        =   4
      Top             =   630
      Width           =   2415
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   240
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   450
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   90
         TabIndex        =   13
         Top             =   660
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Preferences"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   3390
      TabIndex        =   3
      Top             =   630
      Width           =   1695
      Begin VB.OptionButton optDB2 
         Caption         =   "DB2"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   600
         Value           =   -1  'True
         Width           =   675
      End
      Begin VB.OptionButton optOracle 
         Caption         =   "Oracle"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   300
         Width           =   915
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   7830
      Top             =   450
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_DLI2RDBMS.frx":1E2A
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_DLI2RDBMS.frx":1F3C
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_DLI2RDBMS.frx":2116
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   60
      TabIndex        =   0
      Top             =   630
      Width           =   3285
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected DBD"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Value           =   -1  'True
         Width           =   2655
      End
      Begin VB.OptionButton OptAll 
         Caption         =   "All DLI Databases"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   2655
      End
   End
End
Attribute VB_Name = "MadmdF_DLI2RDBMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public AddTbs As Boolean
'Public TBSOpt As String
'M = MULTI o S = SINGLE
Public IdIndexCur As Long
Dim IdDBDinMigr As Long
Public segIndex As Integer
Public DEFAULT_NULL As Boolean
Public DEFAULT_DEFAULT As Boolean
Public DEFAULT_CREATOR As String
Dim headerColumns As Integer    'solo il numero... diventera' tutta la struttura (recordset)
Dim isDispatcher As Boolean     'SQ - Tabella PILOTA per le REDEFINES...
Dim inheritedPrefix As String   'SQ - prefisso per i nomi delle colonne ereditate

'SQ tmp
Dim occursFields() As String

Dim FkNameParam As String, AkNameParam As String 'Environment Parameters: IMSDB-FK-NAME
Dim IndexNameParam As String    'Environment Parameters:
Dim DbNameParam As String, TableNameParam As String, ColNameParam As String, TableChNameParam As String
Dim ForeignKeyName As String

Private Sub Cancella_colonne_disp(wSegIndex As Long)
  'con gli idcolonna join con dmorc_colonne e cancellare tutte le <> e tabella=tabell
  Dim r1 As Recordset, r2 As Recordset, r3 As Recordset, rs As Recordset
  Dim l As Long
  Dim s As String
  Dim maxIdcol As Long
  Dim maxOrdinale As Long
  
  Set r1 = m_fun.Open_Recordset("Select idTable From " & PrefDb & "_Tabelle where idOrigine = " & wSegIndex)
  r1.MoveNext
  If Not r1.EOF Then
    l = r1!IdTable
    Set r2 = m_fun.Open_Recordset("Select distinct idcolonna From " & PrefDb & "_idxcol where idtable = " & r1!IdTable)
    s = ""
    If Not r2.EOF Then
      While Not r2.EOF
        s = s & r2!IdColonna & ","
        r2.MoveNext
      Wend
      s = Mid(s, 1, Len(s) - 1)
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_colonne where " & _
                                   "idtable = " & r1!IdTable & " and idcolonna not in(" & s & ")"
    'TILVIA 3-4-2009 COSA SERVE LO STOP???
    Else
      'Stop
      r1.Close
      Exit Sub
    End If
  'TILVIA 3-4-2009 COSA SERVE LO STOP???
  Else
    '  Stop
    r1.Close
    Exit Sub
  End If

  'AGGIUNTA COLONNA "DATA_TABLE":
  Set r3 = m_fun.Open_Recordset("Select max(idcolonna) as Massimo From " & PrefDb & "_colonne ")
  If Not r3.EOF Then maxIdcol = r3!massimo + 1
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_colonne where " & _
                                "idtable = " & r1!IdTable & " order by ordinale desc")
  If Not rs.EOF Then
    maxOrdinale = rs!Ordinale + 1
    rs.AddNew
    rs!IdColonna = maxIdcol
    rs!IdTable = r1!IdTable
    rs!idTableJoin = r1!IdTable
    rs!Nome = "DATA_TABLE"
    rs!Ordinale = maxOrdinale
    rs!IdCmp = -1
    rs!Tipo = "CHAR"
    rs!lunghezza = 18
    rs!Decimali = 0
    rs!Descrizione = "Dispatcher Column"
    rs!Null = False
    rs!IsDefault = DEFAULT_DEFAULT
  
    If DEFAULT_DEFAULT Then
      rs!defaultValue = ""
    Else
      rs!defaultValue = ""
    End If
  
    rs!Tag = "AUTOMATIC-DISPATCHER"
    rs!RoutLoad = "load_Table_DISPATCHER"
    rs!RoutRead = "rdbms2dli_Table_DISPATCHER"
    rs!RoutWrite = "dli2rdbms_Table_DISPATCHER"
    rs!RoutKey = "dli2rdbms_Table_KEY_DISPATCHER"
    rs.Update
  
    r1.Close
    r2.Close
    r3.Close
    rs.Close
  End If
  rs.Close
  
End Sub

Private Sub CmdChangeAtt_Click()
  Dim SQL As String
  Dim i As Long
  Dim r As Recordset
  Dim nfile As Integer, ErrFile As Integer
  Dim RigaTxt As String
  Dim RecordTxt() As String
  Dim rsDb2Name As Recordset, rsCobolName As Recordset
  Dim CmpTrovato As Boolean, ColTrovata As Boolean
  Dim Segmenti As String
        
  If MsgBox("Column Exceptions managing: continue?" & vbCrLf & _
            "Input file: 'ColumnExceptions.txt' in 'input-prj' directory.", vbYesNo + vbQuestion, "i-4.Migration - Column Exceptions") = vbNo Then
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  If optDB2.Value Then
    TipoDB = "DB2"
    PrefDb = "DMDB2"
  ElseIf optOracle.Value Then
    TipoDB = "ORACLE"
    PrefDb = "DMORC"
  End If
  '??
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
  
  m_fun.FnProcessRunning = True
  nfile = FreeFile
  Open m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.txt" For Binary As nfile
  ErrFile = FreeFile
  Open m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.log" For Output As ErrFile
  
  Do While Loc(nfile) < FileLen(m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.txt")
    Line Input #nfile, RigaTxt
    If Trim(RigaTxt) = "" Then Exit Do
    RecordTxt = Split(RigaTxt, " ")
    CmpTrovato = False
    'Set rsCobolName = m_fun.Open_Recordset("select * from MgData_Cmp where Nome = '" & RecordTxt(1) & "' and Tipo = '" & RecordTxt(2) & "' and Lunghezza = " & RecordTxt(4))
    'DEVO FILTRARE I SEGMENTI!
    'SQ 19-06-06
    'TRATTO (AUTOMATICAMENTE) ANCHE LE COLONNE DEI SEGMENTI VIRTUALI
    Set r = m_fun.Open_Recordset("SELECT idSegmento from PsDli_segmenti where nome = '" & RecordTxt(0) & "'" & _
                                 " UNION " & _
                                 "SELECT idsegmento from MgDli_segmenti where correzione = 'I' and " & _
                                 "nome like '" & RecordTxt(0) & "__'")
    Do While Not r.EOF
      Set rsCobolName = m_fun.Open_Recordset("select * from MgData_Cmp where Nome = '" & RecordTxt(1) & "' and " & _
                                             "Tipo = '" & RecordTxt(2) & "' and [usage] = '" & RecordTxt(3) & "' and " & _
                                             "Lunghezza = " & RecordTxt(4) & " and decimali = " & RecordTxt(5) & " AND " & _
                                             "idSegmento = " & r!IdSegmento)
      If rsCobolName.RecordCount Then
        CmpTrovato = True
        ColTrovata = False
        ' Mauro 13-04-2007 : non modificava i campi ereditati
'        Set rsDb2Name = m_fun.Open_Recordset("select a.* from DMDB2_Colonne as a, DMDB2_Tabelle as b where " & _
'                                             "a.IdCmp = " & rsCobolName!IdCmp & " and " & _
'                                             "a.idtable = b.idtable and " & _
'                                             "a.idtable = a.idtablejoin and " & _
'                                             "b.idorigine = " & r!IdSegmento)
        Set rsDb2Name = m_fun.Open_Recordset("select a.* from " & PrefDb & "_Colonne as a, " & PrefDb & "_Tabelle as b where " & _
                                             "a.IdCmp = " & rsCobolName!IdCmp & " and " & _
                                             "a.idtablejoin = b.idtable and " & _
                                             "b.idorigine = " & r!IdSegmento)
        'ATTENZIONE, CE NE DOVREBBE ESSERE SOLO UNO!
        Do While Not rsDb2Name.EOF
          ColTrovata = True
          'VC: per non contraggiornare il nome
          rsDb2Name!Nome = RecordTxt(6)
          rsDb2Name!Tipo = RecordTxt(7)
          rsDb2Name!lunghezza = RecordTxt(8)
          'DECIMALI!
          rsDb2Name!Decimali = RecordTxt(9)
          ' Mauro 13-04-2007 : Se � una chiave ereditata deve cambiare solo tipo e lunghezze
          If rsDb2Name!IdTable = rsDb2Name!idTableJoin Then
            'VC: inseriti per aggiornare l'associazione con il template corretto
            If rsDb2Name!Descrizione = "Column migrated from DLI - inherited from parent key" Then
              rsDb2Name!RoutLoad = ""
            Else
              rsDb2Name!RoutLoad = RecordTxt(10)
            End If
            If UBound(RecordTxt) > 10 Then
              rsDb2Name!RoutRead = RecordTxt(11)
            End If
            If UBound(RecordTxt) > 11 Then
              rsDb2Name!RoutWrite = RecordTxt(12)
            End If
            If UBound(RecordTxt) > 12 Then
              rsDb2Name!RoutKey = RecordTxt(13)
            End If
          End If
          If InStr(1, rsDb2Name!Descrizione, " Changed!") = 0 Then
            rsDb2Name!Descrizione = rsDb2Name!Descrizione & " Changed!"
          End If
          rsDb2Name.Update
          rsDb2Name.MoveNext
        Loop
        rsDb2Name.Close
      Else
        Segmenti = r!IdSegmento & ","
      End If
      r.MoveNext
    Loop
    If Not CmpTrovato Then
      Segmenti = Mid(Segmenti, 1, Len(Segmenti) - 1)
      If InStr(Segmenti, ",") Then
        Print #ErrFile, "NO CAMPI - " & RigaTxt & " per i Segmenti " & Segmenti
      Else
        Print #ErrFile, "NO CAMPI - " & RigaTxt & " per il Segmento " & Segmenti
      End If
    Else
      If Not ColTrovata Then
        Print #ErrFile, "NO COLONNA - " & RigaTxt
      End If
    End If
  Loop
  Close nfile
  Close ErrFile
  
  m_fun.FnProcessRunning = False
  Screen.MousePointer = vbDefault
  MsgBox "Process terminated." & vbCrLf & "Log file: 'ColumnExceptions.log' in 'input-prj' directory.", vbInformation, "i-4.Migration - Column Exceptions"
End Sub

Private Sub CmdConvert_Click()
  Dim i As Long
  CmdExpLog.Enabled = True
  Screen.MousePointer = vbHourglass
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents

  m_fun.FnProcessRunning = True
  
  'Carica per ogni segmento una tabella DB2 in memoria
  LswLog.ListItems.Add , , Time & " - Starting Analysis..."
  MadmdF_DLI2RDBMS.LswLog.Refresh
  LswLog.ListItems.Add , , Time & " - Starting Data Migration..."
    
  ' Mauro 01/08/2007 : Aggiunto per un'unificazione futura delle routine DB2 e ORACLE
  If optDB2.Value Then
    TipoDB = "DB2"
    PrefDb = "DMDB2"
  ElseIf optOracle.Value Then
    TipoDB = "ORACLE"
    PrefDb = "DMORC"
  End If
    
  Carica_Tabelle_DLI
  
  PBar01.MAX = lswDBSel.ListItems.Count
  For i = 1 To lswDBSel.ListItems.Count
    'VC: coerente con la modifica fatta per i tipi DBD
    'If lswDBSel.ListItems(i).Selected And Is_Hidam(UCase(Trim(lswDBSel.ListItems(i).Text))) Then
    If OptAll.Value Or lswDBSel.ListItems(i).Selected Then
      LswLog.ListItems.Add , , Time & " - Porting DBD: " & lswDBSel.ListItems(i).text
      MadmdF_DLI2RDBMS.LswLog.Refresh
      
      If lswDBSel.ListItems(i).SubItems(1) = "X" Then
        ' Mauro 01/08/2007 : Unificata la routine DB2-ORACLE
        Translate_DLI_2_RDBMS UCase(Trim(lswDBSel.ListItems(i).text)), val(Trim(lswDBSel.ListItems(i).Tag))
        If Not m_fun.is_GSAM(val(Trim(lswDBSel.ListItems(i).Tag))) Then
          Add_Key_Idx PBarK, val(Trim(lswDBSel.ListItems(i).Tag))
        End If
''        If optDB2.Value Then
''          Translate_DLI_2_DB2 UCase(Trim(lswDBSel.ListItems(i).text)), val(Trim(lswDBSel.ListItems(i).Tag))
''          'VC: (SQ) - GSAM: no indici
''          If Not m_fun.is_GSAM(val(Trim(lswDBSel.ListItems(i).Tag))) Then
''            Add_Key_Idx_DB2 PBarK, val(Trim(lswDBSel.ListItems(i).Tag))
''          End If
''        ElseIf optOracle.Value Then
''          Translate_DLI_2_ORACLE UCase(Trim(lswDBSel.ListItems(i).text)), val(Trim(lswDBSel.ListItems(i).Tag))
''          If Not m_fun.is_GSAM(val(Trim(lswDBSel.ListItems(i).Tag))) Then
''            Add_Key_Idx_ORACLE PBarK, val(Trim(lswDBSel.ListItems(i).Tag))
''          End If
''        End If
      Else
        LswLog.ListItems.Add , , Time & " - Porting DBD " & lswDBSel.ListItems(i).text & " not success!"
        MadmdF_DLI2RDBMS.LswLog.Refresh
      End If
    End If
    
    PBar01.Value = PBar01.Value + 1
  Next i
''''''''  End If
  
  PBar01.Value = PBar01.MAX

  If optDB2.Value Then
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Data Migration from DLI to DB2 Finished"
  ElseIf optOracle.Value Then
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Data Migration from DLI to ORACLE Finished"
  End If
  
  MadmdF_DLI2RDBMS.LswLog.Refresh
  LswLog.ListItems.Add , , ""
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  WaitTime 0.1  '?
  
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0
  
  CLsOn False
  
  m_fun.FnProcessRunning = False
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdErase_Click()
  Dim SQL As String
  Dim i As Long
  Dim r As Recordset
  Dim scelta As Long
  
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
  
      
  m_fun.FnProcessRunning = True
  
  scelta = MsgBox("All DLI To RDBMS data migration will be Lost. Are you sure?", vbExclamation + vbYesNo, "i-4.Migration")
  
  If scelta = vbYes Then
    scelta = MsgBox("Click Yes if you want to continue.", vbExclamation + vbYesNoCancel, "i-4.Migration")
    
    If scelta = vbYes Then
      CLsOn True
      LswLog.ListItems.Add , , Time & " - Deleting Old DB2 Data Migration..."
      MadmdF_DLI2RDBMS.LswLog.Refresh
    
      DataMan.DmConnection.Execute "DELETE * From DMDB2_DB Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_TableSpaces Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Tabelle Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Colonne Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Index Where tag like 'AUTOMATIC%'"
      'SQ:
      DataMan.DmConnection.Execute "DELETE * From DMDB2_IdxCol Where tag like 'AUTOMATIC%'"
      'gli alias?
      
      LswLog.ListItems.Add , , Time & " - Deleting Old DB2 Data Migration [SUCCESSFUL]"
      MadmdF_DLI2RDBMS.LswLog.Refresh
      
      LswLog.ListItems.Add , , Time & " - Deleting Old ORACLE data migration..."
      MadmdF_DLI2RDBMS.LswLog.Refresh
           
      DataMan.DmConnection.Execute "DELETE * From DMORC_DB Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_TableSpaces Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Tabelle Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Colonne Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Index Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_IdxCol Where tag like 'AUTOMATIC%'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Trigger Where tag like 'AUTOMATIC%'"
       
      LswLog.ListItems.Add , , Time & " - Deleting Old ORACLE data migration [SUCCESSFUL]"
      MadmdF_DLI2RDBMS.LswLog.Refresh
    
      CLsOn False
       
      'Cancella gli archivi VSAM
      '''''''''''''''''
    End If
  End If
  m_fun.FnProcessRunning = False
End Sub

Private Sub CmdExpLog_Click()
' Roba della SILVIA
  Dim i As Long
  Dim nfile As Integer
  Dim RigaTxt  As String
  nfile = FreeFile
  MsgBox "Export log in text file: " & m_fun.FnPathDB & "\Input-Prj\lswlog.txt"
  Open m_fun.FnPathDB & "\Input-Prj\lswlog.txt" For Output As nfile
  
  For i = 1 To LswLog.ListItems.Count
    RigaTxt = LswLog.ListItems(i).text
    Print #nfile, RigaTxt
  Next i
  Close nfile
End Sub

Private Sub CmdExtraColums_Click()
  Dim SQL As String, i As Long
  Dim r As Recordset
  
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
  
  If optDB2.Value Then
    MadmdF_ExtraColumns.TipoDB = "DB2"
  ElseIf optOracle Then
    MadmdF_ExtraColumns.TipoDB = "ORACLE"
  End If
  MadmdF_ExtraColumns.Show
'  SetParent MadmdF_ExtraColumns.hwnd, formParentResize.hwnd
'  MadmdF_ExtraColumns.Show
'  MadmdF_ExtraColumns.Move 0, 0, formParentResize.Width, formParentResize.Height
End Sub
                               
'� settato da carica_Default... spostarlo!
Private Sub Form_Load()
  'Setta il parent della form
  'SetParent Me.hwnd, DataMan.DmParent
  
  'ResizeControls Me, , True
  
  'TBSOpt = "S"
  'SQ OptTBSSingle.Value = True
  
  lswDBSel.ColumnHeaders(1).Width = lswDBSel.Width - 270

  Load_Array_Char
  
  'Carica la lista completa dei DB DLI
  Carica_Lista_DBDLI
  
  'Carica i parametri
  DataMan.Carica_Parametri_DataManager
  
  'SQ
  Carica_Default
  getEnvironmentParameters
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ
' Accede alle tabelle (FINIRE) di "properties" che definiscono
' alcuni parametri di configurazione iniziale:
' - DEFAULT_NULL
' - DEFAULT_DEFAULT
' - INTEGRITA REFERENZIALE
' - (COLONNE DI HEADER)
' - ...?
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Carica_Default()
  Dim r As Recordset
  '''''''''''''
  ' TMP:
  '''''''''''''
  DEFAULT_NULL = False
  DEFAULT_DEFAULT = False
  DEFAULT_CREATOR = LeggiParam("DM_CREATOR")
  
  Set r = m_fun.Open_Recordset("Select count(*) From DMheader_Columns")
  headerColumns = IIf(r.EOF, 0, r(0))
  r.Close
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   Dim wScelta As Long
   
   If m_fun.FnProcessRunning = True Then
      wScelta = m_fun.Show_MsgBoxError("FB01I")
      
      If wScelta = vbNo Then
         Cancel = 0 'No unload
         m_fun.FnStopProcess = False
      Else
         Cancel = 1 'Unload
         m_fun.FnStopProcess = True
      End If
   End If
End Sub
Private Sub Form_Resize()
    ResizeForm Me
    
  'Resize delle liste
  lswDBSel.ColumnHeaders(2).Width = 900
  lswDBSel.ColumnHeaders(1).Width = (lswDBSel.Width - 380) - lswDBSel.ColumnHeaders(2).Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
   m_fun.RemoveActiveWindows Me
   m_fun.FnActiveWindowsBool = False

   SetFocusWnd m_fun.FnParent
End Sub
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Key_Idx_DB2(Pb As ProgressBar, IdOgg As Long)
Public Sub Add_Key_Idx(Pb As ProgressBar, IdOgg As Long)
  Dim r As Recordset
  Dim i As Integer
  Dim IdSegmento As Long, IdTable As Long
  Dim env As Collection
  
  Carica_Segmenti IdOgg 'HO CARICATO TUTTO IL DATABASE, TABELLE, ECC... E ADESSO RICICLO I SEGMENTI?
  
  Pb.MAX = UBound(Segmenti)
  Pb.Value = 0
  
  For segIndex = 1 To UBound(Segmenti)
    IdSegmento = Segmenti(segIndex).IdSegmento
    
    '''''''''''''''''''''''''''''''''''
    ' Strutture FIELD/XDFIELD:
    '''''''''''''''''''''''''''''''''''
    Carica_Field Segmenti(segIndex)
    Carica_XDField Segmenti(segIndex)
    
    If segIndex < UBound(Segmenti) And Not isDispatcher Then
      ' Mauro 03-04-2007 : Gestione Redefines nel segmento Virtuale
      'isDispatcher = Segmenti(segIndex + 1).IdSegmentoOrigine And Segmenti(segIndex).IdSegmentoOrigine = 0  'OCCURS (ocio: unico caso?!)
      Dim rsVirtualSeg As Recordset
      Dim TabOrigine As Long
      Dim a As Long
      If Segmenti(segIndex + 1).IdSegmentoOrigine And Segmenti(segIndex).IdSegmentoOrigine = 0 Then
        isDispatcher = False
        TabOrigine = Segmenti(segIndex).IdSegmento
        a = segIndex
        Do Until a > UBound(Segmenti)
          If TabOrigine = Left(Segmenti(a).IdSegmento, Len(TabOrigine & "")) Then
            Set rsVirtualSeg = m_fun.Open_Recordset("select redefines from mgdli_segmenti where " & _
                                                    "idsegmento = " & Segmenti(a).IdSegmento)
            If rsVirtualSeg.RecordCount Then
              If rsVirtualSeg!Redefines Then
                isDispatcher = True
              End If
            End If
            rsVirtualSeg.Close
            a = a + 1
          Else
            Exit Do
          End If
        Loop
      Else
        isDispatcher = False
      End If
    Else
      'Ultima tabella: non pu� avere redefines!
      isDispatcher = False
    End If
  
    'SQ:
    'GESTIONE CHIAVI PRIMARIE.
    'Memorizzo la chiave del segmento padre...
    'BPER:
    Dim extraHeaderColumn As Integer
    If Segmenti(segIndex).Parent <> "0" Then
      'TMP
      'If m_fun.FnNomeDB = "banca.mty" Then extraHeaderColumn = 1
      For i = 1 To UBound(Segmenti)
        If Segmenti(i).Nome = Segmenti(segIndex).Parent Then
          'ATTENZIONE: IdxcolumnsKey � valorizzato solo se il segmento
          'padre � gi� stato "processato"; in teoria sempre! (verificare)
          'ex SQ: parentIdxCol = Segmenti(i).IdxcolumnsKey
          parentSeqKeys = Segmenti(i).SeqKeys
          Exit For
        End If
      Next i
    Else
      ReDim parentSeqKeys(0)
    End If
    
    'IdTable:
    '7-05-06: OCCURS: N TABELLE CON STESSA idOrigine
    'Set r = m_fun.Open_Recordset("Select IdTable From DMORC_Tabelle Where IdOrigine = " & IdSegmento)
    'stefano: forzatura per ora
    'Set r = m_fun.Open_Recordset("Select IdTable From DMORC_Tabelle Where IdOrigine = " & IdSegmento & " AND TAG='AUTOMATIC'")
    Set r = m_fun.Open_Recordset("Select IdTable, nome From " & PrefDb & "_Tabelle Where " & _
                                 "IdOrigine = " & IdSegmento & " AND TAG = 'AUTOMATIC'")

    Dim nomeTab As String
    If r.RecordCount = 1 Then
      IdTable = r!IdTable
      nomeTab = r!Nome
    Else
      'MsgBox "problema... ho " & r.RecordCount & " tabelle?"
      m_fun.WriteLog "problem... n� " & r.RecordCount & " tables? - IdSegment: " & IdSegmento
      Exit Sub
    End If
    r.Close
    
    '''''''''''''''''''''''''''''''''
    'TMP
    'Gestioni segmenti senza chiave:
    '''''''''''''''''''''''''''''''''
    Select Case m_fun.FnNomeDB
      Dim seqM As Boolean
      Case "banca.mty"
        '...
        Field(UBound(Field)).lunghezza = 4
        Field(UBound(Field)).Nome = "NB_SEQ"       'ex EXTRA_KEY_BPER
        Field(UBound(Field)).Primario = False  'True - discrimino con Prov...
        Field(UBound(Field)).Unique = True
        Field(UBound(Field)).Prov = IIf(seqM, "BPER-MPK", "BPER-PK")
        '...
        r!Nome = "NB_SEQ" ' ex "BPER_EXTRAKEY"
        r!Tipo = "SMALLINT"
        r!lunghezza = 4  '  SMALLINT
        '...
        r!Descrizione = "BPER key column"
        '...
      Case Else
        seqM = False
        'add_FieldKey_BPER idTb
        '''''''''''''''''''''''''''''''''''''''''
        For i = 1 To UBound(Field)
          If Field(i).Primario Then
            If Field(i).Unique Then
              Exit For
            Else
              'vedi sotto Field(UBound(Field)).Prov
              seqM = True
            End If
          End If
        Next i
        
        'CHIAVE PRIMARIA DA AGGIUNGERE:
        If i > UBound(Field) Then
          ReDim Preserve Field(UBound(Field) + 1)
          ' Mauro 10/09/2010 : Se ho gi� un IdField da DLI, lo mantengo
          If UBound(Field) > 1 Then
            Field(UBound(Field)).IdField = Field(UBound(Field) - 1).IdField
          Else
            ' Metto -1 e conseguentemente l'IdColonna solo se non ho Field da DLI
            Field(UBound(Field)).IdField = -1  'attenzione, ci appoggio IdColonna! dopo...
          End If
          Field(UBound(Field)).IdSegmento = IdSegmento
          Field(UBound(Field)).lunghezza = 4  'NO!?
'stefano: da sostituire con getEnvironmentParam(str, "tabelle", env), ma per ora non ho tutti
'  gli elementi per farlo....
          Field(UBound(Field)).Nome = Replace(LeggiParam("IMSDB-PK-NAME"), "(TABLE-NAME)", nomeTab) 'controllo...
          Field(UBound(Field)).Primario = False  'True - discrimino con Prov...
          Field(UBound(Field)).Unique = True
          Field(UBound(Field)).Prov = IIf(seqM, "BPER-MPK", "BPER-PK")
          
          'SQ 7-06-06
          'MANCA QUESTO...
          'VERIFICARE
          Segmenti(segIndex).SeqKeys = parentSeqKeys
          
          'SHIFT ORDINALE COLONNE ORIGINALI:
          'BPER: colonna header extra:
          Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where " & _
                                       "IdTable = " & IdTable & " AND Ordinale > " & headerColumns + extraHeaderColumn)
          While Not r.EOF
            r!Ordinale = r!Ordinale + 1
            r.MoveNext
          Wend
          r.AddNew
          r!IdColonna = getId("IdColonna", PrefDb & "_Colonne")  '-1 se errori...
          '!!!
          If Field(UBound(Field)).IdField = -1 Then
            Field(UBound(Field)).IdField = r!IdColonna
          End If
          r!IdTable = IdTable
          r!Ordinale = headerColumns + 1 + extraHeaderColumn
          r!idTableJoin = IdTable 'ATTENZIONE: PUO' DARE FASTIDIO?
          'stefano: proprio forzato... da rivedere
          r!Nome = Replace(LeggiParam("IMSDB-PK-NAME"), "(TABLE-NAME)_", "") 'controllo...
          r!Tipo = LeggiParam("IMSDB-PK-TYPE") 'controllo...
          r!lunghezza = IIf(Len(LeggiParam("IMSDB-PK-LENGHT")), CInt(LeggiParam("IMSDB-PK-LENGHT")), 0) 'controllo...
          r!Decimali = IIf(Len(LeggiParam("IMSDB-PK-DECIMALS")), CInt(LeggiParam("IMSDB-PK-DECIMALS")), 0) 'controllo...
          'r!default = gestire...
          r!Null = False
          r!Descrizione = "REV-FIELD key column"
          r!IdCmp = -1
          r!Tag = "AUTOMATIC"
          r!RoutLoad = "load_REVKEY"
          r!RoutWrite = "dli2rdbms_REVKEY"
          r!RoutKey = "dli2rdbms_KEY_REVKEY"
          r.Update
          r.Close
        End If
    End Select
    
    Add_Index IdTable, Get_IDTBS_Da_Seg(IdSegmento)
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE SOTTOTABELLE OCCURS:
    ' SQ - 21-06-06
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set r = m_fun.Open_Recordset("Select idTable,idTableJoin From " & PrefDb & "_Tabelle Where " & _
                                 "IdTableJoin = " & IdTable & " AND TAG = 'AUTOMATIC-OCCURS'")
    
    If r.RecordCount Then
      'SILVIA 11-03-2008 gestione nome tabelle child con naming convention
''      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
''        Add_Index r!IdTable, Get_IDTBS_Da_Tabella(r!IdTable)
''      Else
        createOccursIndex r
''      End If
    End If
    r.Close
    
    Pb.Value = Pb.Value + 1
    Pb.Refresh
    ''''''''''''WaitTime 0.1  '?
    DoEvents
    
    'virgilio _________
    If segIndex < UBound(Segmenti) And isDispatcher Then
     Cancella_colonne_disp Segmenti(segIndex).IdSegmento
    End If
    'virgilio _________
    
  Next segIndex
End Sub
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Carica_IdxCol_DB2(idx As Long, idTb As Long)
Public Sub Carica_IdxCol(idx As Long, idTb As Long, Optional ckField = False)
  Dim r As Recordset
  Dim i As Long, idLastTable As Long
  Dim idCmpList As String
  
  If UBound(parentSeqKeys) = 0 Then 'attenzione, controllo significativo solo per le chiavi...
    ReDim idxcol(0)
  End If

  For i = 1 To UBound(Key)
    'SQ - 22-06-06
    'FACEVA CASINO CON LE OCCURS!!!!!
    'Set r = m_fun.Open_Recordset("Select * From DMDB2_Colonne Where IdCmp = " & Key(i).IdCmp)
    'Gestione idCmp = -1 (devo andare sulla tabella corretta!)
    'SQ 3-1-2007 - Prova:
    'If Key(i).IdCmp > 0 Then
      'Set r = m_fun.Open_Recordset("Select * From DMDB2_Colonne Where IdCmp = " & Key(i).IdCmp & " and IdTable = " & idTb & " and TAG='AUTOMATIC'")
      'Set r = m_fun.Open_Recordset("Select * From DMDB2_Colonne Where IdCmp = " & Key(i).IdCmp & " and TAG='AUTOMATIC'")
    'Else
    'VIRGILIO 22/2/2007
    If ckField Then
      Set r = m_fun.Open_Recordset("Select a.idColonna,a.nome From " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b Where " & _
                                   "a.IdCmp = " & Key(i).IdCmp & " and " & _
                                   "a.TAG = 'AUTOMATIC' and " & _
                                   "a.idTableJoin = b.idTable and " & _
                                   "b.idOrigine = " & Key(i).IdSegmento & " and a.idTable = " & idTb)
    Else
      '''''''''''''''''
      'SQ 6-02-08
      ' COME FACEVA A FUNZIONARE?! (ancora il discorso idCmp...)
      ' -> aggiunto a.idTable=a.idTableJoin
      '''''''''''''''''
'''silvia 18-03-2008 : per la dispatcher ...
''''      Set r = m_fun.Open_Recordset("Select a.idColonna,a.nome From " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b Where " & _
''''                                   "a.IdCmp = " & Key(i).IdCmp & " and " & _
''''                                   "a.TAG = 'AUTOMATIC' and " & _
''''                                   "a.idTable = b.idTable and a.idTable=a.idTableJoin AND " & _
''''                                   "b.idOrigine = " & Key(i).IdSegmento)
    
    
      Set r = m_fun.Open_Recordset("Select a.idColonna,a.nome From " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b Where " & _
                                   "a.IdCmp = " & Key(i).IdCmp & " and " & _
                                   "a.TAG <> 'AUTOMATIC-OCCURS' and " & _
                                   "a.idTable = b.idTable and a.idTable=a.idTableJoin AND " & _
                                   "b.idOrigine = " & Key(i).IdSegmento)
    
    
    
    End If
    'End If
    If r.RecordCount Then
      'Deve essere un campo solo
      ReDim Preserve idxcol(UBound(idxcol) + 1)
      idxcol(UBound(idxcol)).IdColonna = r!IdColonna
      idxcol(UBound(idxcol)).IdIndex = idx
      idxcol(UBound(idxcol)).IdTable = idTb
      'tmp
      'If Key(i).IdCmp = -1 Then
      '  idxcol(UBound(idxcol)).Nome = Key(i).NomeKey
      'Else
        idxcol(UBound(idxcol)).Nome = r!Nome
        r.MoveNext
      'End If
      ''''idLastTable = r!IdTable
    End If
    r.Close
  Next i
End Sub

' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Carica_IdxCol_DB2_XD(idx As Long, idTb As Long, lunghezza As Long, Posizione As Long, Optional ckField = False)
Public Sub Carica_IdxCol_XD(idx As Long, idTb As Long, lunghezza As Long, Posizione As Long, Optional ckField = False)
  Dim rCiclo As Recordset
  Dim rIdxCol As Recordset
  Dim strQRY As String
  Dim bOk As Boolean
  Dim LenPos As Integer
  
  strQRY = "Select * from " & PrefDb & "_Colonne where " & _
           "IdTable <> IdTablejoin and IdTable = " & idTb & " order by ordinale "
  
  Set rCiclo = m_fun.Open_Recordset(strQRY)
    
  LenPos = 0
  While Not rCiclo.EOF
    If rCiclo!Posizione = Posizione Or bOk Then
      Select Case rCiclo!Tipo
        Case "DECIMAL", "FLOAT", "INTEGER"
          LenPos = LenPos + ((rCiclo!lunghezza / 2) + 1) - 1
        Case "SMALLINT"
          LenPos = LenPos + (rCiclo!lunghezza / 2) - 1
        Case Else
          LenPos = LenPos + (rCiclo!lunghezza) - 1
      End Select
      bOk = True
      If LenPos <= lunghezza Then
        'inserisce l'indice
        ReDim Preserve idxcol(UBound(idxcol) + 1)
        idxcol(UBound(idxcol)).IdColonna = rCiclo!IdColonna
        idxcol(UBound(idxcol)).IdIndex = idx
        idxcol(UBound(idxcol)).IdTable = rCiclo!idTableJoin
        idxcol(UBound(idxcol)).Nome = rCiclo!Nome
      End If
    End If
    rCiclo.MoveNext
  Wend
  rCiclo.Close
End Sub

Public Function Get_IDTB_Da_Seg(idSeg As Long, TipoDB As String) As Long
  Dim r As Recordset
  
  Select Case TipoDB
    Case "DB2"
      Set r = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdOrigine = " & idSeg)
      If r.RecordCount Then
        Get_IDTB_Da_Seg = r!IdTable
      Else
        Get_IDTB_Da_Seg = 0
      End If
      r.Close
    Case "ORACLE"
      Set r = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where IdOrigine = " & idSeg)
      If r.RecordCount Then
        Get_IDTB_Da_Seg = r!IdTable
      Else
        Get_IDTB_Da_Seg = 0
      End If
      r.Close
  End Select
End Function

Public Function Get_IDTBS_Da_Seg(idSeg As Long) As Long
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where IdOrigine = " & idSeg)
  If r.RecordCount Then
    Get_IDTBS_Da_Seg = r!IdTableSpc
  Else
    Get_IDTBS_Da_Seg = 0
  End If
  r.Close
End Function

Public Function Get_IDTBS_Da_Tabella(idTab As Long) As Long
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where IdTable = " & idTab)
  If r.RecordCount Then
    Get_IDTBS_Da_Tabella = r!IdTableSpc
  Else
    Get_IDTBS_Da_Tabella = 0
  End If
  r.Close
End Function

Public Sub Carica_Colonne_Da_Field(idSeg As Long, lunghezza As Long, pos As Long, IdField As Long)
  Dim r As Recordset
  Dim i As Long, j As Long
  Dim Somma As Long
  Dim StPos As Long
  Dim LenPos As Long
  Dim PKExist As Boolean
  
  Dim KApp() As GbField
  ReDim KApp(0)
  
  'Se ho solo un IdField come faccio ad avere un array di KApp?
  'Non faccio prima a passargli direttamente il Field(i)?
  For i = 1 To UBound(Field)
    If Field(i).IdField = IdField Then
      ReDim Preserve KApp(UBound(KApp) + 1)
      KApp(UBound(KApp)) = Field(i)
      Exit For
    End If
  Next i
  
  'Seleziono tutti campi di almeno un byte
  'stefano: deve escludere le redefines
  'Set r = m_fun.Open_Recordset("Select * From MgData_Cmp Where IdSegmento = " & idSeg & " And Lunghezza > 0  Order By Ordinale")
  Set r = m_fun.Open_Recordset("Select * From MgData_Cmp Where IdSegmento = " & idSeg & " Order By Ordinale")
  Dim FineCmp As Long
  Dim flgRed As Boolean, liv As Integer
  
  While Not r.EOF
    'silvia 18-03-2008
    If isDispatcher Then
      liv = r!livello
      StPos = r!Posizione
      LenPos = r!Byte
      'Cerco i campi COBOL che mappano il (i) FIELD:
      For i = 1 To UBound(KApp)
        FineCmp = LenPos + StPos - 1

'        If KApp(i).Posizione + KApp(i).Lunghezza - 1 <= LenPos Then
        If FineCmp <= KApp(i).lunghezza + KApp(i).Posizione - 1 And StPos >= KApp(i).Posizione Then
          'Il field compone una chiave primaria
          'Controlla se gi� esiste
          For j = 1 To UBound(Key)
            If Key(j).IdSegmento = idSeg And Key(j).IdCmp = r!IdCmp Then
              PKExist = True
              Exit For
            End If
          Next j
          If Not PKExist Then
            ReDim Preserve Key(UBound(Key) + 1)
            Key(UBound(Key)).IdSegmento = idSeg
            Key(UBound(Key)).NomeKey = r!Nome
            Key(UBound(Key)).IdCmp = r!IdCmp
            r.Close
            Exit Sub
          Else
            PKExist = False
          End If
          Exit For
        End If
      Next i
    Else
      If r!lunghezza = 0 And r!livello > 1 Then
        If Len(Trim(r!NomeRed)) Then
          flgRed = True
        Else
          flgRed = False
        End If
        liv = r!livello
      Else
        If r!livello <= liv And Len(Trim(r!NomeRed)) = 0 Then
          flgRed = False
        End If
        If Not flgRed Then
          StPos = r!Posizione
          LenPos = r!Byte
      'Cerco i campi COBOL che mappano il (i) FIELD:
          For i = 1 To UBound(KApp)
            FineCmp = LenPos + StPos - 1
           
  '          If KApp(i).Posizione + KApp(i).Lunghezza - 1 <= LenPos Then
            If FineCmp <= KApp(i).lunghezza + KApp(i).Posizione - 1 And StPos >= KApp(i).Posizione Then
              'Il field compone una chiave primaria
              'Controlla se gi� esiste
              For j = 1 To UBound(Key)
                If Key(j).IdSegmento = idSeg And Key(j).IdCmp = r!IdCmp Then
                  PKExist = True
                  Exit For
                End If
              Next j
              If Not PKExist Then
                ReDim Preserve Key(UBound(Key) + 1)
                Key(UBound(Key)).IdSegmento = idSeg
                Key(UBound(Key)).NomeKey = r!Nome
                Key(UBound(Key)).IdCmp = r!IdCmp
              Else
                PKExist = False
              End If
              Exit For
            End If
          Next i
        End If
      End If
    End If
    r.MoveNext
  Wend
  r.Close
End Sub

Public Sub Carica_Field(Segmento As GbSeg)
  Dim r As Recordset, rg As Recordset
  
  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Load DLI Field..."
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
    
  ReDim Field(0)
  
  If Segmento.Prov = "PS" Then
''silvia 8-4-2008
''    Set r = m_fun.Open_Recordset("Select * From PSDli_Field Where " & _
''         "IdSegmento = " & Segmento.IdSegmento & " And Lunghezza <> 0 Order By Primario")
    Set r = m_fun.Open_Recordset("Select * From PSDli_Field Where " & _
         "IdSegmento = " & Segmento.IdSegmento & " And Lunghezza <> 0 Order By Primario, IdField")
    While Not r.EOF
      'Per ogni Field in Ps, controllo se ce l'ho in Mg e controllo Correzione:
      ' - "M": prendo il record da Mg;   (cosa cambia? il primary?)
      ' - "C": ignoro il Field!
      ' - else: prendo il record da Ps.
      ReDim Preserve Field(UBound(Field) + 1)
      Set rg = m_fun.Open_Recordset("Select * From MgDli_Field Where " & _
            "IdSegmento = " & Segmento.IdSegmento & " And Lunghezza <> 0 and idfield = " & r!IdField)
      If rg.RecordCount Then
        Select Case Trim(UCase(rg!Correzione))
          Case "M" 'Prende il field MG
            Field(UBound(Field)).IdField = rg!IdField
            Field(UBound(Field)).IdSegmento = rg!IdSegmento
            Field(UBound(Field)).lunghezza = rg!lunghezza
            Field(UBound(Field)).Nome = rg!Nome
            Field(UBound(Field)).Posizione = rg!Posizione
            Field(UBound(Field)).Primario = rg!Seq
            Field(UBound(Field)).Unique = rg!Unique
            Field(UBound(Field)).Prov = "MG-M"  'ex MG
          Case "C" 'Non prende niente
            'IGNORO IL CAMPO!... mi serve per gli XD... devo portarlo dietro...
            'SQ: modifica temporanea!
            Field(UBound(Field)).IdField = rg!IdField
            Field(UBound(Field)).IdSegmento = rg!IdSegmento
            Field(UBound(Field)).lunghezza = rg!lunghezza
            Field(UBound(Field)).Nome = rg!Nome
            Field(UBound(Field)).Posizione = rg!Posizione
            Field(UBound(Field)).Primario = rg!Seq
            Field(UBound(Field)).Unique = rg!Unique
            Field(UBound(Field)).Prov = "MG-C"  'FONDAMENTALE, E' IL DISCRIMINANTE!
          Case Else
            'Prende il field PS
            Field(UBound(Field)).IdField = r!IdField
            Field(UBound(Field)).IdSegmento = r!IdSegmento
            Field(UBound(Field)).lunghezza = r!lunghezza
            Field(UBound(Field)).Nome = r!Nome
            Field(UBound(Field)).Posizione = r!Posizione
            Field(UBound(Field)).Primario = r!Primario
            Field(UBound(Field)).Unique = r!Unique
            Field(UBound(Field)).Prov = "MG-X"  'ex PS
        End Select
      Else
        'Prende il field PS (i FIELD servono tutti... per gli XDFIELD...)
        Field(UBound(Field)).IdField = r!IdField
        Field(UBound(Field)).IdSegmento = r!IdSegmento
        Field(UBound(Field)).lunghezza = r!lunghezza
        Field(UBound(Field)).Nome = r!Nome
        Field(UBound(Field)).Posizione = r!Posizione
        
        'MG
''        If r!Primario = False And r!Seq = False Then
''          Field(UBound(Field)).Primario = False
''        ElseIf r!Primario = False And r!Seq Then
''          Field(UBound(Field)).Primario = True
''        ElseIf r!Primario Then
''          Field(UBound(Field)).Primario = True
''        End If
        ' Mauro 07/02/2008 : Squibbo Dixit
        ' Da ricontrollare anche il parser per la valorizzazione di Primario
        Field(UBound(Field)).Primario = r!Seq
        
        '''Field(UBound(Field)).Primario = r!Primario
        
        Field(UBound(Field)).Unique = r!Unique
        Field(UBound(Field)).Prov = "PS"
      End If
      rg.Close
      r.MoveNext
    Wend
    r.Close
  Else
    Set r = m_fun.Open_Recordset("Select * From MGDli_Field Where " & _
         "IdSegmento = " & Segmento.IdSegmento & " And Lunghezza <> 0 Order By Seq")
    While Not r.EOF
      ReDim Preserve Field(UBound(Field) + 1)
      Field(UBound(Field)).IdField = r!IdField
      Field(UBound(Field)).IdSegmento = r!IdSegmento
      Field(UBound(Field)).lunghezza = r!lunghezza
      Field(UBound(Field)).Nome = r!Nome
      Field(UBound(Field)).Posizione = r!Posizione
      Field(UBound(Field)).Primario = r!Seq
      Field(UBound(Field)).Unique = r!Unique
      Field(UBound(Field)).Prov = "MG-I"
      r.MoveNext
    Wend
    r.Close
  End If
End Sub
''''''''
'SQ:
''''''''
Public Sub Carica_XDField(Segmento As GbSeg)
  Dim r As Recordset
  Dim i As Integer
  
  ReDim XDFields(0)
  If Segmento.Prov = "PS" Then
    Set r = m_fun.Open_Recordset("Select * From PSDli_XDField Where IdSegmento = " & Segmento.IdSegmento)
    If r.RecordCount Then 'tutta 'sta pantomima perche VB NON USA LA LUNGHEZZA DELL'ARRAY MA L'UBOUND!
      ReDim XDFields(r.RecordCount - 1)
      For i = 0 To r.RecordCount - 1
        'ATTENZIONE: GESTIRE LE Mg?
        'Set rg = m_fun.Open_Recordset("Select * From MgDli_XDField Where IdSegmento = " & IdSeg & " And Lunghezza <> 0 and idfield = " & r!IdField)
        XDFields(i).IdField = r!IdxdField
        XDFields(i).Nome = r!Nome
        XDFields(i).srchFields = r!srchFields
        XDFields(i).extRtn = r!extRtn
        r.MoveNext
      Next i
    End If
    r.Close
  ElseIf Segmento.Prov = "MG" Then
    Set r = m_fun.Open_Recordset("Select * From MGDli_XDField Where IdSegmento = " & Segmento.IdSegmento)
    If r.RecordCount Then 'tutta 'sta pantomima perche VB NON USA LA LUNGHEZZA DELL'ARRAY MA L'UBOUND!
      ReDim XDFields(r.RecordCount - 1)
      For i = 0 To r.RecordCount - 1
        'ATTENZIONE: GESTIRE LE Mg?
        'Set rg = m_fun.Open_Recordset("Select * From MgDli_XDField Where IdSegmento = " & IdSeg & " And Lunghezza <> 0 and idfield = " & r!IdField)
        XDFields(i).IdField = r!IdxdField
        XDFields(i).Nome = r!Nome
        XDFields(i).srchFields = r!srchFields
        XDFields(i).extRtn = r!extRtn
        r.MoveNext
      Next i
    End If
    r.Close
  Else
    ReDim XDFields(0)
  End If
End Sub

Public Sub Carica_Segmenti(IdOggetto As Long)
  Dim r As Recordset, rg As Recordset
  
  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Load DLI Segment..."
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  ReDim Segmenti(0)
  Set r = m_fun.Open_Recordset("Select * From PSDLI_Segmenti Where IdOggetto = " & IdOggetto & " order by idsegmento")
  While Not r.EOF
    'Controlla i segmenti Mg
    'controllare: arrivano tutti con IdSegmentoOrigine = 0... serve? (il nome?)
    Set rg = m_fun.Open_Recordset("Select * From MgDLI_Segmenti Where IdOggetto = " & IdOggetto & " AND Nome = '" & r!Nome & "' order by idsegmento")
    If rg.RecordCount Then
      Select Case rg!Correzione
        Case "M" 'Prende il segmento MG
          ReDim Preserve Segmenti(UBound(Segmenti) + 1)
          Segmenti(UBound(Segmenti)).IdSegmento = rg!IdSegmento
          Segmenti(UBound(Segmenti)).IdSegmentoOrigine = rg!IdSegmentoOrigine
          Segmenti(UBound(Segmenti)).Nome = rg!Nome
          Segmenti(UBound(Segmenti)).Parent = rg!Parent
          Segmenti(UBound(Segmenti)).Prov = "MG"
        Case "C", "O" 'SQ: 30-05-05; sono quelli NON USATI!!!!!!!!!!!!!!!!!!!!!!!!!!
          'VANNO IGNORATI...
          ' "non usato!" & rg!Nome
          m_fun.WriteLog "Segmento non usato!" & rg!Nome
        Case Else
          'prende il segmento PS
          ReDim Preserve Segmenti(UBound(Segmenti) + 1)
          Segmenti(UBound(Segmenti)).IdSegmento = r!IdSegmento
          Segmenti(UBound(Segmenti)).IdSegmentoOrigine = r!IdSegmentoOrigine
          Segmenti(UBound(Segmenti)).Nome = r!Nome
          Segmenti(UBound(Segmenti)).Parent = r!Parent
          Segmenti(UBound(Segmenti)).Prov = "PS"
      End Select
    Else
      'Prende il Segmento PS
      ReDim Preserve Segmenti(UBound(Segmenti) + 1)
      Segmenti(UBound(Segmenti)).IdSegmento = r!IdSegmento
      Segmenti(UBound(Segmenti)).IdSegmentoOrigine = r!IdSegmentoOrigine
      Segmenti(UBound(Segmenti)).Nome = r!Nome
      Segmenti(UBound(Segmenti)).Parent = r!Parent
      Segmenti(UBound(Segmenti)).Prov = "PS"
    End If
    rg.Close
    '''''''''''''''''''''''''''''''''''''''''
    ' Segmenti virtuali
    '''''''''''''''''''''''''''''''''''''''''
    Set rg = m_fun.Open_Recordset("Select * From MgDLI_Segmenti Where idSegmentoOrigine = " & r!IdSegmento & " AND Correzione = 'I'")
    While Not rg.EOF
      ReDim Preserve Segmenti(UBound(Segmenti) + 1)
      Segmenti(UBound(Segmenti)).IdSegmento = rg!IdSegmento
      Segmenti(UBound(Segmenti)).IdSegmentoOrigine = rg!IdSegmentoOrigine
      Segmenti(UBound(Segmenti)).Nome = rg!Nome
      Segmenti(UBound(Segmenti)).Parent = rg!Parent
      Segmenti(UBound(Segmenti)).Prov = "MG-I"
      rg.MoveNext
    Wend
    rg.Close
    r.MoveNext
  Wend
  r.Close
  
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' AGGIUNGE INDICE PRIMARIO    (FIELD UNIQUE)
' AGGIUNGE INDICI ALTERNATIVI (XDFIELD)
' AGGIUNGE INDICI ALTERNATIVI (PER I FIELD aggiunti a mano da DM)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Index_DB2(idTb As Long, IdTbs As Long) 'a cosa serve idTBS????????????????
Public Sub Add_Index(idTb As Long, IdTbs As Long) 'a cosa serve idTBS????????????????
  Dim r As Recordset, rs As Recordset
  Dim Adesso As String, indexName As String
  Dim idIdx As Long, idTabellaPK As Long
  Dim Count As Integer, i As Integer, j As Integer, k As Integer, z As Integer, idPrimario As Long
  Dim env As Collection
  'SILVIA
  Dim count1 As Integer
  
  Adesso = Now
  ReDim idxcol(0)
  Count = 0
  'ReDim parentSeqKeys(0)

  'Nuovo ID per tabella DM*_Index:
  idIdx = getId("IdIndex", PrefDb & "_Index")
  If idIdx < 0 Then
    m_fun.WriteLog "Problemi sulla tabella " & PrefDb & "_Index... " & ERR.Description
  End If
  
  'Foreign Key Name:
  Set env = New Collection
  env.Add idTb
  'SILVIA
  env.Add count1 + 1
  ForeignKeyName = getEnvironmentParam(FkNameParam, "tabella", env, TipoDB)
  If Len(ForeignKeyName) = 0 Then ForeignKeyName = "F_KEY"
          
  '''''''''''''''''''''''''''''''''''
  'INDICE PRIMARIO + ALTERNATIVI
  '''''''''''''''''''''''''''''''''''
  'For i = 1 To UBound(Field) virgilio mammamia comme sto
  Dim wPrimarioBol As Boolean
  
  wPrimarioBol = False
  
  For i = 1 To UBound(Field)
    If Field(i).Primario Then
      wPrimarioBol = True
      Exit For
    End If
  Next
  
  Dim indStart As Integer
  Dim indEnd As Integer
  Dim indStep As Long
  
  'SQ chi ha fatto ste modifiche? sono sbagliate...
  If wPrimarioBol Then
    'SQ
    'indStart = 0
    indStart = 1
    indEnd = UBound(Field)
    indStep = 1
  Else
    indStart = UBound(Field)
    ' Mauro
    'indEnd = 0
    indEnd = 1
    indStep = -1
  End If
  
  'silvia 18-03-2008
  If isDispatcher Then
   ' indEnd = 1
  End If
  
  'silvia
  count1 = 0
  If Len(XDFields(0).Nome) Then 'tocca fare cosi'...
    count1 = UBound(XDFields) + 1
  End If
  For i = indStart To indEnd Step indStep
    ReDim Key(0)
    If Left(Field(i).Nome, 1) <> "/" Then   'campi "fasulli"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Riempio Key() e IdxCol() con i campi cobol che mappano i FIELD
      'NON SERVONO DUE FUNZIONI E DUE STRUTTURE...l'unica info in piu' e' la colonna...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Field(i).Primario Then
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' GESTIONE CHIAVI MULTIPLE
        ' - Arriva il Field "Primario" + "Multiplo"
        ' - L'ultimo Field sar� "BPER-PK" (come per i casi senza chiave)
        ' => L'indice che devo generare � uguale al caso di primario+unique
        '    con in pi� il campo "contatore" ("BPER-PK")
        '    => lo creo SOLO nel giro finale del ("BPER-PK")
        ' SQ 8-06-06
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Key = parentSeqKeys
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' CHIAVE PRIMARIA - eredito:
        ' - le colonne appartenenti alla chiave primaria del padre.
        ' - la chiave primaria dal padre (si aggiunge "davanti")
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Aggiornamento KEY
        Carica_Colonne_Da_Field Field(i).IdSegmento, Field(i).lunghezza, Field(i).Posizione, Field(i).IdField
        
        'key -> idxcol
        Carica_IdxCol idIdx, idTb 'cicla Key e riempie idxCol
        
        If UBound(parentSeqKeys) Then
          inheritParentKey idIdx, idTb   'Aggiornamento DM*_Colonne con quelle ereditate
        End If
        'SQ: memorizzo la struttura Key; serve per le PK dei figli...
        Segmenti(segIndex).SeqKeys = Key
      Else
'''        ReDim parentSeqKeys(0)
        ReDim Key(0)
        ReDim idxcol(0)
      
        'SQ 4-07-06
        If Field(i).Prov <> "BPER-PK" And Field(i).Prov <> "BPER-MPK" Then
        
          Carica_Colonne_Da_Field Field(i).IdSegmento, Field(i).lunghezza, Field(i).Posizione, Field(i).IdField
          Carica_IdxCol idIdx, idTb 'cicla Key e riempie idxCol
          'VC: SQ: METTERE IN LINEA... gestione dei campi XDFLD con SLASH non accoppiati ad un field
'        If Left(Field(i).Nome, 1) = "/" Then
'           Accoppia_Xdfld_Da_Field idTb, Field(i).IdSegmento, Field(i).Lunghezza, Field(i).Posizione, Field(i).IdField, tableName
'           Carica_IdxCol_DB2 idIdx, idTb 'cicla Key e riempie idxCol
'        Else
'           Carica_Colonne_Da_Field Field(i).IdSegmento, Field(i).Lunghezza, Field(i).Posizione, Field(i).IdField
'           Carica_IdxCol_DB2 idIdx, idTb 'cicla Key e riempie idxCol
'        End If
        End If
      End If
      
      '''''''''''''''''''''''''''''
      ' keyField "EXTRA" per BPER!
      '''''''''''''''''''''''''''''
      If Field(i).Prov = "BPER-PK" Then
        Key = parentSeqKeys
        Carica_IdxCol idIdx, idTb 'cicla Key e riempie idxCol
        If UBound(parentSeqKeys) Then
          inheritParentKey idIdx, idTb
        End If
        '!
        ReDim Preserve Key(UBound(Key) + 1)
        Key(UBound(Key)).IdSegmento = Field(i).IdSegmento 'Key(UBound(Key) - 1).IdSegmento
        Key(UBound(Key)).NomeKey = Field(i).Nome
        Key(UBound(Key)).IdCmp = -1
        'SQ: memorizzo la struttura Key; serve per le PK dei figli...
        Segmenti(segIndex).SeqKeys = Key
        Field(i).Primario = True
        ReDim Preserve idxcol(UBound(idxcol) + 1)
        idxcol(UBound(idxcol)).IdColonna = Field(i).IdField  'scorrettezza...
        idxcol(UBound(idxcol)).Nome = Field(i).Nome
        idxcol(UBound(idxcol)).IdIndex = idxcol(UBound(idxcol) - 1).IdIndex 'sono tutti uguali!
        idxcol(UBound(idxcol)).IdTable = idxcol(UBound(idxcol) - 1).IdTable 'sono tutti uguali!
      ElseIf Field(i).Prov = "BPER-MPK" Then  'Field Seq Multiplo
        Key = Segmenti(segIndex).SeqKeys 'parentSeqKeys
        ReDim idxcol(0)
        Carica_IdxCol idIdx, idTb   'cicla Key e riempie idxCol
        'ATTENZIONE: ho il contatore prima delle colonne chiave!
        '=> Ho #parentSeqKey COLONNE, CONTATORE, #idxCol-#parentSeqKey
        If UBound(parentSeqKeys) Then
        
          'SQ: non mi ricordo cosa fa!? Probabil. sposta sotto la REVKEY!
          Set r = m_fun.Open_Recordset("Select idColonna,ordinale From " & PrefDb & "_Colonne Where " & _
                                       "IdTable = " & idTb & " AND Ordinale > " & headerColumns + UBound(parentSeqKeys) + 1 & _
                                       " order by ordinale")
          'TILVIA 7-4-2009
          If r.RecordCount Then
            For j = 1 To UBound(idxcol) - UBound(parentSeqKeys)
              r!Ordinale = r!Ordinale - 1
              r.Update
              
              r.MoveNext
            Next
          End If
           r.Close
          'SQ 26-01-07 - VERIFICARE: sbagliava con due REVKEY! (va per nome... prende la prima?!)
          'Set r = m_fun.Open_Recordset("Select ordinale From DMDB2_Colonne Where IdTable = " & idTb & " AND nome='" & Field(i).Nome & "'")
          Set r = m_fun.Open_Recordset("Select ordinale From " & PrefDb & "_Colonne Where " & _
                                       "IdTable=" & idTb & " AND nome='" & Field(i).Nome & "' AND idTable=idTableJoin")
          r!Ordinale = r!Ordinale + UBound(idxcol) - UBound(parentSeqKeys)
          r.Update
          r.Close
          
          Set r = m_fun.Open_Recordset("Select idColonna From " & PrefDb & "_Colonne Where " & _
                                       "IdTable = " & idTb & " AND Ordinale > " & headerColumns & " order by ordinale")
          For j = 1 To UBound(parentSeqKeys)
            idxcol(j).IdColonna = r!IdColonna
            r.MoveNext
          Next
          r.Close
          'inheritParentKeyDB2 idIdx, idTb
        End If
        'SQ - testare...
        ReDim Preserve Key(UBound(Key) + 1)
        Key(UBound(Key)).IdSegmento = Field(i).IdSegmento 'Key(UBound(Key) - 1).IdSegmento
        Key(UBound(Key)).NomeKey = Field(i).Nome
        Key(UBound(Key)).IdCmp = -1
        '.
        ReDim Preserve idxcol(UBound(idxcol) + 1)
        idxcol(UBound(idxcol)).IdColonna = Field(i).IdField  'scorrettezza...
        idxcol(UBound(idxcol)).Nome = Field(i).Nome
        idxcol(UBound(idxcol)).IdIndex = idxcol(UBound(idxcol) - 1).IdIndex 'sono tutti uguali!
        idxcol(UBound(idxcol)).IdTable = idxcol(UBound(idxcol) - 1).IdTable 'sono tutti uguali!
        
        'SQ: memorizzo la struttura Key; serve per le PK dei figli...
        Segmenti(segIndex).SeqKeys = Key
        Field(i).Primario = True  'lo forzo per entrare sotto a scrivere l'indice
      End If
      'Controllo matching FIELD/COLONNE (segnalarlo prima, nel DM DLI)
      If UBound(idxcol) = 0 Then
        'MsgBox "Wrong Cobol structure: no match for field " & Field(i).Nome & " (idSegment: " & Field(i).IdSegmento & ")", vbExclamation, DataMan.DmNomeProdotto
        'm_fun.WriteLog "## Wrong Cobol structure: no match for field " & Field(i).Nome & " (idSegment: " & Field(i).IdSegmento & ")"
        'SQ 17-09-07 fare funzioncina
        MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & "### Wrong structure: no match for field " & Field(i).Nome & " (idSegment: " & Field(i).IdSegmento & ")"
        MadmdF_DLI2RDBMS.LswLog.Refresh
        MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
      End If
      'appioppo l'idxCol al Field: mi serve per gli XDFld...
      Field(i).idxColumns = idxcol
      
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Inserimento INDICE PRIMARIO/ALTERNATIVO
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
      ''''MG 24.08.2007 '''''''''''''''''''''''''''''''''''''''''''''
      '''' Modificato condizione per permettere l'inserimento -> ''''
      '''' nelle tabelle figlie degli indici secondari.          ''''
      '''' sostituito ....'''''''''''''''''''''''''''''''''''''''''''
      If Field(i).Prov = "MG-M" Or (Field(i).Primario And Field(i).Unique) Then  'Vedi Carica_Field: unico caso...
      
        'SQ 13-11-07
        Set env = New Collection  'resetto;
        env.Add Field(i)
        'SQ 13-11-07
        env.Add idTb
        'SQ 13-11-07
        'SILVIA 03-04-2008
        If Field(i).Prov = "MG-M" Then
          count1 = count1 + 1
          env.Add count1
          indexName = getEnvironmentParam(AkNameParam, "Index", env, TipoDB)
        Else
          env.Add i
          indexName = getEnvironmentParam(IndexNameParam, "Index", env, TipoDB)
        End If
        'If Len(IndexNameParam) = 0 Then
        '  indexName = Field(i).Nome
        'End If
        indexName = Replace(indexName, "-", "_")
        'SQ TMP gestione nomi VIRTUALI:
        Dim tmpVirtuale As String
        If Len(Field(i).IdSegmento & "") > 4 Then
          tmpVirtuale = CInt(Right(Field(i).IdSegmento, 2)) 'elimino lo 0 "leading"
          indexName = indexName & tmpVirtuale
        End If
        
        'INSERIMENTO in DM*_Index/DM*_IdxCol
        Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index Where " & _
                                     "Nome = '" & indexName & "' and idtable = " & idTb)
        'Creo il record, se non esiste gia'
        If r.RecordCount = 0 Then
          r.AddNew
          If (Field(i).Primario And Field(i).Unique) Then  'PROVVISORIO; SE NON HA PARTICOLARE GESTIONE, NON RIPETERE QUELLI COMUNI...
            idPrimario = Field(i).IdField
            r!Tipo = "P"
            r!Unique = True
            r!IdIndex = idIdx
            r!IdTable = idTb
            r!IdTableSpc = IdTbs
            r!IdOrigine = Field(i).IdField
            r!IdOrigineParent = Field(i).IdField
            r!Used = True
            r!Nome = indexName
          Else
            r!Tipo = "I"
            r!Unique = False
            r!IdIndex = idIdx
            r!IdTable = idTb
            r!IdTableSpc = IdTbs
            r!IdOrigine = Field(i).IdField
            r!IdOrigineParent = idPrimario
            r!Used = True 'SQ: DA GESTIRE!!!!!!
            r!Nome = indexName
            'Questo field contiene i campi della Chiave SK
          End If
          r!Descrizione = "MultiSeq Field Index..."
          r!Data_Creazione = Adesso
          r!Data_Modifica = Adesso
          r!Tag = "AUTOMATIC"
          r.Update
          
          idIdx = idIdx + 1
          
          'INDICE TABELLA DISPATCHER
          If isDispatcher Then
          'tilvia
          ''   Add_IdxCol_In_DB

          'silvia 16-4-2008
            ReDim Key(0)
            If Field(i).Prov <> "MG-M" Then
              Set rs = r.Clone
              rs.AddNew
              '3: IdTableJoin... valutare...
              For j = 1 To r.fields.Count - 1
                If Not IsNull(r.fields.Item(j)) Then
                  rs.fields.Item(j) = r.fields.Item(j)
                End If
              Next
              rs!IdIndex = idIdx
              rs!Nome = Field(i).Nome
              idTb = idTb - 1
              rs!IdTable = idTb
              rs!Tag = "AUTOMATIC-DISPATCHER"
              rs.Update
              rs.Close
            
              createDispatcherIndex idIdx, Field(i), idTb
              idIdx = idIdx + 1
              r.Close

              ReDim idxcol(0)
              Key = parentSeqKeys
              Carica_IdxCol idIdx, idTb

              ReDim Preserve Key(UBound(Key) + 1)
              Key(UBound(Key)).IdSegmento = Field(i).IdSegmento 'Key(UBound(Key) - 1).IdSegmento
              Key(UBound(Key)).NomeKey = Field(i).Nome
              Key(UBound(Key)).IdCmp = Field(i).IdField
  '
              Segmenti(segIndex).SeqKeys = Key

              Set r = m_fun.Open_Recordset("Select a.idColonna,a.nome From " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b Where " & _
                             "a.IdCmp = " & Field(i).IdField & " and " & _
                             "a.TAG = 'AUTOMATIC-DISPATCHER' and " & _
                             "a.idTable = b.idTable and a.idTable=a.idTableJoin AND " & _
                             "b.idOrigine = " & Field(i).IdSegmento)

              If r.RecordCount Then
          '     Deve essere un campo solo
                ReDim Preserve idxcol(UBound(idxcol) + 1)
                idxcol(UBound(idxcol)).IdColonna = r!IdColonna
                idxcol(UBound(idxcol)).IdIndex = idIdx
                idxcol(UBound(idxcol)).IdTable = idTb
                idxcol(UBound(idxcol)).Nome = r!Nome
              End If
              r.Close

            Else
              idIdx = idIdx - 1
              createDispatcherIndex idIdx, Field(i), idTb
              idIdx = idIdx + 1
              r.Close
            End If
            '''
          Else
            'Inserimento in DM*_IdxCol: (input: idxcol)
          '''End If
          Add_IdxCol_In_DB
            
              
          MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Process Field: " & Field(i).Nome
          MadmdF_DLI2RDBMS.LswLog.Refresh
          MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
            
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            ' FOREIGN KEY:
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            'SQ verificare: qui entro anche con Field(i).Prov = "BPER-MPK"...
            'ma ci devo passare una volta sola! e ci sono gi� passato per FIELD 1 (nei casi di chiave multipla...)
            'SILVIA : qui non devo entrare con Field(i).Prov = "MG-M"
            If UBound(parentSeqKeys) And Field(i).Prov <> "MG-M" Then
            ''If UBound(parentSeqKeys) Then
              'If UBound(parentSeqKeys) And Field(i).Prov <> "BPER-MPK" Then
              Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where " & _
                                          "idtable = " & idTb & " and tipo = 'F'")
              If rs.RecordCount = 0 Then
                rs.AddNew
                rs!IdIndex = idIdx
                rs!IdTable = idTb
                rs!IdTableSpc = IdTbs
                rs!Nome = ForeignKeyName
                rs!Tipo = "F"
                rs!Used = True
                rs!Unique = True
                rs!Descrizione = "FOREIGN KEY Index"
                rs!Tag = "AUTOMATIC"
                ''''''''
                ''''''''
                ''SILVIA 18-03-2008 verifico che ci sia anche la dispatcher padre
                
                '''Set r = m_fun.Open_Recordset("Select idTable From " & PrefDb & "_Tabelle " & _
                '''                             "where idOrigine=" & parentSeqKeys(UBound(parentSeqKeys)).IdSegmento & " And TAG='AUTOMATIC'")
                
                Set r = m_fun.Open_Recordset("Select idTable From " & PrefDb & "_Tabelle " & _
                                             "where idOrigine=" & parentSeqKeys(UBound(parentSeqKeys)).IdSegmento & " And TAG='AUTOMATIC-DISPATCHER'")
                If r.RecordCount Then
                  idTabellaPK = r!IdTable
                  r.Close
                Else
                  Set r = m_fun.Open_Recordset("Select idTable From " & PrefDb & "_Tabelle " & _
                                             "where idOrigine=" & parentSeqKeys(UBound(parentSeqKeys)).IdSegmento & " And TAG='AUTOMATIC'")
                
                  idTabellaPK = r!IdTable
                  r.Close
                End If
                ''''''''
                ''''''''
                
                rs!ForeignIdTable = idTabellaPK
                rs.Update
              End If
              rs.Close
                          
              Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol")
              '4-01-07
              'For j = 1 To UBound(parentSeqKeys)
              j = 1
              Dim r1 As Recordset
              Set r1 = m_fun.Open_Recordset("select idtablejoin, idcmp from " & PrefDb & "_Colonne" & _
                                            " where idtable = " & idTb & _
                                            " and idtable <> idtablejoin" & _
                                            " order by ordinale")
              Do While Not (r1.EOF)
                rs.AddNew
                rs!IdIndex = idIdx
                rs!Ordinale = j
                rs!IdTable = idTb
                rs!IdColonna = idxcol(j).IdColonna
                rs!Order = "ASCENDING"
                rs!Tag = "AUTOMATIC"
                Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne" & _
                                             " where idCmp = " & r1!IdCmp & _
                                             " and idtable = " & idTabellaPK & _
                                             " and idTablejoin =" & r1!idTableJoin & _
                                             " order by ordinale")
                If r.RecordCount = 1 Then
                  rs!forIdColonna = r!IdColonna
                ElseIf r.RecordCount > 1 Then
                  rs!forIdColonna = r!IdColonna
                  m_fun.WriteLog "## Warning on FOREIGN KEY: " & r!Nome & "-" & idTb & ". More records found for IdCmp " & parentSeqKeys(j).IdCmp
                Else
                  'TMP!
                  rs!forIdColonna = 0
                  'm_fun.WriteLog "## Warning on FOREIGN KEY: " & r!Nome & "-" & idTb & ". No record found for IdCmp " & parentSeqKeys(j).IdCmp
                  'm_fun.WriteLog "## Warning on FOREIGN KEY on table: " & idTb & ". No record found for IdCmp " & r1!IdCmp
                  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & "## Warning on FOREIGN KEY on table: " & idTb & ". No record found for IdCmp " & r1!IdCmp
                  MadmdF_DLI2RDBMS.LswLog.Refresh
                  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible

                End If
                r.Close
                r1.MoveNext
                rs.Update
                j = j + 1
              Loop
              r1.Close
              rs.Close
              idIdx = idIdx + 1
            End If
          End If
        Else
          ' Mauro 22/01/2008 : Se la Naming Convention non � corretta, gli Indici vengono uguali e non li aggiunge
          ' Segnalo se l'indice � gi� presente in tabella
          MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - ## ERROR on INDEX on table: " & idTb & ". Index already exist " & indexName
          MadmdF_DLI2RDBMS.LswLog.Refresh
          MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
          m_fun.WriteLog "## ERROR on INDEX on table: " & idTb & ". Index already exist " & indexName
        End If
      End If
    Else
      '''' VIRGILIO 22/2/2007
      ReDim idxcol(0)
    
      Carica_IdxCol_XD idIdx, idTb, Field(i).lunghezza, Field(i).Posizione, True  'cicla Key e riempie idxCol
      Field(i).idxColumns = idxcol
  
    End If
    DoEvents
  Next i
  
  'Case "banca.mty"
      ' Aggiunta FK - colonna ID_<ParenttableName> -> ID_<tableName> padre
  '    Set r = m_fun.Open_Recordset("Select Nome,idColonna From DMDB2_Colonne Where IdTable=" & idTb & " AND Nome like 'ID_%' AND descrizione='Header Column Extra'")
          
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ' INDICI ALTERNATIVI XDFIELD
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(XDFields(0).Nome) Then 'tocca fare cosi'...
    For i = 0 To UBound(XDFields)
      Count = 1
      ''''''''''''''''''''''''
      'SQ 13-11-07
      'nuovo parametro: AkNameParam
      ''''''''''''''''''''''''
      Set env = New Collection  'resetto;
      env.Add XDFields(i)
      'SQ 13-11-07
      env.Add idTb
      env.Add i + 1
      'SQ 13-11-07
      indexName = getEnvironmentParam(AkNameParam, "Index", env, TipoDB)
      'SQ default gestiti al caricamento...
      'If Len(indexName) = 0 Then
      '  indexName = XDFields(i).Nome
      'End If
      indexName = Replace(indexName, "-", "_")
      
      Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index Where Nome = '" & indexName & "' and idtable =" & idTb)
      'Creo il record, se non esiste gia'... altrimenti?
      'SQ
      'If r.RecordCount = 0 Then
        r.AddNew
        r!Tipo = "I"  'SQ: attenzione: esiste gia' un tipo per questi?
        r!Unique = False
        r!IdIndex = idIdx
        r!IdTable = idTb
        r!IdTableSpc = IdTbs
        r!IdOrigine = XDFields(i).IdField * -1
        r!IdOrigineParent = idPrimario
        r!Used = True 'SQ: DA GESTIRE!!!!!!
        r!Nome = indexName
        'Questo field contiene i campi della Chiave SK
        r!Descrizione = "DLI To " & TipoDB & " Indexes Automatic Data Migration..."
        r!Data_Creazione = Adesso
        r!Data_Modifica = Adesso
        r!Tag = "AUTOMATIC-XD"  'SQ: attenzione; modo provvisorio per riconoscerli - UTILIZZARE Tipo!
        r.Update
        'SQ
        r.Close
        
        Dim srchFields() As String
        Dim splitFields As Integer 'virgilio
        splitFields = InStr(XDFields(i).srchFields, ",") 'virgilio
        If splitFields = 0 Then 'virgilio
          ReDim srchFields(1) 'virgilio
          srchFields(1) = XDFields(i).srchFields 'virgilio
        Else
          srchFields = Split(XDFields(i).srchFields, ",")
        End If
        'Per ogni FIELD appartenente a XDFLD:
        For j = 0 To UBound(srchFields)
          'cerco l'indice del Field corrispondente:
          ''If Left(srchFields(j), 1) <> "/" Then   'campi "fasulli"
            For k = 1 To UBound(Field())
              'controllare che non siano minuscole...
              'eliminare i bianchi quando inserisco in tabella
              If Field(k).Nome = srchFields(j) Then
                'trovato il Field giusto:
                idxcol = Field(k).idxColumns
                'Scrive le info di idxcol per riempire la "DM*_IdxCol"
                'Add_IdxCol_In_DB_DB2
                'SQ - prendeva anche le ereditate!
                'For z = 1 To UBound(idxcol)
                For z = IIf(Field(k).Primario, UBound(parentSeqKeys) + 1, 1) To UBound(idxcol)
                  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where IdIndex = " & idIdx & _
                             " And IdColonna = " & idxcol(z).IdColonna & " And IdTable = " & idTb)
                  If r.RecordCount = 0 Then
                    r.AddNew
          
                    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Index Column: " & Get_NomeColonna(idxcol(z).IdColonna)  '?
                    MadmdF_DLI2RDBMS.LswLog.Refresh
                    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
                  
                    r!IdIndex = idIdx
                    r!IdTable = idTb
                    r!IdColonna = idxcol(z).IdColonna
                    
                      ''''''''''''''''''''''''''''''''''''
                      'TEMPORANEO
                      ''''''''''''''''''''''''''''''''''''
                      Select Case m_fun.FnNomeDB
                      Case "banca.mty"
                        updateNullable_BPH idxcol(z).IdColonna, XDFields(i).extRtn
                      End Select
                      
                    r!Order = "ASCENDING"
                    r!Tag = "AUTOMATIC-XD" 'SQ
                    r!Ordinale = Count
                    r.Update
                    Count = Count + 1
                  End If
                  'SQ
                  r.Close
                Next z
                'trovato
                Exit For
              End If
            Next k
            'SQ 12-03-08
            If k > UBound(Field()) Then
              MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & "## Index column non found for  search field : " & srchFields(j) & " - XDField: " & XDFields(i).Nome & " - idSegment: " & Field(UBound(Field())).IdSegmento & ""
              MadmdF_DLI2RDBMS.LswLog.Refresh
              MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
            End If
            
''''          Else
''''            ''''''''''''''''''m_fun.WriteLog "## Delete field " & srchFields(j) & " from XDField " & XDFields(i).Nome
''''          End If
        Next j
      'End If
      
      MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Process XDField: " & XDFields(i).Nome
      MadmdF_DLI2RDBMS.LswLog.Refresh
      MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
      idIdx = idIdx + 1
      DoEvents
    Next i
  End If
  '''''''''''''''''''''''
  'If isDispatcher Then
  '  isDispatcher = False
  '  Add_Index_DB2 idTb, IdTbs
  'End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' TMP: SOLO PER BPH!
' Le colonne corrispondenti ad XDFLD con extRtn definita devono essere "NULLABILI"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub updateNullable_BPH(idColumn As Long, extRtn As String)
  Dim rs As Recordset
  
  'default ancora non chiaro, quindi a scanso di equivoci facciamo comunque l'update!
  If Len(extRtn) = 0 Then
    Set rs = m_fun.Open_Recordset("Select * from DMDB2_Colonne where idColonna=" & idColumn)
    If rs.RecordCount Then
      rs!Null = False
      rs.Update
    Else
      MsgBox "TMP: column " & idColumn & " does not exist?"
    End If
    rs.Close
  End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Update su DM*_Colonne:
'  Inserimento colonne chiave del parent "davanti" alle attuali
'  Update "ordinale" colonne chiave
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Sub inheritParentKeyDB2(Index As Long, IdTable As Long)
Sub inheritParentKey(Index As Long, IdTable As Long)
  Dim r As Recordset, r1 As Recordset
  Dim i As Integer, j As Integer, Ordinale As Integer
  Dim IdColonna As Long
  Dim bFirst As Boolean
  Dim pos As Integer
  Dim tipoOld As String
  Dim lunghezzaOld As Integer
  
  'UPDATE ORDINALE COLONNE GIA' INSERITE: (la exec non e' d'uso...)
  'BPER:
  'Set r = m_fun.Open_Recordset("Select ordinale From DMDB2_Colonne Where IdTable = " & idTable & " AND Ordinale > " & headerColumns)
  Dim extraHeaderColumns As Integer
  
  'extraHeaderColumns (Lasciarla: potrebbe servire come parametro... la banca aveva 1...)
  
  Set r = m_fun.Open_Recordset("Select NOME,ordinale From " & PrefDb & "_Colonne Where " & _
                               "IdTable = " & IdTable & " AND Ordinale > " & headerColumns + extraHeaderColumns & _
                               " order by ordinale")
  While Not r.EOF
    r!Ordinale = r!Ordinale + UBound(parentSeqKeys)
    r.MoveNext
  Wend
  r.Close
  
  'Nuovo IdColonna:
  IdColonna = getId("IdColonna", PrefDb & "_Colonne")   '-1 in caso d'errore...
  
  For j = 1 To UBound(parentSeqKeys)
    Ordinale = Ordinale + 1
    
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & idxcol(j).Nome
    MadmdF_DLI2RDBMS.LswLog.Refresh
    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
    
    Set r1 = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdColonna = " & idxcol(j).IdColonna)
    Set r = r1.Clone   'attenzione!!!!!!!!! il nome non lo ricalcolo... non va molto bene! (vedere Add...Colonne)
                       'NomeNorm = Normalizza_Nome(idx, "RDBMSCL")
                       'Trascodifica_Tipo_DLI_DB2 r!Tipo, r!Usage, r!Lunghezza, r!Decimali, NomeNorm
                       '...
    'clone non fa la copia... la faccio a mano
    r.AddNew
    '3: IdTableJoin... valutare...
    For i = 3 To r1.fields.Count - 1 'drogato... parte da 0... e visualizza 1
      If Not IsNull(r1.fields.Item(i)) Then
        r.fields.Item(i) = r1.fields.Item(i)
      End If
    Next i
    r!IdColonna = IdColonna + Ordinale
    r!IdTable = IdTable
    'SQ 19-06-06
    'Nuovo Environment Parameter
    If Len(inheritedPrefix & idxcol(j).Nome) > 18 Then
      r!Nome = Left(inheritedPrefix & idxcol(j).Nome, 18)
      m_fun.WriteLog "##Index Name too long: trunked at char 18 - idTable: " & IdTable & ";Column name: " & inheritedPrefix & idxcol(j).Nome
    Else
      r!Nome = inheritedPrefix & idxcol(j).Nome
    End If
    r!Ordinale = Ordinale + headerColumns + extraHeaderColumns
    r!Descrizione = "Column migrated from DLI - inherited from parent key"
    'SQ 14-06-06
    'LE MOVE LE PORTIAMO NELLE COPY LOAD/MOVE, NON PIU' FUORI...
    'LASCIAMO I TEMPLATE EREDITATI...
    r!RoutLoad = "load_INHERITED"
    r!RoutRead = ""
    r!RoutWrite = ""
    r!RoutKey = "KEY_INHERITED"
    
    If Not bFirst Then
      pos = 1
      r!Posizione = 1
      tipoOld = r1!Tipo
      lunghezzaOld = r1!lunghezza
      bFirst = True
    Else
      If TipoDB = "DB2" Then
        Select Case tipoOld
          Case "DECIMAL", "FLOAT", "INTEGER"
              r!Posizione = pos + Round(lunghezzaOld / 2 + 1)
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
          Case "SMALLINT"
              r!Posizione = pos + Round(lunghezzaOld / 2)
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
          Case Else
              r!Posizione = pos + lunghezzaOld
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
        End Select
      ElseIf TipoDB = "ORACLE" Then
        Select Case tipoOld
          Case "NUMBER"
              r!Posizione = pos + Round(lunghezzaOld / 2 + 1)
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
          Case "SMALLINT"
              r!Posizione = pos + Round(lunghezzaOld / 2)
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
          Case Else
              r!Posizione = pos + lunghezzaOld
              tipoOld = r1!Tipo
              lunghezzaOld = r1!lunghezza
        End Select
      End If
      pos = r!Posizione
    End If
    
    '''''''''''''''''
    ' update idxcol:
    '''''''''''''''''
    idxcol(j).IdColonna = r!IdColonna
      
    r.Update
  Next j
  
  r1.Close
  r.Close  'ci vuole?

End Sub

Public Function Crea_Nome_By_Number(wNome As String, wNum As Long, wPos As Long) As String
  Dim ch01 As String, ch02 As String
  Dim wApp As String
  Dim wDiv As Long, wRest As Long
  
  wApp = wNome
  
  If wNum < 36 Then
    ch01 = "A"
    ch02 = m_Array_Char(wNum)
  Else
    wDiv = wNum / 36
    wRest = wNum Mod 36
    
    ch01 = m_Array_Char(wDiv)
    ch02 = m_Array_Char(wRest)
  End If
  
  Mid$(wApp, wPos + 1, 2) = ch01 & ch02
  
  Crea_Nome_By_Number = wApp
End Function

''''''''''''''''''''''''''''''''''''
' Input: idxCol
''''''''''''''''''''''''''''''''''''
' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_IdxCol_In_DB_DB2()
Public Sub Add_IdxCol_In_DB()
  Dim r As Recordset
  Dim i As Long, Count As Long
  Dim NomeCol As String
  
  Count = 1
  
  'SQ 17-11-06
  On Error GoTo catch
  
  For i = 1 To UBound(idxcol)
    Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where IdIndex = " & idxcol(i).IdIndex & _
                           " And IdColonna = " & idxcol(i).IdColonna & " And IdTable = " & idxcol(i).IdTable)
    If r.RecordCount = 0 Then
      r.AddNew
      NomeCol = Get_NomeColonna(idxcol(i).IdColonna)  'c'e' l'ho gia' il nome per ereditate...
        
      MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Index Column: " & NomeCol
      MadmdF_DLI2RDBMS.LswLog.Refresh
      MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
                
      r!IdIndex = idxcol(i).IdIndex
      r!IdTable = idxcol(i).IdTable
      r!IdColonna = idxcol(i).IdColonna
      r!Order = "ASCENDING"
      r!Tag = "AUTOMATIC"
      r!Ordinale = Count
      r.Update
      Count = Count + 1
    End If
    r.Close
  Next
  Exit Sub
catch:
  'SQ 17-11-06 TMP!
  m_fun.WriteLog "# Add index column: " & ERR.Description
  Resume Next
End Sub

Public Function Controlla_MgSegmento(idSeg As Long) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From MGDli_Segmenti Where IdSegmentoOrigine = " & idSeg)
  If r.RecordCount Then
    Controlla_MgSegmento = r!Correzione
  Else
    Controlla_MgSegmento = ""
  End If
  r.Close
  
End Function

'SQ: modificata
' Mauro 01/08/2007 : unificate per DB2 e ORACLE
'Public Sub Translate_DLI_2_DB2(nomeDbd As String, idDbd As Long)
Public Sub Translate_DLI_2_RDBMS(snomeDbd As String, idDbd As Long)
  Dim dbName As String
  
  AddTbs = False
  'silvia 25-03-2009
  nomeDbd = snomeDbd
  LswLog.ListItems.Add , , Time & " - Deleting Old Data Migration... " & nomeDbd
  MadmdF_DLI2RDBMS.LswLog.Refresh
  
  deleteDBDTables idDbd
  
  ''''''''''''''''''''''''
  'DB NAME CRITERIA
  'SQ 8-06-06
  ''''''''''''''''''''''''
  Dim env As Collection
  If Len(DbNameParam) Then
    Set env = New Collection  'resetto;
    env.Add nomeDbd
    dbName = getEnvironmentParam(DbNameParam, "dbd", env, TipoDB)
  End If
  If Len(dbName) = 0 Then
    dbName = nomeDbd
  End If
  dbName = Replace(dbName, "-", "_")
  
  Add_Database dbName, idDbd
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' IMPORTANTISSIMO: ELIMINAZIONE ELEMENTI CON:
' TAG LIKE 'AUTOMATIC%'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub deleteDBDTablesDB2(idDbd As Long)
Public Sub deleteDBDTables(idDbd As Long)
  Dim wTb As Recordset
  
  On Error GoTo errdb
  'Decidere la gestione del DB eventualmente rimasto "vuoto"
  '...
  'Decidere la gestione del TableSpace eventualmente rimasto "vuoto"
  '...
  
  'SQ 13-06-06
  'NO!!!!!!!!!!
  'Set wTb = m_fun.Open_Recordset("select * from DMDB2_DB where idDBor = " & idDbd)
  'DM*_DB.IdDB e' la chiave di accesso per la cascata di tutti gli altri oggetti
  'If wTb.RecordCount Then
    'idDB_DBD = wTb!IdDb
    'Set wTb = m_fun.Open_Recordset("select * from DMDB2_Tabelle where iddb = " & idDB_DBD & " and tag = 'AUTOMATIC'")
    'SILVIA 11-03-2008
    Set wTb = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idObjSource = " & idDbd & " and tag like 'AUTOMATIC%' ORDER BY IDTABLE")
    '''Set wTb = m_fun.Open_Recordset("select idTable from " & PrefDb & "_Tabelle where idObjSource = " & idDbd & " and tag like 'AUTOMATIC%' ORDER BY IDTABLE")
    While Not wTb.EOF
''      If TipoDB = "DB2" Then
''        deleteTableDB2 wTb!IdTable
''      ElseIf TipoDB = "ORACLE" Then
        deleteTableSpace wTb!IdTableSpc
        deleteTable wTb!IdTable
    'SILVIA 11-03-2008 : gestisco anche la delete dei tablespaces relativi alle tabelle che sto cancellando
''      End If
      wTb.MoveNext
    Wend
    wTb.Close
    'DataMan.DmConnection.Execute "DELETE * from DMDB2_DB where idDBor = " & idDbd
  'End If
  Exit Sub
errdb:
  MsgBox "tmp: " & ERR.Description
  Resume Next
End Sub

' Mauro 01/08/2007 : unificate le routine DB2-ORACLE
'Public Sub Add_Database_DB2(dbName As String, IdDBDOr As Long)
Public Sub Add_Database(dbName As String, IdDBDOr As Long)
  Dim r As Recordset
  Dim Adesso As String, tableName As String
  Dim idDB As Long, IdTableSpace As Long, i As Long, k As Integer
  k = 0
  IdDBDinMigr = IdDBDOr
  Adesso = Now
  'silvia 25-03-2009
  '''''''''''''''''''''''''''''''''
  'AGGIUNTA DB:
  '''''''''''''''''''''''''''''''''
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where Nome = '" & dbName & "'")
  If r.RecordCount Then
    'SQ - MODIFICA IdDBDor!!! Gestire i casi NON 1-1 (DBD <-> DB)
    r!iddbor = IdDBDOr
    r!Nome = dbName
    r!Data_Modifica = Adesso
    r!Descrizione = "DLI To " & TipoDB & " Database Automatic DATA MIGRATION..."
    r!Tag = "AUTOMATIC"
    'SQ
    'If Len(DEFAULT_CREATOR) Then r!Creator = DEFAULT_CREATOR
  Else
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Database: " & dbName
    MadmdF_DLI2RDBMS.LswLog.Refresh
    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
    
    r.AddNew
    'r!IdDb = r.RecordCount + 1
    r!iddbor = IdDBDOr  'IdOggetto in da BS_Oggetti del DBD corrispondente
    r!IdObjSource = IdDBDOr
    r!Nome = dbName
    r!Data_Creazione = Adesso
    r!Data_Modifica = Adesso
    r!Descrizione = "DLI To " & TipoDB & " Database Automatic DATA MIGRATION..."
    r!Tag = "AUTOMATIC"
  End If
  'Prende l'id DB
  idDB = r!idDB
  r.Update
  r.Close
  
  PBar.MAX = UBound(Tabelle)
  PBar.Value = 0
  '''''''''''''''''''''''''''''''''
  'AGGIUNTA TABELLE:
  '''''''''''''''''''''''''''''''''
  'Aggiunge Le tabelle
  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding Tables..."
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  For i = 1 To UBound(Tabelle)
    If Tabelle(i).idDB = IdDBDOr Then 'HO TUTTE LE TABELLE DEI DBD SELEZIONATI
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' GESTIONE REDEFINES: TABELLA "DISPATCHER"
      ' COLONNE CHIAVE + "TABLE_NAME" & TUTTI GLI INDICI DELL'ORIGINALE
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If i < UBound(Tabelle) And Not isDispatcher Then
        ' Mauro 03-04-2007 : Gestione Redefines nel segmento Virtuale
        'isDispatcher = Tabelle(i + 1).IdSegmOR = Tabelle(i).IdOrigine
        Dim rsVirtualSeg As Recordset
        Dim TabOrigine As Long
        Dim a As Long
        If Tabelle(i + 1).IdSegmOR = Tabelle(i).IdOrigine Then
          isDispatcher = False
          TabOrigine = Tabelle(i).IdOrigine
          a = i
          Do Until a > UBound(Tabelle)
            If TabOrigine = Left(Tabelle(a).IdOrigine, Len(TabOrigine & "")) Then
              Set rsVirtualSeg = m_fun.Open_Recordset("select redefines from mgdli_segmenti where idsegmento = " & Tabelle(a).IdOrigine)
              If rsVirtualSeg.RecordCount Then
                If rsVirtualSeg!Redefines Then
                  isDispatcher = True
                End If
              End If
              rsVirtualSeg.Close
              a = a + 1
            Else
              Exit Do
            End If
          Loop
        Else
          isDispatcher = False
        End If
      Else
        'Ultima tabella: non pu� avere redefines!
        isDispatcher = False
      End If
      ''''''''''''''''''
      'GESTIONE NOMI:
      ''''''''''''''''''
      'tableName = Applica_PatternString(tableName, pn_Tables)
      If Len(TableNameParam) Then
        Dim env As Collection
        Set env = New Collection  'resetto;
        env.Add Tabelle(i)
        tableName = getEnvironmentParam(TableNameParam, "TabelleDM", env, TipoDB)
      Else
        If Len(Tabelle(i).NomeTb & "") Then
          tableName = Tabelle(i).NomeTb
        Else
          tableName = Tabelle(i).Nome
        End If
      End If
            
      '''''''''''''''''''''''
      ' GESTIONE TABLESPACE
      ' SQ 8-06-06
      '''''''''''''''''''''''
      IdTableSpace = getIdTableSpc(Tabelle(i), k)
      
      Add_Table idDB, Tabelle(i).IdOrigine, tableName, dbName, IdTableSpace, k
   End If
   
   PBar.Value = PBar.Value + 1
  Next i
  
  PBar.Value = 0
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Tabella PILOTA per le REDEFINES
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Sub createDispatcherTableDB2(idDB As Long, IdOr As Long, tableName As String, NomeDB As String, IdTbs As Long)
Sub createDispatcherTable(idDB As Long, IdOr As Long, tableName As String, NomeDB As String, IdTbs As Long)
  Dim rs As Recordset
  Dim IdTable As Long, IdColonna As Long
  
  ''''''''''''''''''''''''''''''''''
  ' Tabella
  ''''''''''''''''''''''''''''''''''
  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding DISPATCHER Table: " & tableName & "_DISP"
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle")
  rs.AddNew
  IdTable = rs!IdTable  'contatore
  rs!idDB = idDB
  rs!Nome = tableName & "_DISP" 'Leggi Parametro
  'SQ - Non ci vuole:
  'rs!idTableJoin = IdTable + 1  'La tabella che non creer� dopo!
  rs!IdTableSpc = IdTbs
  rs!NomeDB = NomeDB
  rs!IdOrigine = IdOr
  rs!IdObjSource = IdDBDinMigr
  rs!Descrizione = "DLI to " & TipoDB & " Migration: DISPATCHER TABLE"
  rs!Data_Creazione = Now
  rs!Data_Modifica = rs!Data_Creazione
  rs!Tag = "AUTOMATIC-DISPATCHER"
  If Len(DEFAULT_CREATOR) Then rs!Creator = DEFAULT_CREATOR
  rs.Update
  rs.Close
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' COLONNA "TABLE_NAME": unica "non chiave"
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Set rs = m_fun.Open_Recordset("Select * From DMDB2_Colonne")
  'IdColonna = getId("idColonna", "DMDB2_Colonne")
  'rs.AddNew
  'rs!IdColonna = IdColonna
  'rs!IdTable = IdTable
  'rs!IdTableJoin = IdTable + 1
  'rs!Nome = "TABLE_NAME"
  'rs!Ordinale = 1
  'rs!IdCmp = -1
  'rs!Tipo = "CHAR"
  'rs!Lunghezza = 18
  'rs!Decimali = 0
  'rs!Descrizione = "Dispatcher Column"
  'rs!Null = False
  'rs!Default = False
  'rs!Tag = "AUTOMATIC-DISPATCHER"
  'rs.Update
  'rs.Close
End Sub

' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Table_DB2(idDB As Long, IdOr As Long, NomeTb As String, NomeDB As String, IdTbs As Long)
Public Sub Add_Table(idDB As Long, IdOr As Long, NomeTb As String, NomeDB As String, IdTbs As Long, Optional k As Integer)
  Dim rsTable As Recordset, rs As Recordset, ts As Recordset, ts1 As Recordset
  Dim Adesso As String, occursTableName As String
  Dim IdTable As Long, IdColonna As Long, IdTableSpace As Long
  Dim TbName As TabDb
  Dim i As Integer, j As Long
  Adesso = Now
  GbNomeTbInAdd = NomeTb
  
  'SQ NO!
  'Va per nome DB... per cui sbaglia in caso di nomi duplicati (situazione scrausa, ma possibile...)
  Set rsTable = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where " & _
                                     "IdDB = " & idDB & " And IdOrigine = " & IdOr & " And Nome = '" & NomeTb & "' And " & _
                                     "NomeDB ='" & NomeDB & "'")
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Create DISPATCHER (Tabella Pilota per REDEFINES)
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ''''''isDispatcher = False    'NON IN LINEA!!!
  If isDispatcher Then
    createDispatcherTable idDB, IdOr, NomeTb, NomeDB, IdTbs
  End If
 '''SILVIA 17-03-2008
 '''' If (rsTable.RecordCount = 0 And Not isDispatcher) Or (isDispatcher And rsTable.RecordCount = 1) Then
  
  If rsTable.RecordCount = 0 Or (isDispatcher And rsTable.RecordCount = 1) Then
     MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding Table: " & NomeTb
     MadmdF_DLI2RDBMS.LswLog.Refresh
     MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
     
     rsTable.AddNew
     rsTable!idDB = idDB
     rsTable!Nome = NomeTb
     rsTable!IdTableSpc = IdTbs
     rsTable!NomeDB = NomeDB
     rsTable!IdOrigine = IdOr
     rsTable!IdObjSource = IdDBDinMigr
     rsTable!Descrizione = "DLI to " & TipoDB & " Automatic Migration"
     rsTable!Data_Creazione = Adesso
     rsTable!Data_Modifica = Adesso
     rsTable!Tag = "AUTOMATIC"
     'SQ
     If Len(DEFAULT_CREATOR) Then rsTable!Creator = DEFAULT_CREATOR
     IdTable = rsTable!IdTable
     rsTable.Update
   End If
  
    Add_Colonne IdTable, IdOr, NomeTb, NomeDB 'GLI ULTIMI DUE PARAMETRI MI POSSONO SERVIRE PER LE "EXTRA COLUMNS"..
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE OCCURS:
    ' - CREAZIONE SOTTOTABELLA OCCURS
    ' - TABELLA PRINCIPALE SENZA COLONNE OCCURSATE
    ' SQ - 21-06-06
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ
    If Len(occursFields(0)) Then
      Dim tableName As String
      Dim tbchld As TabDb
      ''''
      j = 1
      For i = 0 To UBound(occursFields)
        'TMP: Fare parametro!
        Dim idCmpOccurs As String
        idCmpOccurs = Left(occursFields(i), IIf(InStr(occursFields(i), ","), InStr(occursFields(i), ",") - 1, occursFields(i)))
        Set rs = m_fun.Open_Recordset("Select nome from MgData_cmp where " & _
                                      "idCmp = " & idCmpOccurs & " and idsegmento = " & IdOr & " and damigrare = -1")
            
        If rs.RecordCount Then
          '''silvia 7-3-2008 tabella child con naming convention
          If Len(TableChNameParam) Then
            'If j = 1 Then
            Dim env As Collection
            Set env = New Collection  'resetto;
            tbchld.NomeTb = NomeTb
            'silvia 25--03-2009
            tbchld.NomeDB = nomeDbd
            env.Add tbchld
            env.Add j
            tableName = getEnvironmentParam(TableChNameParam, "TabelleDM", env, TipoDB)
            'silvia cambio il nome del tablespaces della madre
            If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
              Set ts = m_fun.Open_Recordset("select * from " & PrefDb & "_TableSpaces where idtablespc = " & IdTbs)
              If ts.RecordCount Then
              'table space gia' presente:
                ts!Nome = Replace(tableName, Mid(tableName, 6, 1), "S")
                ts.Update
              End If
              ts.Close
               'silvia : cambio il nume della tabella madre
              rsTable!Nome = tableName
              rsTable.Update
              Set env = New Collection
              env.Add tbchld
              j = j + 1
              env.Add j
              tableName = getEnvironmentParam(TableChNameParam, "TabelleDM", env, TipoDB)
          '''''If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
              Set ts = m_fun.Open_Recordset("select * from " & PrefDb & "_TableSpaces where nome = '" & tableName & "'")
              If Not ts.RecordCount Then
                ts.AddNew
                ts!Nome = Replace(tableName, Mid(tableName, 6, 1), "S")
                ts!Nome = Replace(tableName, Mid(tableName, 6, 1), "S")
              'Nuovo Id:
                Set ts1 = m_fun.Open_Recordset("Select max(IdTableSpc) From " & PrefDb & "_TableSpaces")
                ts!IdTableSpc = IIf(IsNull(ts1(0)), 1, ts1(0) + 1)
                IdTbs = ts!IdTableSpc
                ts!StorageGroup = Stgroup
                'ts!PRIQTY = "<priqty>"
                'ts!SECQTY = "<secqty>"
                'ts!FREEPAGE = "<freepage>"
                'ts!PCTFREE = "<pctfree>"
                ts!BufferPool = "<bufferPool>"
                ts!Compress = False
                'CCSID?
                ts1.Close
                ts.Update
              End If
              ts.Close
            Else
              'Tbname.Nametb = tableName
              IdTbs = getIdTableSpc(tbchld, k)
              j = j + 1
            End If
          Else
            tableName = NomeTb & "_" & Replace(rs!Nome, "-", "_")
          End If
        Else
          tableName = NomeTb & "_" & idCmpOccurs 'TMP name: ma univoco!
        End If
        rs.Close
        
        MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding 'OCCURS SubTable': " & tableName
        MadmdF_DLI2RDBMS.LswLog.Refresh
        MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
        rsTable.AddNew
        rsTable!idDB = idDB
        rsTable!Nome = tableName
        rsTable!IdTableSpc = IdTbs
        rsTable!NomeDB = NomeDB
        rsTable!IdOrigine = IdOr
        rsTable!idTableJoin = IdTable
        '''''''''''''''''''''''''''''''''''''
        ' Origine occurs! campo provvisorio
        '''''''''''''''''''''''''''''''''''''
        rsTable!Dimensionamento = idCmpOccurs
        rsTable!IdObjSource = IdDBDinMigr
        rsTable!Descrizione = "DLI to " & TipoDB & " Automatic Migration"
        rsTable!Data_Creazione = Adesso
        rsTable!Data_Modifica = Adesso
        rsTable!Tag = "AUTOMATIC-OCCURS"
        If Len(DEFAULT_CREATOR) Then rsTable!Creator = DEFAULT_CREATOR
        'IdTable = rsTable!IdTable
        rsTable.Update
        
        addOccursColumns rsTable!IdTable, IdTable, IdOr, occursFields(i)  'per ciclicit�
        
      Next
    End If
  ''End If
  rsTable.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'GLI ULTIMI DUE PARAMETRI MI POSSONO SERVIRE PER LE "EXTRA COLUMNS"...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Colonne_DB2(IdTable As Long, idSeg As Long, NomeTb As String, NomeDB As String)
Public Sub Add_Colonne(IdTable As Long, idSeg As Long, NomeTb As String, NomeDB As String)
  Dim r As Recordset, rc As Recordset, rD As Recordset
  Dim Adesso As String, NomeNorm As String
  Dim IdColonna As Long, Ordinale As Long, IdTbl As Long
  Dim k As Integer
  ' Mauro 15/01/2008
  Dim Occurs As Long, occursLevel As Integer
  Dim OccursFld() As String, rsOccursCmp As Recordset
  
  Adesso = Now
  'ordinale = 0
  
  'IdColonna:
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  If IdColonna < 0 Then
    'gestire
    MsgBox "Problems! " & ERR.Description & " Impossible add columns"
    Exit Sub
  End If
  
  On Error GoTo errdb
  
  'PS: UTILIZZARE UN SOLO unica open e close di un unico rc...
  
  Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
  ''''''''''''''''''''''''''''''''''''''
  ' AGGIUNTA COLONNE HEADER
  ''''''''''''''''''''''''''''''''''''''
  Set r = m_fun.Open_Recordset("Select * from DMheader_columns order by ordinale")
  While Not r.EOF
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & r!Nome
    MadmdF_DLI2RDBMS.LswLog.Refresh
    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
    Ordinale = Ordinale + 1
    rc.AddNew
    rc!IdColonna = IdColonna
    rc!IdTable = IdTable
    rc!idTableJoin = IdTable
    rc!Nome = r!Nome
    rc!Ordinale = Ordinale
    rc!IdCmp = -1
    rc!Tipo = r!Tipo
    rc!lunghezza = r!lunghezza
    rc!Decimali = r!Decimali
    rc!Descrizione = "Header Column"
    rc!Null = r!Null
    
    'MG 130207
    rc!IsDefault = DEFAULT_DEFAULT
    If DEFAULT_DEFAULT Then
      rc!defaultValue = ""
    Else
      rc!defaultValue = ""
    End If
    
    '''MG rc!default = r!default
    rc!Tag = "AUTOMATIC"
    rc!RoutLoad = r!TemplateLoad
    rc!RoutRead = r!TemplateRead
    rc!RoutWrite = r!TemplateWrite
    rc!RoutKey = r!TemplateKey
    rc.Update
    
    r.MoveNext
    IdColonna = IdColonna + 1
  Wend
  r.Close
  rc.Close
  
  'ATTENZIONE, sarebbe una count(*)...
  Set rD = m_fun.Open_Recordset("Select Distinct IdSegmento From MgData_Area Where IdSegmento = " & idSeg)
  If rD.RecordCount Then  'verificare: e' solo un controllo...
    rD.Close
    Dim colonnaOk As Boolean 'serve solo per alpitour...
    'Set r = m_fun.Open_Recordset("Select * From MgData_Cmp Where IdSegmento = " & rD!IdSegmento & " And Lunghezza > 0 Order By Ordinale")
    Set r = m_fun.Open_Recordset(Crea_SQL_Tabelle(idSeg))
    While Not r.EOF
      'NON TUTTI I CAMPI DEVONO DIVENTARE COLONNE:
      colonnaOk = True
      ' Mauro 15/01/2008
      'If colonnaOk Then
      If colonnaOk And r!DaMigrare Then
        Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where " & _
                                      "IdColonna = " & IdColonna & " And IdTable = " & IdTable)
        If r!Nome <> "FILLER" And r!Nome <> "FILLER-REVISER" Then 'PARAMETRIZZARE!!!!!!
          'Le Colonne di questa tabella sono gi� state cancellate in precedenza
          'NomeNorm = Normalizza_Nome(r!Nome, "RDBMSCL")
          Dim env As Collection
          'SQ 13-11-07
          Set env = New Collection  'resetto;
          env.Add r
          
          NomeNorm = getEnvironmentParam(ColNameParam, "CmpName", env, TipoDB)
          
          'Eliminazione caratteri invalidi/sostituzione trattino->underscore
          Dim Ch As String
          Dim i As Long
          Ch = ""
          For i = 1 To Len(NomeNorm)
            If Char_Is_Valid(Mid(NomeNorm, i, 1)) Then
              Ch = Ch & Mid(NomeNorm, i, 1)
            End If
          Next i
          NomeNorm = Ch
          
          Ordinale = Ordinale + 1
          If TipoDB = "DB2" Then
            Trascodifica_Tipo_DLI_DB2 r!Tipo, r!Usage, r!lunghezza, r!Decimali, NomeNorm, r!segno
          ElseIf TipoDB = "ORACLE" Then
            Trascodifica_Tipo_DLI_ORACLE r!Tipo, r!Usage, r!lunghezza, r!Decimali, NomeNorm
          End If
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Eccezioni nomi chiavi; fuori rotellone per via degli inherit...
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          NomeNorm = Replace(NomeNorm, "-", "_")
          ' Mauro 01/08/2007 : Controllo per l'eliminazione degli underscore finali
          For i = Len(NomeNorm) To 1 Step -1
            If Mid(NomeNorm, i, 1) = "_" Then
              NomeNorm = Mid(NomeNorm, 1, i - 1)
            Else
              Exit For
            End If
          Next i
          
          If rc.RecordCount = 0 Then  'la open l'ho fatta 10 operazioni fa!?
            MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
            MadmdF_DLI2RDBMS.LswLog.Refresh
            MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
          
            rc.AddNew
            rc!IdColonna = IdColonna
            rc!IdTable = IdTable
            'SQ - che droghe usa?!
            'rC!idTableJoin = getIdTableJoin(r!IdCmp)
            'If rC!idTableJoin = 0 Then
              rc!idTableJoin = IdTable
            'End If
            rc!Nome = NomeNorm
            rc!Ordinale = Ordinale
            rc!IdCmp = r!IdCmp
            rc!NomeCmp = r!Nome
            If TipoDB = "DB2" Then
              rc!Tipo = DB2Tipo
              rc!lunghezza = DB2Lunghezza
              rc!Decimali = DB2Decimali
            ElseIf TipoDB = "ORACLE" Then
              rc!Tipo = ORACLETipo
              rc!lunghezza = ORACLELunghezza
              rc!Decimali = ORACLEDecimali
            End If
            rc!Descrizione = "Column migrated from DLI"
            rc!Null = DEFAULT_NULL
            'ATTENZIONE, SISTEMARE: DEFAULT NON E' BOOLEAN...
            'MG 130207
            rc!IsDefault = DEFAULT_DEFAULT
            If DEFAULT_DEFAULT Then
              rc!defaultValue = "" '??????
            Else
              rc!defaultValue = ""
            End If
            ''''''MG rc!default = DEFAULT_DEFAULT
            rc!Tag = "AUTOMATIC"
            rc!Commento = ""
            If TipoDB = "DB2" Then
              rc!RoutLoad = "load_" & DB2Tipo
              rc!RoutRead = "rdbms2dli_" & DB2Tipo
              rc!RoutWrite = "dli2rdbms_" & DB2Tipo
              rc!RoutKey = "dli2rdbms_KEY_" & DB2Tipo
            ElseIf TipoDB = "ORACLE" Then
              rc!RoutLoad = "load_" & ORACLETipo
              rc!RoutRead = "rdbms2dli_" & ORACLETipo
              rc!RoutWrite = "dli2rdbms_" & ORACLETipo
              rc!RoutKey = "dli2rdbms_KEY_" & ORACLETipo
              'SQ tmp!! campi BIT
              If r!Usage = "BIT" Then
                rc!RoutLoad = rc!RoutLoad & "_BIT"
                rc!RoutRead = rc!RoutRead & "_BIT"
                rc!RoutWrite = rc!RoutWrite & "_BIT"
                rc!RoutKey = rc!RoutKey & "_BIT"
              End If
            End If
            rc.Update
            IdColonna = IdColonna + 1
          End If
        End If
        'rc.Close
      End If
      If Not r.EOF Then
        r.MoveNext
      End If
      DoEvents
    Wend
    r.Close
    '  rD.MoveNext
    '  DoEvents
    'rD.Close
    Else
      'SQ tmp: se sono qui qualcosa non va... gestire!
      ReDim occursFields(0)
    End If
        
    ''''''''''''''''''''''''''''''''''''''
    ' AGGIUNTA COLONNE FOOTER
    ''''''''''''''''''''''''''''''''''''''
    Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
    Set r = m_fun.Open_Recordset("Select * from DMfooter_columns order by ordinale")
    While Not r.EOF
      NomeNorm = r!Nome
      MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
      MadmdF_DLI2RDBMS.LswLog.Refresh
      MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
      Ordinale = Ordinale + 1
      rc.AddNew
      rc!IdColonna = IdColonna
      rc!IdTable = IdTable
      rc!idTableJoin = IdTable
      rc!Nome = NomeNorm
      rc!Ordinale = Ordinale
      rc!IdCmp = -1
      rc!Tipo = r!Tipo
      rc!lunghezza = r!lunghezza
      rc!Decimali = r!Decimali
      rc!Descrizione = "Footer Column"
      ''''''''''''''''''''''''''''''''''
      'Gestione Commenti... da fare bene
      ''''''''''''''''''''''''''''''''''
      'rc!Commento = "DATA ULTIMA MODIFICA"
      rc!Null = r!Null
      
      'MG 130207
      rc!IsDefault = DEFAULT_DEFAULT
      If DEFAULT_DEFAULT Then
        rc!defaultValue = ""
      Else
        rc!defaultValue = ""
      End If
      '''MG rc!default = r!default
      rc!Tag = "AUTOMATIC"
      rc!RoutLoad = r!TemplateLoad
      rc!RoutRead = r!TemplateRead
      rc!RoutWrite = r!TemplateWrite
      rc!RoutKey = r!TemplateKey
              
      rc.Update
      r.MoveNext
      IdColonna = IdColonna + 1
    Wend
    r.Close
    rc.Close
    
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub

'Function getIdTableJoin(IndCampo As Long) As Long
Function getIdTableJoin(IdSegmento As Long) As Long
  Dim rs As Recordset
  
  On Error GoTo ERR
  
  Set rs = m_fun.Open_Recordset("Select idTable from DMDB2_Tabelle where idorigine = " & IdSegmento & " AND TAG='AUTOMATIC'")
  If rs.RecordCount Then
    getIdTableJoin = rs!IdTable
  Else
    getIdTableJoin = 0
  End If
  
  Exit Function
ERR:
  MsgBox ERR.Description
End Function

Public Function Crea_SQL_Tabelle(IdSegmento As Long) As String
  Dim TbRelazioni As Recordset, TbSegmento As Recordset
  Dim nomeSegmento As String
  Dim i As Integer
  Dim IdOgg As Long
  
  'SQ tmp - Valorizza occursFields (array di "liste field")
  'listaOccurs = TrovaOccurs(IdSegmento)
  getOccursFields IdSegmento
  
  'Crea_SQL_Tabelle = "Select * from MgData_Cmp where (IdSegmento = " & IdSegmento & " and Lunghezza+decimali <>0 )"
  Crea_SQL_Tabelle = "Select * from MgData_Cmp where ((IdSegmento = " & IdSegmento & " and Lunghezza+decimali >0 )"
  
  Set TbSegmento = m_fun.Open_Recordset("Select * from PsDLI_Segmenti where IdSegmento = " & IdSegmento)
  If TbSegmento.RecordCount = 0 Then
    Set TbSegmento = m_fun.Open_Recordset("Select * from MGDLI_Segmenti where IdSegmento = " & IdSegmento)
  End If
    
  If TbSegmento.RecordCount Then
    nomeSegmento = TbSegmento!Nome
    IdOgg = TbSegmento!IdOggetto
  End If
    
  Do While nomeSegmento <> "0"
    Set TbRelazioni = m_fun.Open_Recordset("Select * From MgRel_DLISeg " & _
                                           "Where idoggetto = " & IdOgg & " and Child = '" & nomeSegmento & "' and Relazione = True")
    If TbRelazioni.RecordCount Then
      Set TbSegmento = m_fun.Open_Recordset("Select * from PsDLI_Segmenti " & _
                                            "where IdOggetto = " & TbRelazioni!IdOggetto & " and Nome = '" & TbRelazioni!Father & "'")
      If TbSegmento.RecordCount Then
        Crea_SQL_Tabelle = Crea_SQL_Tabelle & " or (IdSegmento = " & TbSegmento!IdSegmento & " and Correzione = 'R' and Lunghezza <> 0)"
      End If
    End If
    'Mauro 15/01/2007: aggiunta idoggetto a query
    Set TbRelazioni = m_fun.Open_Recordset("Select * From MgRel_DLISeg Where Child='" & nomeSegmento & "' and idoggetto=" & IdOgg)
    nomeSegmento = TbRelazioni!Father
    
    'caso possibile solo per "pezze" manuali...
    If Trim(nomeSegmento) = "" Then
      Exit Do
    End If
    TbRelazioni.Close
  Loop
    
  If Len(occursFields(0)) Then
    'lista idCmp occurs per la WHERE condition:
    Dim listaOccurs As String
    For i = 0 To UBound(occursFields)
      listaOccurs = listaOccurs & IIf(i, ",", "") & occursFields(i)
    Next
    Crea_SQL_Tabelle = Crea_SQL_Tabelle & ") and idcmp not in(" & listaOccurs & ")"
    Crea_SQL_Tabelle = Crea_SQL_Tabelle & " Order by IdSegmento, Ordinale"
  Else
    Crea_SQL_Tabelle = Crea_SQL_Tabelle & ") Order by IdSegmento, Ordinale"
  End If
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Valorizza "occursFields", array di "liste" di idcmp
' relativi ai campi di una occurs
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub getOccursFields(IdSegmento As Long)
  Dim rs As Recordset
  Dim occursLevel As Integer
  Dim i As Integer
  
  ReDim occursFields(0)
  
  Set rs = m_fun.Open_Recordset("select idCmp,livello,occurs,occursflat from MgData_cmp where " & _
                                "idsegmento = " & IdSegmento & " and damigrare = -1 order by ordinale")
  While Not rs.EOF
    If rs!Occurs And Not rs!OccursFlat Then
      ReDim Preserve occursFields(i)
      'Serve? e' il primo della lista!
      'idCmpOccurs = rs!IdCmp  '"ORIGINE" occurs -> DMDB2_Tabelle.Dimensionamento (APPOGGIO PROVVISORIO!!!)
      occursFields(i) = occursFields(i) & rs!IdCmp & ","
      occursLevel = rs!livello
      rs.MoveNext
      Do While Not rs.EOF
        If rs!livello > occursLevel Then
          occursFields(i) = occursFields(i) & rs!IdCmp & ","
          rs.MoveNext
        Else
          i = i + 1
          Exit Do
        End If
      Loop
    Else
      rs.MoveNext
    End If
  Wend
  rs.Close
  
  For i = 0 To UBound(occursFields)
    ' elimino l'ultima virgola
    If Len(occursFields(i)) Then
      occursFields(i) = Left(occursFields(i), Len(occursFields(i)) - 1)
    End If
  Next i
End Sub

Public Function Crea_SQL_Primary(IdSegmento As Long) As String
  Dim TbRelazioni As Recordset
  Dim TbSegmento As Recordset
  Dim nomeSegmento As String
  Dim wIdDB As Variant
  
  Crea_SQL_Primary = "Select * from MgData_Cmp where (IdSegmento = " & IdSegmento & " and Correzione = 'R' and Lunghezza <> 0)"
  'Crea_SQL_Primary = "Select * from MgData_Cmp where (IdSegmento = " & IdSegmento & " and Lunghezza <> 0)"
  
  Set TbSegmento = m_fun.Open_Recordset("Select * from PsDLI_Segmenti where IdSegmento = " & IdSegmento)
    
  If TbSegmento.RecordCount <> 0 Then
    nomeSegmento = TbSegmento!Nome
    wIdDB = TbSegmento!IdOggetto
  End If
    
  While Trim(nomeSegmento) <> "0" And Trim(nomeSegmento) <> ""
    'condizione ulteriormente la query se ...
    Set TbRelazioni = m_fun.Open_Recordset("Select * From MgRel_DLISeg Where idoggetto = " & wIdDB & " and Child = '" & nomeSegmento & "' and Relazione = True")
    If TbRelazioni.RecordCount <> 0 Then
      TbRelazioni.MoveFirst
      Set TbSegmento = m_fun.Open_Recordset("Select * from PsDLI_Segmenti where IdOggetto = " & TbRelazioni!IdOggetto & " and Nome = '" & TbRelazioni!Father & "'")
      If TbSegmento.RecordCount <> 0 Then
        TbSegmento.MoveFirst
        Crea_SQL_Primary = Crea_SQL_Primary & " or (IdSegmento = " & TbSegmento!IdSegmento & " and Correzione = 'R' and Lunghezza <> 0)"
      End If
    End If
    
    Set TbRelazioni = m_fun.Open_Recordset("Select * From MgRel_DLISeg Where idoggetto = " & wIdDB & " and Child = '" & nomeSegmento & "'")
    If TbRelazioni.RecordCount > 0 Then
       nomeSegmento = TbRelazioni!Father
    Else
       nomeSegmento = "0"
    End If
  Wend
  
  Crea_SQL_Primary = Crea_SQL_Primary & " Order by IdSegmento, Ordinale"

End Function

Public Function Trascodifica_Tipo_DLI_DB2(Tipo As String, Usage As String, lunghezza As Long, Dec As Long, NomeCol As String, Sign As String)
  
  Select Case Trim(UCase(Tipo))
    ' il tipo "X" non ha parte decimale
    Case "X"
      Select Case Trim(UCase(Usage))
        Case "ZND", ""
          If lunghezza >= 1 And lunghezza <= 255 Then
            DB2Tipo = "CHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
          
          If lunghezza > 255 Then
            DB2Tipo = "VARCHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
          
        Case "EDT"
          If lunghezza >= 1 Then
            DB2Tipo = "DECIMAL"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
        'silvia 19-03-2009
        Case "BIT"
          DB2Tipo = "DECIMAL"
           DB2Lunghezza = 1 'SILVIA:  gestione specifica per Cliente GRZ
           DB2Decimali = 0
           
           
      End Select
      
    Case "A"
      If Trim(UCase(Usage)) = "ZND" Then
        If lunghezza >= 1 And lunghezza <= 255 Then
            DB2Tipo = "CHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
          
          If lunghezza > 255 Then
            DB2Tipo = "VARCHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
      End If
      
    Case "9"
      Select Case Trim(UCase(Usage))
        Case "EDT", "ZND", "PCK"
          If lunghezza + Dec >= 1 Then
            DB2Tipo = "DECIMAL"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
            'SQ else?!
          End If
          'SQ 13-11-07 - TMP!!!!!!!!!!
          'Deve diventare il default?!
          If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" And Usage = "ZND" Then
            'SILVIA 5-3-2008 : sei il campo ha i decimali o il segno rimane DECIMAL altrimenti diventa CHAR
            'If Dec = 0 Or Sign = "N" Then
            If Dec = 0 And Sign = "N" Then
               DB2Tipo = "CHAR"
              'SQ 7-04-09 DECIMALI PERSI!
               'DB2Lunghezza = lunghezza
               DB2Lunghezza = lunghezza + Dec
               DB2Decimali = 0
            End If
          End If
        Case "BNR"
          If lunghezza + Dec >= 1 And lunghezza + Dec <= 4 Then
            DB2Tipo = "SMALLINT"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
          
          If lunghezza + Dec >= 5 And lunghezza + Dec <= 8 Then
            DB2Tipo = "INTEGER"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
          
          If lunghezza + Dec >= 9 Then
            DB2Tipo = "LONG"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
        Case ""
            DB2Tipo = "DECIMAL"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
      End Select
  End Select
   
  'analisi casi particolari
  'eliminato...
  
End Function

Public Function Trascodifica_Tipo_DLI_ORACLE(Tipo As String, Usage As String, lunghezza As Long, Dec As Long, NomeCol As String)
  
  Select Case Trim(UCase(Tipo))
    Case "X"
      ' il tipo "X" non ha parte decimale
      Select Case Trim(UCase(Usage))
        'SQ 2-10-2010 (added "")
        Case "ZND", ""
          If lunghezza >= 1 And lunghezza <= 255 Then
            ORACLETipo = "CHAR"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
          
          If lunghezza > 255 Then
            ORACLETipo = "VARCHAR2"  '''VARCHAR == deprecated
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
          
        Case "EDT"
          If lunghezza >= 1 Then
            ORACLETipo = "NUMBER"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
        'SQ tmp... 1-10-2010
        Case "BIT"
            ORACLETipo = "CHAR"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
      End Select
    Case "A"
      If Trim(UCase(Usage)) = "ZND" Then
        If lunghezza >= 1 And lunghezza <= 255 Then
            ORACLETipo = "CHAR"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
          
          If lunghezza > 255 Then
            'ORACLETipo = "VARCHAR" VARCHAR == deprecated
            ORACLETipo = "VARCHAR2"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
      End If
    Case "9"
      Select Case Trim(UCase(Usage))
        Case "EDT", "ZND", "PCK"
          If lunghezza + Dec >= 1 Then
            ORACLETipo = "NUMBER"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
        'SQ 1-10-2010 tmp - sono gli zoned provenienti dal PLI...
        'Per ora li trasformo in CHAR, ma andrebbe esternalizzato il tutto (e valutato anche il segno!!)
        Case ""
            ORACLETipo = "CHAR"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
        Case "BNR"
          If lunghezza + Dec >= 1 And lunghezza + Dec <= 4 Then
            ORACLETipo = "NUMBER" 'SMALLINT
            ORACLELunghezza = 5 + Dec
            ORACLEDecimali = Dec
          End If
          
          If lunghezza + Dec >= 5 And lunghezza + Dec <= 8 Then
            ORACLETipo = "NUMBER" 'INTEGER
            ORACLELunghezza = 10 + Dec
            ORACLEDecimali = Dec
          End If
          
          If lunghezza + Dec >= 9 Then
            ORACLETipo = "NUMBER"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
      End Select
  End Select
  
End Function

Public Sub Carica_Tabelle_DLI()
  Dim rMg As Recordset, r As Recordset, rMa As Recordset
  Dim Y As Integer
  Dim DBDList As String
  
  Screen.MousePointer = vbHourglass
  ReDim Tabelle(0)
  
  If OptAll Then
    Set r = m_fun.Open_Recordset("Select * From PsDLi_Segmenti order by idsegmento")
  Else
    For Y = 1 To lswDBSel.ListItems.Count
      If lswDBSel.ListItems(Y).Selected Then
        DBDList = DBDList & lswDBSel.ListItems(Y).Tag & ","
      End If
    Next
    DBDList = Left(DBDList, Len(DBDList) - 1)
    Set r = m_fun.Open_Recordset("Select * From PsDLi_Segmenti where idoggetto in (" & DBDList & ") order by idsegmento")
  End If
  
  While Not r.EOF
    Set rMg = m_fun.Open_Recordset("Select * From MgDli_Segmenti Where IdSegmento = " & r!IdSegmento)
    Set rMa = m_fun.Open_Recordset("Select * From MgData_Area Where IdSegmento = " & r!IdSegmento)
    If rMg.RecordCount Then
      'Esiste controlla il tipo di Correzione
      Select Case Trim(UCase(rMg!Correzione))
        Case "M" 'Modificato Prende il segmento MG
          ReDim Preserve Tabelle(UBound(Tabelle) + 1)
          Tabelle(UBound(Tabelle)).idDB = rMg!IdOggetto
          Tabelle(UBound(Tabelle)).IdOrigine = rMg!IdSegmento
          Tabelle(UBound(Tabelle)).IdSegmOR = rMg!IdSegmentoOr
          Tabelle(UBound(Tabelle)).Nome = rMg!Nome
          Tabelle(UBound(Tabelle)).NomeDB = Restituisci_NomeOgg_Da_IdOggetto(r!IdOggetto)
          Tabelle(UBound(Tabelle)).AliasSegm = IIf(rMa.RecordCount, IIf(IsNull(rMa!Alias_Segm), "", rMa!Alias_Segm), "")
        Case "C", "O" 'Cancellato 'non prende il segmento
       '   Stop
        Case Else
          Stop
          ReDim Preserve Tabelle(UBound(Tabelle) + 1)
          Tabelle(UBound(Tabelle)).idDB = r!IdOggetto
          Tabelle(UBound(Tabelle)).IdOrigine = r!IdSegmento
          Tabelle(UBound(Tabelle)).Nome = r!Nome
          Tabelle(UBound(Tabelle)).NomeTb = r!rdbms_tbname & ""
          Tabelle(UBound(Tabelle)).NomeDB = Restituisci_NomeOgg_Da_IdOggetto(r!IdOggetto)
          Tabelle(UBound(Tabelle)).AliasSegm = IIf(rMa.RecordCount, IIf(IsNull(rMa!Alias_Segm), "", rMa!Alias_Segm), "")
      End Select
    Else
      ReDim Preserve Tabelle(UBound(Tabelle) + 1)
      Tabelle(UBound(Tabelle)).idDB = r!IdOggetto
      Tabelle(UBound(Tabelle)).IdOrigine = r!IdSegmento
      Tabelle(UBound(Tabelle)).Nome = r!Nome
      Tabelle(UBound(Tabelle)).NomeTb = r!rdbms_tbname & ""
      Tabelle(UBound(Tabelle)).NomeDB = Restituisci_NomeOgg_Da_IdOggetto(r!IdOggetto)
      Tabelle(UBound(Tabelle)).AliasSegm = IIf(rMa.RecordCount, IIf(IsNull(rMa!Alias_Segm), "", rMa!Alias_Segm), "")
    End If
    rMg.Close
    'SQ - Tiro su qui i segmenti virtuali...
    Set rMg = m_fun.Open_Recordset("Select * From MgDLi_Segmenti Where IdSegmentoOrigine = " & r!IdSegmento & " AND Correzione = 'I' Order By IdSegmento")
    While Not rMg.EOF
      Set rMa = m_fun.Open_Recordset("Select * From MgData_Area Where IdSegmento = " & rMg!IdSegmento)
      ReDim Preserve Tabelle(UBound(Tabelle) + 1)
      Tabelle(UBound(Tabelle)).idDB = rMg!IdOggetto
      Tabelle(UBound(Tabelle)).IdOrigine = rMg!IdSegmento
      'SQ
      'Esiste la colonna IdSegmentoOrigine: o la usiamo o la brasiamo
      '(Tenerla aiuta quando si fanno le cose a mano... quindi brasiamola!)
      'Tabelle(UBound(Tabelle)).IdSegmOR = Int(rMg!IdSegmento / 100)
      Tabelle(UBound(Tabelle)).IdSegmOR = rMg!IdSegmentoOrigine
      Tabelle(UBound(Tabelle)).Nome = rMg!Nome
      Tabelle(UBound(Tabelle)).NomeDB = Restituisci_NomeOgg_Da_IdOggetto(rMg!IdOggetto)
      Tabelle(UBound(Tabelle)).AliasSegm = IIf(rMa.RecordCount, IIf(IsNull(rMa!Alias_Segm), "", rMa!Alias_Segm), "")
      rMg.MoveNext
    Wend
    rMg.Close
    rMa.Close
    r.MoveNext
  Wend
  r.Close
  
  Screen.MousePointer = vbDefault
End Sub

Public Function Restituisci_NomeOgg_Da_IdOggetto(id As Long) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & id)
  If r.RecordCount Then
    Restituisci_NomeOgg_Da_IdOggetto = r!Nome
  End If
  r.Close
End Function

Public Function Get_IdOggetto(Nome) As Long
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & Nome & "' And Tipo = 'DBD'")
  If r.RecordCount > 0 Then
    Get_IdOggetto = r!IdOggetto
  End If
  r.Close
End Function

Public Function Get_LastID_Index()
  Dim r As Recordset
  Dim LastId As Long
  
  'Prende L'ultimo ID disponibile
  Set r = m_fun.Open_Recordset("Select MAX(IdIndex) From DMDB2_Index")
  If r.RecordCount Then
    r.MoveLast
    If Not IsNull(r.fields(0)) Then
      LastId = r.fields(0) + 1
    Else
      LastId = 1
    End If
  Else
    LastId = 1
  End If
  r.Close
  
  Get_LastID_Index = LastId
End Function

' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Public Function Get_NomeColonna_DB2(IdCol As Long) As String
Public Function Get_NomeColonna(IdCol As Long) As String
  Dim r As Recordset
  
  'Prende L'ultimo ID disponibile
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdColonna = " & IdCol)
  
  If r.RecordCount Then
    Get_NomeColonna = r!Nome
  Else
    Get_NomeColonna = ""
  End If
  r.Close
  
End Function

Public Sub Carica_Lista_DBDLI()
   Dim rs As Recordset
   Dim rsRel As Recordset
   Dim i As Long
   Dim Selected As Boolean
   Dim DBNome As String
   
   lswDBSel.ListItems.Clear
  
   'VC (SQ): modifica di stefano per prelevare tutti i tipi di DB presenti
   'HDAM,HIDAM, etc.. ad eccezione degli INDICI(modificata la where)
   'Set rs = m_fun.Open_Recordset("Select * from BS_Oggetti Where Tipo_DBD = 'HID' Order by Nome")
   Set rs = m_fun.Open_Recordset("Select * from BS_Oggetti Where Tipo = 'DBD'  and tipo_dbd <> 'IND' Order by Nome")
   
   If rs.RecordCount > 0 Then
      While Not rs.EOF
         lswDBSel.ListItems.Add , , rs!Nome
         lswDBSel.ListItems(lswDBSel.ListItems.Count).Tag = rs!IdOggetto
         Set rsRel = m_fun.Open_Recordset("select * from MgRel_DLISeg where idoggetto = " & rs!IdOggetto)
         If rsRel.RecordCount Then
           lswDBSel.ListItems(lswDBSel.ListItems.Count).SubItems(1) = "X"
         Else
           lswDBSel.ListItems(lswDBSel.ListItems.Count).SubItems(1) = ""
         End If
         rsRel.Close
         rs.MoveNext
      Wend
''   Else
''      lswDBSel.ListItems.Add , , "No DLI DB Type Found..."
   End If
End Sub

'VC:
Public Sub Accoppia_Xdfld_Da_Field(IdTable As Long, idSeg As Long, lunghezza As Long, pos As Long, IdField As Long, tableName As String)
  Dim r As Recordset, r1 As Recordset
  Dim i As Long, j As Long
  Dim Somma As Long, StPos As Long, LenPos As Long
  Dim PKExist As Boolean
  Dim IdSegFather As Long
  Dim KApp() As GbField
  ReDim KApp(0)
  Dim wFieldXD As String
  Dim wIdFieldXD As Long
  On Error GoTo errdb
  
  For i = 1 To UBound(Field)
    If Field(i).IdField = IdField Then
      ReDim Preserve KApp(UBound(KApp) + 1)
      KApp(UBound(KApp)) = Field(i)
      Exit For
    End If
  Next i
  
  Get_Parent_Segment Field(i).IdSegmento, IdSegFather, pos, lunghezza, wFieldXD, wIdFieldXD
    
  If wFieldXD <> "" Then
    ReDim Preserve Key(UBound(Key) + 1)
    Key(UBound(Key)).IdSegmento = idSeg
    Key(UBound(Key)).NomeKey = wFieldXD
    Key(UBound(Key)).IdCmp = wIdFieldXD
  Else
    MsgBox "I was not able to associate XDLFD for the segment: " & idSeg
  End If
  
  Exit Sub
errdb:
  MsgBox ERR.Description
End Sub

'VC:
Public Sub Get_Parent_Segment(idSeg As Long, IdSegFather As Long, wPos As Long, wLen As Long, wFieldXD As String, wIdFieldXD As Long)
  Dim rc As Recordset, rf As Recordset, rm As Recordset, rr As Recordset, rk As Recordset
  Dim WnomeParent As String
  Dim wLivMax As Long, wLivMin As Long, wLivRif As Long
  
  Set rc = m_fun.Open_Recordset("Select * From PSDli_Segmenti Where IdSegmento = " & idSeg)
  If rc.RecordCount Then
    WnomeParent = rc!Parent
    Set rf = m_fun.Open_Recordset("Select * From PSDli_Segmenti Where " & _
                                  "nome = " & "'" & WnomeParent & "'" & " and IdOggetto = " & rc!IdOggetto)
    If rf.RecordCount Then
      IdSegFather = rf!IdSegmento
    End If
    rf.Close
  End If
  rc.Close
  
  Set rm = m_fun.Open_Recordset("Select * From MgData_Cmp Where " & _
                                "IdSegmento = " & IdSegFather & " and Posizione = " & wPos & " and " & _
                                "(Lunghezza = " & wLen & " or Byte = " & wLen & ")")
  If rm.RecordCount Then
    If rm!Tipo <> "A" Then
      wFieldXD = rm!Nome
      wIdFieldXD = rm!IdCmp
    Else
      Set rr = m_fun.Open_Recordset("Select * From MgData_Cmp Where " & _
                                    "IdSegmento = " & IdSegFather & " and IdCmp = " & rm!IdCmp & " and Tipo = 'A' ")
      If rr.RecordCount Then
        wLivRif = rr!livello
        Set rk = m_fun.Open_Recordset("Select * From MgData_Cmp Where " & _
                                      "IdSegmento = " & IdSegFather & " and Ordinale > " & rr!Ordinale & " order by Ordinale")
        Do While Not rk.EOF
          wLivMax = rk!livello
          If wLivMax > wLivRif Then
            wFieldXD = rk!Nome
            wIdFieldXD = rk!IdCmp
            ReDim Preserve Key(UBound(Key) + 1)
            Key(UBound(Key)).IdSegmento = idSeg
            Key(UBound(Key)).NomeKey = wFieldXD
            Key(UBound(Key)).IdCmp = wIdFieldXD
          Else
            If wLivMax <= wLivRif Then Exit Do
          End If
          rk.MoveNext
        Loop
        rk.Close
      End If
      rr.Close
    End If
  End If
  rm.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Tabella PILOTA per REDEFINES
' Crea una colonna per ogni FIELD utilizzato per gli accessi
' Crea gli indici corrispondenti
' - Effetto collaterale: idIndex aggiornato
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub createDispatcherIndexDB2(IdIndex As Long, Field As GbField, IdTable As Long)
Public Sub createDispatcherIndex(IdIndex As Long, Field As GbField, IdTable As Long)
  '''Public Sub createDispatcherIndex(IdIndex As Long, Field() As GbField, IdTable As Long)
  Dim rs As Recordset, r As Recordset, rsColonne As Recordset, rsIndex As Recordset
  Dim j As Integer, idColonnaInit As Long
  Dim idTabellaPK As Long
  Dim ordinaleInit As Long
  
  'Scrive le colonne-indici nel DB
  
  'IdColonna iniziale
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne order by idColonna")
  If rs.RecordCount Then
    rs.MoveLast
    idColonnaInit = rs!IdColonna
  Else
    idColonnaInit = 0
  End If
  rs.Close
  
  'Mauro 20/09/2007
  'Ordinale Colonna iniziale
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                "idtable = " & IdTable & " order by idColonna")
  If rs.RecordCount Then
    rs.MoveLast
    ordinaleInit = rs!Ordinale
  Else
    ordinaleInit = 0
  End If
  
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol")
  'silvia 8-4-2008
  If Field.Prov <> "MG-M" Then
  
    For j = 1 To UBound(parentSeqKeys)
      ' Mauro 27-04-2007 : Inserito il controllo idtable <> idtablejoin,
      '                    senn� prendeva il campo della tabella e non quello ereditato dal padre
      Set rsColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                           "idTable=" & IdTable + 1 & " and " & _
                                           "idCmp=" & parentSeqKeys(j).IdCmp & " and " & _
                                           "idtable <> idtablejoin")
      'MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Index Column: " & NomeCol
      'MadmdF_DLI2RDBMS.LswLog.Refresh
      'MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.count).EnsureVisible
      rs.AddNew
      rs!IdColonna = idColonnaInit + j
      rs!IdTable = IdTable
      rs!idTableJoin = getIdTableJoin(parentSeqKeys(j).IdSegmento)
      If Len(inheritedPrefix & idxcol(j).Nome) > 18 Then
        m_fun.WriteLog "##Index Name too long: trunked at char 18 - idTable: " & IdTable & ";Column name: " & inheritedPrefix & idxcol(j).Nome
        rs!Nome = Left(inheritedPrefix & idxcol(j).Nome, 18)
      Else
        rs!Nome = inheritedPrefix & idxcol(j).Nome
      End If
      rs!Ordinale = ordinaleInit + j
      rs!IdCmp = parentSeqKeys(j).IdCmp
      rs!Tipo = rsColonne!Tipo
      rs!lunghezza = rsColonne!lunghezza
      rs!Decimali = rsColonne!Decimali
      rs!Descrizione = "Column migrated from DLI - inherited from parent key"
      rs!RoutLoad = "load_INHERITED"
      rs!RoutRead = ""
      rs!RoutWrite = ""
      rs!RoutKey = "KEY_INHERITED"
      rs!Null = False
      
      'MG 130207
      rs!IsDefault = DEFAULT_DEFAULT
      If DEFAULT_DEFAULT Then
        rs!defaultValue = ""
      Else
        rs!defaultValue = ""
      End If
      '''MG rs!default = False
      rs!Tag = "AUTOMATIC-DISPATCHER"
      rs.Update
      'IDX-COL
      r.AddNew
      r!IdIndex = IdIndex 'verificare
      r!IdTable = IdTable
      r!IdColonna = idColonnaInit + j
      r!Order = "ASCENDING"
      r!Tag = "AUTOMATIC"
      r!Ordinale = ordinaleInit + j
      r.Update
      rsColonne.Close
    Next j
  Else
    j = 1
  End If
''  'MG
''  IdIndex = IdIndex + 1
    
  'AGGIUNTA COLONNA FIELD:
  rs.AddNew
  rs!IdColonna = idColonnaInit + j
  rs!IdTable = IdTable
  rs!idTableJoin = IdTable
  rs!Nome = Field.Nome
  rs!Ordinale = ordinaleInit + j
  rs!IdCmp = Field.IdField  'ATT!
  rs!Tipo = "CHAR"
  rs!lunghezza = Field.lunghezza
  rs!Decimali = 0
  rs!Descrizione = "Dispatcher Column"
  rs!Null = False
  
  'MG 130207
  rs!IsDefault = DEFAULT_DEFAULT
  If DEFAULT_DEFAULT Then
    rs!defaultValue = ""
  End If
  '''MG rs!default = False
  rs!Tag = "AUTOMATIC-DISPATCHER"
  rs!RoutLoad = "load_Field_DISPATCHER"
  rs!RoutRead = "rdbms2dli_Field_DISPATCHER"
  rs!RoutWrite = "dli2rdbms_Field_DISPATCHER"
  rs!RoutKey = "dli2rdbms_KEY_Field_DISPATCHER"
  rs.Update
    
  'inserimento campo IdxCol
  'debug:
  'Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol WHERE IDINDEX=" & IdIndex)
  
  r.AddNew
  r!IdIndex = IdIndex
  r!IdTable = IdTable
  r!IdColonna = idColonnaInit + j
  r!Order = "ASCENDING"
  r!Tag = "AUTOMATIC"
  r!Ordinale = ordinaleInit + j
  r.Update
    
  'silvia 8-4-2008
  If Field.Prov <> "MG-M" Then
    'AGGIUNTA COLONNA "DATA_TABLE":
    rs.AddNew
    rs!IdColonna = idColonnaInit + j + 1
    rs!IdTable = IdTable
    rs!idTableJoin = IdTable
    rs!Nome = "DATA_TABLE"
    rs!Ordinale = ordinaleInit + j + 1
    rs!IdCmp = -1
    rs!Tipo = "CHAR"
    'SQ - sarebbe 128... fare variabile d'ambiente...
    rs!lunghezza = 18
    'rs!lunghezza = 40
    rs!Decimali = 0
    rs!Descrizione = "Dispatcher Column"
    rs!Null = False
    
    'MG 130207
    rs!IsDefault = DEFAULT_DEFAULT
    If DEFAULT_DEFAULT Then
      rs!defaultValue = ""
    End If
    
    rs!Tag = "AUTOMATIC-DISPATCHER"
    rs!RoutLoad = "load_Table_DISPATCHER"
    rs!RoutRead = "rdbms2dli_Table_DISPATCHER"
    rs!RoutWrite = "dli2rdbms_Table_DISPATCHER"
    rs!RoutKey = "dli2rdbms_KEY_Table_DISPATCHER"
    rs.Update
    
    rs.Close
  End If
  r.Close
    
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Foreign Key:
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  If UBound(parentSeqKeys) And Field.Prov <> "MG-M" Then
    ' Trova l'ultimo IdIndex
    'IdIndex = IdIndex + 1
    Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index order by idindex")
    If rs.RecordCount Then
      rs.MoveLast
      IdIndex = rs!IdIndex + 1
    Else
      IdIndex = 1
    End If
    rs.Close
    
    Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where idtable = " & IdTable & " and tipo = 'F'")
    If rs.RecordCount = 0 Then
      rs.AddNew
      rs!IdIndex = IdIndex
      rs!IdTable = IdTable
    ''''''''  rs!IdTableSpc = IdTbs
      rs!Nome = ForeignKeyName
      rs!Tipo = "F"
      rs!Used = True
      rs!Unique = True
      rs!Descrizione = "FOREIGN KEY Index"
      rs!Tag = "AUTOMATIC"
    
      Set r = m_fun.Open_Recordset("Select idTable From " & PrefDb & "_Tabelle where " & _
                                   "idOrigine=" & parentSeqKeys(UBound(parentSeqKeys)).IdSegmento)
      idTabellaPK = r!IdTable
      r.Close
    
      rs!ForeignIdTable = idTabellaPK
      rs.Update
    End If
    rs.Close
    
    Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol")
    For j = 1 To UBound(parentSeqKeys)
      rs.AddNew
      rs!IdIndex = IdIndex
      rs!Ordinale = j
      rs!IdTable = IdTable
      rs!IdColonna = idColonnaInit + j  'idxcol(j).IdColonna
      rs!Order = "ASCENDING"
      rs!Tag = "AUTOMATIC"
      'Sbagliata?!
      Set r = m_fun.Open_Recordset("Select idColonna From " & PrefDb & "_Colonne where " & _
                                   "idCmp = " & parentSeqKeys(j).IdCmp & " and idTable = " & idTabellaPK)
      If r.RecordCount Then
        rs!forIdColonna = r!IdColonna
      Else
        'TMP!
        rs!forIdColonna = 0
      End If
      r.Close
      rs.Update
    Next j
    rs.Close
  End If
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' GESTIONE OCCURS:
' - SOTTOTABELLA OCCURS (PK + OCCURS_I + <COLONNE>)
'   -> crea OCCURS_I + <COLONNE>
'      (PK aggiunte a posteriori in createOccursIndex)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'SQ
'Public Sub addOccursColumns(IdTable As Long, IdTableJoin As Long, IdSeg As Long)
Public Sub addOccursColumns(IdTable As Long, idTableJoin As Long, idSeg As Long, listaOccurs As String)
  Dim rsColonne As Recordset, rsCmp As Recordset
  Dim Adesso As String, NomeNorm As String
  Dim IdColonna As Long, Ordinale As Integer
  Dim i As Long, Ch As String
  Dim idCmpList() As Long
  Dim env As Collection
  
  On Error GoTo errdb
  
  Adesso = Now
  
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  
  Set rsColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
  'OCCURS_I
  MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: OCCURS_I"
  MadmdF_DLI2RDBMS.LswLog.Refresh
  MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  Ordinale = 1
  
  rsColonne.AddNew
  rsColonne!IdColonna = IdColonna
  rsColonne!IdTable = IdTable
  'SQ 19-07-06 - Puntavo alla tabella "origine"... modificato per le WHERE condition...
  rsColonne!idTableJoin = IdTable 'IdTableJoin
  rsColonne!Nome = "KEYSEQ"
  rsColonne!Ordinale = Ordinale
  rsColonne!IdCmp = -1
  rsColonne!NomeCmp = ""
  If TipoDB = "DB2" Then
    rsColonne!Tipo = "SMALLINT"
    rsColonne!lunghezza = 2
  ElseIf TipoDB = "ORACLE" Then
    rsColonne!Tipo = "NUMBER"
    rsColonne!lunghezza = 5
  End If
  rsColonne!Decimali = 0
  rsColonne!Descrizione = "Occurs Index Column"
  rsColonne!Null = False
  
  rsColonne!IsDefault = DEFAULT_DEFAULT
  If DEFAULT_DEFAULT Then
    rsColonne!defaultValue = ""
  Else
    rsColonne!defaultValue = ""
  End If
  
  '''MG rsColonne!default = False
  rsColonne!Tag = "AUTOMATIC-OCCURS"
  rsColonne!Commento = ""
  rsColonne!RoutLoad = "load_OCCURS_I"
  rsColonne!RoutRead = "rdbms2dli_OCCURS_I"
  rsColonne!RoutWrite = "dli2rdbms_OCCURS_I"
  rsColonne!RoutKey = "dli2rdbms_KEY_OCCURS_I"
  rsColonne.Update
  
  IdColonna = IdColonna + 1

  Set rsCmp = m_fun.Open_Recordset("Select * from MgData_Cmp where idCmp in (" & listaOccurs & ") and idsegmento = " & idSeg & _
                                            "And lunghezza > 0 And Nome <> 'FILLER' And Nome <> 'FILLER-REVISER' order by IdCmp")
  While Not rsCmp.EOF
    'SQ
    Set env = New Collection  'resetto;
    env.Add rsCmp
    NomeNorm = getEnvironmentParam(ColNameParam, "CmpName", env, TipoDB)
            
    'Eliminazione caratteri invalidi/sostituzione trattino->underscore
    Ch = ""
    For i = 1 To Len(NomeNorm)
      If Char_Is_Valid(Mid(NomeNorm, i, 1)) Then
        Ch = Ch & Mid(NomeNorm, i, 1)
      End If
    Next i
    NomeNorm = Ch
    
    Ordinale = Ordinale + 1
    If TipoDB = "DB2" Then
      Trascodifica_Tipo_DLI_DB2 rsCmp!Tipo, rsCmp!Usage, rsCmp!lunghezza, rsCmp!Decimali, NomeNorm, rsCmp!segno
    ElseIf TipoDB = "ORACLE" Then
      Trascodifica_Tipo_DLI_ORACLE rsCmp!Tipo, rsCmp!Usage, rsCmp!lunghezza, rsCmp!Decimali, NomeNorm
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Eccezioni nomi chiavi; fuori rotellone per via degli inherit...
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    NomeNorm = Replace(NomeNorm, "-", "_")
                      
    ' Mauro 01/08/2007 : Controllo per l'eliminazione degli underscore finali
    For i = Len(NomeNorm) To 1 Step -1
      If Mid(NomeNorm, i, 1) = "_" Then
        NomeNorm = Mid(NomeNorm, 1, i - 1)
      Else
        Exit For
      End If
    Next i
          
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
    MadmdF_DLI2RDBMS.LswLog.Refresh
    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.Count).EnsureVisible
            
    rsColonne.AddNew
    rsColonne!IdColonna = IdColonna
    rsColonne!IdTable = IdTable
    'SQ 19-07-06 - Puntavo alla tabella "origine"... modificato per le WHERE condition...
    rsColonne!idTableJoin = IdTable 'IdTableJoin
    rsColonne!Nome = NomeNorm
    rsColonne!Ordinale = Ordinale
    rsColonne!IdCmp = rsCmp!IdCmp
    rsColonne!NomeCmp = rsCmp!Nome
    If TipoDB = "DB2" Then
      rsColonne!Tipo = DB2Tipo
      rsColonne!lunghezza = DB2Lunghezza
      rsColonne!Decimali = DB2Decimali
    ElseIf TipoDB = "ORACLE" Then
      rsColonne!Tipo = ORACLETipo
      rsColonne!lunghezza = ORACLELunghezza
      rsColonne!Decimali = ORACLEDecimali
    End If
    rsColonne!Descrizione = "Column migrated from DLI"
    rsColonne!Null = DEFAULT_NULL
    
    'MG 130207 Modificato default
    rsColonne!IsDefault = DEFAULT_DEFAULT
    If DEFAULT_DEFAULT Then
      rsColonne!defaultValue = "" '?? che metto???
    Else
      rsColonne!defaultValue = ""
    End If
    '''MG rsColonne!default = DEFAULT_DEFAULT
    
    rsColonne!Tag = "AUTOMATIC-OCCURS"
    rsColonne!Commento = ""
    If TipoDB = "DB2" Then
      rsColonne!RoutLoad = "load_" & DB2Tipo & "_OCCURS"
      rsColonne!RoutRead = "rdbms2dli_" & DB2Tipo & "_OCCURS"
      rsColonne!RoutWrite = "dli2rdbms_" & DB2Tipo & "_OCCURS"
      rsColonne!RoutKey = "dli2rdbms_KEY_" & DB2Tipo & "_OCCURS"
    ElseIf TipoDB = "ORACLE" Then
      rsColonne!RoutLoad = "load_" & ORACLETipo & "_OCCURS"
      rsColonne!RoutRead = "rdbms2dli_" & ORACLETipo & "_OCCURS"
      rsColonne!RoutWrite = "dli2rdbms_" & ORACLETipo & "_OCCURS"
      rsColonne!RoutKey = "dli2rdbms_KEY_" & ORACLETipo & "_OCCURS"
    End If
    rsColonne.Update
    
    IdColonna = IdColonna + 1
        
    rsCmp.MoveNext
  Wend
  rsCmp.Close
  rsColonne.Close
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' GESTIONE OCCURS:
' - SOTTOTABELLA OCCURS (PK + OCCURS_I + <COLONNE>)
'   -> crea PK e inserisce le colonne ereditate
'   -> crea FK
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub createOccursIndex(rsTable As Recordset)
  Dim rsOrig As Recordset, rsNew As Recordset, rOrig As Recordset, rNew As Recordset, rsIndexOrig As Recordset
  Dim i As Integer, j As Integer, columnsCount As Integer
  Dim IdColonna As Long, IdIndex As Long, idIndexOrig As Long
  Dim env As Collection
  
  On Error GoTo errdb
  
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  IdIndex = getId("IdIndex", PrefDb & "_Index")
  
  Dim idColonnaInit As Long
  
  While Not rsTable.EOF
    'INDICE
    Set rsIndexOrig = m_fun.Open_Recordset( _
      "Select * From " & PrefDb & "_Index where idTable=" & rsTable!idTableJoin & " and Tipo='P'")
    If Not rsIndexOrig.EOF Then
      idIndexOrig = rsIndexOrig!IdIndex
      Set rsNew = rsIndexOrig.Clone
      rsNew.AddNew
      For i = 1 To rsIndexOrig.fields.Count - 1
        If Not IsNull(rsIndexOrig.fields.Item(i)) Then
          rsNew.fields.Item(i) = rsIndexOrig.fields.Item(i)
        End If
      Next
      '''
      'SILVIA 11-03-2008 gestione nome tabella indici PK per tabelle child con naming convention
''      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
''        Set env = New Collection  'resetto;
''        env.Add ""
''        env.Add rsTable!IdTable
''        env.Add 1
''        rsNew!Nome = getEnvironmentParam(IndexNameParam, "Index", env, TipoDB)
''      Else
      
      'SQ tmp - gestione nomi duplicati:
        rsNew!Nome = rsNew!Nome & "T"
''      End If
      
      rsNew!IdIndex = IdIndex
      rsNew!IdTable = rsTable!IdTable
      rsNew!Tag = "AUTOMATIC-OCCURS"
      rsNew.Update
      
      'SILVIA 11-03-2008 gestione nome tabella indici PK per tabelle child con naming convention
      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set env = New Collection  'resetto;
        env.Add ""
        env.Add rsTable!IdTable
        env.Add 1
        rsNew!Nome = getEnvironmentParam(IndexNameParam, "Index", env, TipoDB)
      End If
      
      
      rsNew.Update
      rsNew.Close
      
      'IDXCOL
      Set rsOrig = m_fun.Open_Recordset( _
        "Select * From " & PrefDb & "_IdxCol where idIndex=" & idIndexOrig)
      columnsCount = rsOrig.RecordCount  'lo devo bloccare, perch� con le clone aumenta!
            
      'Update Ordinale Colonne Sottotabella Occurs
      DataMan.DmConnection.Execute _
        "Update " & PrefDb & "_Colonne set Ordinale=Ordinale+" & rsOrig.RecordCount & _
        " where idTable=" & rsTable!IdTable
      
      Set rsNew = rsOrig.Clone
      
      idColonnaInit = IdColonna
      For j = 1 To columnsCount
        'COLONNA
        Set rOrig = m_fun.Open_Recordset( _
          "Select * From " & PrefDb & "_Colonne where idColonna=" & rsOrig!IdColonna)
        Set rNew = rOrig.Clone
        rNew.AddNew
        For i = 1 To rOrig.fields.Count - 1
          If Not IsNull(rOrig.fields.Item(i)) Then
            rNew.fields.Item(i) = rOrig.fields.Item(i)
          End If
        Next
        rNew!IdColonna = IdColonna
        rNew!IdTable = rsTable!IdTable
        'SQ - 19-07-06
        'rNew!IdTableJoin = rsOrig!IdTableJoin
        'SILVIA 07-04-2008: per la REVKEY come chiave ereditata occorre mettere il template load_INHERITED
        If rOrig!idTableJoin <> rsTable!IdTable Then
          If rOrig!Nome = "REVKEY" Then
            rNew!RoutLoad = "load_INHERITED"
          End If
        End If
        rNew!Tag = "AUTOMATIC-OCCURS"
        rNew.Update
        rOrig.Close
        rNew.Close
        
        'Colonna di IDXCOL
        rsNew.AddNew
        For i = 1 To rsOrig.fields.Count - 1
          If Not IsNull(rsOrig.fields.Item(i)) Then
            rsNew.fields.Item(i) = rsOrig.fields.Item(i)
          End If
        Next
        rsNew!IdIndex = IdIndex
        rsNew!IdTable = rsTable!IdTable
        rsNew!IdColonna = IdColonna
        rsNew!Tag = "AUTOMATIC-OCCURS"
        rsNew.Update
        
        IdColonna = IdColonna + 1
        
        rsOrig.MoveNext
      Next
      rsOrig.Close
            
      'Colonna di IDXCOL: OCCURS_I
      'Mi serve l'idColonna:
      Set rOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                       "idTable=" & rsTable!IdTable & " and ordinale=" & columnsCount + 1)
      rsNew.AddNew
      rsNew!IdIndex = IdIndex
      rsNew!Ordinale = columnsCount + 1
      rsNew!IdTable = rsTable!IdTable
      rsNew!IdColonna = rOrig!IdColonna
      rsNew!Order = "ASCENDING"
      rsNew!Tag = "AUTOMATIC-OCCURS"
      rsNew.Update
      rsNew.Close
      rOrig.Close
    End If
    rsIndexOrig.Close
    
    IdIndex = IdIndex + 1
    
    Set rsIndexOrig = m_fun.Open_Recordset( _
      "Select * From " & PrefDb & "_Index where idTable=" & rsTable!idTableJoin & " and Tipo='F'")
    If rsIndexOrig.RecordCount Then
      'SILVIA 17-04-2008 idIndexOrig � quella della madre
      'idIndexOrig = rsIndexOrig!IdIndex
      Set rsNew = rsIndexOrig.Clone
      rsNew.AddNew
      For i = 1 To rsIndexOrig.fields.Count - 1
        If Not IsNull(rsIndexOrig.fields.Item(i)) Then
          rsNew.fields.Item(i) = rsIndexOrig.fields.Item(i)
        End If
      Next
      rsNew!IdIndex = IdIndex
      rsNew!IdTable = rsTable!IdTable
      'SILVIA 17-04-2008 : la foreign key deve essere riferita alla tabella madre..
      rsNew!ForeignIdTable = rsTable!idTableJoin
      rsNew!Tag = "AUTOMATIC-OCCURS"
            '''
      'SILVIA 11-03-2008 gestione nome tabella indici PK per tabelle child con naming convention
      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        'Foreign Key Name:
        Set env = New Collection
        env.Add rsTable!IdTable
        rsNew!Nome = getEnvironmentParam(FkNameParam, "tabella", env, TipoDB)
      End If
      '''
      rsNew.Update
      rsNew.Close
      
      'IDXCOL
      Set rsOrig = m_fun.Open_Recordset( _
        "Select * From " & PrefDb & "_IdxCol where idIndex=" & idIndexOrig)
      columnsCount = rsOrig.RecordCount  'lo devo bloccare, perch� con le clone aumenta!
      
      Set rsNew = rsOrig.Clone
      For j = 1 To columnsCount
        'COLONNA
        Set rOrig = m_fun.Open_Recordset( _
          "Select * From " & PrefDb & "_Colonne where idColonna=" & rsOrig!IdColonna)
        
        'Colonna di IDXCOL
        rsNew.AddNew
        For i = 1 To rsOrig.fields.Count - 1
          If Not IsNull(rsOrig.fields.Item(i)) Then
            rsNew.fields.Item(i) = rsOrig.fields.Item(i)
          End If
        Next
        rsNew!IdIndex = IdIndex
        rsNew!IdTable = rsTable!IdTable
        rsNew!IdColonna = idColonnaInit + j - 1
        'SILVIA AHHH
        rsNew!forIdColonna = rOrig!IdColonna
        rsNew!Tag = "AUTOMATIC-OCCURS"
        rsNew.Update
        
        rsOrig.MoveNext
      Next
      rsOrig.Close
      
      IdIndex = IdIndex + 1
    End If
    rsIndexOrig.Close
    
    rsTable.MoveNext
  Wend
  'rsTable.Close  'chiudo fuori!
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub
''''''''''''''
'SQ 13-11-07
'Portare qui tutti quelli necessari;
'Da chiamare al caricamento dei form...
''''''''''''''
Private Sub getEnvironmentParameters()
  DbNameParam = LeggiParam("IMSDB_DBNAME")
  If Len(DbNameParam) = 0 Then DbNameParam = "(DBD-NAME)"
  TableNameParam = LeggiParam("IMSDB_TBNAME")
  TableChNameParam = LeggiParam("IMSDB_TBNAMEC")
  ColNameParam = LeggiParam("IMSDB_COLNAME")
  If Len(ColNameParam) = 0 Then ColNameParam = "(CMP-NAME#1-18#)"
  'Prefisso nomi colonne ereditate
  inheritedPrefix = LeggiParam("IMSDB-INHERITED-PREFIX")
  'PRIMARY KEY
  IndexNameParam = LeggiParam("IMSDB_IDXNAME")
  If Len(IndexNameParam) = 0 Then IndexNameParam = "(FIELD-NAME)"
  'ALTERNATIVE KEY
  AkNameParam = LeggiParam("IMSDB-AK-NAME")
  If Len(AkNameParam) = 0 Then AkNameParam = "(FIELD-NAME)"
  'FOREIGN KEY
  FkNameParam = LeggiParam("IMSDB-FK-NAME")
  'silvia nome dello Stogroup
  Stgroup = LeggiParam("IMSDB_STOGROUP")
  If Len(Stgroup) = 0 Then Stgroup = "<storageGroup>"

  ' � nella funzione dei default... DEFAULT_CREATOR = LeggiParam("DM_CREATOR")
End Sub

Private Sub lswDBSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswDBSel.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswDBSel.SortOrder = Order
  lswDBSel.SortKey = Key - 1
  lswDBSel.Sorted = True
End Sub
