VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_Indici"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdIndex As Long
Private m_Nome As String
Private m_Tipo As String
Private m_IdOrigine As Long
Private m_Used As Boolean
Private m_Unique As Boolean
Private m_Descrizione As String
Private m_DataCreazione As String
Private m_DataModifica As String
Private m_Tag As String

'Contiene le colonne che fanno parte dell'indice
Private m_Colonne As New Collection

'id Index
Public Property Get IdIndex() As Long
   IdIndex = m_IdIndex
End Property

Public Property Let IdIndex(cIdIndex As Long)
   m_IdIndex = cIdIndex
End Property

'NOME
Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let Nome(cNome As String)
   m_Nome = cNome
End Property

'TIPO
Public Property Get Tipo() As String
   Tipo = m_Tipo
End Property

Public Property Let Tipo(cTipo As String)
   m_Tipo = cTipo
End Property

'ID ORIGINE
Public Property Get IdOrigine() As Long
   IdOrigine = m_IdOrigine
End Property

Public Property Let IdOrigine(cIdOrigine As Long)
   m_IdOrigine = cIdOrigine
End Property

'USED
Public Property Get Used() As Boolean
   Used = m_Used
End Property

Public Property Let Used(mUsed As Boolean)
   m_Used = mUsed
End Property

'UNIQUE
Public Property Get Unique() As Boolean
   Unique = m_Unique
End Property

Public Property Let Unique(mUnique As Boolean)
   m_Unique = mUnique
End Property

'DESCRIZIONE
Public Property Get Descrizione() As String
   Descrizione = m_Descrizione
End Property

Public Property Let Descrizione(cDescrizione As String)
   m_Descrizione = cDescrizione
End Property

'DATA CR
Public Property Get DataCreazione() As String
   DataCreazione = m_DataCreazione
End Property

Public Property Let DataCreazione(cDataCreazione As String)
   m_DataCreazione = cDataCreazione
End Property

'DATA MOD
Public Property Get DataModifica() As String
   DataModifica = m_DataModifica
End Property

Public Property Let DataModifica(cDataModifica As String)
   m_DataModifica = cDataModifica
End Property

'TAG
Public Property Get Tags() As String
   Tags = m_Tag
End Property

Public Property Let Tags(cTag As String)
   m_Tag = cTag
End Property

'COLONNE
Public Sub AddColonna(colonna As MadmdC_IdxCol)
   m_Colonne.Add colonna
End Sub

Public Function ColonneCount() As Long
   ColonneCount = m_Colonne.Count
End Function

Public Sub RemoveColonna(Index As Long)
   m_Colonne.Remove (Index)
End Sub

Public Function ItemColonna(Index As Long) As MadmdC_IdxCol
   Set ItemColonna = m_Colonne.Item(Index)
End Function

Public Sub ClearColonne()
   Set m_Colonne = New Collection
End Sub

