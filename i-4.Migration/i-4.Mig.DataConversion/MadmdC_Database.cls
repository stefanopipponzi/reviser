VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_Database"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdTableSpace As Long
Private m_IdDatabase As Long
Private m_IdOrigine As Long
Private m_Nome As String
Private m_DataCreazione As String
Private m_DataModifica As String
Private m_Descrizione As String
Private m_Tags As String
Private m_Tabelle As New Collection

'ID TABLESPACE
Public Property Get IdTableSpace() As Long
   IdTableSpace = m_IdTableSpace
End Property

Public Property Let IdTableSpace(cIdTableSpace As Long)
   m_IdTableSpace = cIdTableSpace
End Property

'ID DATABASE
Public Property Get IdDatabase() As Long
   IdDatabase = m_IdDatabase
End Property

Public Property Let IdDatabase(cIdDb As Long)
   m_IdDatabase = cIdDb
End Property

'ID ORIGINE
Public Property Get IdOrigine() As Long
   IdOrigine = m_IdOrigine
End Property

Public Property Let IdOrigine(cIdOr As Long)
   m_IdOrigine = cIdOr
End Property

'NOME
Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let Nome(cNome As String)
   m_Nome = cNome
End Property

'DATA CR
Public Property Get DataCreazione() As String
   DataCreazione = m_DataCreazione
End Property

Public Property Let DataCreazione(cDataCreazione As String)
   m_DataCreazione = cDataCreazione
End Property

'DATA MOD
Public Property Get DataModifica() As String
   DataModifica = m_DataModifica
End Property

Public Property Let DataModifica(cDataModifica As String)
   m_DataModifica = cDataModifica
End Property

'DESCRIZIONE
Public Property Get Descrizione() As String
   Descrizione = m_Descrizione
End Property

Public Property Let Descrizione(cDescrizione As String)
   m_Descrizione = cDescrizione
End Property

'TAG
Public Property Get Tags() As String
   Tags = m_Tags
End Property

Public Property Let Tags(cTags As String)
   m_Tags = cTags
End Property
