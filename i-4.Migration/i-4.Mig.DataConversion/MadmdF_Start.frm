VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form MadmdF_Start 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   6495
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6495
   ScaleWidth      =   8370
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7620
      Top             =   2730
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":015A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":06F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":084E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":09A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":0DFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":16D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":19EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":1B48
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Start.frx":212E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   5715
      Left            =   30
      TabIndex        =   6
      Top             =   750
      Width           =   8265
      Begin VB.TextBox txtFilter 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Frame FraDb 
         Height          =   1335
         Left            =   30
         TabIndex        =   7
         Top             =   4380
         Width           =   8235
         Begin RichTextLib.RichTextBox rtbKey 
            Height          =   165
            Left            =   360
            TabIndex        =   19
            Top             =   1020
            Visible         =   0   'False
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   291
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"MadmdF_Start.frx":26C8
         End
         Begin RichTextLib.RichTextBox rtbLocal 
            Height          =   195
            Left            =   660
            TabIndex        =   17
            Top             =   1020
            Visible         =   0   'False
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   344
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"MadmdF_Start.frx":274A
         End
         Begin RichTextLib.RichTextBox rtbFile 
            Height          =   165
            Left            =   180
            TabIndex        =   16
            Top             =   1020
            Visible         =   0   'False
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   291
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"MadmdF_Start.frx":27CC
         End
         Begin VB.TextBox txtMdDate 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   6660
            TabIndex        =   11
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtDescrizione 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   525
            Left            =   1200
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Top             =   720
            Width           =   6945
         End
         Begin VB.TextBox txtCrDate 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   3900
            TabIndex        =   9
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtNomeDB 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   660
            MaxLength       =   18
            TabIndex        =   8
            Top             =   240
            Width           =   1845
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Update Date"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   5430
            TabIndex        =   15
            Top             =   300
            Width           =   1050
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Description"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   90
            TabIndex        =   14
            Top             =   750
            Width           =   975
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Creation Date"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   2670
            TabIndex        =   13
            Top             =   300
            Width           =   1170
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Name"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   300
            Width           =   450
         End
      End
      Begin MSComctlLib.ListView LswExistObj 
         Height          =   4335
         Left            =   0
         TabIndex        =   18
         Top             =   0
         Width           =   8265
         _ExtentX        =   14579
         _ExtentY        =   7646
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Name"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Type"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Creation Date"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Last Modify Date"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Description"
            Object.Width           =   3528
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid Filter_List 
         Height          =   915
         Left            =   0
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   8265
         _ExtentX        =   14579
         _ExtentY        =   1614
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         BackColor       =   -2147483624
         ForeColor       =   12582912
         BackColorSel    =   16777088
         ForeColorSel    =   16711680
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H8000000D&
      BorderStyle     =   0  'None
      Height          =   345
      Left            =   30
      TabIndex        =   1
      Top             =   390
      Width           =   8265
      Begin VB.OptionButton OptSelDLI 
         BackColor       =   &H8000000D&
         Caption         =   "D L I"
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   4500
         TabIndex        =   5
         Top             =   70
         Value           =   -1  'True
         Width           =   705
      End
      Begin VB.OptionButton OptSelVsam 
         BackColor       =   &H8000000D&
         Caption         =   "F i l e s"
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   6300
         TabIndex        =   4
         Top             =   70
         Width           =   870
      End
      Begin VB.OptionButton OptSelOracle 
         BackColor       =   &H8000000D&
         Caption         =   "O r a c l e"
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Top             =   60
         Width           =   1020
      End
      Begin VB.OptionButton OptSelDB2 
         BackColor       =   &H8000000D&
         Caption         =   "D B 2"
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   2190
         TabIndex        =   2
         Top             =   70
         Width           =   765
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8370
      _ExtentX        =   14764
      _ExtentY        =   635
      ButtonWidth     =   2540
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   12
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Open"
            Key             =   "Open"
            Object.ToolTipText     =   "Database Viewer"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "New"
            Key             =   "NEWDB"
            Object.ToolTipText     =   "Create new Database"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Erase"
            Key             =   "Erase"
            Object.ToolTipText     =   "Erase selected DB..."
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Create"
            Key             =   "CREATECOMP"
            Object.ToolTipText     =   "Create components..."
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   13
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Programmi"
                  Text            =   "Program load"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "JCL"
                  Text            =   "Jcl Load"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Copy"
                  Text            =   "Structure copy"
               EndProperty
               BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Move_Copy"
                  Text            =   "Move Copy"
               EndProperty
               BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "MoveKey_Copy"
                  Text            =   "Move-Key Copy"
               EndProperty
               BeginProperty ButtonMenu8 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Load_Copy"
                  Text            =   "Load Copy"
               EndProperty
               BeginProperty ButtonMenu9 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Routine_Copy"
                  Text            =   "All-Procedure copy"
               EndProperty
               BeginProperty ButtonMenu10 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu11 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SQL"
                  Text            =   "DDL files"
               EndProperty
               BeginProperty ButtonMenu12 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "-"
               EndProperty
               BeginProperty ButtonMenu13 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Trigger"
                  Text            =   "Trigger"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Report"
            Key             =   "Report"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "DBD details"
            Key             =   "Properties"
            Object.ToolTipText     =   "DBDs Properties"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Imp. Structures"
            Key             =   "ImpStr"
            Object.ToolTipText     =   "Import Structures"
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refresh Areas"
            Key             =   "Refr_Area"
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Filter"
            Key             =   "Filter_Conf"
            Object.ToolTipText     =   "Filter Configuration"
            Style           =   1
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Active Filter"
            Key             =   "Filter_Exe"
            Object.ToolTipText     =   "Filter Execution"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog ComD 
      Left            =   3120
      Top             =   6840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "MadmdF_Start"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'SQ - scelte menu
Dim createCopyMoveKey As Boolean, createCopyMove As Boolean, createCopyLoad As Boolean
Public formParent1 As Form

Private Sub Filter_List_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Dim XN As Long, yN As Long
  Dim i As Long
  
  'assegna alla globale la colonna selezionata
  Filter_List_Column = Filter_List.ColSel
  'txtFilter = ""
  txtFilter.text = ""
  txtFilter.Visible = True
  txtFilter.SetFocus
  
  'prende le coordinate
  For i = 0 To Filter_List.Cols - 1
    If i < Filter_List_Column Then
      Filter_List.Col = i
      Filter_List.Row = 1
      XN = XN + Filter_List.CellWidth
    Else
      Exit For
    End If
  Next i
  Filter_List.Row = 0
  Filter_List.Col = 0
  yN = Filter_List.CellHeight
  
  'posiziona la text box
  Filter_List.Row = 1
  Filter_List.Col = Filter_List_Column
  
  txtFilter.Top = Filter_List.Top + Filter_List.CellTop
  txtFilter.Left = Filter_List.Left + Filter_List.CellLeft
  txtFilter.Width = Filter_List.CellWidth
  txtFilter.Height = Filter_List.CellHeight
  
  Filter_List.Refresh
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  LswExistObj.ColumnHeaders(LswExistObj.ColumnHeaders.count).Width = withTot(LswExistObj)
End Sub

Private Sub cmdExit_Click()
  Unload Me
End Sub

Public Function Add_Db_In_DbSys() As Long
  'Aggiunge il Db nel db systema e restituisce l'id
  Dim rs As Recordset
  Dim idDat As Long
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   
  Select Case Trim(UCase(TipoDB))
    Case "DB2", "ORACLE"
      Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB")

      'Aggiunge il record
      rs.AddNew
      rs!iddbor = -1
      rs!Nome = "TEMPDB_NAME"
      rs!Data_Creazione = Now
      rs!Descrizione = ""
      rs!Tag = ""
      rs.Update

      idDat = rs!idDB
      rs.Close

      'Va in update sul nome
      Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & idDat)
      If rs.RecordCount Then
        rs!Nome = "NEWDB" & Format(idDat, "0000")
        rs.Update
        rs.Close
      End If
  
    Case "VSAM"
        '...
  End Select

  'cDb.Nome = "NEWDB" & Format(idDat, "0000")

  Add_Db_In_DbSys = idDat
End Function

Private Sub Form_Load()
  Dim rs As Recordset
  Dim wTip As Boolean
  
  Toolbar1.ImageList = ImageList1
  Toolbar1.Buttons(1).image = 5
  Toolbar1.Buttons(3).image = 4
  Toolbar1.Buttons(4).image = 1
  Toolbar1.Buttons(6).image = 3
  Toolbar1.Buttons(7).image = 5
  Toolbar1.Buttons(8).image = 6
  Toolbar1.Buttons(9).image = 7
  Toolbar1.Buttons(10).image = 10
  Toolbar1.Buttons(11).image = 9
  Toolbar1.Buttons(12).image = 8
  
  OpenMode = 1 'NEW
     
  NomeDatabaseCorrente = ""
  If m_fun.Type_License = LIC_REV_NOIMS Or m_fun.Type_License = LIC_REV_NODLIIMS Or m_fun.Type_License = LIC_REV_NOAGG Then
    OptSelDLI.Visible = False
    TipoDB = m_fun.GetFirstParam("DM_DEFSQLTYPE")
    If Trim(TipoDB) = "" Then TipoDB = "DB2"
    If TipoDB = "DB2" Then
      OptSelDB2.Value = True
    ElseIf TipoDB = "ORACLE" Then
      OptSelOracle.Value = True
    End If
  Else
    OptSelDLI.Visible = True
    TipoDB = "DLI"
    OptSelDLI.Value = True
  End If
  
  'Ottiene Un id disponibile
  wTip = False
  Select Case Trim(UCase(TipoDB))
    Case "DLI"
      wTip = True
    Case "DB2"
      Set rs = m_fun.Open_Recordset("Select MAX(IdDb) From DMDB2_DB")
    Case "ORACLE"
      Set rs = m_fun.Open_Recordset("Select MAX(IdDb) From DMORC_DB")
    Case "VSAM"
  End Select

  If Not wTip Then
    If rs.RecordCount Then
      If IsNull(rs.fields(0)) Then
        pIndex_NewDB = 1
      Else
        pIndex_NewDB = rs.fields(0) + 1
      End If
    End If
    rs.Close
  End If
  
  On Error Resume Next
  
  OptSelDLI.Enabled = True
  OptSelOracle.Enabled = True
  OptSelVsam.Enabled = True
  
  OpenMode = 2 'EXIST

  LswExistObj.ListItems.Clear
  
  Select Case Trim(UCase(TipoDB))
    Case "DLI"
      OptSelDLI_Click
      OptSelDLI.Value = True
    Case "DB2"
      OptSelDB2_Click
      OptSelDB2.Value = True
    Case "ORACLE"
      OptSelOracle_Click
      OptSelOracle.Value = True
    Case "VSAM"
      OptSelVsam.Value = True
  End Select
  
  'SQ 21-11-07
  getEnvironmentParameters
  
  m_fun.FnActiveWindowsBool = True
  m_fun.AddActiveWindows Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub LswExistObj_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = LswExistObj.SortOrder
  Key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  'ListObjCol = Key
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LswExistObj.SortOrder = Order
  LswExistObj.SortKey = Key - 1
  LswExistObj.Sorted = True
End Sub

Private Sub LswExistObj_DblClick()
  'ALE
  Dim myButton As MSComctlLib.Button
  
  Set myButton = Toolbar1.Buttons(1)
  myButton.Caption = "Open"
  Toolbar1_ButtonClick myButton
End Sub

Private Sub LswExistObj_ItemClick(ByVal Item As MSComctlLib.ListItem)
  'assegna il nome DB alla globale
  Dim rs As Recordset
  
  pNome_DBCorrente = Item.text
  pIndexDBSelected = Item.Index
  
  'Recupera l'id del database selezionato
  pIndex_DBCorrente = Item.Tag
  txtNomeDB.Enabled = False
  txtDescrizione.Enabled = False
  txtCrDate.Enabled = False
  txtMdDate.Enabled = False
  txtNomeDB.text = ""
  txtDescrizione.text = ""
  txtCrDate.text = ""
  txtMdDate.text = ""
  
  txtNomeDB.Enabled = True
  txtDescrizione.Enabled = True
  txtCrDate.Enabled = True
  txtMdDate.Enabled = True
  
  Toolbar1.Buttons(6).ButtonMenus(1).Enabled = False
  Toolbar1.Buttons(6).ButtonMenus(4).Enabled = False
  'SP corso
  Toolbar1.Buttons(6).ButtonMenus(3).Enabled = False
  
  If TipoDB = "DB2" Or TipoDB = "ORACLE" Then
    'PrefDb = get_PrefixQuery_TipoDB(TipoDB)
    Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
    If rs.RecordCount Then
      'Riempie le textbox
      txtNomeDB.text = rs!Nome
      txtDescrizione.text = rs!Descrizione
      txtCrDate.text = rs!Data_Creazione
      txtMdDate.text = TN(rs!Data_Modifica)
    Else
      txtNomeDB.text = ""
      txtDescrizione.text = ""
      txtCrDate.text = ""
      txtMdDate.text = ""
    End If
    If TipoDB = "DB2" Then
      AbilitaMenuDB2 Item.Index
    ElseIf TipoDB = "ORACLE" Then
      AbilitaMenuOracle Item.Index
    End If
  ElseIf TipoDB = "DLI" Then
    txtNomeDB.Enabled = False
    txtDescrizione.Enabled = False
    txtCrDate.Enabled = False
    txtMdDate.Enabled = False

    Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & pIndex_DBCorrente)
    If rs.RecordCount Then
      'Riempie le textbox
      txtNomeDB.text = rs!Nome
      txtDescrizione.text = "DLI Database"
      txtCrDate.text = rs!DtImport & ""
      txtMdDate.text = rs!DtImport & ""
    Else
      txtNomeDB.text = ""
      txtDescrizione.text = ""
      txtCrDate.text = ""
      txtMdDate.text = ""
    End If
  Else
    '  per files
    txtNomeDB.Enabled = False
    txtDescrizione.Enabled = False
    txtCrDate.Enabled = False
    txtMdDate.Enabled = False
    
    Set rs = m_fun.Open_Recordset("Select * From DMVSM_Archivio Where Idarchivio = " & pIndex_DBCorrente)
    If rs.RecordCount Then
      'Riempie le textbox
      txtNomeDB.text = rs!Nome
      txtDescrizione.text = rs!Tag & ""
      txtCrDate.text = rs!Data_Creazione & ""
      txtMdDate.text = rs!Data_Modifica & ""
    Else
      txtNomeDB.text = ""
      txtDescrizione.text = ""
      txtCrDate.text = ""
      txtMdDate.text = ""
    End If
  End If
End Sub

Private Sub OptSelDB2_Click()
  Dim tb As Recordset
  
  TipoDB = "DB2"
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
    'SILVIA 21-03-2008
  Toolbar1.Buttons(9).Visible = False
  ' Mauro 13/01/2011
  Toolbar1.Buttons(10).Visible = False
  Toolbar1.Buttons(11).Visible = False
  Toolbar1.Buttons(12).Visible = False
  Filter_List.Visible = False
  LswExistObj.Visible = True

  Toolbar1.Buttons(8).Enabled = False
  Toolbar1.Buttons(8).Caption = "... Details"
  SettaFileBase
  'carica la lista con gli oggetti DATABASE Presenti in tutto il progetto
  Carica_Lista_Oggetti_Esistenti LswExistObj, TipoDB

  If LswExistObj.ListItems.count > 0 Then
    Toolbar1.Buttons(1).Enabled = True
    Toolbar1.Buttons(6).Enabled = True
    Toolbar1.Buttons(2).Enabled = True
    'SP corso
    'Toolbar1.Buttons(3).Enabled = True
    Toolbar1.Buttons(3).Enabled = False
    Toolbar1.Buttons(4).Enabled = True
    Toolbar1.Buttons(5).Enabled = True
  Else
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(6).Enabled = False
    Toolbar1.Buttons(2).Enabled = False
    Toolbar1.Buttons(3).Enabled = True
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(5).Enabled = False
    txtCrDate.Enabled = False
    txtDescrizione.Enabled = False
    txtMdDate.Enabled = False
    txtNomeDB.Enabled = False
    txtNomeDB.text = ""
    txtDescrizione.text = ""
    txtCrDate.text = ""
    txtMdDate.text = ""
  End If
  
'  If LswExistObj.ListItems.Count Then
'    Set tb = m_fun.Open_Recordset("select * from DMDB2_DB where nome = '" & LswExistObj.SelectedItem.text & "'")
'    If tb.RecordCount = 0 Then
'    ElseIf tb!iddbor = -1 Then
'    End If
'    tb.Close
'  End If
  AbilitaMenuDB2 1
End Sub

Private Sub OptSelDLI_Click()
  TipoDB = "DLI"
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  SettaFileBase
  
  'carica la lista con gli oggetti DATABASE Presenti in tutto il progetto
  Carica_Lista_Oggetti_Esistenti LswExistObj, TipoDB
  
  If LswExistObj.ListItems.count Then
    Toolbar1.Buttons(1).Enabled = True
  Else
    Toolbar1.Buttons(1).Enabled = False
    txtCrDate.Enabled = False
    txtDescrizione.Enabled = False
    txtMdDate.Enabled = False
    txtNomeDB.Enabled = False
    txtNomeDB.text = ""
    txtDescrizione.text = ""
    txtCrDate.text = ""
    txtMdDate.text = ""
  End If
  Toolbar1.Buttons(2).Enabled = False
  Toolbar1.Buttons(3).Enabled = False
  Toolbar1.Buttons(4).Enabled = False
  Toolbar1.Buttons(5).Enabled = False
  Toolbar1.Buttons(6).Enabled = False
  Toolbar1.Buttons(8).Enabled = True
  'SILVIA 21-03-2008
  Toolbar1.Buttons(9).Visible = False
  ' Mauro 13/01/2011
  Toolbar1.Buttons(10).Visible = False
  Toolbar1.Buttons(11).Visible = False
  Toolbar1.Buttons(12).Visible = False
  Filter_List.Visible = False
  LswExistObj.Visible = True
  
  Toolbar1.Buttons(8).Caption = "DBD Details"
End Sub

Private Sub OptSelOracle_Click()
  Dim tb As Recordset
  
  TipoDB = "ORACLE"
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
    
  'SILVIA 21-03-2008
  Toolbar1.Buttons(9).Visible = False
  ' Mauro 13/01/2011
  Toolbar1.Buttons(10).Visible = False
  Toolbar1.Buttons(11).Visible = False
  Toolbar1.Buttons(12).Visible = False
  Filter_List.Visible = False
  LswExistObj.Visible = True
  
  Toolbar1.Buttons(8).Enabled = False
  Toolbar1.Buttons(8).Caption = "... Details"
  SettaFileBase
  'carica la lista con gli oggetti DATABASE Presenti in tutto il progetto
  Carica_Lista_Oggetti_Esistenti LswExistObj, TipoDB

  If LswExistObj.ListItems.count > 0 Then
    Toolbar1.Buttons(1).Enabled = True
    Toolbar1.Buttons(6).Enabled = True
    Toolbar1.Buttons(2).Enabled = True
    'SP corso
    'Toolbar1.Buttons(3).Enabled = True
    Toolbar1.Buttons(3).Enabled = False
    Toolbar1.Buttons(4).Enabled = True
    Toolbar1.Buttons(5).Enabled = True
  Else
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(6).Enabled = False
    Toolbar1.Buttons(2).Enabled = False
    Toolbar1.Buttons(3).Enabled = True
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(5).Enabled = False
    txtCrDate.Enabled = False
    txtDescrizione.Enabled = False
    txtMdDate.Enabled = False
    txtNomeDB.Enabled = False
    txtNomeDB = ""
    txtDescrizione = ""
    txtCrDate = ""
    txtMdDate = ""
  End If
  If LswExistObj.ListItems.count Then
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("SQL").Enabled = True
  End If
  
  AbilitaMenuOracle 1
End Sub

Private Sub OptSelVsam_Click()
  TipoDB = "VSAM"
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   
  Screen.MousePointer = vbHourglass
  Toolbar1.Buttons(8).Enabled = True
  Toolbar1.Buttons(8).Caption = "VSAM Details"
  'carica la lista con gli oggetti DATABASE Presenti in tutto il progetto
  Carica_Lista_Oggetti_Esistenti LswExistObj, TipoDB
  If LswExistObj.ListItems.count = 0 Then
    Toolbar1.Buttons(1).Enabled = False
  Else
    Toolbar1.Buttons(1).Enabled = True
    LswExistObj_ItemClick LswExistObj.ListItems.Item(1)
  End If
  Toolbar1.Buttons(3).Enabled = False
  Toolbar1.Buttons(4).Enabled = False
  Toolbar1.Buttons(6).Enabled = False
  'SILVIA 21-03-2008
  Toolbar1.Buttons(9).Visible = True
  ' Mauro 13/01/2011
  Toolbar1.Buttons(10).Visible = True
  Toolbar1.Buttons(11).Visible = True
  Toolbar1.Buttons(12).Visible = True
  Toolbar1.Buttons(11).Value = 0
  Toolbar1.Buttons(12).Value = 0

  Filter_List.Visible = False
  LswExistObj.Visible = True
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim tb As Recordset
  Dim pIndex_DBCorrente_old As String, pNome_DBCorrente_old As String
  Dim i As Integer, myItem As ListItem, ICountSel As Integer
  
  Select Case Trim(Button.Key)
    Case "Report"
      If MsgBox("Do you want create the Report Table? ", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
        Screen.MousePointer = vbHourglass
        'valori da ripristinare:
        pIndex_DBCorrente_old = pIndex_DBCorrente
        pNome_DBCorrente_old = pNome_DBCorrente
        'PULISCO LA TABELLA tmp_report:  'ex Mauro
        DataMan.DmConnection.Execute "DELETE * From tmp_report"
        'CICLO TUTTI I DBD
        Set tb = m_fun.Open_Recordset("select IdOggetto,Nome from Bs_oggetti where Tipo='DBD' order by Nome")
        While Not tb.EOF
          pIndex_DBCorrente = tb!IdOggetto
          pNome_DBCorrente = tb!Nome
          MadmdM_DLI_Dati.CaricaStruttura
          structureToTable  'ex Mauro
          tb.MoveNext
        Wend
        tb.Close
        Screen.MousePointer = 0
        'Ripristino situazione:
        'NON FUNGE?!
        pIndex_DBCorrente = pIndex_DBCorrente_old
        pNome_DBCorrente = pNome_DBCorrente_old
        'LswExistObj_ItemClick LswExistObj.SelectedItem
        'MadmdM_DLI_Dati.CaricaStruttura
      End If
    Case "Open"
      Screen.MousePointer = vbHourglass

      If pIndex_DBCorrente = 0 Then
        MsgBox "You must select a Database", vbInformation, DataMan.DmNomeProdotto
        Screen.MousePointer = vbDefault
        Exit Sub
      End If

      If LswExistObj.ListItems.count <> 0 Then
        OpenMode = 2
        Lancia_Mappa_Gestione_DB
      End If
    Case "Erase"
      'gloria 30/04/2009: gestisco selezione multipla
      '******************************************************
      If pIndex_DBCorrente = 0 Then
        MsgBox "You must select a Database", vbInformation, DataMan.DmNomeProdotto
        Exit Sub
      End If
      
      ICountSel = 0
      For i = 1 To LswExistObj.ListItems.count
        Set myItem = LswExistObj.ListItems(i)
        If myItem.Selected = True Then
          ICountSel = ICountSel + 1
        End If
      Next i
      
      If ICountSel > 1 Then
        If MsgBox("Do you want to Erase all selected DataBases? ", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          For i = LswExistObj.ListItems.count To 1 Step -1
            Set myItem = LswExistObj.ListItems(i)
            If myItem.Selected = True Then
              pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
              deleteDBDTables pIndex_DBCorrente
              LswExistObj.ListItems.Remove (LswExistObj.SelectedItem.Index)
              LswExistObj.Refresh
            End If
          Next i
        Else
          Exit Sub
        End If
      Else
        If MsgBox("Do you want to Erase the selected DataBase? ", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          ' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
          deleteDBDTables pIndex_DBCorrente
          LswExistObj.ListItems.Remove (LswExistObj.SelectedItem.Index)
        End If
      End If
    Case "NEWDB"
      'Crea un nuovo Db
      Create_New_DB
      If LswExistObj.ListItems.count Then
        OpenMode = 2
        Lancia_Mappa_Gestione_DB
      End If
    Case "Properties"
      'ALE
      If OptSelDLI Then
        m_fun.Show_DisplayTab "PSDLI_DBD", "DBD Details"
      ElseIf OptSelVsam Then
        m_fun.Show_DisplayTab "DMVSM_NomiVSAM", "VSAM Details"
      End If
    'SILVIA 21-03-2008
    Case "ImpStr"
      AddStructure_From_List
    ' Mauro 07/02/2011
    Case "Refr_Area"
      
      Refresh_Areas

    ' Mauro 13/01/2011
    Case "Filter_Conf"
      'Seleziona Filtro
      Seleziona_Filtro_Selezione LswExistObj, Filter_List, Toolbar1.Buttons(11).Value, 10
    Case "Filter_Exe"
      'esegue la Query di filtro
      Execute_Filter Filter_List, LswExistObj, Toolbar1.Buttons(11).Value
      txtFilter.Visible = False
      Filter_List.Visible = False
  End Select
End Sub

''''''''''''''''''''''''''''''''''
'Riempie la tabella per il record
''''''''''''''''''''''''''''''''''
Sub structureToTable()
  Dim indseg As Integer, indCmp As Integer
  Dim TbAreaSeg As Recordset

  Set TbAreaSeg = m_fun.Open_Recordset("select * from tmp_report") 'ex Mauro
  'ATTENZIONE: STA STRUTTURA HA I PRIMI DUE ELEMENTI INUTILI!!!!!!!!!!
  For indseg = 1 To UBound(gSegm)
    'If indexseg = gSegm(indseg).IdSegm Then
    For indCmp = 1 To UBound(gSegm(indseg).Strutture(1).Campi)
      TbAreaSeg.AddNew
      TbAreaSeg!DBD = pNome_DBCorrente
      TbAreaSeg!Segmento = gSegm(indseg).Name
      TbAreaSeg!Parent = gSegm(indseg).Parent
      TbAreaSeg!livello = gSegm(indseg).Strutture(1).Campi(indCmp).livello
      TbAreaSeg!Ordinale = gSegm(indseg).Strutture(1).Campi(indCmp).Ordinale
      TbAreaSeg!Nome = gSegm(indseg).Strutture(1).Campi(indCmp).Nome
      TbAreaSeg!bytes = gSegm(indseg).Strutture(1).Campi(indCmp).bytes
      TbAreaSeg!lunghezza = gSegm(indseg).Strutture(1).Campi(indCmp).lunghezza
      TbAreaSeg!Decimali = gSegm(indseg).Strutture(1).Campi(indCmp).Decimali
      TbAreaSeg!Posizione = gSegm(indseg).Strutture(1).Campi(indCmp).Posizione
      TbAreaSeg!Tipo = gSegm(indseg).Strutture(1).Campi(indCmp).Tipo & " - " & gSegm(indseg).Strutture(1).Campi(indCmp).Usage
      TbAreaSeg!Occurs = gSegm(indseg).Strutture(1).Campi(indCmp).Occurs
      TbAreaSeg!NomeRed = gSegm(indseg).Strutture(1).Campi(indCmp).NomeRed
      TbAreaSeg!valore = gSegm(indseg).Strutture(1).Campi(indCmp).valore
      ' Mauro 15/01/2008
      TbAreaSeg!OccursFlat = gSegm(indseg).Strutture(1).Campi(indCmp).OccursFlat
      TbAreaSeg!DaMigrare = gSegm(indseg).Strutture(1).Campi(indCmp).DaMigrare
      TbAreaSeg.Update
'    End With
    Next indCmp
    'End If
  Next indseg
End Sub

Public Sub Create_New_DB()
  txtNomeDB.text = ""
  txtCrDate.text = ""
  txtMdDate.text = ""
  txtDescrizione.text = ""
  pIndex_DBCorrente = -1
  
  If LswExistObj.ListItems.count Then
    LswExistObj.SelectedItem.Selected = False
  End If
    
  'Aggiunge il db nel Database di progetto
  pIndex_NewDB = Add_Db_In_DbSys()
  
  'Si posiziona sul database appena creato
  pNome_DBCorrente = "NEWDB" & Format(pIndex_NewDB, "0000")
  pIndex_DBCorrente = pIndex_NewDB
  
  'Aggiunge a video un nuovo DB
  LswExistObj.ListItems.Add , , "NEWDB" & Format(pIndex_NewDB, "0000")
  LswExistObj.ListItems(LswExistObj.ListItems.count).Tag = pIndex_NewDB
  
  LswExistObj.ListItems(LswExistObj.ListItems.count).ListSubItems.Add , , UCase(TipoDB)
  LswExistObj.ListItems(LswExistObj.ListItems.count).ListSubItems.Add , , Now
  LswExistObj.ListItems(LswExistObj.ListItems.count).ListSubItems.Add , , ""
  LswExistObj.ListItems(LswExistObj.ListItems.count).ListSubItems.Add , , ""
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Dim wResp As Integer, i As Integer
  ' Mauro 20/11/2007 : ciclo veloce per contare quanti oggetti sono selezionati
  Dim a As Long, selCount As Long
  Dim rs As Recordset
  'gloria 30/04/2009: pulisco il log
  DataMan.DmFinestra.ListItems.Clear
  
  For a = 1 To LswExistObj.ListItems.count
    If LswExistObj.ListItems(a).Selected Then
      selCount = selCount + 1
    End If
  Next a
   
  On Error GoTo loadError
   
  MousePointer = vbHourglass
   
  Select Case ButtonMenu.Key
    Case "Programmi"
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Load Programs for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          If MsgBox("Do you want to create Load Programs for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
            'COBOL
            For i = 1 To LswExistObj.ListItems.count
              If LswExistObj.ListItems(i).Selected Then
                'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
                pNome_DBCorrente = LswExistObj.ListItems(i).text
                pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
                pOrigin_DBCorrente = LswExistObj.ListItems(i).SubItems(1)
                Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
                'tilvia 04-05-2009
                pIndex_DBDCorrente = rs!iddbor
                rs.Close
                ' Mauro 23/11/2007 : Gestione DCP VSAM2RDBMS
                If pOrigin_DBCorrente = "VSAM" Then
                  GeneraProgrammaLoadVSAM rtbLocal
                  ' DA FARE
''                  GeneraJclTran rtbLocal
''
''                  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
''                  GeneraCtlCopy rtbLocal
                Else
                  GeneraProgrammaLoad rtbLocal
                  'GeneraJclLoad rtbLocal
                  
                  'GeneraJclTran rtbLocal
                  
                  'DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
                  'GeneraCtlCopy rtbLocal
                End If
              End If
            Next i
            MsgBox "Programs created.", vbInformation, DataMan.DmNomeProdotto
          Else
            'PLI
'''TO DO
            For i = 1 To LswExistObj.ListItems.count
              If LswExistObj.ListItems(i).Selected Then
                'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
                pNome_DBCorrente = LswExistObj.ListItems(i).text
                pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
                Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
                'tilvia 04-05-2009
                pIndex_DBDCorrente = rs!iddbor
                rs.Close
                '''
                GeneraProgrammaLoad_PLI rtbLocal
                ''''GeneraJclLoad rtbLocal

                'GeneraJclTran_PLI rtbLocal

                ''''DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & DbType & "\Cpy\"
                ''''GeneraCtlCopy rtbLocal
              End If
            Next i
            MsgBox "Programs created.", vbInformation, DataMan.DmNomeProdotto
          End If
'          MsgBox "Programs created.", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "JCL"
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Load JCLs for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
            'COBOL
            For i = 1 To LswExistObj.ListItems.count
              If LswExistObj.ListItems(i).Selected Then
                'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
                pNome_DBCorrente = LswExistObj.ListItems(i).text
                pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
                pOrigin_DBCorrente = LswExistObj.ListItems(i).SubItems(1)
                'tilvia 04-05-2009
                Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
                pIndex_DBDCorrente = rs!iddbor
                rs.Close

                ' Mauro 23/11/2007 : Gestione DCP VSAM2RDBMS
                If pOrigin_DBCorrente = "VSAM" Then
                  'GeneraProgrammaLoadVSAM rtbLocal
                  
                  ' DA FARE
''                  GeneraJclTran rtbLocal
''
''                  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
''                  GeneraCtlCopy rtbLocal
                Else
                  'GeneraProgrammaLoad rtbLocal
                   
                  'GeneraJclLoad rtbLocal
                  
                  GeneraJclTran rtbLocal
                  
                  'DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
                  'GeneraCtlCopy rtbLocal
                End If
              End If
            Next i
          MsgBox "JCLs created.", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "Copy"
      '
      cCopyParam = LeggiParam("C-COPY")
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Structure copies for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          If MsgBox("Do you want to create Structure for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
            'COBOL
            For i = 1 To LswExistObj.ListItems.count
              If LswExistObj.ListItems(i).Selected Then
                'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
                pNome_DBCorrente = LswExistObj.ListItems(i).text
                pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
                pOrigin_DBCorrente = LswExistObj.ListItems(i).ListSubItems(1).text
                
                If pOrigin_DBCorrente = "VSAM" Then
                  'GeneraCpyRR rtbFile   'C
                  
                  MadmdM_VSAM.generaCpyRR_VSAM RtbFile  'X + C
                  
                  GeneraCpyRD_VSAM RtbFile    'T = V
                Else
                  GeneraCpyRR RtbFile   'C
                  
                  MadmdM_DB2.generaCpyX RtbFile  ' crea le copy riguardanti le exec dei db db2/oracle  ''''' X
                                      
                  GeneraCpyRD RtbFile    'T
                End If
              End If
            Next i
          Else
            'PLI
            For i = 1 To LswExistObj.ListItems.count
              If LswExistObj.ListItems(i).Selected Then
                'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
                pNome_DBCorrente = LswExistObj.ListItems(i).text
                pIndex_DBCorrente = LswExistObj.ListItems(i).Tag

                'OK
                GeneraCpyRR_PLI RtbFile   'C

                MadmdM_DB2.generaCpyX RtbFile, "PLI"   'X
            
                'OK
                GeneraCpyRD_PLI RtbFile  'T
              End If
            Next i
          End If
          MsgBox "Structure copybooks created", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "Move_Copy"
      createCopyMove = True
      createCopyMoveKey = False
      createCopyLoad = False
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Routine copies for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
      
          If MsgBox("Do you want to create Move Copy for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
            'COBOL
            createRoutineCopies    'M
          Else
            'PLI
            createRoutineCopies_PLI
          End If
        End If
        MsgBox "Copy Move created", vbInformation, DataMan.DmNomeProdotto

      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "MoveKey_Copy"
      createCopyMove = False
      createCopyMoveKey = True
      createCopyLoad = False
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Routine copies for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          If MsgBox("Do you want to create Move Key Copy for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
            'COBOL
            createRoutineCopies    'K
          Else
            'PLI
            createRoutineCopies_PLI
          End If
        End If
          MsgBox "Copy Move-Key created", vbInformation, DataMan.DmNomeProdotto
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "Load_Copy"
      createCopyMove = False
      createCopyMoveKey = False
      createCopyLoad = True
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Routine copies for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          If MsgBox("Do you want to create Load Copy for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          'COBOL
            createRoutineCopies   'L
          Else
          'PLI  OK
            createRoutineCopies_PLI 'L
          End If
          MsgBox "Load copybooks created", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If

    Case "Routine_Copy"
      createCopyMove = True
      createCopyMoveKey = True
      createCopyLoad = True
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Routine copies for selected elements?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          If MsgBox("Do you want to create Load Copy for COBOL?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          'COBOL
            createRoutineCopies
          Else
          'PLI
            createRoutineCopies_PLI
          End If
        End If
        ''  MsgBox "Procedure copybooks created", vbInformation, DataMan.DmNomeProdotto
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "SQL"
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create DDL files?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          For i = 1 To LswExistObj.ListItems.count
            If LswExistObj.ListItems(i).Selected Then
              pNome_DBCorrente = LswExistObj.ListItems(i).text
              pIndex_DBCorrente = LswExistObj.ListItems(i).Tag  'E' L'ID!!!
              If TipoDB = "DB2" Then
                'Declare_Table rtbFile
                'createDDL_Fase2 rtbFile
                createDDL_BPERmodel RtbFile
              ElseIf TipoDB = "ORACLE" Then
                Declare_TableORA RtbFile
                Declare_Load_ORA RtbFile
                Declare_Ctl_ORA RtbFile
              End If
            End If
          Next i
          MsgBox "DDL files created.", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    Case "Trigger"
      'If LswExistObj.SelectedItem.Selected Then
      If selCount Then
        If MsgBox("Do you want to create Trigger?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto) = vbYes Then
          For i = 1 To LswExistObj.ListItems.count
            If LswExistObj.ListItems(i).Selected Then
              pIndex_DBCorrente = LswExistObj.ListItems(i).Tag  'E' L'ID!!!
''              If TipoDB = "DB2" Then
''              Else
              If TipoDB = "ORACLE" Then
                Carica_ArrRelations pIndex_DBCorrente
                CreaTrigger
              End If
            End If
          Next i
          MsgBox "Triggers created.", vbInformation, DataMan.DmNomeProdotto
        End If
      Else
        MsgBox "No elements selected.", vbInformation, DataMan.DmNomeProdotto
      End If
    
    End Select
    
    MousePointer = vbNormal
    
  Exit Sub
loadError:
  MousePointer = vbNormal
  MsgBox ERR.Description, vbExclamation, DataMan.DmNomeProdotto
  Resume Next
End Sub

Private Sub txtCrDate_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub txtDescrizione_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
   
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_DB Where " & _
                                    "IdDb = " & pIndex_DBCorrente)
      
      If rs.RecordCount Then
        Adesso = Now
        rs!Descrizione = txtDescrizione.text
        rs!Data_Modifica = Adesso
        rs.Update
        
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
      rs.Close
  End Select
End Sub

Private Sub txtMdDate_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtNomeDB_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
   
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_DB Where " & _
                                    "IdDb = " & pIndex_DBCorrente)
      
      If rs.RecordCount Then
        Adesso = Now
        rs!Nome = txtNomeDB.text
        rs!Data_Modifica = Adesso
        rs.Update
        
        LswExistObj.ListItems(LswExistObj.SelectedItem.Index).text = txtNomeDB.text
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
      rs.Close
  End Select
End Sub

Public Sub Lancia_Mappa_Gestione_DB()
  Select Case Trim(UCase(TipoDB))
    Case "DLI"
      SetParent MadmdF_EditDli.hwnd, formParentResize.hwnd
      MadmdF_EditDli.Show
      MadmdF_EditDli.Move 0, 0, formParentResize.Width, formParentResize.Height
       
    Case "DB2", "ORACLE"
      SetParent MadmdF_GestDB.hwnd, formParentResize.hwnd
      MadmdF_GestDB.Show
      MadmdF_GestDB.Move 0, 0, formParentResize.Width, formParentResize.Height
       
    Case "VSAM"
      Screen.MousePointer = vbDefault
      SetParent MadmdF_VSAM.hwnd, formParentResize.hwnd
      MadmdF_VSAM.Show
      MadmdF_VSAM.Move 0, 0, formParentResize.Width, formParentResize.Height

  End Select
End Sub

Sub createRoutineCopies()
  Dim i As Integer
  'SQ portato al caricamento nel getEnv...
  'setEnvironmentParameters
  
  For i = 1 To LswExistObj.ListItems.count
    If LswExistObj.ListItems(i).Selected Then
      'SQ 30-11-07 - importante: questo � il nome del DB! (non DBD)
      '-> verificare se serve anche il nome DB...
      'pNome_DBCorrente = LswExistObj.ListItems(i).text
      pNome_DBCorrente = LswExistObj.ListItems(i).SubItems(2)
      pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
      pOrigin_DBCorrente = LswExistObj.ListItems(i).ListSubItems(1).text
'      If TipoDB = "DB2" Then
      DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
'      ElseIf TipoDB = "ORACLE" Then
'        DataMan.DmParam_OutField_Orc = DataMan.DmPathDb & "\OutPut-Prj\ORA\Cpy\"
'      End If
      If pOrigin_DBCorrente = "VSAM" Then
        If createCopyMove Then GeneraCpyMoveVSAM RtbFile
        If createCopyMoveKey Then GeneraCpyMoveKeyVSAM RtbFile
        If createCopyLoad Then GeneraCpyLoadVSAM RtbFile
      Else
        If createCopyMove Then GeneraCpyMove RtbFile
        If createCopyMoveKey Then GeneraCpyMoveKey RtbFile
        If createCopyLoad Then GeneraCpyLoad RtbFile
      End If
    End If
  Next
  MsgBox "Procedure copybooks created.", vbInformation, DataMan.DmNomeProdotto
End Sub

Sub createRoutineCopies_PLI()
  Dim i As Integer
  
  For i = 1 To LswExistObj.ListItems.count
    If LswExistObj.ListItems(i).Selected Then
      'le sub lavorano su pNome_DBCorrente/pIndex_DBCorrente:
      pNome_DBCorrente = LswExistObj.ListItems(i).text
      pIndex_DBCorrente = LswExistObj.ListItems(i).Tag
      'tilvia
      'DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
      DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Include\"
          
'''MG130207 Attualmente gestite solo le copy L
'''AC290507 Ripristinate anche Move (differiscono solo per percorso da cui prendono template
     If createCopyMove Then GeneraCpyMove_PLI RtbFile
     If createCopyMoveKey Then GeneraCpyMoveKey_PLI RtbFile
     If createCopyLoad Then GeneraCpyLoad_PLI RtbFile
      
    End If
  Next
  MsgBox "Procedure copybooks created.", vbInformation, DataMan.DmNomeProdotto
End Sub

Sub AbilitaMenuOracle(dbsel As Long)
  If LswExistObj.ListItems.count > 0 Then
    If LswExistObj.ListItems(dbsel).ListSubItems(1) = "DB2" Then
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("JCL").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = False
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Trigger").Enabled = False
    ElseIf LswExistObj.ListItems(dbsel).ListSubItems(1) = "DLI" Then
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = True
    ElseIf LswExistObj.ListItems(dbsel).ListSubItems(1) = "VSAM" Then
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = True
    End If
  End If
End Sub

Sub AbilitaMenuDB2(dbsel As Long)
  If LswExistObj.ListItems.count > 0 Then
    If LswExistObj.ListItems(dbsel).ListSubItems(1) = "DB2" Then
      Toolbar1.Buttons("CREATECOMP").Enabled = False
    ElseIf LswExistObj.ListItems(dbsel).ListSubItems(1) = "DLI" Then
      Toolbar1.Buttons("CREATECOMP").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("SQL").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Trigger").Enabled = True
    ElseIf LswExistObj.ListItems(dbsel).ListSubItems(1) = "VSAM" Then
      Toolbar1.Buttons("CREATECOMP").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Programmi").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Move_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("MoveKey_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Load_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Routine_Copy").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("SQL").Enabled = True
      Toolbar1.Buttons("CREATECOMP").ButtonMenus("Trigger").Enabled = False
    End If
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''
'SQ 21-11-07
'Portare qui tutti quelli necessari;
'Da chiamare al caricamento dei form...
''''''''''''''''''''''''''''''''''''''''''''
Private Sub getEnvironmentParameters()
  PkNameParam = LeggiParam("IMSDB-PK-NAME")
  If Len(PkNameParam) = 0 Then PkNameParam = "REVKEY"
  JclLoadParam = LeggiParam("JCL-LOAD")
  If Len(JclLoadParam) = 0 Then JclLoadParam = "JL(ROUTE-NAME)"
  JclTranParam = LeggiParam("JCL-TRAN")
  If Len(JclTranParam) = 0 Then JclTranParam = "JT(ROUTE-NAME)"
  LoadProgramParam = LeggiParam("LOAD-PROGRAM")
  If Len(LoadProgramParam) = 0 Then LoadProgramParam = "PL(ROUTE-NAME)"
  LoadCopyParam = LeggiParam("LOAD-COPY")
  If Len(LoadCopyParam) = 0 Then LoadCopyParam = "L(ROUTE-NAME)(SEGM-NAME#-4#)"

  'gestire i default
  cCopyColumnParam = LeggiParam("C-COPY-COLUMN")
  KCopyParam = LeggiParam("K-COPY")
  MoveCopyParam = LeggiParam("MOVE-COPY")
  cCopyAreaParam = LeggiParam("C-COPY-AREA")
End Sub

Private Sub txtFilter_Change()
  Filter_List.Col = Filter_List_Column
  Filter_List.Row = 1
  
  Filter_List.text = txtFilter.text
End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
  Filter_List.text = ""
  If KeyAscii = 13 Then
    If Filter_List_Column = 0 Then
      txtFilter.text = Format(txtFilter.text, "000000")
    End If
    
    txtFilter.Visible = False
  End If
End Sub
