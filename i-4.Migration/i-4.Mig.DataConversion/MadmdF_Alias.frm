VERSION 5.00
Begin VB.Form MadmdF_Alias 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Alias' Table properties"
   ClientHeight    =   3690
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8325
   ControlBox      =   0   'False
   Icon            =   "MadmdF_Alias.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3690
   ScaleWidth      =   8325
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   6150
      Picture         =   "MadmdF_Alias.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2610
      Width           =   945
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   7230
      Picture         =   "MadmdF_Alias.frx":1E8C
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2610
      Width           =   945
   End
   Begin VB.TextBox txtDescriptionAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   975
      Left            =   1230
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   12
      Top             =   1470
      Width           =   6915
   End
   Begin VB.ComboBox cboTypeAlias 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      ItemData        =   "MadmdF_Alias.frx":3BB6
      Left            =   6120
      List            =   "MadmdF_Alias.frx":3BC0
      TabIndex        =   11
      Top             =   1020
      Width           =   2025
   End
   Begin VB.TextBox txtLocationAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1245
      MaxLength       =   18
      TabIndex        =   8
      Top             =   1020
      Width           =   3525
   End
   Begin VB.TextBox txtNomeTbAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1245
      MaxLength       =   18
      TabIndex        =   3
      Top             =   120
      Width           =   3525
   End
   Begin VB.TextBox txtCreatorAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1245
      MaxLength       =   18
      TabIndex        =   2
      Top             =   570
      Width           =   3525
   End
   Begin VB.TextBox txtCrDateAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   6135
      MaxLength       =   18
      TabIndex        =   1
      Top             =   120
      Width           =   2025
   End
   Begin VB.TextBox txtMdDateAlias 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   6135
      MaxLength       =   18
      TabIndex        =   0
      Top             =   570
      Width           =   2025
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   180
      TabIndex        =   13
      Top             =   1560
      Width           =   975
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Type"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5700
      TabIndex        =   10
      Top             =   1080
      Width           =   390
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Location"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   450
      TabIndex        =   9
      Top             =   1080
      Width           =   705
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Name"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   705
      TabIndex        =   7
      Top             =   180
      Width           =   450
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Creator"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   510
      TabIndex        =   6
      Top             =   630
      Width           =   645
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Creation Date"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   4920
      TabIndex        =   5
      Top             =   180
      Width           =   1170
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Update Date"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5040
      TabIndex        =   4
      Top             =   630
      Width           =   1050
   End
End
Attribute VB_Name = "MadmdF_Alias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim f_Change As Boolean

Private Sub cboTypeAlias_Click()
  Dim rs As Recordset
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!Tipo = cboTypeAlias.text
    txtMdDateAlias.text = Now
    rs!Data_Modifica = txtMdDateAlias.text
    
    rs.Update
  End If
  rs.Close
   
  f_Change = True
End Sub

Private Sub cboTypeAlias_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub cmdAnnulla_Click()
  Dim wRet As Long
   
  If f_Change = True Then
    wRet = MsgBox("Do you want to close this window without save Creator's parameters?", vbYesNo + vbQuestion, DataMan.DmNomeProdotto)
  Else
    wRet = vbYes
  End If
   
  If wRet = vbYes Then
    DataMan.DmConnection.RollbackTrans
    Unload Me
  End If
End Sub

Private Sub cmdOK_Click()
  DataMan.DmConnection.CommitTrans
  MadmdF_GestDB.txtAlias.text = txtNomeTbAlias.text
   
  Unload Me
End Sub

Private Sub Form_Load()
  Me.Caption = "DB-Engine : " & TipoDB & " - Alias' Table [" & pNome_TBCorrente & "]"
   
  'Deve creare un alias nuovo nel database
  DataMan.DmConnection.BeginTrans
      
  Load_Alias_Tabella
   
  f_Change = False
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub txtCrDateAlias_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtCreatorAlias_Change()
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!Creator = txtCreatorAlias.text
    txtMdDateAlias.text = Now
    rs!Data_Modifica = txtMdDateAlias.text
   
    rs.Update
  End If
  rs.Close
  f_Change = True
End Sub

Private Sub txtDescriptionAlias_Change()
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!Descrizione = txtDescriptionAlias.text
    txtMdDateAlias.text = Now
    rs!Data_Modifica = txtMdDateAlias.text
  
    rs.Update
  End If
  rs.Close
  f_Change = True
End Sub

Private Sub txtLocationAlias_Change()
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!Location = txtLocationAlias.text
    txtMdDateAlias.text = Now
    rs!Data_Modifica = txtMdDateAlias.text
      
    rs.Update
  End If
  rs.Close
   
  f_Change = True
End Sub

Private Sub txtMdDateAlias_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub Load_Alias_Tabella()
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    txtCrDateAlias.text = TN(rs!Data_Creazione)
    txtCreatorAlias.text = TN(rs!Creator)
    txtDescriptionAlias.text = TN(rs!Descrizione)
    txtLocationAlias.text = TN(rs!Location)
    txtMdDateAlias.text = TN(rs!Data_Modifica)
    txtNomeTbAlias.text = TN(rs!Nome)
   
    Select Case TN(rs!Tipo)
      Case "S"
        cboTypeAlias.ListIndex = 0
      Case Else
        If Not IsNull(rs!Tipo) Then
          cboTypeAlias.ListIndex = cboTypeAlias.ListCount - 1
        End If
    End Select
  Else
    txtCrDateAlias.text = Now
    txtCreatorAlias.text = ""
    txtDescriptionAlias.text = ""
    txtLocationAlias.text = ""
    txtMdDateAlias.text = ""
    txtNomeTbAlias.text = UCase(Trim(pNome_TBCorrente)) & "_ALIAS"  'Crea un nome fittizio
    cboTypeAlias.ListIndex = 0
    
    rs.AddNew
    rs!Nome = txtNomeTbAlias.text
    rs!IdTable = pIndex_TbCorrente
    rs!Data_Creazione = txtCrDateAlias.text
    rs!Tipo = "S"
    rs.Update
  End If
  rs.Close
End Sub

Private Sub txtNomeTbAlias_Change()
  'Aggiorna il database con il nome
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Alias Where IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!Nome = txtNomeTbAlias.text
    txtMdDateAlias.text = Now
    rs!Data_Modifica = txtMdDateAlias.text
      
    rs.Update
  End If
  rs.Close
   
  f_Change = True
End Sub
