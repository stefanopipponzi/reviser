VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_Colonne"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdTable As Long
Private m_IdTableJoin As Long
Private m_IdColonna As Long
Private m_IdCampo As Long
Private m_Ordinale As Long
Private m_Nome As String
Private m_Tipo As String
Private m_Lunghezza As Long
Private m_Decimali As Long
Private m_Formato As String
Private m_Etichetta As String
Private m_Null As Boolean
Private m_Descrizione As String
Private m_Tag As String
Private m_RoutLoad As String
Private m_RoutRead As String
Private m_RoutWrite As String

'ID TABLE
Public Property Get IdTable() As Long
   IdTable = m_IdTable
End Property

Public Property Let IdTable(cIdTable As Long)
   m_IdTable = cIdTable
End Property

'ID TABLE JOIN
Public Property Get IdTableJoin() As Long
   IdTableJoin = m_IdTableJoin
End Property

Public Property Let IdTableJoin(cIdTableJoin As Long)
   m_IdTableJoin = cIdTableJoin
End Property

'ID COLONNA
Public Property Get IdColonna() As Long
   IdColonna = m_IdColonna
End Property

Public Property Let IdColonna(cIdColonna As Long)
   m_IdColonna = cIdColonna
End Property

'ORDINALE
Public Property Get Ordinale() As Long
   Ordinale = m_Ordinale
End Property

Public Property Let Ordinale(cOrdinale As Long)
   m_Ordinale = cOrdinale
End Property


'ID Campo
Public Property Get IdCampo() As Long
   IdCampo = m_IdCampo
End Property

Public Property Let IdCampo(cIdCampo As Long)
   m_IdCampo = cIdCampo
End Property

'NOME
Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let Nome(cNome As String)
   m_Nome = cNome
End Property

'TIPO
Public Property Get Tipo() As String
   Tipo = m_Tipo
End Property

Public Property Let Tipo(cTipo As String)
   m_Tipo = cTipo
End Property

'LUNGHEZZA
Public Property Get Lunghezza() As Long
   Lunghezza = m_Lunghezza
End Property

Public Property Let Lunghezza(cLunghezza As Long)
   m_Lunghezza = cLunghezza
End Property

'DECIMALI
Public Property Get Decimali() As Long
   Decimali = m_Decimali
End Property

Public Property Let Decimali(cDecimali As Long)
   m_Decimali = cDecimali
End Property

'FORMATO
Public Property Get Formato() As String
   Formato = m_Formato
End Property

Public Property Let Formato(cFormato As String)
   m_Formato = cFormato
End Property

'ETICHETTA
Public Property Get Etichetta() As String
   Etichetta = m_Etichetta
End Property

Public Property Let Etichetta(cEtichetta As String)
   m_Etichetta = cEtichetta
End Property

'NULL
Public Property Get cNull() As Boolean
   cNull = m_Null
End Property

Public Property Let cNull(mNull As Boolean)
   m_Null = mNull
End Property

'DESCRIZIONE
Public Property Get Descrizione() As String
   Descrizione = m_Descrizione
End Property

Public Property Let Descrizione(cDescrizione As String)
   m_Descrizione = cDescrizione
End Property

'TAG
Public Property Get Tags() As String
   Tags = m_Tag
End Property

Public Property Let Tags(cTag As String)
   m_Tag = cTag
End Property

'ROUT LOAD
Public Property Get RoutLoad() As String
   RoutLoad = m_RoutLoad
End Property

Public Property Let RoutLoad(cRoutLoad As String)
   m_RoutLoad = cRoutLoad
End Property

'ROUT READ
Public Property Get RoutRead() As String
   RoutRead = m_RoutRead
End Property

Public Property Let RoutRead(cRoutRead As String)
   m_RoutRead = cRoutRead
End Property

'ROUT WRITE
Public Property Get RoutWrite() As String
   RoutWrite = m_RoutWrite
End Property

Public Property Let RoutWrite(cRoutWrite As String)
   m_RoutWrite = cRoutWrite
End Property


