VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_RDBMSConv 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " RDBMS Convertion - DB2 to Oracle"
   ClientHeight    =   6420
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8340
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   8340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraName 
      Caption         =   "ORACLE Name:"
      Height          =   615
      Left            =   90
      TabIndex        =   7
      Top             =   5730
      Width           =   2685
      Begin VB.TextBox txtNewName 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   120
         TabIndex        =   8
         ToolTipText     =   "New name for target DB"
         Top             =   240
         Width           =   2445
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "DB2 Database List"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   5355
      Left            =   90
      TabIndex        =   5
      Top             =   390
      Width           =   2685
      Begin MSComctlLib.ListView lswTarget 
         Height          =   4965
         Left            =   120
         TabIndex        =   6
         Top             =   270
         Width           =   2445
         _ExtentX        =   4313
         _ExtentY        =   8758
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Database"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.CommandButton cmdErase 
      Height          =   435
      Left            =   2940
      Picture         =   "MadmdF_RDBMSConv.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Disassociate selected Item..."
      Top             =   2820
      Width           =   405
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   8340
      _ExtentX        =   14711
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "START"
            Object.ToolTipText     =   "Start migration..."
            ImageIndex      =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AUTOMATIC"
            Object.ToolTipText     =   "Generate Automatic Associations..."
            ImageIndex      =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ERASE"
            ImageIndex      =   9
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ERASE_ALL"
                  Text            =   "Erase all migrated databases"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ERASE_SEL"
                  Text            =   "Erase selected migrated databases "
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXIT"
            Object.ToolTipText     =   "Close this window..."
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "DataType"
            Key             =   "DataType"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdAssocia 
      Height          =   435
      Left            =   2940
      Picture         =   "MadmdF_RDBMSConv.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Associate (update) selected Item... "
      Top             =   2160
      Width           =   405
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2850
      Top             =   5220
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":0294
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":03EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":0988
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":0F22
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":123C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":1396
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":30D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":33EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_RDBMSConv.frx":3544
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Conversion Panel - Target List (ORACLE DB)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   5985
      Left            =   3480
      TabIndex        =   0
      Top             =   390
      Width           =   4785
      Begin MSComctlLib.ListView lswOutput 
         Height          =   5565
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   4485
         _ExtentX        =   7911
         _ExtentY        =   9816
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Database Name (DB2)"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Database Name (Oracle)"
            Object.Width           =   2822
         EndProperty
      End
   End
End
Attribute VB_Name = "MadmdF_RDBMSConv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAssocia_Click()
  'Cerca se il nome � gi� presente in lista, se si va in update, altrimenti crea una nuova associazione
  Dim i As Long
  Dim bExists As Boolean, bExistsList As Boolean
  Dim rsOrc As Recordset
  Dim nName As String
  Dim cc As Long
   
  If Trim(txtNewName.text) = "" Then
    MsgBox "Please, Insert a valid targed DB name", vbExclamation, DataMan.DmNomeProdotto
    txtNewName.SetFocus
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
   
  For i = 1 To lswTarget.ListItems.Count
    If lswTarget.ListItems(i).Selected Then
      cc = cc + 1
    End If
   
    If cc > 1 Then
      MsgBox "You can associate only one DB name at the time...", vbExclamation, DataMan.DmNomeProdotto
      Exit Sub
    End If
  Next i
   
  Screen.MousePointer = vbHourglass
   
  Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_DB Where Nome = '" & Trim(txtNewName.text) & "'")
         
  If rsOrc.RecordCount > 0 Then
    'conflitto
    bExists = False
  Else
    'Ok
    bExists = True
  End If
   
  For i = 1 To lswOutput.ListItems.Count
    If lswOutput.ListItems(i).Key = txtNewName.Tag Then
      bExistsList = True
      Exit For
    End If
  Next i
   
  'Controlla se il nome � diverso (deve essere diverso)
  If Trim(UCase(lswTarget.SelectedItem.text)) = Trim(UCase(txtNewName.text)) And Not bExists Then
    MsgBox "This database name is not a valid name. Please, change it.", vbExclamation, DataMan.DmNomeProdotto
    lswTarget.SelectedItem.SmallIcon = 2
      
    If bExistsList Then
      'Edit
      lswOutput.ListItems(i).ListSubItems(1).text = txtNewName.text
    End If
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
   
  If bExistsList Then
    'Edit
    lswOutput.ListItems(i).ListSubItems(1).text = txtNewName.text
  Else
    'Nuovo
    lswOutput.ListItems.Add , txtNewName.Tag, lswTarget.SelectedItem.text, , 4
    lswOutput.ListItems(lswOutput.ListItems.Count).ListSubItems.Add , , txtNewName.text
  End If
   
  'Aggiunge nel db il crossing
  Add_Crossing_DB2ORACLE_In_Db CLng(Mid(txtNewName.Tag, 1, 4)), lswTarget.SelectedItem.text, txtNewName.text
  
  'Elimina l'errore sulla lista target
  lswTarget.SelectedItem.SmallIcon = 3
  lswTarget.SelectedItem.Bold = True
   
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdErase_Click()
  Dim i As Long, j As Long
   
  For i = lswOutput.ListItems.Count To 1 Step -1
    If lswOutput.ListItems(i).Selected Then
      DataMan.DmConnection.Execute "DELETE " & DataMan.DmAsterixForDelete & " From DM_DB2ORC Where IdDb_DB2 = " & CLng(Mid(lswOutput.ListItems(i).Key, 1, 4))
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Remove from output list a database name association [" & lswOutput.ListItems(i).text & " --> " & lswOutput.ListItems(i).ListSubItems(1).text & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      
      For j = 1 To lswTarget.ListItems.Count
        If lswTarget.ListItems(j).Key = lswOutput.ListItems(i).Key Then
          lswTarget.ListItems(j).Bold = False
          Exit For
        End If
      Next j
      
      lswOutput.ListItems.Remove i
    End If
  Next i
   
  If lswOutput.ListItems.Count Then
    lswOutput.ListItems(1).Selected = True
  End If
End Sub

Private Sub Form_Load()
  'Setta il parent della form
  'SetParent Me.hwnd, DataMan.DmParent

  'ResizeControls Me, , True
 
  Load_DB2_Database_In_List
 
  lswTarget.ColumnHeaders(1).Width = lswTarget.Width - 270
  lswOutput.ColumnHeaders(1).Width = (lswOutput.Width / 2) - 135
  lswOutput.ColumnHeaders(2).Width = (lswOutput.Width / 2) - 135
 
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Load_DB2_Database_In_List()
  Dim rs As Recordset, rsOrc As Recordset, rsCtrl As Recordset
  Dim i As Long
  Dim cKey As String
  
  Screen.MousePointer = vbHourglass
  lswTarget.ListItems.Clear
  lswOutput.ListItems.Clear
  
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From DM_DB2ORC Order by IdDb_DB2")
  While Not rs.EOF
    cKey = Format(rs!IdDb_DB2, "0000") & "IDDB"
    
    'Aggiunge il database in automatico con il suo nome nella lista di output
    If rs!Migrato Then
      lswOutput.ListItems.Add , cKey, rs!Nome_DB2, , 3
    Else
      lswOutput.ListItems.Add , cKey, rs!Nome_DB2, , 4
    End If
    lswOutput.ListItems(lswOutput.ListItems.Count).ListSubItems.Add , , rs!Nome_Oracle
    
    rs.MoveNext
    DoEvents
  Wend
  rs.Close
   
  'Carica la lista dei Database DB2 nella lista target
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_DB")
  While Not rs.EOF
    cKey = Format(rs!idDB, "0000") & "IDDB"
    
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_DB Where Nome = '" & rs!Nome & "'")
    If rsOrc.RecordCount Then
      'Conflitto
      'Controlla se � gi� stato cambiato il nome
      Set rsCtrl = m_fun.Open_Recordset("Select * From DM_DB2ORC Where IdDb_DB2 = " & rs!idDB)
      If rsCtrl.RecordCount Then
        'OK
        lswTarget.ListItems.Add , cKey, rs!Nome, , 1
      Else
        'Reale conflitto
        lswTarget.ListItems.Add , cKey, rs!Nome, , 8
      End If
      rsCtrl.Close
    Else
      'Ok
      lswTarget.ListItems.Add , cKey, rs!Nome, , 3
    End If
    rsOrc.Close
    
    rs.MoveNext
    DoEvents
  Wend
  rs.Close
   
  'Font = bold per quelli gi� in lista
  Set rsCtrl = m_fun.Open_Recordset("Select * From DM_DB2ORC")
  While Not rsCtrl.EOF
    For i = 1 To lswTarget.ListItems.Count
      If UCase(Trim(lswTarget.ListItems(i).text)) = UCase(Trim(rsCtrl!Nome_DB2)) Then
        lswTarget.ListItems(i).Bold = True
        Exit For
      End If
    Next i

    rsCtrl.MoveNext
    DoEvents
  Wend
  rsCtrl.Close
        
  Screen.MousePointer = vbDefault
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
  
  If m_fun.FnProcessRunning = True Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswOutput_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswOutput.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswOutput.SortOrder = Order
  lswOutput.SortKey = Key - 1
  lswOutput.Sorted = True
End Sub

Private Sub lswOutput_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim i As Long
  
  txtNewName.text = Item.ListSubItems(1).text
  txtNewName.Tag = Item.Key
  
  'Seleziona sulla lista l'item
  For i = 1 To lswTarget.ListItems.Count
    If lswTarget.ListItems(i).Key = Item.Key Then
      lswTarget.ListItems(i).Selected = True
      Exit For
    End If
  Next i
End Sub

Private Sub lswTarget_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswTarget.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswTarget.SortOrder = Order
  lswTarget.SortKey = Key - 1
  lswTarget.Sorted = True
End Sub

Private Sub lswTarget_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim i As Long
  
  txtNewName.text = Item.text
  txtNewName.Tag = Item.Key
  
  For i = 1 To lswOutput.ListItems.Count
    lswOutput.ListItems(i).Selected = False
  Next i
  
  'Seleziona sulla lista l'item
  For i = 1 To lswOutput.ListItems.Count
    If lswOutput.ListItems(i).Key = Item.Key Then
      lswOutput.ListItems(i).Selected = True
      Exit For
    End If
  Next i
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim i As Long
   
  On Error GoTo catch
   
  Select Case UCase(Trim(Button.Key))
    Case "START" 'Avvia la migrazione
      m_fun.FnProcessRunning = True
      Screen.MousePointer = vbHourglass
      
      Migra_DB2_TO_ORACLE
      
      Screen.MousePointer = vbDefault
      m_fun.FnProcessRunning = False
    Case "AUTOMATIC" 'Genera in automatico le associazioni dei nomi (solo quelli non in conflitto)
      m_fun.FnProcessRunning = True
      Screen.MousePointer = vbHourglass
      
      Genera_Associazioni_Automatico
      Load_DB2_Database_In_List
      
      Screen.MousePointer = vbDefault
      m_fun.FnProcessRunning = False
    Case "ERASE" 'Cancella la migrazione (Solo Selezionati
      m_fun.FnProcessRunning = True
      Screen.MousePointer = vbHourglass
        
      For i = 1 To lswOutput.ListItems.Count
        If lswOutput.ListItems(i).Selected Then
          DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Erase selected Migration (" & lswOutput.ListItems(i).text & ")"
          DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      
          Erase_Migration Mid(lswOutput.ListItems(i).Key, 1, 4)
          lswOutput.ListItems(i).SmallIcon = 4
         
          Modifica_Flag_Migrato Mid(lswOutput.ListItems(i).Key, 1, 4), False
        End If
      Next i
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Process finished..."
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
        
      m_fun.FnProcessRunning = False
      Screen.MousePointer = vbDefault
    Case "EXIT" 'Che c'� da spiegare!?!??!
      Unload Me
    'silvia 15-04-2008
    Case "DATATYPE"
      m_fun.Show_DisplayTab "TrascodificaDB2_ORC", "DataType Details"
  End Select
catch:
End Sub

Public Sub Migra_DB2_TO_ORACLE()
  Dim i As Long, cId As Long
  Dim rsIdDbOrc As Recordset, rsTabOrc As Recordset
   
  On Error GoTo ErrorHandler
   
  DataMan.DmFinestra.ListItems.Clear
  DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] Starting DB2 to ORACLE migration..."
   
  'DataMan.DmConnection.BeginTrans
  For i = 1 To lswOutput.ListItems.Count
    If lswOutput.ListItems(i).Selected Then
      DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] Add a new Oracle Database [" & lswOutput.ListItems(i).text & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
        
      cId = Mid(lswOutput.ListItems(i).Key, 1, 4)
      Migra_Database cId, i
    End If
  Next i
   
'''MG 290107
  'dopo aver migrato i db con le relative tabelle
  'migra le foreign key ed i trigger
  For i = 1 To lswOutput.ListItems.Count
    If lswOutput.ListItems(i).Selected Then
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add new Foreign Key for Oracle Database [" & lswOutput.ListItems(i).text & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      
      cId = Mid(lswOutput.ListItems(i).Key, 1, 4)
      
      'ricava l'id del db oracle associato
      Set rsIdDbOrc = m_fun.Open_Recordset("Select * from DMORC_DB where IdDBOr=" & cId)
      While Not rsIdDbOrc.EOF
        'ricava l'id della tabella e il corrispondente id tabella in db2
        'idDbOr == IdTabDB2
        Set rsTabOrc = m_fun.Open_Recordset("Select * from DMORC_Tabelle where IdDb = " & rsIdDbOrc!idDB)
        While Not rsTabOrc.EOF
          'Associa le Foreign Id Table alle Foreign key
          AssegnazioneForeignTable rsTabOrc!iddbor
          
          'MG 160107 migra i trigger
          Migra_Trigger_DB2_To_ORACLE rsTabOrc!IdTable, rsTabOrc!iddbor, cId
          
          rsTabOrc.MoveNext
        Wend
        rsTabOrc.Close
        
        rsIdDbOrc.MoveNext
      Wend
      rsIdDbOrc.Close
    End If
  Next i
   
  'migra idxcol nello step due perch� simultaneamente alcune colonne in oracle per le
  'associazioni potrebbero non essere disponibili
  For i = 1 To lswOutput.ListItems.Count
    If lswOutput.ListItems(i).Selected Then
      cId = Mid(lswOutput.ListItems(i).Key, 1, 4)
      Migra_IdxCol_DB2_To_ORACLE cId
      Modifica_Flag_Migrato cId, True
    End If
  Next i
  
  'Finalizza la transazione
  'DataMan.DmConnection.CommitTrans
  
  'cancella gli indici doppi
  Drop_Indici_Doppi
  
  DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Process finished..."
  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  
  '''Modifica_Flag_Migrato cId, True
  Exit Sub
ErrorHandler:
  'DataMan.DmConnection.RollbackTrans
  DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Process failed!!!"
  lswOutput.ListItems(i).SmallIcon = 2
  
  Screen.MousePointer = vbDefault
  
  'Leva il flag migrato
  Modifica_Flag_Migrato cId, False
End Sub

Public Sub Modifica_Flag_Migrato(cId As Long, vFlag As Boolean)
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From DM_DB2ORC Where IdDB_Db2 = " & cId)
  If rs.RecordCount Then
    rs!Migrato = vFlag
    rs.Update
  End If
  rs.Close
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Dim i As Long
  
  Select Case Trim(UCase(ButtonMenu.Key))
    Case "ERASE_ALL"
      Screen.MousePointer = vbHourglass
      
      For i = 1 To lswOutput.ListItems.Count
        DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Erase selected Migration (" & lswOutput.ListItems(i).text & ")"
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    
        Erase_Migration Mid(lswOutput.ListItems(i).Key, 1, 4)
        lswOutput.ListItems(i).SmallIcon = 4
       
        Modifica_Flag_Migrato Mid(lswOutput.ListItems(i).Key, 1, 4), False
      Next i
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Process finished..."
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      
      Screen.MousePointer = vbDefault
    Case "ERASE_SEL"
      For i = 1 To lswOutput.ListItems.Count
        If lswOutput.ListItems(i).Selected Then
          DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Erase selected Migration (" & lswOutput.ListItems(i).text & ")"
          DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      
          Erase_Migration Mid(lswOutput.ListItems(i).Key, 1, 4)
          lswOutput.ListItems(i).SmallIcon = 4
         
          Modifica_Flag_Migrato Mid(lswOutput.ListItems(i).Key, 1, 4), False
        End If
      Next i
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Process finished..."
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  End Select
End Sub

Public Sub Erase_Migration(cId As Long)
  Dim wTb As Recordset
  Dim PrefDb As String
  Dim idDB As Long
  
  PrefDb = "DMORC"
  Set wTb = m_fun.Open_Recordset("select * from DMORC_DB where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & cId)
  'DM*_DB.IdDB e' la chiave di accesso per la cascata di tutti gli altri oggetti
  If wTb.RecordCount Then
    idDB = wTb!idDB
    Set wTb = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & idDB)
    While Not wTb.EOF
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Alias Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Colonne Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Index Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_IdxCol Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Trigger Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Check_Constraint Where idtable = " & wTb!IdTable
      wTb.Delete
      wTb.MoveNext
    Wend
    DataMan.DmConnection.Execute "DELETE * from " & PrefDb & "_DB where idDB = " & idDB
    DataMan.DmConnection.Execute "DELETE * from " & PrefDb & "_Tablespaces where idDB = " & idDB
  End If
  wTb.Close
End Sub

Public Sub Genera_Associazioni_Automatico()
  Dim i As Long, cId As Long
  Dim cName As String
  Dim wRet As Long
  
  DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] Start automatic procedure of generating new Database Names for Oracle..."
  
  For i = 1 To lswTarget.ListItems.Count
    cId = CLng(Mid(lswTarget.ListItems(i).Key, 1, 4))
    cName = lswTarget.ListItems(i).text
    If lswTarget.ListItems(i).SmallIcon <> 8 Then 'Non Conflitto
      Add_Crossing_DB2ORACLE_In_Db cId, cName, cName
      DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] Generate new Database name for Oracle : " & cName
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    Else
      'Log
      DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] " & cName & "DB2 Database name match with an existing Oracle Database name..."
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
  Next i
  
  DataMan.DmFinestra.ListItems.Add , , Now & ": [DB2 --> ORACLE] Process finished..."
  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
End Sub

Public Sub Migra_Database(cId As Long, cIndexList As Long)
  Dim rs As Recordset, rsDB2 As Recordset, rsOrc As Recordset
   
  Set rs = m_fun.Open_Recordset("Select * From DM_DB2ORC Where IdDb_DB2 = " & cId & " Order By IdDb_DB2")
  If rs.RecordCount Then
    'nessuna domanda di conferma: brutali!
    Erase_Migration rs!IdDb_DB2
    
    Set rsDB2 = m_fun.Open_Recordset("Select * From DMDB2_DB Where IdDb = " & rs!IdDb_DB2)
    If rsDB2.RecordCount Then
      Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_DB")
      
      rsOrc.AddNew
      'rsOrc!IdDbOr = rsDb2!IdDbOr
      rsOrc!Nome = rs!Nome_Oracle
      rsOrc!iddbor = rsDB2!idDB 'Id Database DB2 da cui proviene
      rsOrc!Data_Creazione = Now
      rsOrc!Descrizione = "Automatic migration from DB2 to ORACLE"
      rsOrc!Tag = "AUTOMATIC_DB2ORC"
      
      rsOrc.Update
      
      'Migra i tablespaces
      Migra_TableSpaces_DB2_ORACLE rsOrc!idDB, rsOrc!Nome, rsDB2!idDB
         
      'Migra le tabelle
      Migra_Tabelle_DB2_ORACLE rsOrc!idDB, rsOrc!Nome, rsDB2!idDB
      
      'Update dei table join
      Update_TableJoin_ORACLE rsDB2!idDB
      
      rsOrc.Close
    End If
    rsDB2.Close
      
    'Update : Scrive db migrato nel db
'    rs!Migrato = True
'    rs.Update
'
    'Cambia icona sulla lista
    lswOutput.ListItems(cIndexList).SmallIcon = 3
  End If
  rs.Close
End Sub
