       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.
       DATE-WRITTEN.  DATA-CREAZ.
       DATE-COMPILED. DATA-COM.

      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   DATA-CREAZ        (RELEASE 1.1A ) **
      **                                                              **
      **  EXECUTED BY           :   DEVELOPMENT AND SUPPORT GROUP     **
      **                            MAINFRAME AFFINITY                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE "RECEIVE MAP"         **
      **     GENERATED FROM IMS/DC                                    **
      **                                                              **
      **  STANDARD TEMPLATE SOURCE                                    **
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            **
      **                                                              **
      **--------------------------------------------------------------**
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*

       COPY #CICS-MAP#.

       01  RECEIVE-AREA           PIC X(2000).
       01  RESPONSE               PIC S9(8) COMP.
       01  WMFS-PFK               PIC X(#LENPFK#).
       
       #FORMAT-AREAS#.
       
           COPY DFHAID.

       01  BASE-PAGE               PIC S9(8) COMP.   
       01  LEN-PAGE                PIC S9(8) COMP.  

       
       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDCRT.
      
      *----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.

       MAIN-START.
      
      *    #OPE#

            

       MAIN-END.
           GOBACK.
           
      
      *    #OPEREC#           
      
       ANALYZE-PFK.
           MOVE #dfvalue# TO WMFS-PFK  
           IF EIBAID = DFHPF1
              MOVE #value1# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF2
              MOVE #value2# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF3
              MOVE #value3# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF4
              MOVE #value4# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF5
              MOVE #value5# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF6
              MOVE #value6# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF7
              MOVE #value7# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF8
              MOVE #value8# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF9
              MOVE #value9# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF10
              MOVE #value10# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF11
              MOVE #value11# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF12
              MOVE #value12# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF13
              MOVE #value13# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF14
              MOVE #value14# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF15
              MOVE #value15# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF16
              MOVE #value16# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF17
              MOVE #value17# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF18
              MOVE #value18# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF19
              MOVE #value19# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF20
              MOVE #value20# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF21
              MOVE #value21# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF22
              MOVE #value22# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF23
              MOVE #value23# TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF24
              MOVE #value24# TO WMFS-PFK
           END-IF
           . 
       EX-ANALYZE-PFK.
           EXIT.
