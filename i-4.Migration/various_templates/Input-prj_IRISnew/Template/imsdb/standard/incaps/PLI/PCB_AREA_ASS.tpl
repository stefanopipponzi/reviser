 /*--------------------------------------------------------------*I-4M*/
 /*  PCB AREAS ASSIGNMENT                                       /*I-4M*/
 /*--------------------------------------------------------------*I-4M*/
  DCL 1 REV_PCBS(<NUMPCBENTRIES>) UNALIGNED,                                 /*I-4M*/
        2 REV_NUM FIXED BIN(31),                                /*I-4M*/
        2 REV_PCB CHAR(66) ;                                    /*I-4M*/
  REV_PCBS = ' ';                                               /*I-4M*/
  DCL 1 REV_PCBS_AL UNALIGNED,                                  /*I-4M*/<NEW_PCB>
        2 REV_PCB_A(9) POINTER;                                 /*I-4M*/<NEW_PCB>
<<PCBLIST>>