*ITER-W--------------------------------------------------------------*
*ITER-W  INTERFACE COPY USED BY PROGRAM TO ROUTINES IMSDB
*ITER-W        BY  I-TER                               
*ITER-W--------------------------------------------------------------*

*ITER-W IO COPY -----------------------------------------------------*
%LNKDBRT

*ITER-W IO ROUTINE NAME ---------------------------------------------*
IMSRTNAME W 8 A                

*ITER-W MANAGE MULTIFUNCTION ----------------------------------------*
REVOPER W 2 A
REVEQ REVOPER   1 2 A VALUE 'EQ'
REVGT REVOPER   1 2 A VALUE 'GT'
REVGE REVOPER   1 2 A VALUE 'GE'
REVLT REVOPER   1 2 A VALUE 'LT'
REVLE REVOPER   1 2 A VALUE 'LE'
REVNE REVOPER   1 2 A VALUE 'NE'
*ITER-W--------------------------------------------------------------*
