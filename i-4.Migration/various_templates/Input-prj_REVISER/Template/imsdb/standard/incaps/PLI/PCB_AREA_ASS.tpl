 /*--------------------------------------------------------------*BPHX*/
 /*  PCB AREAS ASSIGNMENT                                       /*BPHX*/
 /*--------------------------------------------------------------*BPHX*/
  DCL 1 REV_PCBS(<NUMPCBENTRIES>) UNALIGNED,                                 /*BPHX*/
        2 REV_NUM FIXED BIN(31),                                /*BPHX*/
        2 REV_PCB CHAR(66) ;                                    /*BPHX*/
  REV_PCBS = ' ';                                               /*BPHX*/
  DCL 1 REV_PCBS_AL UNALIGNED,                                  /*BPHX*/<NEW_PCB>
        2 REV_PCB_A(9) POINTER;                                 /*BPHX*/<NEW_PCB>
<<PCBLIST>>