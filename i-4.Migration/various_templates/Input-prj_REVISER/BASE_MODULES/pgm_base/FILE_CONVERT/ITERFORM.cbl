       ID DIVISION.
       PROGRAM-ID. ITERFORM.
       AUTHOR. I-TER.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
                    DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
      *
           SELECT   REDFILIN      ASSIGN TO REDFILIN
                            ORGANIZATION IS LINE SEQUENTIAL.
      *
           SELECT   REDFILOUT      ASSIGN TO REDFILOU
                            ORGANIZATION IS LINE SEQUENTIAL.
       DATA                DIVISION.
      *
       FILE SECTION.
      *
       FD  REDFILIN
           LABEL RECORD ARE STANDARD
           BLOCK CONTAINS 0 RECORDS.
       01  RECORD-REDFILIN                                PIC X(80).
      *
       FD  REDFILOUT
           LABEL RECORD ARE STANDARD
           BLOCK CONTAINS 0 RECORDS.
       01  RECORD-REDFILOUT                               PIC X(80).

      *
       WORKING-STORAGE SECTION.
      *
       01  WR-ZONED                 PIC S9(18) COMP.
       01  WR-COMP                  PIC S9(18) COMP.
       01  WR-COMP-3                PIC S9(18) COMP-3.
      *
       01 EL-REDFILE.
            05 NUM-REDEFINES                  PIC 9(02).
            05 STRINGA-REDEFINES              PIC X(62).
            05 OPERATORE-LOGICO               PIC X(02).
            05 STR-RED-DA                     PIC 9(06).
            05 STR-RED-PER                    PIC 9(06).
            05 TIPO-RED-PIC                   PIC X(01).
            05 TIPO-RED-SEGNO                 PIC X(01).
      *
       01 COMODI.
          05 FINE-REDFILIN                    PIC 9 VALUE 0.
          05 CTR-RECORD                       PIC 9(9) VALUE 0.
          05 SW-RED                           PIC 9 VALUE 0.
          05 COM-NUM-RED                      PIC 9(02).
          05 NUMERO-RED                       PIC 9(02).
          05 COM-RED-DA                       PIC 9(06).
          05 COM-RED-PER                      PIC 9(06).
          05 COM-DA                           PIC 9(06).
          05 COM-PER                          PIC 9(06).
          05 COM-PER2                         PIC 9(06).
          05 COM-S-R-DA                       PIC 9(06).
          05 COM-STRINGA                      PIC X(62).
          05 COM-STRINGA2                     PIC X(62).
          05 COM-STRINGA-IN                   PIC X(62).
          05 SEGNO-IN                         PIC X VALUE ' '.
          05 NUMERIC-IN                       PIC 9(18).
      *
       01 VAL-APPO       PIC 999.
       01 RESULT         PIC X.
       01 IND-STR        PIC 9(06) VALUE 1.
       01 IND-STR-MAX    PIC 9(06) VALUE 1.
       01 IND-RED        PIC 999 VALUE 1.
       01 IND-RED-MAX    PIC 999 VALUE 0.
       01 IND-INP        PIC 9(06) VALUE ZEROES.
       01 IND-DA         PIC 99 VALUE ZEROES.
       01 IND-PER        PIC 99 VALUE ZEROES.
       01 IND-DIV        PIC 99 VALUE 0.
       01 QUOTO          PIC 9(06).
       01 RESTO          PIC 9(06).
       01 IND2           PIC 999.
       01 FINE PIC X.
           88 OK  VALUE 'S'.
           88 KO  VALUE 'N'.

      *
       LINKAGE SECTION.
      *
       PROCEDURE DIVISION.
      *
       ELABORAZIONE.
           PERFORM 1000-OPEN-FILES
              THRU 1000-OPEN-FILES-EX
           PERFORM 3100-LEGGI-REDFILIN
              THRU 3100-LEGGI-REDFILIN-EX
           PERFORM 3000-TRATTA-DISCRIMINANTI
              THRU 3000-TRATTA-DISCRIMINANTI-EX
              UNTIL FINE-REDFILIN = 1
           PERFORM 9000-CLOSE-FILES
              THRU 9000-CLOSE-FILES-EX.
       FINE-ELABORAZIONE.
           GOBACK.
      *
       1000-OPEN-FILES.
           OPEN INPUT  REDFILIN.
           OPEN OUTPUT REDFILOUT.
       1000-OPEN-FILES-EX.
           EXIT.
      *
      *
       3000-TRATTA-DISCRIMINANTI.
            MOVE 1 TO IND-RED
            EVALUATE TIPO-RED-PIC
            WHEN = 'X'
              MOVE EL-REDFILE TO RECORD-REDFILOUT
            WHEN OTHER
             PERFORM 3300-FORMATTA-STRINGAIN
               THRU 3300-FORMATTA-STRINGAIN-EX
            END-EVALUATE
            PERFORM 3200-SCRIVI-REDFILOUT
               THRU 3200-SCRIVI-REDFILOUT
            PERFORM 3100-LEGGI-REDFILIN
               THRU 3100-LEGGI-REDFILIN-EX.
       3000-TRATTA-DISCRIMINANTI-EX.
           EXIT.
      *
       3100-LEGGI-REDFILIN.
             READ REDFILIN INTO EL-REDFILE
             AT END MOVE 1 TO FINE-REDFILIN
             END-READ.
       3100-LEGGI-REDFILIN-EX.
           EXIT.
      *
       3200-SCRIVI-REDFILOUT.
            WRITE RECORD-REDFILOUT.
       3200-SCRIVI-REDFILOUT-EX.
           EXIT.
      *
       3300-FORMATTA-STRINGAIN.
            MOVE SPACES TO COM-STRINGA2
            MOVE STRINGA-REDEFINES TO COM-STRINGA-IN
            PERFORM UNTIL COM-STRINGA-IN(IND-RED:1) NOT = ' '
               IF COM-STRINGA-IN(IND-RED:1) = ' '
                 ADD 1 TO IND-RED
               END-IF
            END-PERFORM
            IF TIPO-RED-SEGNO = 'S'
               IF COM-STRINGA-IN(IND-RED:1) = '-' OR '+'
                  MOVE COM-STRINGA-IN(IND-RED:1) TO SEGNO-IN
                  MOVE ' ' TO COM-STRINGA-IN(IND-RED:1)
                  ADD 1 TO IND-RED
               ELSE
                  MOVE ' ' TO SEGNO-IN
               END-IF
            ELSE
               MOVE ' ' TO SEGNO-IN
            END-IF
            IF OPERATORE-LOGICO = '<>'
               MOVE IND-RED TO IND-DA
               PERFORM UNTIL COM-STRINGA-IN(IND-RED:1) = ' '
                  IF COM-STRINGA-IN(IND-RED:1) NOT = ' '
                     ADD 1 TO IND-RED
                  END-IF
               END-PERFORM
               COMPUTE IND-PER = IND-RED - IND-DA
               MOVE COM-STRINGA-IN(IND-DA:IND-PER)
                 TO COM-STRINGA(67 - IND-PER:)
            ELSE
               MOVE COM-STRINGA-IN TO COM-STRINGA
            END-IF
            MOVE 1 TO COM-DA
            INSPECT COM-STRINGA REPLACING ALL ' ' BY ZERO.
            EVALUATE TIPO-RED-PIC
             WHEN = '9'
              PERFORM 4000-ZONED
               THRU  4000-ZONED-EX
             WHEN = 'B'
              PERFORM 4100-BINARY
               THRU  4100-BINARY-EX
             WHEN = 'P'
              PERFORM 4200-PACKED
               THRU  4200-PACKED-EX
            END-EVALUATE
            IF OPERATORE-LOGICO = '<>'
               COMPUTE IND-DA = IND-DA + IND-PER
               ADD 1 TO IND-DA
               MOVE ' ' TO COM-STRINGA
               MOVE COM-STRINGA-IN(IND-DA:)
                 TO COM-STRINGA(IND-DA:)
               IF TIPO-RED-SEGNO = 'S'
                  IF COM-STRINGA-IN(IND-DA:1) = '-' OR '+'
                     MOVE COM-STRINGA-IN(IND-DA:1) TO SEGNO-IN
                     MOVE ' ' TO COM-STRINGA-IN(IND-DA:1)
                  ELSE
                     MOVE ' ' TO SEGNO-IN
                  END-IF
               ELSE
                  MOVE ' ' TO SEGNO-IN
               END-IF
               ADD 1 TO IND-DA
               MOVE SPACES TO COM-STRINGA
               MOVE COM-STRINGA-IN(IND-DA:IND-PER)
                 TO COM-STRINGA(67 - IND-PER:)
               INSPECT COM-STRINGA REPLACING ALL ' ' BY ZERO
               COMPUTE COM-DA = COM-DA + STR-RED-PER + 1
               EVALUATE TIPO-RED-PIC
                WHEN = '9'
                 PERFORM 4000-ZONED
                  THRU  4000-ZONED-EX
                WHEN = 'B'
                 PERFORM 4100-BINARY
                  THRU  4100-BINARY-EX
                WHEN = 'P'
                 PERFORM 4200-PACKED
                  THRU  4200-PACKED-EX
               END-EVALUATE
             END-IF.
             MOVE COM-STRINGA2 TO STRINGA-REDEFINES
             MOVE EL-REDFILE TO RECORD-REDFILOUT.
       3300-FORMATTA-STRINGAIN-EX.
           EXIT.
      *
       4000-ZONED.
             MOVE COM-STRINGA TO WR-ZONED
             IF SEGNO-IN = '-'
                COMPUTE WR-ZONED = WR-ZONED * -1
             END-IF
             COMPUTE COM-RED-DA = 17 - STR-RED-PER + 1
             MOVE WR-ZONED(COM-RED-DA:STR-RED-PER)
               TO COM-STRINGA2(COM-DA:STR-RED-PER).
       4000-ZONED-EX.
           EXIT.
      *
       4100-BINARY.
             MOVE COM-STRINGA TO WR-COMP
             IF SEGNO-IN = '-'
                COMPUTE WR-COMP = WR-COMP * -1
             END-IF
             COMPUTE COM-RED-DA = 17 - STR-RED-PER + 1
             MOVE WR-COMP(COM-RED-DA:STR-RED-PER)
               TO COM-STRINGA2(COM-DA:STR-RED-PER).
       4100-BINARY-EX.
           EXIT.
      *
       4200-PACKED.
             MOVE COM-STRINGA TO WR-COMP-3
             IF SEGNO-IN = '-'
                COMPUTE WR-COMP-3 = WR-COMP-3 * -1
             END-IF
      *      COMPUTE STR-RED-PER = ( STR-RED-PER / 2 ) + 1
             COMPUTE COM-RED-DA = 10 - STR-RED-PER + 1
             MOVE WR-COMP-3(COM-RED-DA:STR-RED-PER)
               TO COM-STRINGA2(COM-DA:STR-RED-PER).
       4200-PACKED-EX.
           EXIT.
      *
       9000-CLOSE-FILES.
           CLOSE REDFILIN REDFILOUT.
       9000-CLOSE-FILES-EX.
           EXIT.
      *
