 /*____________________________________________________________*
 /*____________________________________________________________*
 * LAST MODIFICATION DATE:   DATA_MODIF       ( RELEASE 1,0 )  *
 *                                                             *
 * EXECUTED BY           :   Development and support group     *
 *                           Mainframe Affinity                *
 *                                                             *
 *_____________________________________________________________*
 *                                                             *
 *              GENERIC MESSAGES AREA
 *                                                             *
 *_____________________________________________________________*/
 DCL 1  ACTION_ERRORE            PIC'9';
 DCL 1  ERRDBRT_AREA,
       2  ERRDB_NOOP,
          5  CODDB_NOER,
             10  FIL_NOER       CHAR(2) INITIAL('XX'),
             10  FIL_NOER1      CHAR(6),
          5  MSGDB_NOER          CHAR(50) INITIAL(
      'GENERIC ERROR                                     '),
          5  ACTION_NOER         PIC'9' INITIAL(0),
          5  CODDB_NOOP,
             10  FIL_NOOP           CHAR(2) INITIAL('XX'),
             10  FIL_NOOP1          CHAR(6),
          5  MSGDB_NOOP          CHAR(50) INITIAL(
      'OPERATION NOT DECODED                             '),
          5  ACTION_NOOP         PIC'9' INITIAL(1),
          5  CODDB_DLSA,
             10  FIL_DLSA           CHAR(2) INITIAL('XX'),
             10  FIL_DLSA1          CHAR(6),
          5  MSGDB_DLSA          CHAR(50) INITIAL(
      'SSA NOT BUILT CORRECTLY                           '),
          5  ACTION_DLSA         PIC'9' INITIAL(1);
 /*____________________________________________________________*
 *          DEFINIZIONE DEI CODICI OPERATIVI PER LE            *
 *             ROUTINES GENERALIZZATE DI I_O IMS               *
 *_____________________________________________________________*/
 DCL 1 ERRDB_SQLRESP,
        3  ERRDB_00,
          5  FILL00             FIXED BIN(30) INITIAL(0),
          5  FILL001            CHAR(36) INITIAL(
          'NORMAL '),
          5 FILL002             CHAR(2) INITIAL('  '),

        3  ERRDB_100,
          5  FIL100             FIXED BIN(30) INITIAL(+100),
          5  FIL1001            CHAR(36) INITIAL(
          'ROW NOT FOUND'),
          5  FIL1002            CHAR(2) INITIAL('GE'),

        3  ERRDB_180,
          5  FIL180             FIXED BIN(30) INITIAL(_180),
          5  FIL1801            CHAR(36) INITIAL(
             'FORMAT DATE_TIME INVALID'),
          5  FIL1802            CHAR(2) INITIAL('XD'),

        3  ERRDB_501,
          5  FIL501             FIXED BIN(30) INITIAL(_501),
          5  FIL5011            CHAR(36) INITIAL(
              'FETCH/CLOSE CURSOR NOT OPEN'),
          5  FIL5012            CHAR(2) INITIAL('GE'),

        3  ERRDB_803,
          5  FIL803             FIXED BIN(30) INITIAL(_803),
          5  FIL8031            CHAR(36) INITIAL(
            'DUPLICATE KEY'),
          5  FIL8032            CHAR(2) INITIAL('II'),

        3  ERRDB_805,
          5  FIL803             FIXED BIN(30) INITIAL(_805),
          5  FIL8031            CHAR(36) INITIAL(
            'BIND ERROR'),
          5  FIL8032            CHAR(2) INITIAL('BD'),

        3  ERRDB_922,
          5  FIL922             FIXED BIN(30) INITIAL(_922),
          5  FIL9221            CHAR(36) INITIAL(
              'NOT AUTHORIZED '),
          5  FIL9222            CHAR(2) INITIAL('TH'),

        3  ERRDB_IT_GENERIC,
          5  FILGEN             FIXED BIN(30) INITIAL(_111101),
          5  FILGEN1            CHAR(36) INITIAL(
           'BPHX _ GENERIC'),
          5  FILGEN2            CHAR(2) INITIAL('AJ'),

        3  ERRDB_IT_NOTFOUND,
          5  FILNFO0            FIXED BIN(30) INITIAL(_111102),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ NOT FOUND'),
          5  FILNFO2            CHAR(2) INITIAL('GE');

        3  ERRDB_IT_NOTFOUND,
          5  FILNFO0            FIXED BIN(30) INITIAL(_111103),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ REC ALREADY EXIST'),
          5  FILNFO2            CHAR(2) INITIAL('II');

        3  ERRDB_IT_NOTFOUND,
          5  FILNFO0            FIXED BIN(30) INITIAL(_111104),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ FATHER CHANGED'),
          5  FILNFO2            CHAR(2) INITIAL('GA');

        3  ERRDB_IT_NOTFOUND,
          5  FILNFO0            FIXED BIN(30) INITIAL(_111105),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ BROTHER CHANGED'),
          5  FILNFO2            CHAR(2) INITIAL('GK');

        3  ERRDB_IT_NOTFOUND,
          5  FILNFO0            FIXED BIN(30) INITIAL(_111106),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ END OF FILE'),
          5  FILNFO2            CHAR(2) INITIAL('GB');

        3  ERRDB_IT_NOTFOUNDERR,
          5  FILNFO0            FIXED BIN(30) INITIAL(_999999),
          5  FILNFO1            CHAR(36) INITIAL(
           'BPHX _ NOT FOUND ERROR'),
          5  FILNFO2            CHAR(2) INITIAL('XI');

 DCL 1 TABERRDB_SQLRESP         BASED(ADDR(ERRDB_SQLRESP)),
       2 TABELLADEF(14),
          5  ERRDB_CODRESP      FIXED BIN(30),
          5  ERRDB_MSGRESP      CHAR(36),
          5  ERRDB_CODDLI       CHAR(2);

 DCL 1  MAX_SQLRESP              PIC'(8)9' INITIAL(14);
